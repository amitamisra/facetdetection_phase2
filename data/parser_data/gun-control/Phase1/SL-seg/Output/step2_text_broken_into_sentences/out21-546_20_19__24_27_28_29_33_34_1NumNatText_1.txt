<PARAGRAPH> Author2 tells Author1 that no one is safe on a plane even if they have a gun, and that a good citizen will not play fair with terrorists.
Author1 asserts that passengers cannot stop a terrorist from hijacking a plane, and it would not make him feel secure knowing the passengers had guns.
The plane provides enough security, but Author2 tells Author1 that this is trading your Second Amendment rights.
Author1 refutes this by stating that these are carefully planned attacks, and that bringing guns on planes poses far more threats than securities.
Author2 states that self-defense should not be the government's decision, because the terrorist win when you trade your freedom.
Author1 believes that people should not be put in danger to save oneself.
