<PARAGRAPH> Author1 jokes about a guy who trained under the bullet proof monk, but Author2 states that he is not bullet proof or a monk because he is a white ex-cop.
Author1 asserts that rubber knives are different from real ones which is why he believes the trainer is a charlatan.
Author2 asks Author1 if he thinks that the training methods of both the military and the police are bogus.
Author1 answers him by stating that people trained by the military and police do not claim to be trained in real life situations.
Author2 states that he was accused of being untrained which is why he stated that he was trained.
The accusation that he claimed to have been trained in real life situations is false.
