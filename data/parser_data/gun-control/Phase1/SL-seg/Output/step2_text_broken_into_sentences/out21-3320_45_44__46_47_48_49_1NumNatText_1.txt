<PARAGRAPH> Author1 states in order to claim self defense, you have to leave if you can do so safely.
However, if your safety is being immediately threatened, you may not be able to leave the scene.
He says if there is no immediate threat, you can not claim self defense.
The law realizes there are times during an immediate threat where you can not safely retreat and then it is self defense, which is why the statue requiring you to leave was abolished.
He suggests seeking legal advice to clarify this.
Author2 says he is wrong because there is no requirement to flee, only prove you were in danger.
Author1 says he only said you can not prove it was self defense if you had a safe way to flee and did not do so, not that you have to attempt to flee.
He says you should confirm this with a lawyer.
Author2 sarcastically says people should try calling a lawyer while facing someone with a weapon instead of using a gun in self defense.
