<PARAGRAPH> Author1 believes that security procedures like the ones that work at airports could protect whole cities from guns.
These kinds of procedures should be legal for cities too, just as they are for airports.
It is essential to take away the guns.
The way it is now, criminals, kids, drug addicts, the insane and terrorists go armed.
That is wrong.
By using the methods that work at the US/Mexican border, it would be possible to keep guns out of D.C.
Certainly Gangsters would evade the checkpoints and there would be other problems of course.
But there would be many fewer guns, as seems to be the case in Britain.
Author2 believes that the logistics would make these checkpoints impossible.
Also people might rebel, because they only put up with airport security procedures out of fear of terrorism.
Lawyers would have a field day.
This massive undertaking would not prevent all gun crime.
For example, there would be a gun black market.
Alcohol Prohibition kept alcohol out.
Yet it made people's lives worse, not better.
