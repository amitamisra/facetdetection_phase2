<PARAGRAPH> Author1 makes the assertion that in Miami an AK-47 can be bought on the street illegally for less than the price of a PlayStation game console.
Author2 demands that Author1 provide evidence of this by stating the price of each, and accuses Author1 of trying to derail the discussion of gun regulations with a sensational statement.
Author1 counters that Author2 will accuse anyone who disagrees with their position on the issue of gun regulation of lying.
Author2 quotes prices of different models of PlayStation and a higher price for the least expensive AK-47 manufactured.
Author1 counters by accusing Author2 of making the prices up.
Author2 says the gun would not be sold for less than it would cost to buy the guns from the manufacturer.
