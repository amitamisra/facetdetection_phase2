<PARAGRAPH> Author1 sees no sense in the claim that every state should protect the same liberties, and Author1 tells Author2 that he is misquoting an article of the Constitution.
Author2 tells Author1 that he is actually misquoting him, and he asks Author1 to explain the Civil War.
Author1 apologizes for the misquotation, then he asks Author2 what kind of explanation he should offer concerning the Civil War.
Author2 wants Author1 to apply his stance in terms of the Civil War, because Author1 is claiming State's rights just like the South.
Author1 states that the Civil War covers State rights, and he asks Author2 if he is referring to slavery being allowed before the Fourteenth Amendment existed.
Author2 reminds him that the South had slaves and autonomy, they wanted to keep their free will.
Author1 believes the Fourteenth Amendment caused the states to have less autonomy, but Author2 disagrees.
Author2 states that the North also had slaves, and Lincoln never justified Northern aggression.
Author1 counters by stating that Lincoln justified Northern aggression when he preserved the Union.
