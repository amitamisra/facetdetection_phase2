<PARAGRAPH> Author1 argues that the only reason that second amendment rights can be removed is by a felony conviction.
Other misdemeanors are not enough to remove one's right to own weapons.
The states individually can deny permits when there are felony charges or warrants for arrest.
When there are open charges against people it is difficult to determine if someone should be allowed to acquire a new weapon.
Transactions and ownership are protected by the Constitution, but the Constitution does not guarantee the right to drive, that is reserved for the states.
Gun ownership could be considered in a similar way.
Driving is licensed and regulated, but there is no right to drive.
Author2 argues that having a gun means that you must have the right to have a gun.
The courts have decided, unconstitutionally, that there is no individual federal right to keep a handgun.
The second amendment does not protect individual rights.
Vehicles are registered, while rights are supposed to be determined to the Constitution.
The second amendment is not what most people think.
