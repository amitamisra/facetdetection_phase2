<PARAGRAPH> Author1 is discussing a police officer who had a shootout against other police officers.
He states how citizens were in danger from stray bullets.
He ask how Australians think they would protect themselves if an officer went crazy since they can not own guns.
He says there are many examples of authority figures going crazy like that.
He chides Author2 for his country being so unprepared.
He says giving arms to only the people you think can be trusted like police is foolish because if there is suddenly a time they are not trustworthy, you have no way to defend yourself.
He says there is no laws that will stop a murderer who wants to murder.
Author2 points out citizens go crazy as well.
He says they do not have that problem over there with police.
He makes jokes about what they would do.
He says the chances of an incident happening are very low, they've only had two incidents, and one was a citizen.
He says they believe in God and the power of prayer.
