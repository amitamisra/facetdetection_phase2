<PARAGRAPH> Author1 says his wife would object to him having a husband.
While he does not know what his response would be, he thinks he should follow Christ's example when the disciples responded to a violent mob that came to take him and not respond with violence.
Author2 says Author1 should not have a family if he is not going to protect them and using Christ is a cop out.
He says God used more violence and destroyed Sodom and Gomorrah.
Also, Jesus was sent as a sacrifice.
Author1 says Jesus commanded us to follow him.
The acts of vengeance in the old testament are God's alone.
Author2 thinks unless the Bible says not to defend yourself the position is nothing but a cop out.
The correct answer is to keep your family safe.
Author1 quotes "put away your sword, Peter".
He says money is not worth taking someone's life.
Author2 thinks Author1 is confusing Catholicism and Christianity.
God ordered people to wipe out non-believers.
Author1 says Catholics are the majority of Christians.
Christianity expresses belief in the saints.
