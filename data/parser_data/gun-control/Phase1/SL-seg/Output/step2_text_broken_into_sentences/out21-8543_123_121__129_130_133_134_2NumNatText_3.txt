<PARAGRAPH> Author1 states that lethal lead patterns are minimized by sufficient regulation, but Author2 tells Author1 that modern guns are of superior quality.
He believes that guns will always exist, and Author2 wants Author1 to define sufficient regulation.
Author1 refutes by stating that the quality of manufacture does not lessen the lethality of a gun.
Author2 stresses his weariness of the argument that people would be safer if guns did not exist, and he gives various examples of dangerous things that exist.
Author2 wonders how Author1 came to the conclusion that 32 people would have been saved if their killer did not have gun access.
Author1 mentioned the UK where nobody can own a handgun, so Author2 wonders if Author1 wants guns to not exist at all.
The killer got access because he was not in the system, meaning that he could pass any background check like most Americans.
He believes that unless Author1 thinks the system should be reformed, then he must want to deny people their right to own guns in the first place.
