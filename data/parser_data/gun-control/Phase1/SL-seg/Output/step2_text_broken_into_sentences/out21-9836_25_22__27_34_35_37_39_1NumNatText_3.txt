<PARAGRAPH> Author1 and Author2 are discussing death by perpetrator fatalities of police officers.
Author1 feels most of these incidents stem from drug use, while Author2 believes many of these acts are planned attacks.
Author2 cites an investigation into a Midwest militia group which was alleged to have planned to kill officers.
Author1 feels blaming militia groups is the attempt at a cover up and to draw attention away from Islamic connections.
Author2 advises he did not blame a militia group, but did cite the investigation currently taking place.
Author1 compares this to the Branch Davidian case in which a prosecutor would not allow armed federal investigators in the proceedings.
He advises a person is to be presumed innocent until proven guilty beyond doubt and it is up to a jury to make the decision as to whether or not there is a shadow of a doubt related to guilt.
He advises that shadow must be found by a jury of the person's peers not by a judge or prosecutor.
