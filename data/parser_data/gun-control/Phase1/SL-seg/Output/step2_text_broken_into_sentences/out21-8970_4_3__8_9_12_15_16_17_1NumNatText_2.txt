<PARAGRAPH> Author1 makes the claim that in Miami, an AK-47 can be bought on the street for less than the cost of a Sony Playstation.
He objects to being called a liar by Author2, and defines a lie as a deliberately false statement.
He says his refusal to comply with Author2's demands for proof does not make him a liar, and he challenges Author2 to prove that he's wrong.
Author2 calls on Author1 to back up his assertion with evidence, and asks what the exact street price for an AK-47 in Miami is.
He accuses Author1 of being a lying troll, and gives prices for different Playstation models, and for the cheapest AK-47.
He says he does not have to prove Author1 is wrong.
