<PARAGRAPH> Author1 jokes kids should be armed at birth.
He jokes about Author2's kids becoming school shooters.
He says they are not afraid there since there is a lot of space between their gun free worry and Author2's home where people are afraid.
Author2 says he started teaching his kids to shoot at 4-5.
He says it teaches them respect for firearms.
He says that Author2 lives in Australia, where he has to be afraid if he even says the word gun he will go to jail.
He says people here understand firearms.
He says Australians are not free from gun crime just because they are illegal.
He cites several gun related violence articles.
They include a hostage situation, 3 shootings a week in Sydney with neighborhoods in fear, and 34 shootings in two years in Fairfield.
He says the number of shootings in the past year was up to 157 from 129 the year before.
He list several other shooting statistics, and mocks Author1 for saying he is not afraid of gun violence.
