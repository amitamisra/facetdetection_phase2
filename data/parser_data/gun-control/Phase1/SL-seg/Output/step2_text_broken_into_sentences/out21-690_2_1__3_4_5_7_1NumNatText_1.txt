<PARAGRAPH> Author1 asks Author1 to view threads that start with "I carried a gun" and says that how their actions did not endanger anyone, increase crime, or violate rights.
They ask Author2 if it's okay to carry guns.
Author2 asks if it's okay to carry a nuclear weapon.
Author1 challenges Author2 to find a nuclear weapon for sale.
Author1 asks how they could get a gun if they were banned.
Author2 says that since the US has nukes, they should be allowed one too.
They say that the goal should be to repeal a ban, not break it.
Author1 implies nukes are not comparable since there are fewer vendors.
Author2 says that the free market may allow nuclear weapons to be bought if there was a demand.
