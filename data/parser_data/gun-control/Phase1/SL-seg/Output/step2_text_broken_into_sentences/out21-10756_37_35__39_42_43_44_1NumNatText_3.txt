<PARAGRAPH> Author1 states that Author2 made the argument that California should set the standard for the United States, but one state cannot regulate the activities of another state.
A state should not ignore home rule issues that may have created a problem that they cannot respond to.
Author2 believes that other states could adopt controls like California, and he further believes that the reform will not take place because other states do not like the California laws.
Four Oakland police officers were gunned down by a hood with an AK-47, which serves as a valid reason for some restrictions in areas like California has.
Author1 believes that this is not a good example, but Author2 believes the cop murder proves that it is a good measure.
