<PARAGRAPH> Author1 claims that in Miami a PlayStation costs more than an AK-47.
Author2 asks Author1 what the street price of an AK-47 is, and that if they do not know they're not contributing.
Author1 responds less than a PlayStation.
Author2 insists Author1 backs up their claim and calls them a liar.
They ask again for the price of an AK-47.
Author1 says they're not lying with their assertion and that their claim also is not false.
Author2 lists the prices of various models of PlayStations and says an AK-47 is more expensive.
Author1 says they can not back up that claim.
Author2 places the burden of proof on Author1 and says their initial statement had no proof and their claims are thus invalid.
