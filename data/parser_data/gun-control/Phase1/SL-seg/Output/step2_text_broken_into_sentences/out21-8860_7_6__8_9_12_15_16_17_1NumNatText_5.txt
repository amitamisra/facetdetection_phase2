<PARAGRAPH> Author1 ask what part of the Brady law covers unannounced searches.
He points out the section of the code saying "at any time" only pertains to when the gun is involved in a criminal investigation.
He says gun dealers records are rarely searched in the middle of the night.
He says after hours inspections would only be during an investigation.
Author2 cites US code Section 923 of title 18 covering Arsenal License.
It says the licensee is subject to all obligations and requirements pertaining to dealers.
It was changed from once every 12 months to 3 times every 12 months, or any time related to a firearm involved in a criminal investigation.
He notes the "or at any time" part of the code, and mocks they make convenient appointments.
He implies the "or any time during an investigation" means they could just say they were doing an investigation if they wanted to search more than 3 times a year.
He says Author1 should know police are not restricted to investigating only during business hours.
