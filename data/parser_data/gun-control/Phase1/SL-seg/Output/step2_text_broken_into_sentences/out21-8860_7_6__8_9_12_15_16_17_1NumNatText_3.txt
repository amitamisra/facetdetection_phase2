<PARAGRAPH> Author1 asks where the authorization of "unannounced searches" of the holders of arsenal license comes from.
Upon learning that the "at anytime" pertains to the records relating to a firearm involved in a criminal investigation and is traced back to the owner of the arsenal license.
He claims that Author2 is just trying to scare people.
Author2 disagrees and says that he is not trying to scare anyone and that Author1 must think that the Bureau of Alcohol, Tobacco, and Firearms send out written invitations when the want to come by and inspect.
Author1 says he did not think that - but on the other hand - the BATF does not just show up in the middle of the night, right?
Author2 tries to set Author1 straight and explains that when they are investigating a firearm the BATF is not limited to the stores hours, but that they can show up anytime they want up to 3 times in a year.
He goes on to say that not all the agents are "nice guys".
