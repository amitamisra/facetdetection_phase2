<PARAGRAPH> Author1 states that he should not be compared to someone that makes judgments about gun laws in the USA by making comparisons to Britain, then Author2 tells Author1 that he is ashamed that Author1 is a Mensa member.
Author1 asserts that he does not have an opinion either way in regards to the gun laws of the USA, then Author1 tells Author2 that he never expressed an opinion.
Author1 insists that his Mensa certificate is proof that a meaningless certificate can be obtained, and that is why he mentioned it to Author2.
Author2 argues with Author1 over a typo that Author1 made, and Author1 tells Author2 that he should be able to keep his pop gun.
Author2 continues to mention the typo, then he mentions a vote on keeping their rights and his popgun.
Author2 insults Author1 even further, which causes Author1 to respond by saying that he is proud to have his shortcomings pointed out by Author2.
Author2 states that he took advantage of a simple error to lighten the seriousness of the debate.
