<PARAGRAPH> Author1 believes that we should follow Christs example and not react violently to anyone.
Even when people are assaulting us or taking our belongings violence is not the answer.
Christ should be used as an example in all things.
Vengance is not something that we should try to do, but rather we should be willing to lose our life for other people peacefully.
When being threatened by an attacker it is better to give them what they want than to react with violence.
Even when people are evil we should not be evil back to them.
Christians are made up of Catholics, Anglicans, and Eastern Orthodox, who all represent 2 billion people.
Author2 argues that one should be prepared to defend one's family.
Pacifism is not justified.
God himself used force and killed many people.
Force and violence should be used to defends one's self and family.
The Bible does not anywhere instruct people to not defend themselves, and being this pacifist is not safe for your loved ones.
