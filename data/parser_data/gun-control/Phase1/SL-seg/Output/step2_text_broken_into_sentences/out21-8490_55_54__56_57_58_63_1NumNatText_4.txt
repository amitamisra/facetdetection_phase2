<PARAGRAPH> Author1 tells Author2 that it is not easy to apologize for mistaking the intent of a person once they are dead.
Author2 reads from a headline that states that a woman was in critical condition after a random supermarket stabbing.
Author1 believes that the headline would have read differently if the attacker had a gun, and that the headline would state that seven people died in a supermarket shooting spree.
Author2 reads to Author2 a headline that states that seven people were killed in a Tokyo stabbing spree.
Author1 brings up Columbine, but Author2 asks Author1 what the relevancy was.
Two crazed individuals got guns through illegal channels, and they massacred people in a gun-free zone.
Columbine is different than carrying a gun for self-defense.
