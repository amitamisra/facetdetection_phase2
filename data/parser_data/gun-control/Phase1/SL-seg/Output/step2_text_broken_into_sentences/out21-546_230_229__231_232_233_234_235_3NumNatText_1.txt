<PARAGRAPH> Author1 asks if there was a case of something that ever happened.
They say that people like to use that quote when talking about unarmed passengers.
They think people are vilifying people defending themselves.
Author2 says there is no difference between dying in a crash and dying during defense.
They say that the danger is from mistakes of intent.
Author1 says there are no incidents of Author2's claims, that they are just chanting anti gun ideas.
They say Author2 wants to ban right to own guns.
Author2 says they do not want to ban, just restrict.
Author1 accuses Author2 of using undocumented statements to limit or ban the right to bear arms.
Author2 says they think and that regular people can not defend planes anyway.
