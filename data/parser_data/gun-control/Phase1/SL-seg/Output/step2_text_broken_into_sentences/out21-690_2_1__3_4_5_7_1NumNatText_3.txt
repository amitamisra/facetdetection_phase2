<PARAGRAPH> Author1 received no posts on threads about gun carrying endangering people, increasing crime or violating rights.
So he asks rhetorically if it's okay to carry guns.
Author2 answers it's as okay as a country storing a nuclear weapon in a foreign city.
Carrying guns is inherently dangerous.
Author1 replies that nukes are effectively banned, invalidating the comparison.
His unanswered posts prove guns needn't be banned.
Author2 says no citizen should have nukes.
And guns are similarly dangerous.
Author1 says nobody sells individuals nukes.
Author2 says he talks about guns with respect to his opponent's argument; about nuclear weapons with respect to his own.
Makers would sell nukes, if allowed, for profit and power.
So, he asks rhetorically, should not people be allowed to buy them?
