<s> <PARAGRAPH> </s>
<s>  Author1 mocks a training course Author2 took, in which students used fake weapons to simulate real fights. </s>
<s> He disparages Author2 for thinking such training is the same as having real life experience in dangerous situations. </s>
<s> Author1 thinks Author2 is in awe of the course's instructor, who Author1 views as a charlatan. </s>
<s> He says he would hope that people training for the military or police force understand the difference between simulated and real life danger. </s>
<s> Author2 is frustrated by Author1's continued mischaracterization of what he ( Author2) said about the course and its instructor. </s>
<s> He says he never claimed to have trained in real life situations, and explains the context in which the topic had originally come up, which was Doc calling him ( Author2) untrained. </s>
<s>  </s>
