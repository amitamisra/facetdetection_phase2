<s> <PARAGRAPH> </s>
<s>  Author2 insults Author1 and then asks him what his post has to do with the topic of gun control, then he proceeds to tell him the definition of infringement. </s>
<s> Author2 defines infringement as an encroachment or trespass on a right or privilege, then Author2 questions his stance on the right to bear arms because Author1 wants to limit those rights. </s>
<s> Author1 states that he was not addressing Author2, which leads Author2 to repeat his stance. </s>
<s> Author1 clarifies his statement by saying that it was directed to another person. </s>
<s> Author2 insists that Author1 is a gun hater, but Author1 wants to debate without labeling someone as a member of a hate group. </s>
<s> Author2 accuses Author1 of vilifying his opponents, then Author2 defines infringement one more time. </s>
<s>  </s>
