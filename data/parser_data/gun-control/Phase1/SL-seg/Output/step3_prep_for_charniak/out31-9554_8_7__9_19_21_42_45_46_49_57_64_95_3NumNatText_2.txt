<s> <PARAGRAPH> </s>
<s>  Author1 is in favor of gun control advocates closing down gun shows to avoid guns sold to undesirables. </s>
<s> He states often times, people who would otherwise be unable to purchase a gun due to criminal background or age would send a girlfriend or someone else to purchase the gun for them. </s>
<s> He notes in most states people can sell their firearms to anyone, no questions asked. </s>
<s> He contends the reason he supports the closing of gun shows is due to the fact people purchase firearms at a gun show with no background check. </s>
<s> He does concede the loop holes are not only in gun shows but other places as well. </s>
<s> Author2 supports gun shows and disagrees with the statement that gang bangers purchase their weapons there. </s>
<s> He also states he has been to many shows. </s>
<s> He says most gang bangers do not buy their guns at gun shows nor do they send their girlfriends to do it for them. </s>
<s> He feels Author1 is not sticking to the statements made. </s>
<s>  </s>
