<s> <PARAGRAPH> </s>
<s>  Author1 argues that, depending on the survey, up to one third of households in the US own guns. </s>
<s> All gun sales are through middlemen, as even "straight from the dealer" guns are not bought directly from the factory. </s>
<s> Gun ownership has been increasing since 35 years ago, not decreasing. </s>
<s> More people own guns now that Obama is president. </s>
<s> People buy guns from licensed dealers, and when they do not they are arrested and put in prison. </s>
<s> The police are not incompetent, so we know that there must not be a large black weapons market. </s>
<s> The system is working how it should, as people get punished for breaking laws. </s>
<s> Author2 argues that only one quarter of households in the US owns guns, and that there is a black market composed of mostly domestically made handguns. </s>
<s> Gun ownership has been declining over the past 35 years. </s>
<s> Only a few people are arrested each year for black market sales but there is a massive number of weapons moved. </s>
<s> The people who get caught are not the only ones. </s>
<s>  </s>
