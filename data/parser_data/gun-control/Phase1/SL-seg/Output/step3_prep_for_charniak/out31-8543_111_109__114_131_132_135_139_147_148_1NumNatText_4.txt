<s> <PARAGRAPH> </s>
<s>  Author1 says it's fine for Author2 but what he feels is not right for the rest of society. </s>
<s> Author2 says only 75% of households in the US do not keep firearms. </s>
<s> On the black market legally registered weapons are stolen, or sold by licensed dealers more interested in profit than where guns end up. </s>
<s> Author1 asks where Author2 got his figures. </s>
<s> A conservative estimate of possession is 85 million, a liberal estimate suggests over 100 million. </s>
<s> Both are higher than only 1 in 4 households having a gun. </s>
<s> He is unaware that it is legal to get your guns directly from the factory that made them without going through a middle man. </s>
<s> Author2 says they go through middle men, but come from the same assembly line and cites a survey. </s>
<s> Author1 says the survey is wrong and if there are no records there is no evidence of dealers doing anything wrong. </s>
<s> Author2 says there is the stolen market. </s>
<s> Author1 says the system is working, people break laws, and they have to break the law to be caught. </s>
<s>  </s>
