<s> <PARAGRAPH> </s>
<s>  Author1 thinks rival gangs are forced to share guns, analysis suggests guns are so scarce in the UK that they are rented out to both sides. </s>
<s> Author2 thinks this is an admission of failure. </s>
<s> He asks if it's against the law to possess, borrow or loan a firearm in the UK. </s>
<s> Author1 says that's his point. </s>
<s> Author2 says it's his. </s>
<s> Author1's was just admitting failure. </s>
<s> Author1 thinks if a gang member wants to shoot someone, having to rent a gun is not a failure. </s>
<s> If so, you should be equally lampooning the recent DC statistics thread because newly allowed guns have not dropped the crime rate to 0. </s>
<s> Author2 says it must be a handicap to go next door to get one. </s>
<s> Author1 thinks no law is 100% successful. </s>
<s> Author2 says if you turn on your allies you can not trust them. </s>
<s> Author1 claims an increase in crime due purely to criminal refugees should not be treated as due to 2A. </s>
<s> Author2 thinks that Author1 will blame citizens and the availability of guns for criminal actions. </s>
<s>  </s>
