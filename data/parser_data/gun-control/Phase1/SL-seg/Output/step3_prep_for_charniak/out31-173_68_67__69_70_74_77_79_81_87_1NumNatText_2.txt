<s> <PARAGRAPH> </s>
<s>  Author1 argues only a felony conviction can take away someone's second amendment right, and this was a misdemeanor charge. </s>
<s> Author2 states he said it was a misdemeanor, and was pointing out the court did not recognize an individual right. </s>
<s> Author1 argues Love did get to keep her gun. </s>
<s> The state is allowed to deny permits if someone has an open arrest record. </s>
<s> She had 3 charges, and one was a misdemeanor, but the other two were still open cases. </s>
<s> The sheriff stated the other two charges were okay, but the permit was still denied. </s>
<s> The court ruled it was justified to deny the permit because of the issues, but they were cleared up and she still had her gun. </s>
<s> Author2 argues there is no individual constitutional right to own a gun. </s>
<s> She did win the first case, but the appeals court dismissed the case when she filed in district court it had been a violation of the second amendment. </s>
<s> She appealed to the federal court, who also disagreed her constitutional right had been violated. </s>
<s>  </s>
