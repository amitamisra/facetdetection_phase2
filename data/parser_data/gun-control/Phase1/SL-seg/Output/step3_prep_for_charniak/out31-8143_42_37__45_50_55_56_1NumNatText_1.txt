<s> <PARAGRAPH> </s>
<s>  Author1 takes issue with Author2's interpretation of results from a poll that Author2 created about gun control. </s>
<s> Author1 believes Author2 is judging others by Author2's own standards, preconceptions and life experiences. </s>
<s> Author1 is trying to convince Author2 that respondents provided truthful answers and that the results prove that Author1's pole hypothesis was simply wrong. </s>
<s> Author2 believes his life experience and personal standards support his conclusion that gun owners will not be huge advocates for gun control, regardless of what the poll results showed. </s>
<s> He thinks respondents could have lied and he refuses to derive any conclusions from the results of his own poll. </s>
<s> He thinks his poll's accuracy is questionable because the poll results do not match what logic tells him. </s>
<s>  </s>
