<s> <PARAGRAPH> </s>
<s>  Author1 says injuries are reduced by regulation. </s>
<s> He says no one said anything about if guns did not exist except Author2. </s>
<s> He accuses Author2 of misstating what other people say. </s>
<s> Author2 says saying if something did not exist is not realistic. </s>
<s> Guns are well made and last a long time. </s>
<s> He ask what Author1 means by "sufficient regulation". </s>
<s> He says regulations have not stopped school shootings, the only thing that has is when the shooter is killed or injured. </s>
<s> He was simply pointing out how untrue it would be to imagine if guns did not exist. </s>
<s> He says Author1 DID make a reference to guns not existing when he said if it was like in Australia and the UK where the killer could not have bought a gun, the 32 victims would be safe and alive. </s>
<s> He says this proves Author2 thinks there should be a total ban on handguns like in the UK. </s>
<s> He says that would only be possible if all handguns were destroyed. </s>
<s> He says the system failed because the person was not entered in the system. </s>
<s>  </s>
