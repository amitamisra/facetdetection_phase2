<s> <PARAGRAPH> </s>
<s>  Author1 thinks government is restrained by the Constitution and it's Bill of Rights. </s>
<s> He thinks Author2 acts like the 2nd Amendment gives him the right to all of the gun control he wants. </s>
<s> Author2 says he's cited evidence the Bill of Rights is only a restriction on the Federal government. </s>
<s> The Constitution only grants power to the Federal government, all other powers are left to the States or the people. </s>
<s> Author1 replies states have their own Bill of Rights and there are currently 27 amendments limiting government's power. </s>
<s> Author1 says states have their own Bill of Rights and 44 of them currently have the Right to Keep and Bear Arms. </s>
<s> Also, there are currently 27 Amendments limiting government's power. </s>
<s> Author2 says before the 14th Amendment it's untrue that rights listed in the Bill of Rights applied to the state. </s>
<s> Author1 thinks both the State and the Federal protected rights were the same. </s>
<s> Author2 questions all states having the same constitutions that match the Federal Constitution. </s>
<s> Author1 says state and federal protected rights are the same. </s>
<s>  </s>
