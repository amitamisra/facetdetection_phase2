<s> <PARAGRAPH> </s>
<s>  Author1 says you can not apologize a mistake of someone's intent if they're dead. </s>
<s> Author2 names a headline about a supermarket stabbing. </s>
<s> Author1 says if the attacker had a gun the headline would be that more people died. </s>
<s> Author2 lists a headline where seven people died in a stabbing spree. </s>
<s> Author1 makes fun of Author2 for using anecdotes and mentions Columbine. </s>
<s> Author2 asks how Columbine is relevant and denounces it as an edge case. </s>
<s> They say it's different from somebody wanting a gun for self defense. </s>
<s>  </s>
