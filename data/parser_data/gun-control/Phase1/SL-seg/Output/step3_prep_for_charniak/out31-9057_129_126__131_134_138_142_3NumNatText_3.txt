<s> <PARAGRAPH> </s>
<s>  Author1 tells Doc to be careful because this guy trained under the bullet proof monk for two. </s>
<s> Author2 says he is not bullet proof or a monk, he's a white ex-cop. </s>
<s> Author1 says if he says that fighting with rubber knives is like the real thing then he's a charlatan. </s>
<s> He asks what he says to do if you get stabbed. </s>
<s> Author2 asks if Author1 is saying military and police training does not work. </s>
<s> Author1 says he hopes they would not say they train in real situations when they do not. </s>
<s> Author2 asks if he should say he has never been trained. </s>
<s> He thinks Author1 is bashing someone he has never met and falsely accusing him of saying he trained in real life situations when he did not say that. </s>
<s>  </s>
