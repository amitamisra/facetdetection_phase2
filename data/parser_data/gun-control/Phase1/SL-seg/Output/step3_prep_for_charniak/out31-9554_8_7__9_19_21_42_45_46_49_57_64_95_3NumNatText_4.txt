<s> <PARAGRAPH> </s>
<s>  Author1 argues that pro-gun enthusiasts do not have a plan to combat illegal sales of guns to other people. </s>
<s> People who own guns can legally sell their own guns to anyone without needed to go through background checks or other measures. </s>
<s> This law can allow people to purchase guns from reputable stores, and then turn around and sell these weapons to people who should not posses weapons ( those who are too young, have a record, or are in part of a gang). </s>
<s> The lack of background checks for buyers after the first one opens up a large hole in the control of gun sales. </s>
<s> If it is legal to check the first purchasers ability to own guns, then it should be legal to check the buyers of that gun for subsequent purchases. </s>
<s> Author2 argues that illegal sales decrease when more people have guns to start with. </s>
<s> Most guns are not sold at guns shows. </s>
<s> People do not usually buy guns via proxy. </s>
<s> People should not need background checks to sell a gun individually. </s>
<s>  </s>
