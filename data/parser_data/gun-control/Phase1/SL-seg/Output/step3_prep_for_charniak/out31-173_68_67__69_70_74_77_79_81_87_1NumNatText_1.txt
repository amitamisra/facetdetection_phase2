<s> <PARAGRAPH> </s>
<s>  Author1 argues that the only reason that second amendment rights can be removed is by a felony conviction. </s>
<s> Other misdemeanors are not enough to remove one's right to own weapons. </s>
<s> The states individually can deny permits when there are felony charges or warrants for arrest. </s>
<s> When there are open charges against people it is difficult to determine if someone should be allowed to acquire a new weapon. </s>
<s> Transactions and ownership are protected by the Constitution, but the Constitution does not guarantee the right to drive, that is reserved for the states. </s>
<s> Gun ownership could be considered in a similar way. </s>
<s> Driving is licensed and regulated, but there is no right to drive. </s>
<s> Author2 argues that having a gun means that you must have the right to have a gun. </s>
<s> The courts have decided, unconstitutionally, that there is no individual federal right to keep a handgun. </s>
<s> The second amendment does not protect individual rights. </s>
<s> Vehicles are registered, while rights are supposed to be determined to the Constitution. </s>
<s> The second amendment is not what most people think. </s>
<s>  </s>
