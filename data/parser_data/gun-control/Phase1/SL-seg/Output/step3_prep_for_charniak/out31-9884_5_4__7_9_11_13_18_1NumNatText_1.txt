<s> <PARAGRAPH> </s>
<s>  Author1 states most firearms are not recovered. </s>
<s> About 63% of guns recovered in Toronto originated in the US and a large number have their serial numbers removed. </s>
<s> According to Royal Mounted Police 2,637 guns were reported and 925 were linked to a dealer, and 3/4 of those originated in the US. </s>
<s> Author2 asks how they were linked to the US if the serial numbers had been removed. </s>
<s> Author1 says those were not part of the traceable group unless related to ballistics. </s>
<s> Author2 thinks this is ridiculous. </s>
<s> Failed ballistics tests are not traceable. </s>
<s> Author1 says comprehensive statistics are scarce, but recent cases point to the problem. </s>
<s> His point is even with the serial numbers removed, some can still be traced. </s>
<s> Author2 says he does not see anything that suggests ballistics determined anything and there's no mention of testing the slugs. </s>
<s> Furthermore, BD can only compare rounds linked to a crime, so no random match is possible. </s>
<s> Even that is negligible. </s>
<s> Author1 says he never suggested anything about ballistics databases, Author2 did. </s>
<s> He initially used the term ballistics rather than forensics. </s>
<s>  </s>
