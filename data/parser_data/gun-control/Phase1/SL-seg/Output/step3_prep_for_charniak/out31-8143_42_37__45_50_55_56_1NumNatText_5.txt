<s> <PARAGRAPH> </s>
<s>  Author1 tells Author2 he's being illogical in the way he interprets his polling data. </s>
<s> He says that Author2 is rejecting the results of his own poll because he does not like the answers he's getting. </s>
<s> But if the poll results do not match the hypothesis, it means the hypothesis is wrong, not that everyone who took the poll is lying. </s>
<s> Author2 considers what he does to be valid, saying he's using a single set of standards for everyone. </s>
<s> If his life experience tells him one thing, and a poll tells him the opposite, he's not going to assume the poll is accurate. </s>
<s> Author2 believes the fact that the poll results do not match his own experience casts doubt on the poll's accuracy. </s>
<s>  </s>
