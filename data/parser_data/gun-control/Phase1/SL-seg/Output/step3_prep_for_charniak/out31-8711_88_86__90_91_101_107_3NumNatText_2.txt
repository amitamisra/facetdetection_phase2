<s> <PARAGRAPH> </s>
<s>  Author1 supports photographing and fingerprinting for handguns at the point of sale. </s>
<s> He asks Author2 to stop insinuating that he supports gun bans. </s>
<s> He supports Author2s right to own guns for protection and sports. </s>
<s> He explains, those rights are not absolute and unlimited. </s>
<s> Author2 offers a hypothetical situation where Obama and his successor appointed enough Supreme Court Justices who thought like Ginsburg and Bryer and the Supreme court ruled Heller was wrong. </s>
<s> He asks Author1 if they ruled we had no right to bear arms at all, would he still support that right? </s>
<s> Author1 believes he would have to support decisions the Supreme Court makes. </s>
<s> Author2 agrees with S! that he would also have to support the Supreme Court's decision. </s>
<s>  </s>
