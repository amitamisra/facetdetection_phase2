<s> <PARAGRAPH> </s>
<s>  Author2 says 75% of American households do not own guns. </s>
<s> He claims the black market gets them from assembly lines, stolen guns, and unscrupulous dealers. </s>
<s> Author1 questions the numbers of people who own guns. </s>
<s> He says he gets his guns from licensed dealers. </s>
<s> He questions that it is legal to buy directly from a factory. </s>
<s> He ask if Author2 can prove this with names of dealers. </s>
<s> If this is true, then it means they must have been prosecuted. </s>
<s> Author2 cites a report that gun ownership has declined. </s>
<s> He says that although there are only a few prosecutions every year, a large number of guns are moved. </s>
<s> Author1 says that survey is wrong. </s>
<s> He says gun ownership is higher now. </s>
<s> He questions why if criminals get them from dealers, their records are not being used by police for tracking illegal purchases. </s>
<s> Author2 says it is because other people buy them for them, and they get them on the black market. </s>
<s> He cites a report of a dealer and gang members who were charged for trafficking. </s>
<s>  </s>
