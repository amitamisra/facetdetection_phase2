<s> <PARAGRAPH> </s>
<s>  Author1 says he is not making any judgements about gun laws in America. </s>
<s> He states he is annoyed to be mistaken for having the same views as another person who compares the US and Britain. </s>
<s> He says he is neither pro nor con towards the issue. </s>
<s> He did not express an opinion. </s>
<s> He says he thinks Author2 should be allowed to have his gun. </s>
<s> Author1 calls him a gentleman and scholar, and says guns have made the US a great country. </s>
<s> Author2 says Author1 has communication problems. </s>
<s> He mocks him for being a Mensa member. </s>
<s> He says he admitted he's not that smart. </s>
<s> Author2 claims to be humble. </s>
<s> He blames vision problems for his communication problems. </s>
<s> He discusses correct definitions. </s>
<s> He accuses Author1 of jumping to conclusions. </s>
<s> He says he should indeed be allowed to keep his rights and his gun, and thanks Author1 for accepting others views and admitting when he makes mistakes. </s>
<s> He says Author1 is a stimulating person to have a conversation with, and he considers Author1 a friend. </s>
<s>  </s>
