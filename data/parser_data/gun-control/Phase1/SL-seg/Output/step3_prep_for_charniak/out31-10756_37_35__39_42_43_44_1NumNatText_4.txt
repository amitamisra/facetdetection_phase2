<s> <PARAGRAPH> </s>
<s>  Author1 says the argument that the United States should adopt California's standards as national standards has been made several times, but support has been lacking. </s>
<s> A single state can not regulate the activities of others, but that does not mean a state should ignore home rule issues that have created problems. </s>
<s> Author2 says states could adopt controls like California if they wanted, but it would depend on voters. </s>
<s> He says that people miss that California is a large state with large crime rates, and this is one of the reasons for such restrictions. </s>
<s> He gives an example of four officers being gunned down with an AK-47. </s>
<s> Author1 questions an example of a prohibited weapon being used illegally as an example. </s>
<s> Author2 thinks the weapon should not have been available. </s>
<s>  </s>
