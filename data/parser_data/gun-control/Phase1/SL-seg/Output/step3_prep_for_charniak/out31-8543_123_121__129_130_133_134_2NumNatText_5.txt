<s> <PARAGRAPH> </s>
<s>  Author1 argues that deaths can be reduced by regulation of guns. </s>
<s> Guns do not become less lethal when they are not made as well as other guns. </s>
<s> Author1 argues that regulations of guns will decrease violence and that more gun regulation is necessary. </s>
<s> School shootings would happen less often if there were sufficient regulation of guns. </s>
<s> Author2 argues that guns will always exist, because old guns are still around and work just fine. </s>
<s> Modern guns are made of higher quality materials and will still be around many more years from now. </s>
<s> It is impossible to remove all guns from society. </s>
<s> Gun regulation should not be increased. </s>
<s> Gun violence has not decreased due to regulations, but rather guns have helped to end school shootings while they are in progress. </s>
<s> Since guns do exist, we should not be worried about hypothetical situations which will not occur, and should instead provide guns to as many people as possible to protect ourselves. </s>
<s> A total ban on guns is impossible in the US. </s>
<s>  </s>
