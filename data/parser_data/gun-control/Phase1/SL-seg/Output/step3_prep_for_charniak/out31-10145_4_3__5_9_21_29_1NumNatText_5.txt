<s> <PARAGRAPH> </s>
<s>  Author1 argues that firearms should not be unregulated, and supports the recent Supreme Court rulings, but also feels that the laws will not be changed. </s>
<s> The majority of people want gun regulations stricter or at the least kept the same. </s>
<s> Repealing gun regulations will not make individuals safer. </s>
<s> Guns should be regulated and protected, because they are dangerous tools. </s>
<s> Author2 argues that guns should be less regulated. </s>
<s> Author2 points out that, while a majority of people want the gun laws not to change, very few people want stricter gun control than exists right now. </s>
<s> Author2 feels that Author1 is in the minority, and that the right to own guns should be protected. </s>
<s> Additional gun control is taking away the guaranteed rights of the people. </s>
<s>  </s>
