<s> <PARAGRAPH> </s>
<s>  Author1 believes that security procedures like the ones that work at airports could protect whole cities from guns. </s>
<s> These kinds of procedures should be legal for cities too, just as they are for airports. </s>
<s> It is essential to take away the guns. </s>
<s> The way it is now, criminals, kids, drug addicts, the insane and terrorists go armed. </s>
<s> That is wrong. </s>
<s> By using the methods that work at the US/Mexican border, it would be possible to keep guns out of D.C. </s>
<s> Certainly Gangsters would evade the checkpoints and there would be other problems of course. </s>
<s> But there would be many fewer guns, as seems to be the case in Britain. </s>
<s> Author2 believes that the logistics would make these checkpoints impossible. </s>
<s> Also people might rebel, because they only put up with airport security procedures out of fear of terrorism. </s>
<s> Lawyers would have a field day. </s>
<s> This massive undertaking would not prevent all gun crime. </s>
<s> For example, there would be a gun black market. </s>
<s> Alcohol Prohibition kept alcohol out. </s>
<s> Yet it made people's lives worse, not better. </s>
<s>  </s>
