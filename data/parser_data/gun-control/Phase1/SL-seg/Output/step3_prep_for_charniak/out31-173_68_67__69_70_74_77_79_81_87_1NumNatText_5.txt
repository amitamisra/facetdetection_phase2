<s> <PARAGRAPH> </s>
<s>  ( Author1 & Author2 are talking about a recent court case and its effects on firearm rights) Author1 states that only felonies affect 2nd Amendment rights. </s>
<s> In the court case, the defendant had three charges that amounted to two open charges which led to her being denied a firearm permit. </s>
<s> The charges were later cleared but she was still denied. </s>
<s> An appeal was filed dictating once charges are cleared, firearm permits cannot be denied. </s>
<s> You have the right to purchase a car, not drive it, because driving is not a protected right like the RKBA. </s>
<s> The RKBA cannot be regulated because it's a Constitutional right. </s>
<s> A jury would agree. </s>
<s> Author2 states having a gun does not mean that right cannot be taken away due to criminal behavior. </s>
<s> Firearm use, like driving a car, is a privilege not an untouchable right. </s>
<s> Juries have voted against RKBA rights occasionally. </s>
<s> If Constitutional rights were violated, why has not the Supreme Court stepped in to reverse these verdicts? </s>
<s> All rights are given but can be subject to revision or reversal with criminal activity. </s>
<s>  </s>
