<s> <PARAGRAPH> </s>
<s>  Author1 thinks guards being armed would be a good start but asks what if the guard is overpowered. </s>
<s> He thinks kids should be taught to use guns. </s>
<s> Author2 thinks 4 or 5 is a good age to start training. </s>
<s> Author1 jokes that Author2 is a good example, if they go on a killing spree you would not want them to miss anyone. </s>
<s> Author2 says he means teaching them respect for guns, but if they went to a gun free zone and cut loose and missed then he would shoot them himself. </s>
<s> Author1 says the punishment should fit the offence. </s>
<s> If after all those years they miss it's unforgivable, and asks if Author2 approves of kids packing guns in lunchboxes. </s>
<s> Author2 says here we understand guns and are not paranoid about the word. </s>
<s> Author1 should sleep well knowing we have it under control. </s>
<s> Author1 says they sleep well knowing they do not have anything to get under control. </s>
<s> Author2 cites an article about Australians getting guns from overseas and the crime statistics associated with gun use in Australia. </s>
<s>  </s>
