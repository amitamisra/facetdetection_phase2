<s> <PARAGRAPH> </s>
<s>  Author1 argues that the most common reason that police officers die is due to drug use, not anyone who is part of a militia. </s>
<s> Terrorism is associated with Islamic backgrounds, even though they are trying to distance themselves. </s>
<s> Drugs are almost always mentioned in the police report when officers are killed, across a wide range of cases. </s>
<s> Members of militia's are not guilty just because they are part of a militia group. </s>
<s> Federal prosecutors and federal agents often lie in court and attempt to get others convicted on false charges. </s>
<s> The justice system in the US is based on the concept of innocent until proven guilty. </s>
<s> Juries must be used to find the truth in this country. </s>
<s> Author2 argues that there are members of militia groups who have planned to kill police officers that were not on drugs. </s>
<s> Conspiracy theories have no place in a discussion of law. </s>
<s> Federal prosecutors can provide interesting information, but it is up to an informed jury to make a final decision in court cases. </s>
<s>  </s>
