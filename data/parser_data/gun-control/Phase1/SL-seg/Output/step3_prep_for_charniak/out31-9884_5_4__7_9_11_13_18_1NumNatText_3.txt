<s> <PARAGRAPH> </s>
<s>  Author1 says most guns used for crimes are not recovered. </s>
<s> He says in Toronto, 63% of the guns came from America. </s>
<s> Over 2600 guns were recovered by police in Canada, and 925 came from a dealer, of which 3/4 came from the USA. </s>
<s> Author2 questions how if the serial numbers were removed, they were sure those guns had come from the USA. </s>
<s> Author2 says they were through ballistics, but Author2 argues it is impossible to trace guns that way. </s>
<s> He questions what good is the long gun registry. </s>
<s> Author1 details a case in which a gun was used in a crime. </s>
<s> He says although the serial numbers were missing, they were able to trace the gun to a shop in Maine. </s>
<s> A person bought eight guns and sold them to a Canadian who smuggled them. </s>
<s> He said this showed guns could still be traced. </s>
<s> Author2 still questions how ballistics would help identify the guns. </s>
<s> He points out the specifics of this, and Author1 admits he should have said forensics instead of ballistics. </s>
<s>  </s>
