<s> <PARAGRAPH> </s>
<s>  Author1 refers to previous posts about carrying a gun, and the lack of replies as to whether it endangered anyone, increased crime, or violated rights. </s>
<s> He asks is it okay to carry a gun then. </s>
<s> Author2 asks if it is okay for him to own a nuclear weapon. </s>
<s> Author1 says the question is irrelevant since nuclear weapons are not for sale. </s>
<s> Author2 states if the government is allowed to own a nuclear weapon, he should be too. </s>
<s> If they are banned, he should not be able to own one. </s>
<s> Author1 argues there is no ban because they are not for sale. </s>
<s> He says there is no seller who would sell one. </s>
<s> Author2 argues sellers would sell them if they were allowed to. </s>
<s>  </s>
