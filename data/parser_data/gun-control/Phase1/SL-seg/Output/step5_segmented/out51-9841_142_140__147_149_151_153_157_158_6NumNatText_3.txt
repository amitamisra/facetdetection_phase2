<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP believes/VBZ the/DT Constitution/NNP and/CC the/DT Bill/NNP of/IN Rights/NNPS is/AUX the/DT basis/NN of/IN all/DT laws/NNS and/CC the/DT limits/NNS placed/VBN on/IN the/DT government/NN 's/POS control/NN ./. </C>
</S>
<S>
<C>He/she/NNP supports/VBZ this/DT theory/NN </C>
<C>by/IN citing/VBG the/DT fact/NN that/IN each/DT state/NN has/AUX a/DT Bill/NN of/IN Rights/NNPS and/CC out/IN of/IN all/DT of/IN the/DT US/NNP ,/, </C>
<C>44/CD of/IN those/DT states/NNS support/VBP the/DT right/NN to/TO keep/VB and/CC bear/VB arms/NNS ./. </C>
</S>
<S>
<C>He/she/NNP also/RB believes/VBZ every/DT state/NN protects/VBZ the/DT same/JJ rights/NNS and/CC fall/NN as/IN the/DT federal/JJ rights/NNS ./. </C>
</S>
<S>
<C>He/she/NNP feels/VBZ people/NNS view/VB the/DT 2nd/JJ Amendment/NNP as/IN a/DT blanket/NN for/IN gun/NN control/NN ./. </C>
</S>
<S>
<C>Author2/NNP believes/VBZ the/DT Bill/NN of/IN Rights/NNPS is/AUX unnecessary/JJ and/CC is/AUX only/RB a/DT restriction/NN to/TO the/DT federal/JJ government/NN </C>
<C>and/CC it/PRP is/AUX dangerous/JJ to/TO include/VB </C>
<C>when/WRB discussing/VBG gun/NN restrictions/NNS ./. </C>
</S>
<S>
<C>He/she/NNP believes/VBZ the/DT Bill/NNP of/IN Rights/NNPS was/AUX not/RB created/VBN for/IN the/DT states/NNS or/CC the/DT people/NNS ,/, but/CC for/IN the/DT federal/JJ government/NN itself/PRP ./. </C>
</S>
<S>
<C>He/she/NNP does/AUX not/RB feel/VB state/NN and/CC federal/JJ protected/JJ rights/NNS all/DT coincide/VB ./. </C>
</S>
<S>
<C>He/she/NNP also/RB does/AUX not/RB believe/VB individual/JJ state/NN constitutions/NNS are/AUX in/IN line/NN with/IN the/DT federal/JJ constitution/NN ./. </C>
</S>
<S>
<C>He/she/NNP feels/VBZ all/DT needed/VBN information/NN starts/VBZ at/IN the/DT Fourteenth/NNP Amendment/NNP ./. </C>
</S>
</P>
</T>
