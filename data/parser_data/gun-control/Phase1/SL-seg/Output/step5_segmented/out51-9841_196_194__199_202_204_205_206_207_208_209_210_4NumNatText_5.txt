<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP does/AUX not/RB think/VB republics/NNS are/AUX required/VBN to/TO form/VB governments/NNS ./. </C>
</S>
<S>
<C>The/DT fact/NN that/IN the/DT government/NN 's/POS power/NN is/AUX limited/VBN by/IN a/DT constitution/NN makes/VBZ it/PRP constitutional/JJ </C>
<C>and/CC the/DT fact/NN that/IN the/DT head/NN of/IN state/NN is/AUX not/RB a/DT monarch/NNP and/CC the/DT people/NNS play/VB a/DT role/NN makes/VBZ it/PRP a/DT republic/NN ./. </C>
</S>
<S>
<C>He/PRP does/AUX not/RB think/VB every/DT State/NN in/IN the/DT union/NN needs/VBZ to/TO have/AUX the/DT same/JJ laws/NNS ./. </C>
</S>
<S>
<C>Doc/NNP may/MD be/AUX referring/VBG to/TO the/DT Full/JJ Faith/NN and/CC Credit/NNP Clause/NN ,/, </C>
<C>but/CC this/DT Clause/NN does/AUX not/RB have/AUX the/DT meaning/NN he/PRP 's/AUX providing/VBG ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ he/PRP can/MD reply/VB to/TO any/DT post/NN he/PRP wants/VBZ and/CC asks/VBZ about/IN the/DT Civil/NNP War/NNP ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ he/PRP apologized/VBD for/IN the/DT misquote/NN and/CC asks/VBZ what/WP about/IN the/DT Civil/NNP War/NNP ./. </C>
</S>
<S>
<C>Author2/NNP wants/VBZ him/PRP to/TO put/VB this/DT in/IN terms/NNS of/IN the/DT Civil/NNP War/NNP ./. </C>
</S>
<S>
<C>Author1/NNP asks/VBZ does/AUX he/PRP mean/VB slavery/NN was/AUX allowed/VBN and/CC says/VBZ it/PRP was/AUX ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ the/DT states/NNS had/AUX free/JJ will/NN ./. </C>
</S>
<S>
<C>Author1/NNP agrees/VBZ ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ the/DT south/NN wanted/VBD to/TO break/VB away/RP ./. </C>
</S>
<S>
<C>Author1/NNP is/AUX not/RB talking/VBG about/IN motivation/NN ,/, </C>
<C>but/CC the/DT Constitution/NNP applied/VBD ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ this/DT is/AUX fair/JJ but/CC not/RB correct/JJ ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ there/EX was/AUX not/RB Constitutional/JJ justification/NN against/IN slavery/NN ./. </C>
</S>
</P>
</T>
