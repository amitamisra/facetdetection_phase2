<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP sites/NNS some/DT information/NN eluding/VBG to/TO the/DT fact/NN the/DT 2nd/JJ Amendment/NN was/AUX not/RB designed/VBN to/TO protect/VB a/DT preexisting/JJ right/NN to/TO bear/VB arms/NNS ./. </C>
</S>
<S>
<C>He/PRP advises/VBZ the/DT basis/NN of/IN the/DT amendment/NN was/AUX in/IN support/NN of/IN people/NNS who/WP had/AUX been/AUX involved/VBN with/IN the/DT militia/NN in/IN the/DT sixteen/NNS and/CC seventeen/CD hundreds/NNS </C>
<C>and/CC that/IN weapons/NNS owned/VBN by/IN people/NNS who/WP were/AUX not/RB members/NNS were/AUX subject/JJ to/TO reasonable/JJ regulations/NNS ./. </C>
</S>
<S>
<C>He/PRP sites/NNS several/JJ books/NNS to/TO support/VB his/PRP$ theory/NN ./. </C>
</S>
<S>
<C>Author2/NNP does/AUX not/RB support/VB the/DT argument/NN that/IN the/DT right/NN to/TO keep/VB and/CC bear/VB arms/NNS was/AUX established/VBN for/IN the/DT militia/NN or/CC militia/NN ready/JJ persons/NNS only/RB and/CC requests/NNS proof/NN of/IN this/DT fact/NN ./. </C>
</S>
<S>
<C>He/PRP supports/VBZ his/PRP$ ideas/NNS </C>
<C>by/IN citing/VBG the/DT fact/NN that/IN people/NNS who/WP were/AUX considered/VBN unable/JJ to/TO serve/VB the/DT militia/NN would/MD have/AUX lost/VBN their/PRP$ weapons/NNS had/AUX this/DT been/AUX the/DT case/NN ./. </C>
</S>
<S>
<C>He/PRP uses/VBZ retired/JJ militia/NN members/NNS as/IN an/DT example/NN advising/VBG </C>
<C>if/IN the/DT RKBA/NNP were/AUX only/RB to/TO protect/VB militia/NN groups/NNS ,/, </C>
<C>then/RB those/DT retired/VBN would/MD no/RB longer/RB be/AUX able/JJ to/TO hold/VB weapons/NNS </C>
<C>as/IN they/PRP would/MD no/RB longer/RB be/AUX able/JJ to/TO serve/VB the/DT state/NN if/IN needed/VBN ./. </C>
</S>
</P>
</T>
