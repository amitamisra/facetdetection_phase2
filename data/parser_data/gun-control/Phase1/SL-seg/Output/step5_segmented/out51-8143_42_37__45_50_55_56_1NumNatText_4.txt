<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP thinks/VBZ Author2/NNP is/AUX judging/VBG by/IN their/PRP$ own/JJ standards/NNS and/CC not/RB being/AUXG logical/JJ ./. </C>
</S>
<S>
<C>They/PRP say/VBP that/IN Author2/NNP thinks/VBZ things/NNS are/AUX lies/NNS </C>
<C>because/IN they/PRP do/AUX not/RB fit/VB Author2/NNP 's/POS preconceptions/NNS ./. </C>
</S>
<S>
<C>Author2/NNP thinks/VBZ all/DT things/NNS should/MD be/AUX judged/VBN by/IN the/DT same/JJ standards/NNS ./. </C>
</S>
<S>
<C>Author2/NNP thinks/VBZ it/PRP is/AUX reasonable/JJ that/IN gun/NN owners/NNS will/MD not/RB support/VB gun/NN control/NN ,/, </C>
<C>and/CC has/AUX anecdotal/JJ evidence/NN to/TO back/VB it/PRP up/RP ./. </C>
</S>
<S>
<C>They/PRP insist/VBP anecdotal/JJ evidence/NN is/AUX more/RBR important/JJ ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ Author2/NNP needs/VBZ to/TO have/AUX their/PRP$ standards/NNS match/VB other/JJ people/NNS </C>
<C>and/CC that/IN Author2/NNP was/AUX implying/VBG that/IN poll/JJ respondents/NNS were/AUX not/RB lying/VBG </C>
<C>just/RB because/IN it/PRP does/AUX not/RB match/VB Author2/NNP 's/POS anecdotal/JJ evidence/NN ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ their/PRP$ poll/NN is/AUX not/RB biased/VBN </C>
<C>and/CC logic/NN dictates/VBZ that/IN poll/JJ results/NNS other/JJ than/IN expected/VBN may/MD indicate/VB a/DT problem/NN with/IN the/DT poll/NN ./. </C>
</S>
<S>
<C>Author1/NNP accuses/VBZ Author2/NNP of/IN ignoring/VBG contrary/JJ evidence/NN ./. </C>
</S>
</P>
</T>
