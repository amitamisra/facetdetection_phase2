<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP feels/VBZ Author2/NNP is/AUX merely/RB being/AUXG argumentative/VB just/RB for/IN argument/NN 's/POS sake/NN and/CC providing/VBG no/DT factual/JJ information/NN </C>
<C>to/TO support/VB his/her/NN claims/NNS ./. </C>
</S>
<S>
<C>Author2/NNP 's/POS comparing/VBG this/DT recent/JJ legislation/NN to/TO Nazi/NNP Germany/NNP is/AUX far-fetched/JJ and/CC unfair/JJ to/TO Jews/NNPS ./. </C>
</S>
<S>
<C>It/PRP is/AUX not/RB complete/JJ disarmament/NN if/IN you/PRP have/AUX one/CD firearm/NN taken/VBN from/IN you/PRP </C>
<C>but/CC are/AUX free/JJ to/TO purchase/VB other/JJ firearms/NNS ./. </C>
</S>
<S>
<C>With/IN or/CC without/IN guns/NNS the/DT government/NN reigns/VBZ ./. </C>
</S>
<S>
<C>Author2/NNP is/AUX skeptical/JJ of/IN a/DT recently-passed/JJ decision/NN to/TO increase/VB gun/NN restrictions/NNS in/IN Australia/NNP ./. </C>
</S>
<S>
<C>This/DT is/AUX not/RB merely/RB forcing/VBG citizens/NNS to/TO return/VB illegal/JJ weapons/NNS ,/, </C>
<C>it/PRP is/AUX a/DT thinly-veiled/JJ attempt/NN at/IN disarming/VBG Australians/NNPS innocuously/RB ./. </C>
</S>
<S>
<C>The/DT same/JJ thing/NN happened/VBD in/IN Nazi/NNP Germany/NNP when/WRB they/PRP began/VBD confiscating/VBG guns/NNS from/IN Jews/NNPS ./. </C>
</S>
<S>
<C>The/DT government/NN is/AUX insinuating/VBG </C>
<C>if/IN you/PRP do/AUX not/RB adhere/VB to/TO the/DT buy-back/NN you/PRP will/MD be/AUX arrested/VBN ./. </C>
</S>
<S>
<C>This/DT is/AUX tyranny/NN ./. </C>
</S>
</P>
</T>
