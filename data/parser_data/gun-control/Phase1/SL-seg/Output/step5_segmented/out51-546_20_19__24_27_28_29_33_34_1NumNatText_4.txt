<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP sees/VBZ the/DT situation/NN as/IN a/DT clear/JJ argument/NN for/IN gun/NN ownership/NN ./. </C>
</S>
<S>
<C>Author2/NNP does/AUX not/RB agree/VB </C>
<C>because/IN there/EX is/AUX no/DT fairness/NN in/IN dealing/VBG with/IN a/DT terrorist/JJ ./. </C>
</S>
<S>
<C>Author2/NNP does/AUX not/RB believe/VB that/IN rights/NNS should/MD be/AUX restricted/VBN based/VBN on/IN trust/NN </C>
<C>as/IN the/DT responsibility/NN for/IN one/CD 's/POS destiny/NN lies/VBZ with/IN the/DT individual/NN himself/PRP ./. </C>
</S>
<S>
<C>According/VBG to/TO Author2/NNP ,/, rights/NNS are/AUX waived/VBN only/RB for/IN convicted/VBN criminals/NNS ./. </C>
</S>
<S>
<C>Author1/NNP disagrees/VBZ </C>
<C>as/IN the/DT planes/NNS are/AUX for/IN business/NN purposes/NNS ,/, </C>
<C>and/CC guns/NNS should/MD not/RB be/AUX allowed/VBN ./. </C>
</S>
<S>
<C>Author2/NNP is/AUX offended/VBN by/IN the/DT acceptance/NN of/IN gun/NN ownership/NN restriction/NN </C>
<C>when/WRB many/JJ ancestors/NNS perished/VBD </C>
<C>if/IN only/RB to/TO ensure/VB that/IN the/DT 2nd/JJ Amendment/NN stands/VBZ ./. </C>
</S>
<S>
<C>Author1/NNP disapproves/VBZ the/DT claim/NN once/RB </C>
<C>again/RB by/IN pointing/VBG out/RP that/IN the/DT presence/NN of/IN weapons/NNS only/RB increases/VBZ instability/NN </C>
<C>instead/RB of/IN providing/VBG security/NN ./. </C>
</S>
<S>
<C>Author2/NNP sees/VBZ this/DT as/IN a/DT restriction/NN of/IN an/DT individual/JJ right/NN ./. </C>
</S>
</P>
</T>
