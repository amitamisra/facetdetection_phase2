<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP is/AUX discussing/VBG a/DT case-/JJ ''/'' Student/NN expelled/VBN for/IN having/AUXG unloaded/VBN shotguns/NNS in/IN truck/NN ''/'' ./. </C>
</S>
<S>
<C>He/PRP says/VBZ the/DT student/NN consented/VBD </C>
<C>to/TO searching/VBG his/PRP$ truck/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ it/PRP was/AUX legal/JJ to/TO do/AUX a/DT search/NN if/IN no/DT guns/NNS are/AUX allowed/VBN to/TO be/AUX within/IN 1000/CD feet/NNS of/IN a/DT school/NN ./. </C>
</S>
<S>
<C>The/DT rule/NN for/IN no/DT firearms/NN does/AUX not/RB make/VB any/DT exceptions/NNS for/IN them/PRP being/AUXG unloaded/VBN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ there/EX was/AUX birdshot/JJ ammo/NN in/IN the/DT truck/NN ./. </C>
</S>
<S>
<C>He/PRP notes/VBZ he/PRP was/AUX not/RB charged/VBN with/IN a/DT crime/NN ,/, just/RB expelled/VBN from/IN school/NN ./. </C>
</S>
<S>
<C>He/PRP states/VBZ the/DT cops/NNS searched/VBD it/PRP ,/, not/RB the/DT school/NN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ zero/CD tolerance/NN rule/NN means/VBZ </C>
<C>not/RB even/RB if/IN it/PRP is/AUX legal/JJ otherwise/RB ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ the/DT DA/NN did/AUX not/RB prosecute/VB ,/, </C>
<C>then/RB it/PRP must/MD be/AUX legal/JJ ./. </C>
</S>
<S>
<C>He/PRP says/VBZ there/EX is/AUX nothing/NN in/IN the/DT California/NNP firearms/NN laws/NNS that/WDT says/VBZ having/AUXG an/DT unloaded/JJ shotgun/NN in/IN his/PRP$ truck/NN was/AUX illegal/JJ ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ there/EX is/AUX an/DT exception/NN to/TO the/DT footage/NN rule/NN if/IN they/PRP are/AUX legally/RB transported/VBN unloaded/JJ long/JJ guns/NNS ./. </C>
</S>
<S>
<C>Author2/NNP questions/VBZ what/WP gives/VBZ the/DT school/NN the/DT authority/NN to/TO expel/VB a/DT student/NN over/IN a/DT legal/JJ activity/NN ./. </C>
</S>
</P>
</T>
