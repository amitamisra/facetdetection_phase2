<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP argues/VBZ that/IN guns/NNS and/CC tactical/JJ training/NN are/AUX good/JJ skills/NNS ,/, </C>
<C>and/CC that/IN Christians/NNS do/AUX not/RB need/AUX to/TO be/AUX against/IN weapons/NNS and/CC guns/NNS ./. </C>
</S>
<S>
<C>Scripture/NNP often/RB mentions/VBZ weapons/NNS ,/, and/CC it/PRP does/AUX allow/VB for/IN a/DT positive/JJ view/NN of/IN weapons/NNS </C>
<C>while/IN still/RB being/AUXG Christians/NNS ./. </C>
</S>
<S>
<C>People/NNS should/MD not/RB need/AUX a/DT license/NN to/TO purchase/VB a/DT weapon/NN ,/, </C>
<C>as/IN it/PRP is/AUX a/DT constitutional/JJ right/NN to/TO own/JJ weapons/NNS ./. </C>
</S>
<S>
<C>Registering/VBG guns/NNS with/IN the/DT federal/JJ authorities/NNS should/MD not/RB be/AUX enforced/VBN either/RB ./. </C>
</S>
<S>
<C>Waiting/NN periods/NNS are/AUX not/RB as/RB big/JJ of/IN an/DT issue/NN as/IN most/JJS people/NNS make/VBP them/PRP out/RP </C>
<C>to/TO be/AUX ./. </C>
</S>
<S>
<C>Carry/VB and/CC Conceal/VB laws/NNS that/WDT restrict/VBP concealed/VBN carry/VB infringe/VB upon/IN the/DT second/JJ amendment/NN right/RB granted/VBN in/IN the/DT Constitution/NNP ./. </C>
</S>
<S>
<C>Gun/NN control/NN methods/NNS are/AUX not/RB a/DT good/JJ idea/NN ./. </C>
</S>
<S>
<C>The/DT Constitution/NNP guarantees/VBZ the/DT people/NNS 's/POS rights/NNS to/TO own/JJ weapons/NNS ./. </C>
</S>
<S>
<C>Author2/NNP argues/VBZ that/IN cops/NNS spend/VBP their/PRP$ time/NN annoying/JJ teenagers/NNS and/CC tazering/JJ innocent/JJ citizens/NNS ./. </C>
</S>
<S>
<C>Licenses/NNS to/TO purchase/VB guns/NNS ,/, registration/NN ,/, waiting/NN periods/NNS ,/, carry/VB and/CC conceal/VB laws/NNS ,/, are/AUX all/RB unlawful/JJ and/CC should/MD not/RB be/AUX implemented/VBN ./. </C>
</S>
<S>
<C>Author2/NNP agrees/VBZ with/IN Author1/NNP that/IN gun/NN control/NN laws/NNS are/AUX not/RB a/DT good/JJ idea/NN ./. </C>
</S>
</P>
</T>
