<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP feels/VBZ that/IN by/IN owning/VBG and/CC carrying/VBG a/DT gun/NN you/PRP are/AUX claiming/VBG the/DT right/NN to/TO end/VB someone/NN 's/POS life/NN ./. </C>
</S>
<S>
<C>Taking/VBG a/DT life/NN is/AUX not/RB a/DT decision/NN that/WDT should/MD be/AUX put/VBN in/IN our/PRP$ hands/NNS so/RB easily/RB ./. </C>
</S>
<S>
<C>It/PRP 's/AUX taking/VBG the/DT law/NN into/IN your/PRP$ own/JJ hands/NNS ./. </C>
</S>
<S>
<C>Rights/NNS and/CC laws/NNS are/AUX created/VBN by/IN the/DT people/NNS in/IN a/DT society/NN ,/, </C>
<C>they/PRP 're/AUX not/RB intrinsically/RB given/VBN ./. </C>
</S>
<S>
<C>All/DT laws/NNS affect/VBP all/DT citizens/NNS ./. </C>
</S>
<S>
<C>Rights/NNS are/AUX not/RB imposed/VBN by/IN legislation/NN but/CC by/IN their/PRP$ being/AUXG upheld/VBN or/CC suppressed/VBN by/IN the/DT majority/NN ./. </C>
</S>
<S>
<C>Criminals/NNS are/AUX not/RB criminals/NNS </C>
<C>until/IN they/PRP 've/AUX broken/VBN a/DT law/NN ./. </C>
</S>
<S>
<C>The/DT law/NN creates/VBZ criminals/NNS more/RBR than/IN it/PRP prevents/VBZ them/PRP from/IN committing/VBG crime/NN ./. </C>
</S>
<S>
<C>Author2/NNP feels/VBZ that/IN criminals/NNS are/AUX violating/VBG a/DT person/NN 's/POS rights/NNS in/IN the/DT commission/NN of/IN the/DT crime/NN ,/, </C>
<C>they/PRP are/AUX assuming/VBG the/DT risk/NN of/IN being/AUXG punished/killed/VBN ./. </C>
</S>
<S>
<C>Criminals/NNS operate/VBP outside/IN of/IN society/NN 's/POS laws/NNS ./. </C>
</S>
<S>
<C>Gun/NN control/NN laws/NNS do/AUX not/RB keep/VB criminals/NNS from/IN obtaining/VBG weapons/NNS ,/, </C>
<C>they/PRP only/RB serve/VBP as/IN a/DT restriction/NN on/IN those/DT acting/VBG within/IN the/DT laws/NNS in/IN purchasing/VBG them/PRP legally/RB ./. </C>
</S>
<S>
<C>The/DT 2A/NN 's/POS purpose/NN was/AUX to/TO preserve/VB and/CC guarantee/VB these/DT rights/NNS ,/, not/RB grant/VB pre-existing/JJ rights/NNS ./. </C>
</S>
<S>
<C>Laws/NNS serve/VBP to/TO allocate/VB punishment/NN for/IN crimes/NNS committed/VBN ./. </C>
</S>
</P>
</T>
