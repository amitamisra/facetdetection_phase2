<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP states/VBZ that/IN since/RB the/DT airports/NNS are/AUX able/JJ to/TO carry/VB out/RP security/NN measures/NNS such/JJ as/IN searches/NNS ,/, metal/NN detectors/NNS ,/, x-rays/NNS and/CC limits/NNS on/IN what/WP can/MD be/AUX carried/VBN on/IN he/PRP does/AUX not/RB see/VB why/WRB DC/NNS can/MD do/AUX the/DT same/JJ with/IN its/PRP$ commuters/NNS on/IN a/DT daily/JJ basis/NN ./. </C>
</S>
<S>
<C>He/PRP goes/VBZ on/IN asking/VBG for/IN proof/NN that/IN gun/NN owners/NNS want/VBP to/TO repeal/VB the/DT federal/JJ age/NN limit/NN that/WDT covers/VBZ the/DT purchase/NN of/IN firearms/NN </C>
<C>and/CC that/IN people/NNS on/IN the/DT prohibited/VBN list/NN to/TO be/AUX allowed/VBN ./. </C>
</S>
<S>
<C>He/PRP then/RB asks/VBZ what/WP evidence/NN there/EX is/AUX the/DT no/DT gun/NN law/NN in/IN Great/NNP Britain/NNP has/AUX worked/VBN ?/. </C>
</S>
<S>
<C>Author2/NNP states/VBZ that/IN because/IN airports/NNS are/AUX private/JJ property/NN </C>
<C>they/PRP can/MD have/AUX that/DT security/NN ,/, for/IN a/DT city/NN the/DT size/NN of/IN DC/VBG it/PRP would/MD be/AUX expensive/JJ and/CC impossible/JJ ./. </C>
</S>
<S>
<C>Also/RB would/MD have/AUX to/TO convince/VB people/NNS of/IN the/DT constitutionality/NN of/IN it/PRP ./. </C>
</S>
<S>
<C>Airports/NNS can/MD because/IN of/IN the/DT unfounded/JJ fear/NN of/IN death/NN by/IN terrorist/JJ ./. </C>
</S>
<S>
<C>He/PRP says/VBZ that/IN no/DT one/NN believes/VBZ that/IN gun/NN control/NN will/MD stop/VB all/DT gun/NN crime/NN </C>
<C>but/CC it/PRP will/MD slow/VB it/PRP down/RP ./. </C>
</S>
<S>
<C>He/PRP does/AUX concede/VB that/IN both/DT sides/NNS have/AUX no/DT actual/JJ data/NN for/IN proof/NN ./. </C>
</S>
</P>
</T>
