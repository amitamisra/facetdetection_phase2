<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP asks/VBZ where/WRB the/DT authorization/NN of/IN ``/`` unannounced/JJ searches/NNS ''/'' of/IN the/DT holders/NNS of/IN arsenal/NN license/NN comes/VBZ from/IN ./. </C>
</S>
<S>
<C>Upon/IN learning/VBG that/IN the/DT ``/`` at/IN anytime/RB ''/'' pertains/NNS to/TO the/DT records/NNS relating/VBG to/TO a/DT firearm/NN involved/VBD in/IN a/DT criminal/JJ investigation/NN and/CC is/AUX traced/VBN back/RP to/TO the/DT owner/NN of/IN the/DT arsenal/NN license/NN ./. </C>
</S>
<S>
<C>He/PRP claims/VBZ that/IN Author2/NNP is/AUX just/RB trying/VBG to/TO scare/VB people/NNS ./. </C>
</S>
<S>
<C>Author2/NNP disagrees/VBZ and/CC says/VBZ that/IN he/PRP is/AUX not/RB trying/VBG to/TO scare/VB anyone/NN </C>
<C>and/CC that/IN Author1/NNP must/MD think/VB that/IN the/DT Bureau/NNP of/IN Alcohol/NNP ,/, Tobacco/NNP ,/, and/CC Firearms/NNPS send/VBP out/RP written/JJ invitations/NNS </C>
<C>when/WRB the/DT want/VBP to/TO come/VB by/RP and/CC inspect/VB ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ he/PRP did/AUX not/RB think/VB that/DT -/: but/CC on/IN the/DT other/JJ hand/NN -/: the/DT BATF/NN does/AUX not/RB just/RB show/VB up/RP in/IN the/DT middle/NN of/IN the/DT night/NN ,/, right/RB ?/. </C>
</S>
<S>
<C>Author2/NNP tries/VBZ to/TO set/VB Author1/NNP straight/RB and/CC explains/VBZ that/IN when/WRB they/PRP are/AUX investigating/VBG a/DT firearm/NN </C>
<C>the/DT BATF/NN is/AUX not/RB limited/VBN to/TO the/DT stores/NNS hours/NNS ,/, </C>
<C>but/CC that/IN they/PRP can/MD show/VB up/RP anytime/RB they/PRP want/VBP up/IN to/TO 3/CD times/NNS in/IN a/DT year/NN ./. </C>
</S>
<S>
<C>He/PRP goes/VBZ on/RB to/TO say/VB that/IN not/RB all/PDT the/DT agents/NNS are/AUX ``/`` nice/JJ guys/NNS ''/'' ./. </C>
</S>
</P>
</T>
