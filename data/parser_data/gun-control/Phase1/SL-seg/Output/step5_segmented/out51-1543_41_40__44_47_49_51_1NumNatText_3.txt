<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP points/VBZ out/RP there/EX is/AUX no/DT empirical/JJ data/NN suggesting/VBG that/IN gun/NN owners/NNS are/AUX safer/JJR than/IN non-gun/JJ owners/NNS ./. </C>
</S>
<S>
<C>Author2/NNS -LRB-/-LRB- or/CC any/DT staunch/JJ pro-gun/JJ person/NN -RRB-/-RRB- could/MD use/VB different/JJ statistics/NNS </C>
<C>to/TO infer/VB this/DT concept/NN </C>
<C>but/CC it/PRP would/MD not/RB be/AUX truly/RB valid/JJ ./. </C>
</S>
<S>
<C>Comparisons/NNS widely/RB vary/VBP depending/VBG on/IN the/DT perspective/NN terms/NNS they/PRP 're/AUX placed/VBN in/IN ./. </C>
</S>
<S>
<C>Personal/JJ opinions/NNS and/CC emotions/NNS will/MD also/RB make/VB vaguely-relative/JJ data/NNS seem/VB legitimate/JJ ./. </C>
</S>
<S>
<C>Pro-gun/JJ perspective/NN is/AUX :/: </C>
<C>on/IN 9/11/CD ,/, 3000/CD people/NNS died/VBD without/IN the/DT ability/NN to/TO defend/VB themselves/PRP ./. </C>
</S>
<S>
<C>Author2/NNP posits/VBZ that/IN if/IN there/EX is/AUX no/DT definitive/JJ data/NN proving/VBG safety/NN with/IN or/CC without/IN handguns/NNS ,/, </C>
<C>then/RB on/IN what/WP evidence/NN is/AUX a/DT ban/NN enforced/VBN ?/. </C>
</S>
<S>
<C>You/PRP can/MD use/VB statistics/NNS </C>
<C>to/TO promote/VB your/PRP$ stance/NN with/IN anything/NN ./. </C>
</S>
<S>
<C>You/PRP cannot/VBP ,/, though/RB ,/, </C>
<C>use/VBP skewed/JJ statistics/NNS </C>
<C>to/TO undermine/VB Constitutional/JJ rights/NNS ./. </C>
</S>
<S>
<C>People/NNS from/IN Author1/NNP 's/POS side/NN are/AUX violating/VBG civil/JJ rights/NNS in/IN disarmament/NN ./. </C>
</S>
</P>
</T>
