<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP argues/VBZ that/IN firearms/NNS are/AUX moving/VBG into/IN and/CC out/IN of/IN the/DT US/NNP at/IN too/RB high/JJ rates/NNS ./. </C>
</S>
<S>
<C>Many/JJ guns/NNS are/AUX being/AUXG illegally/RB trafficked/VBN and/CC moved/VBN ,/, </C>
<C>and/CC a/DT large/JJ number/NN of/IN the/DT are/AUX untraceable/JJ </C>
<C>because/IN their/PRP$ serial/JJ numbers/NNS were/AUX removed/VBN ./. </C>
</S>
<S>
<C>A/DT large/JJ portion/NN of/IN guns/NNS in/IN Canada/NNP come/VBP from/IN the/DT US/NNP illegally/RB ./. </C>
</S>
<S>
<C>Guns/NNS can/MD be/AUX traced/VBN without/IN their/PRP$ serial/JJ number/NN in/IN some/DT cases/NNS ./. </C>
</S>
<S>
<C>This/DT problem/NN is/AUX getting/VBG worse/JJR ,/, </C>
<C>as/IN people/NNS have/AUX been/AUX smuggling/NN guns/NNS and/CC drugs/NNS from/IN Maine/NNP and/CC Ohio/NNP into/IN Canada/NNP ./. </C>
</S>
<S>
<C>A/DT gun/NN registry/NN would/MD not/RB help/VB this/DT problem/NN ./. </C>
</S>
<S>
<C>Author2/NNP asked/VBD how/WRB the/DT guns/NNS were/AUX traced/VBN ,/, without/IN their/PRP$ serial/JJ numbers/NNS or/CC other/JJ information/NN ./. </C>
</S>
<S>
<C>No/DT ballistics/NNS database/NN has/AUX helped/VBN to/TO put/VB a/DT criminal/NN behind/IN bars/NNS ,/, </C>
<C>or/CC track/VB weapons/NNS that/WDT cross/VBP borders/NNS ./. </C>
</S>
<S>
<C>Long/JJ gun/NN registries/NNS would/MD not/RB help/VB track/VB this/DT ./. </C>
</S>
<S>
<C>Guns/NNS are/AUX not/RB tested/VBN by/IN ballistics/NNS or/CC by/IN registries/NNS </C>
<C>in/IN order/NN to/TO identify/VB them/PRP ,/, unlike/IN TV/NN show/NN dramas/NN ./. </C>
</S>
<S>
<C>Random/NNP matches/VBZ are/AUX simply/RB not/RB possible/JJ in/IN registries/NNS ./. </C>
</S>
<S>
<C>People/NNS confuse/VB the/DT abilities/NNS of/IN tv/NN shows/NNS with/IN real/JJ life/NN ,/, </C>
<C>and/CC we/PRP can/MD not/RB do/AUX much/RB of/IN what/WP is/AUX shown/VBN on/IN TV/NN ./. </C>
</S>
</P>
</T>
