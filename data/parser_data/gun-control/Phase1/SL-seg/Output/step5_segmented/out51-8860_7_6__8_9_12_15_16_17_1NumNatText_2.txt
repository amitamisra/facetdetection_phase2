<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP argues/VBZ that/IN firearms/JJ inspections/NNS are/AUX only/RB allowed/VBN to/TO be/AUX investigated/VBN at/IN any/DT time/NN ,/, </C>
<C>only/RB when/WRB the/DT gun/NN is/AUX involved/VBN in/IN a/DT criminal/JJ investigation/NN ./. </C>
</S>
<S>
<C>Guns/NNS are/AUX not/RB allowed/VBN to/TO be/AUX requested/VBN to/TO be/AUX searched/VBD at/IN any/DT time/NN when/WRB they/PRP are/AUX not/RB related/VBN to/TO a/DT criminal/JJ investigation/NN ./. </C>
</S>
<S>
<C>When/WRB not/RB related/VBN to/TO a/DT criminal/JJ investigation/NN ,/, </C>
<C>searches/NNS are/AUX only/RB legal/JJ not/RB more/RBR than/IN once/RB in/IN any/DT 12/CD month/NN period/NN ./. </C>
</S>
<S>
<C>The/DT only/JJ exception/NN is/AUX that/IN records/NNS can/MD be/AUX searched/VBD three/CD times/NNS each/DT year/NN ./. </C>
</S>
<S>
<C>If/IN they/PRP are/AUX investigating/VBG a/DT firearm/NN in/IN a/DT crime/NN that/WDT belongs/VBZ to/TO a/DT person/NN ,/, </C>
<C>they/PRP can/MD still/RB investigate/VB someone/NN ,/, </C>
<C>but/CC there/EX is/AUX a/DT likelihood/NN that/IN it/PRP would/MD not/RB be/AUX in/IN the/DT middle/NN of/IN the/DT night/NN ./. </C>
</S>
<S>
<C>Author2/NNP argues/VBZ that/IN they/PRP added/VBD two/CD additional/JJ chances/NNS for/IN legal/JJ unannounced/JJ searches/NNS in/IN each/DT 12/CD month/NN period/NN ,/, </C>
<C>and/CC they/PRP also/RB are/AUX now/RB allowed/VBN to/TO search/VB at/IN any/DT time/NN in/IN regard/NN to/TO a/DT firearm/NN in/IN a/DT criminal/JJ investigation/NN ./. </C>
</S>
<S>
<C>They/PRP will/MD not/RB announce/VB their/PRP$ searches/NNS ./. </C>
</S>
<S>
<C>It/PRP is/AUX fair/JJ game/NN for/IN any/DT searches/NNS </C>
<C>because/IN it/PRP is/AUX not/RB stated/VBN otherwise/RB ./. </C>
</S>
</P>
</T>
