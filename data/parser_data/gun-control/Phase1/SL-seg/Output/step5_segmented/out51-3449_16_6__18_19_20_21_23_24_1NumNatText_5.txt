<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP believes/VBZ that/IN Author2/NNP is/AUX taking/VBG a/DT sermon/NN discussed/VBD earlier/RBR out/IN of/IN context/NN as/RB well/RB as/IN not/RB understanding/VBG Author1/NNP 's/POS argument/NN ./. </C>
</S>
<S>
<C>Author1/NNP states/VBZ he/PRP is/AUX armed/JJ and/CC has/AUX had/AUX years/NNS of/IN training/NN ./. </C>
</S>
<S>
<C>Author1/NNP has/AUX been/AUX a/DT Christian/JJ for/IN 26/CD years/NNS ./. </C>
</S>
<S>
<C>Author1/NNP reiterated/VBD the/DT request/NN of/IN Author2/NNP 's/POS opinion/NN on/IN a/DT scripture/NN mentioning/VBG weapons/NNS and/CC states/NNS that/IN both/DT speaker/NN agree/VB on/RP the/DT appropriateness/NN of/IN weapons/NNS </C>
<C>but/CC disagree/VB on/IN copying/VBG and/CC pasting/VBG quotes/NNS in/IN forums/NNS ./. </C>
</S>
<S>
<C>Author1/NNP believes/VBZ Author2/NNP is/AUX arrogant/JJ and/CC irrational/JJ and/CC new/JJ to/TO forums/NNS ./. </C>
</S>
<S>
<C>Author1/NNP believes/VBZ licenses/NNS to/TO buy/VB guns/NNS are/AUX wrong/JJ ,/, </C>
<C>waiting/VBG periods/NNS to/TO purchase/VB guns/NNS should/MD not/RB be/AUX as/RB prevalent/JJ as/IN they/PRP are/AUX </C>
<C>and/CC that/IN conceal/VB carry/NN laws/NNS infringe/VB on/IN American/NNP 's/POS rights/NNS ./. </C>
</S>
<S>
<C>Author2/NNP defines/VBZ ``/`` drubbing/NN ''/'' then/RB goes/VBZ on/RB to/TO refute/VB Author1/NNP 's/POS suggestion/NN that/IN they/PRP are/AUX in/IN agreement/NN and/CC states/NNS that/WDT Author1/NNP desires/VBZ weapons/NNS to/TO be/AUX removed/VBN from/IN society/NN ./. </C>
</S>
<S>
<C>Author2/NNP is/AUX opposed/VBN to/TO licenses/NNS to/TO buy/VB weapons/NNS ,/, gun/NN registrations/NNS ,/, waiting/NN periods/NNS </C>
<C>to/TO purchase/VB guns/NNS and/CC conceal/VB carry/NN laws/NNS ./. </C>
</S>
<S>
<C>S/NNP 2/CD believes/VBZ police/NN officers/NNS should/MD publically/RB claim/VB to/TO defuse/VB interactions/NNS with/IN suspects/NNS </C>
<C>even/RB if/IN not/RB happening/VBG in/IN every/DT interaction/NN with/IN the/DT public/NN ./. </C>
</S>
<S>
<C>Both/DT speakers/NNS disagree/VBP often/RB with/IN word/NN choice/NN ,/, </C>
<C>who/WP agrees/VBZ with/IN whom/WP and/CC the/DT ground/NN rules/NNS for/IN debates/NNS ./. </C>
</S>
</P>
</T>
