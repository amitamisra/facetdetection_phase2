<T>
<P>
</P>
<P>
<S>
<C>Author1/NNS ask/VBP what/WP part/NN of/IN the/DT Brady/NNP law/NN covers/VBZ unannounced/JJ searches/NNS ./. </C>
</S>
<S>
<C>He/PRP points/VBZ out/RP the/DT section/NN of/IN the/DT code/NN saying/NN ``/`` at/IN any/DT time/NN ''/'' only/RB pertains/JJ to/TO when/WRB the/DT gun/NN is/AUX involved/VBN in/IN a/DT criminal/JJ investigation/NN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ gun/NN dealers/NNS records/NNS are/AUX rarely/RB searched/VBD in/IN the/DT middle/NN of/IN the/DT night/NN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ after/IN hours/NNS inspections/NNS would/MD only/RB be/AUX during/IN an/DT investigation/NN ./. </C>
</S>
<S>
<C>Author2/NNP cites/VBZ US/PRP code/VB Section/NNP 923/CD of/IN title/NN 18/CD covering/VBG Arsenal/NN License/NN ./. </C>
</S>
<S>
<C>It/PRP says/VBZ the/DT licensee/NN is/AUX subject/JJ to/TO all/DT obligations/NNS and/CC requirements/NNS pertaining/VBG to/TO dealers/NNS ./. </C>
</S>
<S>
<C>It/PRP was/AUX changed/VBN from/IN once/RB every/DT 12/CD months/NNS to/TO 3/CD times/NNS every/DT 12/CD months/NNS ,/, or/CC any/DT time/NN related/VBN to/TO a/DT firearm/NN involved/VBN in/IN a/DT criminal/JJ investigation/NN ./. </C>
</S>
<S>
<C>He/PRP notes/VBZ the/DT ``/`` or/CC at/IN any/DT time/NN ''/'' part/NN of/IN the/DT code/NN ,/, </C>
<C>and/CC mocks/VBZ they/PRP make/VBP convenient/JJ appointments/NNS ./. </C>
</S>
<S>
<C>He/PRP implies/VBZ the/DT ``/`` or/CC any/DT time/NN during/IN an/DT investigation/NN ''/'' means/VBZ they/PRP could/MD just/RB say/VB they/PRP were/AUX doing/VBG an/DT investigation/NN </C>
<C>if/IN they/PRP wanted/VBD to/TO search/VB more/JJR than/IN 3/CD times/NNS a/DT year/NN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ Author1/NNP should/MD know/VB police/NNS are/AUX not/RB restricted/VBN to/TO investigating/VBG only/RB during/IN business/NN hours/NNS ./. </C>
</S>
</P>
</T>
