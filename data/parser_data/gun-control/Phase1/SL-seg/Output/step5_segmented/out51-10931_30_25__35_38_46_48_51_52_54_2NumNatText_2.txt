<T>
<P>
</P>
<P>
<S>
<C>Author1/CD reports/NNS a/DT mayor/NN arrested/VBN in/IN a/DT sex/NN sting/NN ./. </C>
</S>
<S>
<C>He/PRP mentions/VBZ him/PRP </C>
<C>because/IN he/PRP is/AUX a/DT liberal/JJ mayor/NN who/WP wants/VBZ more/JJR gun/NN control/NN ,/, </C>
<C>yet/CC is/AUX a/DT felon/NN and/CC child/NN rapist/NN ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ Author2/NNP praises/VBZ liberals/NNS </C>
<C>as/IN being/AUXG perfect/JJ ,/, </C>
<C>and/CC this/DT example/NN shows/VBZ they/PRP 're/AUX not/RB ./. </C>
</S>
<S>
<C>He/PRP says/VBZ Author2/NNP hates/VBZ Nugent/NNP who/WP has/AUX never/RB done/AUX such/PDT a/DT thing/NN ,/, </C>
<C>and/CC is/AUX dismissive/JJ of/IN the/DT liberal/NN 's/POS crimes/NNS ./. </C>
</S>
<S>
<C>He/PRP defines/VBZ liberals/NNS </C>
<C>and/CC what/WP they/PRP believe/VBP in/IN :/: entitlement/NN programs/NNS ,/, </C>
<C>removing/VBG personal/JJ responsibility/NN ,/, doing/VBG work/NN yourself/PRP ,/, reduced/VBD standards/NNS of/IN morality/NN ,/, abolishing/VBG religion/NN ,/, worshiping/VBG the/DT government/NN ,/, government/NN power/NN of/IN the/DT Constitution/NNP ,/, and/CC taking/VBG care/NN of/IN all/PDT people/NNS 's/POS needs/NNS ./. </C>
</S>
<S>
<C>Author2/NNS ask/VBP how/WRB this/DT has/AUX to/TO do/AUX with/IN Ted/NNP Nugent/NNP ,/, </C>
<C>who/WP she/PRP feels/VBZ is/AUX a/DT bad/JJ representative/NN for/IN the/DT NRA/NNP ./. </C>
</S>
<S>
<C>She/PRP 's/AUX not/RB discussing/VBG gun/NN control/NN ,/, </C>
<C>but/CC what/WDT bad/JJ person/NN Nugent/NNP is/AUX ./. </C>
</S>
<S>
<C>She/PRP denies/VBZ Obama/NNP is/AUX a/DT liberal/NN ,/, </C>
<C>and/CC questions/VBZ </C>
<C>if/IN Author1/NNP knows/VBZ what/WP that/DT is/AUX ./. </C>
</S>
</P>
</T>
