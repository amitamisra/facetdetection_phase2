<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP believes/VBZ the/DT right/NN to/TO carry/VB a/DT gun/NN is/AUX not/RB a/DT natural/JJ right/NN for/IN citizens/NNS but/CC rather/RB a/DT tool/NN of/IN protection/NN for/IN states/NNS against/IN tyranny/NN ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ the/DT US/NNP constitution//NN Bill/NN of/IN Rights/NNPS is/AUX a/DT contract/NN with/IN US/PRP citizens/NNS ./. </C>
</S>
<S>
<C>He/PRP implies/VBZ that/IN Author2/NNP does/AUX not/RB know/VB constitutional/JJ history/NN or/CC its/PRP$ application/NN to/TO 2A/VB and/CC Bill/NNP or/CC Rights/NNP laws/NNS ./. </C>
</S>
<S>
<C>Author2/NNP reminds/VBZ Author1/NNP that/IN the/DT US/NNP made/VBD their/PRP$ own/JJ laws/NNS ,/, constitution/NN and/CC Bill/NNP of/IN Rights/NNPS ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ US/PRP citizens/NNS have/AUX natural/JJ rights/NNS guaranteed/VBN by/IN the/DT constitution//JJ Bill/NNP of/IN Rights/NNPS ./. </C>
</S>
<S>
<C>Author2/NNP insults/VBZ Author1/NNP </C>
<C>by/IN suggesting/VBG he/PRP take/VB his/PRP$ thinking/NN elsewhere/RB </C>
<C>because/IN his/PRP$ thinking/NN would/MD throw/VB the/DT world/NN into/IN slavery/NN ./. </C>
</S>
<S>
<C>He/PRP reminds/VBZ Author1/NNP the/DT creators/NNS of/IN the/DT constitution/Bill/NN of/IN Rights/NNPS knew/VBD exactly/RB what/WP they/PRP were/AUX doing/VBG ./. </C>
</S>
</P>
</T>
