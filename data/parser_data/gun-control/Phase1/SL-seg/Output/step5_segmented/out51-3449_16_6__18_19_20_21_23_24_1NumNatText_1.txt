<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP says/VBZ is/AUX is/AUX armed/JJ and/CC has/AUX years/NNS of/IN weapons/NNS training/NN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ he/PRP will/MD give/VB Author2/NNP a/DT sermon/NN ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ he/PRP is/AUX trying/VBG to/TO help/VB out/RP a/DT mental/JJ midget/NN since/IN they/PRP asked/VBD for/IN help/NN ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ he/PRP did/AUX not/RB ask/VB for/IN help/NN 
<M>-/: just/RB his/PRP$ opinion/NN on/IN a/DT scripture/NN that/WDT mentions/VBZ weapons/NNS and/CC did/AUX not/RB give/VB the/DT impression/NN he/PRP 's/AUX against/IN Author2/NNP 
<breakWithinParens>because/IN he/PRP is/AUX not/RB </M>
./. </C>
</S>
<S>
<C>Author2/NNP is/AUX treating/VBG him/PRP as/IN an/DT enemy/NN </C>
<C>because/IN he/PRP paraphrased/VBD a/DT scripture/NN to/TO agree/VB with/IN him/PRP ./. </C>
</S>
<S>
<C>Author2/NNP thinks/VBZ </C>
<C>because/IN Author1/NNP can/MD not/RB produce/VB an/DT argument/NN to/TO refute/VB him/PRP that/IN he/PRP has/AUX won/VBN ./. </C>
</S>
<S>
<C>Author1/NNP thinks/VBZ Author2/NNP is/AUX blinded/VBN by/IN arrogance/NN and/CC says/VBZ he/PRP does/AUX not/RB want/VB to/TO refute/VB his/PRP$ stance/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ if/IN Author2/NNP was/AUX rational/JJ he/PRP would/MD have/AUX pointed/VBN out/RP he/PRP said/VBD get/VB rid/VB of/IN and/CC simply/RB explained/VBD that/IN was/AUX not/RB the/DT correct/JJ interpretation/NN ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ he/PRP can/MD see/VB how/WRB the/DT fault/NN is/AUX on/IN him/PRP ,/, </C>
<C>but/CC since/IN he/PRP supports/VBZ some/DT of/IN the/DT issues/NNS </C>
<C>they/PRP still/RB do/AUX not/RB agree/VB ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ Author2/NNP is/AUX in/IN no/DT position/NN to/TO tell/VB him/PRP what/WP he/PRP meant/VBD ./. </C>
</S>
<S>
<C>Author2/NNP thinks/VBZ that/IN he/PRP is/AUX ./. </C>
</S>
</P>
</T>
