<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP quotes/VBZ a/DT headline/NN about/IN about/RB AK/CD records/NNS checks/NNS and/CC thinks/VBZ illegal/JJ ,/, unlawful/JJ and/CC unconstitutional/JJ acts/NNS are/AUX still/RB ongoing/JJ ./. </C>
</S>
<S>
<C>Author2/NNP thinks/VBZ this/DT is/AUX right-wing/JJ conspiracy/NN and/CC asks/VBZ what/WP is/AUX his/PRP$ responsibility/NN when/WRB asked/VBD to/TO trace/VB a/DT gun/NN ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ they/PRP are/AUX not/RB tracing/VBG guns/NNS ,/, </C>
<C>they/PRP are/AUX trying/VBG to/TO seize/VB the/DT entire/JJ dealers/NNS '/POS records/NNS ./. </C>
</S>
<S>
<C>Bound/VBN volumes/NNS always/RB stay/VBP with/IN the/DT dealer/NN and/CC never/RB leave/VBP the/DT shop/NN ./. </C>
</S>
<S>
<C>They/PRP can/MD be/AUX examined/VBN on/IN premises/NNS </C>
<C>but/CC cannot/NN be/AUX taken/VBN from/IN the/DT shop/NN under/IN any/DT circumstance/NN and/CC is/AUX a/DT violation/NN of/IN law/NN ./. </C>
</S>
<S>
<C>Author2/NNP disagrees/VBZ with/IN Author1/NNP 's/POS assertions/NNS ./. </C>
</S>
<S>
<C>Author1/NNP posts/NNS a/DT link/NN about/IN atf/NNS 's/POS records/NNS they/PRP are/AUX illegally/RB withholding/VBG from/IN congress/NNP ./. </C>
</S>
<S>
<C>Author2/NNP thinks/VBZ this/DT is/AUX more/RBR nonsense/JJ ./. </C>
</S>
<S>
<C>Author1/NNP thinks/VBZ that/IN Author2/NNP is/AUX defending/VBG unconstitutional/JJ acts/NNS ./. </C>
</S>
<S>
<C>Author2/NNP asks/VBZ how/WRB asking/VBG for/IN copies/NNS turns/VBZ into/IN an/DT illegal/JJ confiscation/NN of/IN bound/VBN ledgers/NNS ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ it/PRP 's/AUX in/IN contempt/NN of/IN congress/NNP ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ no/DT such/JJ statues/NNS have/AUX been/AUX quoted/VBN ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ he/PRP quoted/VBD congress/NNP ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ the/DT ATF/NN asked/VBD for/IN copies/NNS ,/, not/RB the/DT volumes/NNS ,/, </C>
<C>and/CC the/DT sellers/NNS are/AUX bound/VBN by/IN law/NN to/TO comply/VB ./. </C>
</S>
<S>
<C>Author1/NNP does/AUX not/RB agree/VB ./. </C>
</S>
</P>
</T>
