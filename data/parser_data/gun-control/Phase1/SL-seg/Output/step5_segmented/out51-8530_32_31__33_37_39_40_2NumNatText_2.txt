<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP defends/VBZ an/DT action/NN by/IN the/DT LAPD/NNP ,/, </C>
<C>claiming/VBG that/IN it/PRP was/AUX not/RB actually/RB police/NN brutality/NN ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ that/IN the/DT police/NNS were/AUX just/RB doing/VBG their/PRP$ jobs/NNS ./. </C>
</S>
<S>
<C>Author2/NNP argues/VBZ that/IN the/DT LAPD/NNP behaved/VBD inappropriately/RB </C>
<C>because/IN the/DT firearm/NN during/IN the/DT incident/NN was/AUX unloaded/VBN ./. </C>
</S>
<S>
<C>Author1/NNP rejects/VBZ this/DT argument/NN ,/, </C>
<C>saying/VBG that/IN the/DT LAPD/NNP claimed/VBD that/IN one/CD of/IN the/DT firearms/NN was/AUX actually/RB loaded/VBN ./. </C>
</S>
<S>
<C>He/PRP further/RB states/VBZ that/IN even/RB having/AUXG ammunition/NN and/CC firearm/VB together/RB is/AUX illegal/JJ ,/, </C>
<C>so/IN it/PRP does/AUX not/RB matter/VB </C>
<C>if/IN the/DT firearm/NN was/AUX actually/RB loaded/VBN ./. </C>
</S>
<S>
<C>Author2/NNP shifts/VBZ the/DT topic/NN to/TO the/DT quality/NN of/IN the/DT LAPD/NNP ./. </C>
</S>
<S>
<C>He/PRP cites/VBZ several/JJ instances/NNS where/WRB the/DT police/NN incorrectly/RB identified/VBD weapons/NNS ./. </C>
</S>
<S>
<C>He/PRP uses/VBZ this/DT as/IN evidence/NN that/IN the/DT police/NN may/MD have/AUX incorrectly/RB identified/VBN the/DT firearm/NN in/IN question/NN </C>
<C>as/IN being/AUXG loaded/VBN ,/, </C>
<C>which/WDT would/MD make/VB their/PRP$ behaviors/NNS police/NN brutality/NN ./. </C>
</S>
</P>
</T>
