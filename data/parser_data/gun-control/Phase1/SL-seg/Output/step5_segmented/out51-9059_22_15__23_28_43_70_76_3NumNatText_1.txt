<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP states/VBZ that/IN if/IN the/DT Obama/NNP administration/NN is/AUX seeking/VBG to/TO alter/VB the/DT Constitution/NNP </C>
<C>and/CC but/CC restrictions/NNS on/IN firearms/NN they/PRP should/MD at/IN least/JJS have/AUX some/DT amount/NN of/IN proof/NN that/IN this/DT action/NN would/MD create/VB less/JJR crime/NN ./. </C>
</S>
<S>
<C>Studies/NNS in/IN two/CD countries/NNS -LRB-/-LRB- England/NNP &/CC Australia/NNP -RRB-/-RRB- where/WRB the/DT same/JJ issue/NN was/AUX studied/VBN and/CC found/VBN to/TO produce/VB opposite/JJ results/NNS ./. </C>
</S>
<S>
<C>If/IN gun/NN control/NN does/AUX not/RB actually/RB curtail/VB crime/NN rates/NNS ,/, what/WP is/AUX its/PRP$ purpose/NN ?/. </C>
</S>
<S>
<C>Lack/NN of/IN gun/NN availability/NN or/CC increased/JJ cost/NN will/MD not/RB deter/VB criminals/NNS ./. </C>
</S>
<S>
<C>Criminal/JJ behavior/NN is/AUX not/RB so/RB predictable/JJ ./. </C>
</S>
<S>
<C>-LRB-/-LRB- Author1/NNP cites/VBZ two/CD examples/NNS of/IN kids/NNS caught/VBN possessing/being/JJ in/IN the/DT presence/NN of/IN military-grade/JJ firearms/NNS -RRB-/-RRB- ./. </C>
</S>
<S>
<C>Author2/NNP states/VBZ the/DT goal/NN of/IN firearm/JJ purchase/NN and/CC import/NN restrictions/NNS is/AUX ``/`` to/TO reduce/VB the/DT supply/NN of/IN firearms/NNS available/JJ to/TO criminals/NNS ''/'' ./. </C>
</S>
<S>
<C>Also/RB to/TO identify/VB negligence/NN and/CC criminal/JJ behavior/NN in/IN gun/NN owners/NNS that/WDT is/AUX threatening/VBG ``/`` the/DT general/JJ welfare/NN of/IN their/PRP$ fellow/JJ citizens/NNS ''/'' ./. </C>
</S>
<S>
<C>It/PRP 's/AUX a/DT focus/NN on/IN supply/NN and/CC demand/NN ./. </C>
</S>
<S>
<C>Illegal/JJ weapon/NN prices/NNS will/MD skyrocket/VB making/VBG it/PRP much/RB more/RBR difficult/JJ to/TO procure/VB ./. </C>
</S>
<S>
<C>Author1/NNP is/AUX saying/VBG ``/`` if/IN a/DT law/NN is/AUX broken/VBN ,/, </C>
<C>it/PRP 's/AUX ineffective/pointless/NN ''/'' ?/. </C>
</S>
<S>
<C>No/DT laws/NNS go/VBP unbroken/JJ ,/, </C>
<C>that/IN does/AUX not/RB undermine/VB their/PRP$ necessity/effectiveness/NN ./. </C>
</S>
</P>
</T>
