<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP believes/VBZ that/IN there/EX is/AUX no/DT proof/NN that/IN gun/NN owners/NNS are/AUX safer/JJR than/IN non-gun/JJ owners/NNS ,/, </C>
<C>and/CC that/IN people/NNS who/WP have/AUX had/AUX relatives/NNS die/VB from/IN gun/NN violence/NN have/AUX a/DT different/JJ attitude/NN ./. </C>
</S>
<S>
<C>Author2/NNP tells/VBZ Author1/NNP that/IN nobody/NN has/AUX proven/VBN that/IN non-gun/JJ owners/NNS are/AUX safer/JJR than/IN gun/NN owners/NNS ,/, </C>
<C>so/IN there/EX is/AUX no/DT reason/NN to/TO infringe/VB on/IN his/PRP$ rights/NNS ./. </C>
</S>
<S>
<C>Author1/NNP refutes/VBZ this/DT statement/NN </C>
<C>by/IN stressing/VBG that/IN he/PRP never/RB said/VBD it/PRP can/MD be/AUX proven/VBN or/CC not/RB ,/, </C>
<C>and/CC Author1/NNP asks/VBZ Author2/NNP how/WRB he/PRP is/AUX infringing/VBG on/IN his/PRP$ rights/NNS ./. </C>
</S>
<S>
<C>Author2/NNP answers/VBZ Author1/NNP </C>
<C>by/IN stating/VBG that/IN he/PRP is/AUX being/AUXG illogical/JJ </C>
<C>and/CC he/PRP uses/VBZ revisionist/JJ history/NN </C>
<C>to/TO violate/VB the/DT constitutional/JJ rights/NNS of/IN Author2/NNP ,/, </C>
<C>but/CC Author1/NNP asks/VBZ Author2/NNP to/TO show/VB him/PRP where/WRB it/PRP states/VBZ in/IN the/DT Constitution/NNP that/IN making/VBG an/DT illogical/JJ argument/NN violates/VBZ his/PRP$ rights/NNS ./. </C>
</S>
</P>
</T>
