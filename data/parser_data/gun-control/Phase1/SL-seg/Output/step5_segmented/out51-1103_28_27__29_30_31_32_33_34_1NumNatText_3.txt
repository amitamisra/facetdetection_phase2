<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP believes/VBZ that/IN if/IN you/PRP own/VBP a/DT gun/NN ,/, even/RB for/IN self/NN defense/NN ,/, </C>
<C>that/IN it/PRP is/AUX a/DT violation/NN of/IN everybody/NN else/RB 's/POS right/NN to/TO life/NN ,/, including/VBG criminals/NNS ./. </C>
</S>
<S>
<C>He/PRP continues/VBZ ,/, </C>
<C>not/RB feeling/VBG the/DT need/NN to/TO submit/VB proof/NN to/TO the/DT contrary/NN ,/, </C>
<C>claiming/VBG that/IN this/DT society/NN is/AUX powerful/JJ enough/RB that/IN it/PRP can/MD give/VB and/CC take/VB away/RP rights/NNS away/RB </C>
<C>as/IN it/PRP sees/VBZ fit/JJ ./. </C>
</S>
<S>
<C>And/CC that/IN a/DT person/NN alone/RB is/AUX at/IN the/DT mercy/NN of/IN the/DT mob/NN mentality/NN ./. </C>
</S>
<S>
<C>He/PRP questions/VBZ </C>
<C>whether/IN it/PRP is/AUX all/DT laws/NNS or/CC just/RB gun/NN laws/NNS that/IN criminals/NNS disregard/VB and/CC why/WRB even/RB have/AUX laws/NNS at/IN all/DT then/RB ?/. </C>
</S>
<S>
<C>Author2/NNP believes/VBZ that/IN criminals/NNS are/AUX the/DT ones/NNS who/WP break/VBP the/DT laws/NNS ,/, </C>
<C>not/RB the/DT law/NN abiding/VBG citizens/NNS ./. </C>
</S>
<S>
<C>And/CC that/IN the/DT gun/NN laws/NNS will/MD not/RB hurt/VB the/DT criminals/NNS ,/, </C>
<C>because/IN they/PRP still/RB will/MD get/VB guns/NNS ./. </C>
</S>
<S>
<C>He/PRP goes/VBZ on/RB to/TO claim/VB that/IN the/DT 2nd/JJ amendment/NN was/AUX not/RB made/VBN to/TO take/VB away/RP rights/NNS </C>
<C>but/CC to/TO preserve/VB the/DT preexisting/VBG rights/NNS of/IN the/DT people/NNS to/TO keep/VB and/CC bear/VB arms/NNS ./. </C>
</S>
<S>
<C>Laws/NNS are/AUX there/RB not/RB for/IN prevention/NN of/IN crime/NN </C>
<C>but/CC to/TO ensure/VB punishment/NN </C>
<C>when/WRB it/PRP happens/VBZ ./. </C>
</S>
</P>
</T>
