<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP wants/VBZ a/DT database/NN for/IN handguns/NNS ,/, </C>
<C>but/CC not/RB licenses/VBZ citing/VBG them/PRP as/IN unconstitutional/JJ ./. </C>
</S>
<S>
<C>Author1/NNP states/VBZ that/IN many/JJ handguns/NNS are/AUX stolen/VBN annually/RB </C>
<C>and/CC this/DT policy/NN would/MD help/VB limit/VB theft/NN ./. </C>
</S>
<S>
<C>Author1/NNP tells/VBZ Author2/NNP to/TO not/RB insinuate/VB that/IN they/PRP support/VBP a/DT ban/NN ,/, </C>
<C>because/IN they/PRP do/AUX not/RB ./. </C>
</S>
<S>
<C>Author2/NNP states/VBZ that/IN Author1/NNP 's/POS position/NN is/AUX clear/JJ </C>
<C>and/CC that/IN they/PRP will/MD have/AUX to/TO fight/VB for/IN every/DT bit/NN of/IN gun/NN control/NN then/RB quotes/VBZ John/NNP Paul/NNP Jones/NNP ./. </C>
</S>
<S>
<C>Author1/NNP dismisses/VBZ the/DT quote/NN and/CC talks/VBZ about/IN a/DT pro/JJ gun/NN person/NN who/WP lies/VBZ about/IN safe/JJ storage/NN being/AUXG unconstitutional/JJ ./. </C>
</S>
<S>
<C>They/PRP say/VBP that/IN a/DT particular/JJ pro/JJ gun/NN person/NN is/AUX pro/FW safe/JJ storage/NN ./. </C>
</S>
<S>
<C>Author2/NNP claims/VBZ that/IN they/PRP do/AUX not/RB trust/VB people/NNS who/WP claim/VBP to/TO be/AUX pro/JJ gun/NN ,/, </C>
<C>but/CC do/AUX not/RB interpret/VB the/DT second/JJ amendment/NN in/IN the/DT same/JJ way/NN that/IN they/PRP do/AUX ./. </C>
</S>
</P>
</T>
