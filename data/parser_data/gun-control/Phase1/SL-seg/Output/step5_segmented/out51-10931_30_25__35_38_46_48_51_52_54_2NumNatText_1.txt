<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP argues/VBZ that/IN there/EX are/AUX always/RB people/NNS who/WP agree/VBP with/IN you/PRP that/WDT are/AUX such/JJ terrible/JJ people/NNS that/WDT your/PRP$ position/NN looks/VBZ worse/JJR ./. </C>
</S>
<S>
<C>Liberals/NNS want/VBP free/JJ money/NN for/IN doing/VBG nothing/NN ,/, </C>
<C>and/CC do/AUX not/RB want/VB personal/JJ responsibility/NN ./. </C>
</S>
<S>
<C>Author1/NNP thinks/VBZ that/IN liberals/NNS want/VBP to/TO reduce/VB moral/JJ standards/NNS ,/, get/VB rid/JJ of/IN religion/NN ,/, and/CC provide/VB for/IN every/DT person/NN 's/POS needs/NNS like/IN they/PRP are/AUX a/DT child/NN ./. </C>
</S>
<S>
<C>Liberals/NNS want/VBP nothing/NN more/RBR than/IN to/TO worship/NN the/DT government/NN ,/, to/TO let/VB the/DT government/NN have/AUX more/JJR and/CC more/JJR power/NN ,/, to/TO reduce/VB constitutional/JJ rights/NNS ,/, </C>
<C>so/RB that/IN the/DT government/NN can/MD help/VB all/PDT the/DT people/NNS who/WP can/MD not/RB help/VB themselves/PRP ./. </C>
</S>
<S>
<C>Author1/NNP points/VBZ out/RP that/IN liberal/JJ mayors/NNS can/MD also/RB be/AUX people/NNS who/WP want/VBP to/TO take/VB away/RP guns/NNS and/CC rape/NN small/JJ children/NNS ./. </C>
</S>
<S>
<C>No/DT political/JJ party/NN is/AUX perfect/JJ ,/, </C>
<C>and/CC people/NNS who/WP do/AUX not/RB agree/VB with/IN others/NNS will/MD ignore/VB their/PRP$ opinions/NNS as/IN rantings/NNS and/CC move/VB on/RP ./. </C>
</S>
<S>
<C>Gun/NN control/NN takes/VBZ guns/NNS from/IN honest/JJ people/NNS ./. </C>
</S>
<S>
<C>Author2/NNP argues/VBZ that/IN Ted/NNP Nugent/NNP should/MD be/AUX punished/VBN by/IN the/DT NRA/NNP ./. </C>
</S>
<S>
<C>The/DT NRA/NNP does/AUX not/RB have/AUX any/DT integrity/NN </C>
<C>because/IN they/PRP will/MD not/RB get/VB rid/JJ of/IN him/PRP ./. </C>
</S>
<S>
<C>Obama/NNP is/AUX not/RB a/DT liberal/NN anymore/RB ./. </C>
</S>
</P>
</T>
