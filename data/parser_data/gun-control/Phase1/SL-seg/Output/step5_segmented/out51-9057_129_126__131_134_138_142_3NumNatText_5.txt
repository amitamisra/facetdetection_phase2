<T>
<P>
</P>
<P>
<S>
<C>Author1/CD jokes/NNS about/IN a/DT guy/NN who/WP trained/VBN under/IN the/DT bullet/NN proof/NN monk/VBP ,/, </C>
<C>but/CC Author2/NNP states/VBZ that/IN he/PRP is/AUX not/RB bullet/NN proof/NN or/CC a/DT monk/NN </C>
<C>because/IN he/PRP is/AUX a/DT white/JJ ex-cop/NN ./. </C>
</S>
<S>
<C>Author1/NNP asserts/VBZ that/IN rubber/NN knives/NNS are/AUX different/JJ from/IN real/JJ ones/NNS which/WDT is/AUX why/WRB he/PRP believes/VBZ the/DT trainer/NN is/AUX a/DT charlatan/NN ./. </C>
</S>
<S>
<C>Author2/NNP asks/VBZ Author1/NNP if/IN he/PRP thinks/VBZ that/IN the/DT training/NN methods/NNS of/IN both/PDT the/DT military/NN and/CC the/DT police/NNS are/AUX bogus/JJ ./. </C>
</S>
<S>
<C>Author1/NNP answers/VBZ him/PRP </C>
<C>by/IN stating/VBG that/IN people/NNS trained/VBN by/IN the/DT military/NN and/CC police/NNS do/AUX not/RB claim/VB to/TO be/AUX trained/VBN in/IN real/JJ life/NN situations/NNS ./. </C>
</S>
<S>
<C>Author2/NNP states/VBZ that/IN he/PRP was/AUX accused/VBN of/IN being/AUXG untrained/JJ which/WDT is/AUX why/WRB he/PRP stated/VBD that/IN he/PRP was/AUX trained/VBN ./. </C>
</S>
<S>
<C>The/DT accusation/NN that/IN he/PRP claimed/VBD to/TO have/AUX been/AUX trained/VBN in/IN real/JJ life/NN situations/NNS is/AUX false/JJ ./. </C>
</S>
</P>
</T>
