<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP believes/VBZ people/NNS should/MD be/AUX allowed/VBN to/TO keep/VB their/PRP$ guns/NNS ./. </C>
</S>
<S>
<C>On/IN the/DT other/JJ hand/NN ,/, he/PRP says/VBZ he/PRP has/AUX no/DT desire/NN to/TO take/VB a/DT side/NN in/IN any/DT kind/NN of/IN debate/NN concerning/VBG America/NNP 's/POS gun/NN laws/NNS ./. </C>
</S>
<S>
<C>Belief/NN in/IN God/NNP ,/, ownership/NN of/IN guns/NNS and/CC dauntless/NN courage/NN is/AUX what/WP has/AUX made/VBN America/NNP great/JJ ./. </C>
</S>
<S>
<C>That/DT is/AUX something/NN he/PRP believes/VBZ ./. </C>
</S>
<S>
<C>On/IN the/DT other/JJ hand/NN ,/, he/PRP does/AUX not/RB believe/VB in/IN analyzing/VBG the/DT gun/NN laws/NNS in/IN America/NNP </C>
<C>by/IN comparing/VBG them/PRP to/TO the/DT ones/NNS in/IN Britain/NNP ./. </C>
</S>
<S>
<C>He/PRP says/VBZ he/PRP has/AUX a/DT right/NN to/TO have/AUX no/DT opinion/NN at/IN all/DT ,/, </C>
<C>and/CC states/VBZ that/IN withholding/VBG an/DT opinion/NN does/AUX not/RB make/VB him/PRP a/DT trouble-making/JJ ,/, dissension-sowing/JJ troll/NN ./. </C>
</S>
<S>
<C>Author2/NNP feels/VBZ that/IN any/DT poster/NN who/WP has/AUX no/DT side/NN to/TO take/VB </C>
<C>or/CC point/NN of/IN view/NN to/TO offer/VB in/IN the/DT discussion/NN is/AUX likely/JJ to/TO get/VB labeled/VBN a/DT troll/NN ,/, rather/RB than/IN </C>
<C>as/IN the/DT intellectual/JJ Author1/NNP appears/VBZ to/TO consider/VB himself/PRP </C>
<C>to/TO be/AUX ./. </C>
</S>
<S>
<C>On/IN the/DT other/JJ hand/NN ,/, he/PRP agrees/VBZ that/IN everyone/NN should/MD keep/VB their/PRP$ gun/NN rights/NNS ,/, and/CC particularly/RB their/PRP$ guns/NNS ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ that/IN the/DT two/CD are/AUX friends/NNS and/CC allies/NNS ,/, for/IN the/DT dark/JJ days/NNS that/WDT may/MD come/VB ./. </C>
</S>
</P>
</T>
