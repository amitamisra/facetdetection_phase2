<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP states/VBZ </C>
<C>in/IN order/NN to/TO claim/VB self/NN defense/NN ,/, </C>
<C>you/PRP have/AUX to/TO leave/VB </C>
<C>if/IN you/PRP can/MD do/AUX so/RB safely/RB ./. </C>
</S>
<S>
<C>However/RB ,/, if/IN your/PRP$ safety/NN is/AUX being/AUXG immediately/RB threatened/VBN ,/, </C>
<C>you/PRP may/MD not/RB be/AUX able/JJ to/TO leave/VB the/DT scene/NN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ if/IN there/EX is/AUX no/DT immediate/JJ threat/NN ,/, </C>
<C>you/PRP can/MD not/RB claim/VB self/NN defense/NN ./. </C>
</S>
<S>
<C>The/DT law/NN realizes/VBZ there/EX are/AUX times/NNS during/IN an/DT immediate/JJ threat/NN where/WRB you/PRP can/MD not/RB safely/RB retreat/VB </C>
<C>and/CC then/RB it/PRP is/AUX self/NN defense/NN ,/, </C>
<C>which/WDT is/AUX why/WRB the/DT statue/NN requiring/VBG you/PRP to/TO leave/VB was/AUX abolished/VBN ./. </C>
</S>
<S>
<C>He/PRP suggests/VBZ seeking/VBG legal/JJ advice/NN to/TO clarify/VB this/DT ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ he/PRP is/AUX wrong/JJ </C>
<C>because/IN there/EX is/AUX no/DT requirement/NN to/TO flee/VB ,/, </C>
<C>only/RB prove/VB you/PRP were/AUX in/IN danger/NN ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ he/PRP only/RB said/VBD you/PRP can/MD not/RB prove/VB it/PRP was/AUX self/NN defense/NN if/IN you/PRP had/AUX a/DT safe/JJ way/NN to/TO flee/VB and/CC did/AUX not/RB do/AUX so/RB ,/, </C>
<C>not/RB that/IN you/PRP have/AUX to/TO attempt/VB to/TO flee/VB ./. </C>
</S>
<S>
<C>He/PRP says/VBZ you/PRP should/MD confirm/VB this/DT with/IN a/DT lawyer/NN ./. </C>
</S>
<S>
<C>Author2/NNP sarcastically/RB says/VBZ people/NNS should/MD try/VB calling/VBG a/DT lawyer/NN while/IN facing/VBG someone/NN with/IN a/DT weapon/NN </C>
<C>instead/RB of/IN using/VBG a/DT gun/NN in/IN self/NN defense/NN ./. </C>
</S>
</P>
</T>
