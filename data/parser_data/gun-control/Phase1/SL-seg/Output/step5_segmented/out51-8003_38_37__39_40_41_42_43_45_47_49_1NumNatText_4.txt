<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP claims/VBZ guns/NNS are/AUX not/RB so/RB much/RB the/DT problem/NN in/IN the/DT US/NNP as/IN the/DT people/NNS who/WP think/VBP they/PRP have/AUX to/TO have/AUX them/PRP ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ it/PRP is/AUX not/RB the/DT pro-gun/JJ people/NNS who/WP are/AUX the/DT problem/NN ,/, but/CC the/DT criminals/NNS who/WP misuse/NN guns/NNS and/CC the/DT court/NN system/NN who/WP does/AUX not/RB properly/RB punish/VB criminals/NNS ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ he/PRP lives/VBZ in/IN a/DT large/JJ city/NN in/IN Canada/NNP ,/, </C>
<C>and/CC says/VBZ they/PRP have/AUX 6-7/JJ murders/NNS per/IN year/NN ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ the/DT problem/NN is/AUX not/RB the/DT pro-gun/JJ people/NNS ,/, but/CC the/DT ones/NNS who/WP do/AUX not/RB understand/VB why/WRB people/NNS do/AUX not/RB want/VB to/TO surrender/VB their/PRP$ rights/NNS and/CC be/AUX controlled/VBN by/IN the/DT government/NN and/CC police/NNS ./. </C>
</S>
<S>
<C>Author1/NNS ask/VBP why/WRB is/AUX an/DT armed/JJ group/NN of/IN citizens/NNS better/JJR than/IN a/DT police/NN force/NN ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ being/AUXG able/JJ to/TO defend/VB oneself/PRP is/AUX not/RB the/DT same/JJ as/IN being/AUXG a/DT vigilante/NN ./. </C>
</S>
<S>
<C>Author1/NNP argues/VBZ it/PRP does/AUX because/IN you/PRP are/AUX taking/VBG the/DT law/NN in/IN your/PRP$ own/JJ hands/NNS ./. </C>
</S>
<S>
<C>He/PRP mentions/VBZ how/WRB personal/JJ rights/NNS are/AUX being/AUXG violated/VBN by/IN the/DT Patriot/NN Act/NN ./. </C>
</S>
<S>
<C>Author2/NNP argues/VBZ being/AUXG unarmed/VBD creates/VBZ a/DT police/NN state/NN ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ Author2/NNP is/AUX arguing/VBG citizens/NNS should/MD be/AUX able/JJ to/TO act/VB as/IN police/NNS ./. </C>
</S>
</P>
</T>
