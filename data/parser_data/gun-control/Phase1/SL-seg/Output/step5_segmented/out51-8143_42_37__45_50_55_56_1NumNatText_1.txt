<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP takes/VBZ issue/NN with/IN Author2/NNP 's/POS interpretation/NN of/IN results/NNS from/IN a/DT poll/NN that/WDT Author2/NNP created/VBD about/IN gun/NN control/NN ./. </C>
</S>
<S>
<C>Author1/NNP believes/VBZ Author2/NNP is/AUX judging/VBG others/NNS by/IN Author2/NNP 's/POS own/JJ standards/NNS ,/, preconceptions/NNS and/CC life/NN experiences/NNS ./. </C>
</S>
<S>
<C>Author1/NNP is/AUX trying/VBG to/TO convince/VB Author2/NNP that/IN respondents/NNS provided/VBD truthful/JJ answers/NNS </C>
<C>and/CC that/IN the/DT results/NNS prove/VBP that/IN Author1/NNP 's/POS pole/JJ hypothesis/NN was/AUX simply/RB wrong/JJ ./. </C>
</S>
<S>
<C>Author2/NNP believes/VBZ his/PRP$ life/NN experience/NN and/CC personal/JJ standards/NNS support/VBP his/PRP$ conclusion/NN that/IN gun/NN owners/NNS will/MD not/RB be/AUX huge/JJ advocates/NNS for/IN gun/NN control/NN ,/, regardless/RB of/IN what/WP the/DT poll/JJ results/NNS showed/VBD ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ respondents/NNS could/MD have/AUX lied/VBD </C>
<C>and/CC he/PRP refuses/VBZ to/TO derive/VB any/DT conclusions/NNS from/IN the/DT results/NNS of/IN his/PRP$ own/JJ poll/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ his/PRP$ poll/NN 's/POS accuracy/NN is/AUX questionable/JJ </C>
<C>because/IN the/DT poll/JJ results/NNS do/AUX not/RB match/VB what/WP logic/NN tells/VBZ him/PRP ./. </C>
</S>
</P>
</T>
