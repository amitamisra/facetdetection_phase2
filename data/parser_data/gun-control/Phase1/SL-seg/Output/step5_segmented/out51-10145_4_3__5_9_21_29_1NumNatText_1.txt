<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP is/AUX chastising/VBG Author2/NNP </C>
<C>as/IN putting/VBG people/NNS down/RB </C>
<C>just/RB because/IN they/PRP do/AUX not/RB agree/VB with/IN him/PRP ./. </C>
</S>
<S>
<C>He/PRP mocks/VBZ Author2/NNP 's/POS claims/NNS of/IN a/DT hidden/JJ agenda/NN </C>
<C>to/TO enable/VB authorities/NNS to/TO seize/VB weapons/NNS ./. </C>
</S>
<S>
<C>He/PRP calls/VBZ Author2/NNP a/DT radical/JJ ,/, and/CC in/IN the/DT minority/NN with/IN those/DT beliefs/NNS ./. </C>
</S>
<S>
<C>He/PRP supports/VBZ recent/JJ rulings/NNS ,/, </C>
<C>but/CC does/AUX not/RB believe/VB they/PRP will/MD cause/VB any/DT changes/NNS ,/, because/IN of/IN the/DT constitution/NN ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ he/PRP is/AUX not/RB asking/VBG for/IN repeals/NNS ,/, </C>
<C>but/CC citizens/NNS do/AUX not/RB support/VB stricter/JJR laws/NNS ./. </C>
</S>
<S>
<C>Author1/NNP points/VBZ out/RP that/IN people/NNS want/VBP the/DT laws/NNS to/TO stay/VB the/DT same/JJ ,/, </C>
<C>and/CC only/RB 15/CD %/NN want/VBP laws/NNS repealed/VBN ,/, </C>
<C>making/VBG Author2/NNP in/IN a/DT minority/NN ./. </C>
</S>
<S>
<C>He/PRP accuses/VBZ Author2/NNP of/IN being/AUXG dishonest/JJ ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ that/IN gun/NN laws/NNS steal/VBP rights/NNS from/IN people/NNS ,/, </C>
<C>and/CC he/PRP is/AUX defending/VBG his/PRP$ rights/NNS as/IN a/DT citizen/NN ./. </C>
</S>
</P>
</T>
