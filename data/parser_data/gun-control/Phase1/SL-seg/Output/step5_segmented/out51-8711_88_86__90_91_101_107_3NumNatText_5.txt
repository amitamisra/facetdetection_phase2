<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP argues/VBZ that/IN Canada/NNP and/CC the/DT United/NNP States/NNPS are/AUX comparable/JJ and/CC can/MD be/AUX used/VBN to/TO draw/VB conclusions/NNS about/IN how/WRB gun/NN control/NN and/CC violence/NN are/AUX linked/VBN ./. </C>
</S>
<S>
<C>Each/DT country/NN has/AUX similar/JJ levels/NNS of/IN gun/NN ownership/NN ,/, </C>
<C>but/CC Canada/NNP has/AUX less/JJR gun/NN crime/NN ./. </C>
</S>
<S>
<C>Canada/NNP 's/POS system/NN for/IN gun/NN safety/NN and/CC control/NN may/MD be/AUX a/DT better/JJR system/NN than/IN ours/JJ ./. </C>
</S>
<S>
<C>Comparisons/NNS of/IN this/DT type/NN are/AUX used/VBN in/IN statistics/NNS and/CC mathematics/NNS often/RB ,/, </C>
<C>because/IN there/EX is/AUX no/DT way/NN to/TO determine/VB how/WRB it/PRP would/MD exactly/RB play/VB out/RP </C>
<C>without/IN changing/VBG US/PRP law/NN ./. </C>
</S>
<S>
<C>Author2/NNP argues/VBZ that/IN Author1/NNP 's/POS assumptions/NNS are/AUX wrong/JJ ,/, </C>
<C>and/CC that/IN there/EX are/AUX too/RB many/JJ differences/NNS between/IN the/DT US/NNP and/CC Canada/NNP to/TO compare/VB them/PRP ./. </C>
</S>
<S>
<C>Author2/NNP points/VBZ out/RP how/WRB the/DT population/NN and/CC population/NN density/NN in/IN the/DT US/NNP are/AUX much/RB higher/JJR than/IN in/IN Canada/NNP ./. </C>
</S>
</P>
</T>
