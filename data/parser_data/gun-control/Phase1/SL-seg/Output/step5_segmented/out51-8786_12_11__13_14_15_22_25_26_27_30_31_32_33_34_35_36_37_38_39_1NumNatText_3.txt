<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP asks/VBZ which/WDT countries/NNS that/WDT confiscated/VBD firearms/JJ post/NN registration/NN had/AUX the/DT people/NNS 's/POS RKBA/NN in/IN their/PRP$ constitutions/NNS ./. </C>
</S>
<S>
<C>Author2/NNP replies/VBZ </C>
<C>to/TO look/VB at/IN the/DT NY/JJ and/CC CA/JJ constitutions/NNS ./. </C>
</S>
<S>
<C>Author1/NNP sees/VBZ no/DT reason/NN to/TO assume/VB registration/NN leads/NNS to/TO confiscation/NN ./. </C>
</S>
<S>
<C>Author2/NNP thinks/VBZ he/PRP is/AUX ignoring/VBG facts/NNS ./. </C>
</S>
<S>
<C>Author1/NNP supports/VBZ interpretation/NN of/IN the/DT Constitution/NNP and/CC thinks/VBZ morals/NNS are/AUX relative/JJ ./. </C>
</S>
<S>
<C>Author2/NNP has/AUX no/DT need/NN to/TO control/VB anyone/NN beyond/IN himself/PRP ,/, </C>
<C>and/CC says/VBZ murder/NN crosses/VBZ all/DT cultures/NNS ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ murder/NN is/AUX killing/VBG without/IN society/NN 's/POS sanction/NN and/CC exists/VBZ in/IN all/DT cultures/NNS ,/, </C>
<C>but/CC when/WRB society/NN sanctions/NNS killing/NN varies/VBZ ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ he/PRP just/RB demonstrated/VBD a/DT moral/JJ absolute/JJ ./. </C>
</S>
<S>
<C>Author1/NNP thinks/VBZ killing/VBG is/AUX not/RB always/RB murder/NN ./. </C>
</S>
<S>
<C>Murder/NN is/AUX the/DT killing/NN of/IN one/CD 's/POS own/JJ in/IN group/NN ./. </C>
</S>
<S>
<C>Author1/NNP asks/VBZ if/IN he/PRP kills/VBZ someone/NN who/WP is/AUX about/IN to/TO kill/VB someone/NN else/RB ,/, </C>
<C>has/AUX he/PRP murdered/VBN ./. </C>
</S>
<S>
<C>Author2/NNP replies/VBZ the/DT killing/NN of/IN one/CD 's/POS family/NN or/CC clan/NN is/AUX murder/NN and/CC crosses/VBZ all/DT cultures/NNS ,/, </C>
<C>but/CC killing/VBG of/IN the/DT out/NN group/NN is/AUX not/RB necessarily/RB murder/NN ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ he/PRP never/RB claimed/VBD otherwise/RB ./. </C>
</S>
<S>
<C>He/PRP simply/RB thinks/VBZ we/PRP are/AUX not/RB all/DT intellectual/NN equals/VBZ ./. </C>
</S>
<S>
<C>Author1/NNP and/CC Author2/NNP trade/NN insults/NNS but/CC later/RB apologize/VB for/IN it/PRP ./. </C>
</S>
</P>
</T>
