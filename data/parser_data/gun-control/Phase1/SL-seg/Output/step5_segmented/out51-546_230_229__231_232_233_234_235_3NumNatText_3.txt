<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP is/AUX in/IN favor/NN of/IN having/AUXG law/NN abiding/VBG citizens/NNS carrying/VBG guns/NNS onto/IN passenger/NN aircraft/NN ./. </C>
</S>
<S>
<C>Guns/NNS would/MD be/AUX helpful/JJ in/IN the/DT hands/NNS of/IN civilians/NNS on/IN an/DT aircraft/NN ,/, </C>
<C>in/IN order/NN to/TO defend/VB against/IN the/DT threat/NN of/IN terrorism/NN and/CC hijacking/NN of/IN planes/NNS ./. </C>
</S>
<S>
<C>Author1/NNP accuses/VBZ Author2/NNP that/IN there/EX is/AUX no/DT evidence/NN that/IN having/AUXG citizens/NNS carry/VB guns/NNS onto/IN planes/NNS has/AUX resulted/VBN in/IN any/DT injuries/NNS ,/, </C>
<C>and/CC that/IN it/PRP could/MD only/RB make/VB flights/NNS safer/JJR ./. </C>
</S>
<S>
<C>Guns/NNS are/AUX a/DT beneficial/JJ invention/NN ./. </C>
</S>
<S>
<C>Author2/NNP believes/VBZ that/IN anyone/NN carrying/VBG a/DT gun/NN on/IN a/DT plane/NN would/MD be/AUX a/DT greater/JJR risk/NN to/TO those/DT arround/NN them/PRP ,/, </C>
<C>because/IN they/PRP might/MD be/AUX mistaken/VBN for/IN a/DT terrorist/JJ ./. </C>
</S>
<S>
<C>Guns/NNS should/MD not/RB be/AUX allowed/VBN on/IN planes/NNS ,/, </C>
<C>as/IN the/DT risks/NNS would/MD be/AUX too/RB high/JJ and/CC the/DT reward/NN too/RB unsure/JJ ./. </C>
</S>
<S>
<C>Gun/NN control/NN would/MD be/AUX a/DT good/JJ idea/NN ./. </C>
</S>
</P>
</T>
