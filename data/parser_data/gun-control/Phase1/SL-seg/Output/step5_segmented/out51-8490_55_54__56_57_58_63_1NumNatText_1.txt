<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP is/AUX talking/VBG about/IN someone/NN who/WP left/VBD a/DT knife/NN on/IN a/DT counter/NN ,/, </C>
<C>and/CC how/WRB obvious/JJ the/DT intent/NN to/TO do/AUX harm/NN is/AUX ./. </C>
</S>
<S>
<C>Author2/CD posts/NNS the/DT relevant/JJ headline/NN of/IN an/DT injured/JJ woman/NN stabbed/VBN in/IN a/DT store/NN shooting/NN ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ that/IN if/IN it/PRP had/AUX been/AUX a/DT gun/NN ,/, </C>
<C>the/DT headline/NN would/MD have/AUX been/AUX about/RB seven/CD dead/JJ people/NNS ./. </C>
</S>
<S>
<C>Author2/NNP posts/NNS a/DT headline/NN about/IN seven/CD people/NNS stabbed/VBN to/TO death/NN in/IN Japan/NNP ./. </C>
</S>
<S>
<C>Author1/NNP brings/VBZ up/RP the/DT Columbine/NNP School/NNP shooting/NN ./. </C>
</S>
<S>
<C>Author2/NNP argues/VBZ it/PRP is/AUX not/RB relevant/JJ ./. </C>
</S>
<S>
<C>The/DT two/CD people/NNS were/AUX crazy/JJ ,/, </C>
<C>and/CC got/VBD their/PRP$ guns/NNS illegally/RB ./. </C>
</S>
<S>
<C>They/PRP took/VBD them/PRP to/TO a/DT no/RB guns/NNS area/NN ,/, </C>
<C>and/CC killed/VBD without/IN any/DT regard/NN for/IN people/NNS ./. </C>
</S>
<S>
<C>This/DT is/AUX different/JJ than/IN someone/NN carrying/VBG a/DT gun/NN for/IN protection/NN ,/, he/PRP says/VBZ ./. </C>
</S>
</P>
</T>
