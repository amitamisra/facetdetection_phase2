<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP feels/VBZ a/DT student/NN was/AUX rightfully/RB expelled/VBN from/IN school/NN </C>
<C>after/IN a/DT search/NN of/IN his/PRP$ truck/NN found/VBD an/DT unloaded/JJ shotgun/NN in/IN the/DT back/JJ seat/NN ./. </C>
</S>
<S>
<C>The/DT student/NN knowingly/RB had/AUX a/DT firearm/NN within/IN 1000/CD feet/NNS from/IN the/DT school/NN ,/, </C>
<C>and/CC although/IN he/PRP consented/VBD to/TO the/DT search/NN of/IN his/PRP$ vehicle/NN ,/, </C>
<C>Author1/NNP feels/VBZ the/DT school/NN district/NN was/AUX within/IN its/PRP$ right/NN to/TO act/VB on/IN the/DT expulsion/NN ./. </C>
</S>
<S>
<C>He/PRP notes/VBZ the/DT report/NN also/RB indicates/VBZ there/EX were/AUX shotgun/JJ shells/NNS in/IN the/DT truck/NN which/WDT were/AUX found/VBN to/TO have/AUX been/AUX bird/NN shot/NN ./. </C>
</S>
<S>
<C>Author2/NNP feels/VBZ the/DT school/NN was/AUX wrong/JJ for/IN contacting/VBG the/DT police/NN to/TO search/VB the/DT young/JJ man/NN 's/POS truck/NN </C>
<C>because/IN it/PRP was/AUX parked/VBN on/IN a/DT public/JJ roadway/NN ,/, not/RB in/IN the/DT school/NN parking/NN lot/NN ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ the/DT DA/NN 's/POS decision/NN not/RB to/TO prosecute/VB the/DT young/JJ man/NN indicates/VBZ the/DT gun/NN was/AUX in/IN a/DT closed/JJ container/NN ;/: therefore/RB ,/, </C>
<C>indicating/VBG an/DT exception/NN to/TO the/DT law/NN regarding/VBG the/DT distance/NN limit/NN ./. </C>
</S>
<S>
<C>He/PRP disagrees/VBZ with/IN the/DT school/NN district/NN 's/POS decision/NN to/TO expel/VB the/DT young/JJ man/NN due/JJ to/TO the/DT fact/NN his/PRP$ vehicle/NN was/AUX parked/VBN on/IN a/DT public/JJ roadway/NN </C>
<C>and/CC there/EX was/AUX no/DT indication/NN of/IN illegal/JJ activity/NN ./. </C>
</S>
</P>
</T>
