<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP says/VBZ it/PRP 's/AUX just/RB a/DT purchase/NN of/IN citizens/NNS guns/NNS ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ it/PRP is/AUX a/DT forced/JJ sale/NN ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ not/RB all/DT guns/NNS are/AUX being/AUXG banned/VBN ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ that/DT is/AUX like/IN saying/VBG they/PRP did/AUX not/RB take/VB away/RP all/PDT the/DT Jews/NNPS ./. </C>
</S>
<S>
<C>He/PRP doubts/VBZ Australians/NNPS will/MD be/AUX able/JJ to/TO have/AUX any/DT guns/NNS next/JJ year/NN ./. </C>
</S>
<S>
<C>Author1/NNP points/VBZ out/RP the/DT differences/NNS between/IN Jews/NNPS and/CC guns/NNS ./. </C>
</S>
<S>
<C>He/PRP says/VBZ even/RB if/IN everyone/NN had/AUX guns/NNS ,/, </C>
<C>the/DT government/NN could/MD still/RB oppress/VB them/PRP ./. </C>
</S>
<S>
<C>Author2/NNP clarifies/VBZ he/PRP meant/VBD they/PRP took/VBD away/RP guns/NNS from/IN the/DT Jews/NNPS ./. </C>
</S>
<S>
<C>He/PRP argues/VBZ the/DT congress/NN of/IN 1775/CD was/AUX able/JJ to/TO stop/VB a/DT government/NN from/IN oppressing/VBG them/PRP ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ the/DT world/NN is/AUX more/RBR connected/JJ now/RB and/CC stable/JJ ,/, </C>
<C>and/CC overthrowing/VBG a/DT government/NN would/MD be/AUX hard/JJ ./. </C>
</S>
<S>
<C>Author2/NNP points/VBZ out/RP taking/VBG away/RP guns/NNS is/AUX oppression/NN ./. </C>
</S>
</P>
</T>
