<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP refers/VBZ to/TO previous/JJ posts/NNS about/IN carrying/VBG a/DT gun/NN ,/, and/CC the/DT lack/NN of/IN replies/NNS as/IN to/TO whether/IN it/PRP endangered/VBN anyone/NN ,/, increased/VBN crime/NN ,/, or/CC violated/VBN rights/NNS ./. </C>
</S>
<S>
<C>He/PRP asks/VBZ is/AUX it/PRP okay/JJ to/TO carry/VB a/DT gun/NN then/RB ./. </C>
</S>
<S>
<C>Author2/NNP asks/VBZ if/IN it/PRP is/AUX okay/JJ for/IN him/PRP to/TO own/VB a/DT nuclear/JJ weapon/NN ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ the/DT question/NN is/AUX irrelevant/JJ </C>
<C>since/IN nuclear/JJ weapons/NNS are/AUX not/RB for/IN sale/NN ./. </C>
</S>
<S>
<C>Author2/NNP states/VBZ </C>
<C>if/IN the/DT government/NN is/AUX allowed/VBN to/TO own/VB a/DT nuclear/JJ weapon/NN ,/, </C>
<C>he/PRP should/MD be/AUX too/RB ./. </C>
</S>
<S>
<C>If/IN they/PRP are/AUX banned/VBN ,/, </C>
<C>he/PRP should/MD not/RB be/AUX able/JJ to/TO own/VB one/NN ./. </C>
</S>
<S>
<C>Author1/NNP argues/VBZ there/EX is/AUX no/DT ban/NN </C>
<C>because/IN they/PRP are/AUX not/RB for/IN sale/NN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ there/EX is/AUX no/DT seller/NN who/WP would/MD sell/VB one/CD ./. </C>
</S>
<S>
<C>Author2/NNP argues/VBZ sellers/NNS would/MD sell/VB them/PRP </C>
<C>if/IN they/PRP were/AUX allowed/VBN to/TO ./. </C>
</S>
</P>
</T>
