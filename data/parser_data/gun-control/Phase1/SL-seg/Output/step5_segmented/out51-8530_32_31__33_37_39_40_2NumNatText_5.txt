<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP says/VBZ he/PRP hears/VBZ opinion/NN ,/, not/RB fact/NN ./. </C>
</S>
<S>
<C>The/DT police/NN acted/VBD properly/RB ./. </C>
</S>
<S>
<C>Running/VBG over/RP someone/NN is/AUX different/JJ 
<M>-/: trucks/NNS do/AUX not/RB only/RB shoot/VB </M>
./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ a/DT gun/NN must/MD hold/VB a/DT magazine/NN to/TO be/AUX loaded/VBN ./. </C>
</S>
<S>
<C>Author1/NNP replies/VBZ one/CD gun/NN was/AUX loaded/VBN ./. </C>
</S>
<S>
<C>He/PRP trusts/VBZ the/DT police/NN to/TO recognize/VB loaded/JJ guns/NNS ./. </C>
</S>
<S>
<C>Having/AUXG guns/NNS and/CC ammo/NN together/RB is/AUX itself/PRP illegal/JJ ,/, </C>
<C>and/CC any/DT gun/NN might/MD be/AUX loaded/VBN ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ cops/NNS make/VBP mistakes/NNS ./. </C>
</S>
<S>
<C>They/PRP misidentified/VBD a/DT knife/NN and/CC have/AUX let/VB people/NNS go/VB with/IN illegal/JJ weapons/NNS ./. </C>
</S>
<S>
<C>Having/AUXG guns/NNS and/CC ammo/NN is/AUX legal/JJ ./. </C>
</S>
<S>
<C>The/DT facts/NNS are/AUX not/RB known/VBN ./. </C>
</S>
<S>
<C>And/CC a/DT gun/NN without/IN a/DT magazine/NN could/MD hold/VB a/DT bullet/NN ./. </C>
</S>
<S>
<C>Author1/NNP asks/VBZ if/IN you/PRP can/MD have/AUX the/DT magazine/NN in/IN but/CC no/DT bullet/NN in/IN the/DT chamber/NN ,/, </C>
<C>like/IN he/PRP saw/VBD in/IN a/DT movie/NN ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ yes/UH ,/, </C>
<C>if/IN the/DT slide/NN is/AUX forward/RB ./. </C>
</S>
</P>
</T>
