<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP says/VBZ the/DT right/NN to/TO carry/VB a/DT gun/NN is/AUX not/RB a/DT natural/JJ right/NN ./. </C>
</S>
<S>
<C>It/PRP is/AUX a/DT granted/JJ right/NN ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ that/IN Author1/NNP is/AUX forgetting/VBG that/IN nobody/NN gave/VBD United/NNP States/NNP citizens/NNS rights/NNS ,/, </C>
<C>they/PRP had/AUX to/TO take/VB them/PRP ./. </C>
</S>
<S>
<C>Furthermore/RB ,/, the/DT Bill/NNP of/IN Rights/NNPS is/AUX a/DT statement/NN of/IN what/WP those/DT rights/NNS are/AUX ./. </C>
</S>
<S>
<C>Author2/NNP thinks/VBZ Author1/NNP has/AUX a/DT very/RB silly/JJ notion/NN of/IN the/DT law/NN ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ Author2/NNP believes/VBZ in/IN natural/JJ laws/NNS that/WDT are/AUX not/RB natural/JJ ,/, such/JJ as/IN laws/NNS allowing/VBG him/PRP to/TO defend/VB his/PRP$ state/NN being/AUXG used/VBN to/TO defend/VB himself/PRP from/IN a/DT mugger/NN ./. </C>
</S>
<S>
<C>Author2/NNP states/VBZ the/DT the/DT Bill/NN of/IN rights/NNS is/AUX not/RB a/DT contract/NN ,/, </C>
<C>and/CC the/DT rights/NNS in/IN the/DT Bill/NNP of/IN Rights/NNPS are/AUX natural/JJ rights/NNS ./. </C>
</S>
<S>
<C>Author1/NNP questions/VBZ how/WRB the/DT rights/NNS are/AUX natural/JJ if/IN they/PRP were/AUX taken/VBN ./. </C>
</S>
<S>
<C>Author2/NNP thinks/VBZ that/IN Author1/NNP is/AUX anti-American/JJ ./. </C>
</S>
</P>
</T>
