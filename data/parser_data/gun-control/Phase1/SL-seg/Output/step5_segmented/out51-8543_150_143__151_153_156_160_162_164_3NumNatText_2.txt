<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP believes/VBZ that/IN security/NN procedures/NNS like/IN the/DT ones/NNS that/WDT work/VBP at/IN airports/NNS could/MD protect/VB whole/JJ cities/NNS from/IN guns/NNS ./. </C>
</S>
<S>
<C>These/DT kinds/NNS of/IN procedures/NNS should/MD be/AUX legal/JJ for/IN cities/NNS too/RB ,/, </C>
<C>just/RB as/IN they/PRP are/AUX for/IN airports/NNS ./. </C>
</S>
<S>
<C>It/PRP is/AUX essential/JJ to/TO take/VB away/RP the/DT guns/NNS ./. </C>
</S>
<S>
<C>The/DT way/NN it/PRP is/AUX now/RB ,/, </C>
<C>criminals/NNS ,/, kids/NNS ,/, drug/NN addicts/NNS ,/, the/DT insane/JJ and/CC terrorists/NNS go/VBP armed/JJ ./. </C>
</S>
<S>
<C>That/DT is/AUX wrong/JJ ./. </C>
</S>
<S>
<C>By/IN using/VBG the/DT methods/NNS that/WDT work/VBP at/IN the/DT US/Mexican/NNP border/NN ,/, </C>
<C>it/PRP would/MD be/AUX possible/JJ to/TO keep/VB guns/NNS out/IN of/IN D.C./NNP ./. </C>
</S>
<S>
<C>Certainly/RB Gangsters/NNS would/MD evade/VB the/DT checkpoints/NNS </C>
<C>and/CC there/EX would/MD be/AUX other/JJ problems/NNS of/IN course/NN ./. </C>
</S>
<S>
<C>But/CC there/EX would/MD be/AUX many/RB fewer/JJR guns/NNS ,/, </C>
<C>as/IN seems/VBZ to/TO be/AUX the/DT case/NN in/IN Britain/NNP ./. </C>
</S>
<S>
<C>Author2/NNP believes/VBZ that/IN the/DT logistics/NNS would/MD make/VB these/DT checkpoints/NNS impossible/JJ ./. </C>
</S>
<S>
<C>Also/RB people/NNS might/MD rebel/NN ,/, </C>
<C>because/IN they/PRP only/RB put/VBD up/RP with/IN airport/NN security/NN procedures/NNS out/IN of/IN fear/NN of/IN terrorism/NN ./. </C>
</S>
<S>
<C>Lawyers/NNS would/MD have/AUX a/DT field/NN day/NN ./. </C>
</S>
<S>
<C>This/DT massive/JJ undertaking/NN would/MD not/RB prevent/VB all/DT gun/NN crime/NN ./. </C>
</S>
<S>
<C>For/IN example/NN ,/, there/EX would/MD be/AUX a/DT gun/NN black/JJ market/NN ./. </C>
</S>
<S>
<C>Alcohol/NNP Prohibition/NNP kept/VBD alcohol/NN out/RP ./. </C>
</S>
<S>
<C>Yet/CC it/PRP made/VBD people/NNS 's/POS lives/NNS worse/JJR ,/, not/RB better/JJR ./. </C>
</S>
</P>
</T>
