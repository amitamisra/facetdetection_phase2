<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP believes/VBZ the/DT long/JJ gun/NN registry/NN is/AUX not/RB for/IN him/PRP ./. </C>
</S>
<S>
<C>He/PRP knows/VBZ most/JJS guns/NNS used/VBN in/IN crimes/NNS are/AUX not/RB recovered/VBN ,/, and/CC quotes/VBZ statistics/NNS to/TO back/VB up/RP his/PRP$ position/NN ./. </C>
</S>
<S>
<C>Many/JJ are/AUX not/RB traceable/JJ except/IN through/IN ballistics/NNS ./. </C>
</S>
<S>
<C>Some/DT horrible/JJ Canadian/NNP gun/NN crimes/NNS involve/VBP guns/NNS smuggled/VBN from/IN the/DT United/NNP States/NNPS ./. </C>
</S>
<S>
<C>Most/JJS are/AUX not/RB traced/VBN ,/, </C>
<C>but/CC some/DT can/MD be/AUX </C>
<C>even/RB though/IN the/DT serial/JJ numbers/NNS are/AUX gone/VBN ./. </C>
</S>
<S>
<C>They/PRP may/MD not/RB have/AUX been/AUX traced/VBN with/IN ballistics/NNS ,/, </C>
<C>he/PRP does/AUX not/RB know/VB ./. </C>
</S>
<S>
<C>He/PRP says/VBZ he/PRP used/VBD the/DT wrong/JJ word/NN </C>
<C>when/WRB he/PRP said/VBD ballistics/NNS ./. </C>
</S>
<S>
<C>He/PRP meant/VBD the/DT authorities/NNS used/VBD forensics/NNS to/TO trace/VB guns/NNS ./. </C>
</S>
<S>
<C>Author2/NNP strongly/RB believes/VBZ the/DT long/JJ gun/NN registry/NN is/AUX useless/JJ ./. </C>
</S>
<S>
<C>He/PRP wonders/VBZ how/WRB the/DT authorities/NNS could/MD trace/VB guns/NNS without/IN serial/JJ numbers/NNS to/TO the/DT US/PRP ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ ballistics/NNS traces/NNS are/AUX impossible/JJ ./. </C>
</S>
<S>
<C>People/NNS only/RB think/VB it/PRP possible/JJ because/IN of/IN movies/NNS they/PRP have/AUX seen/VBN ./. </C>
</S>
<S>
<C>Ballistics/NNS cannot/VBP trace/VB guns/NNS ./. </C>
</S>
<S>
<C>Striations/NNS on/IN bullets/NNS do/AUX not/RB work/VB </C>
<C>because/IN the/DT gun/NN bores/VBZ that/IN make/VB the/DT striations/NNS change/VB shape/VB readily/RB ./. </C>
</S>
<S>
<C>Anyway/RB ,/, ballistics/NNS databases/NNS only/RB compare/VBP what/WP is/AUX in/IN the/DT database/NN ./. </C>
</S>
<S>
<C>New/JJ guns/NNS are/AUX not/RB ./. </C>
</S>
<S>
<C>Most/JJS guns/NNS are/AUX not/RB ./. </C>
</S>
<S>
<C>It/PRP is/AUX useless/JJ ./. </C>
</S>
</P>
</T>
