<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP asks/VBZ if/IN Author2/NNP is/AUX acknowledging/VBG that/IN we/PRP have/AUX no/DT idea/NN how/WRB many/JJ of/IN the/DT 29,000/CD are/AUX of/IN non-US/JJ manufacture/NN ,/, </C>
<C>and/CC asks/VBZ Author2/NNP to/TO read/VB slowly/RB ./. </C>
</S>
<S>
<C>Author2/NNP thinks/VBZ Author1/NNP should/MD take/VB his/PRP$ own/JJ advice/NN </C>
<C>and/CC they/PRP should/MD agree/VB that/IN the/DT Obama/NNP administrations/NNS lied/VBD ,/, and/CC continues/VBZ to/TO lie/VB ./. </C>
</S>
<S>
<C>He/PRP thinks/VBZ we/PRP need/AUX to/TO seal/VB the/DT border/NN for/IN various/JJ reasons/NNS ./. </C>
</S>
<S>
<C>Author1/NNP thinks/VBZ Obama/NNP 's/POS use/NN of/IN the/DT word/NN recovered/VBN was/AUX misleading/JJ bit/NN not/RB intentional/JJ ,/, and/CC therefore/RB ,/, not/RB a/DT lie/NN ./. </C>
</S>
<S>
<C>He/PRP would/MD end/VB border/NN control/NN ./. </C>
</S>
<S>
<C>Author2/NNP does/AUX not/RB think/VB Author1/NNP would/MD make/VB that/DT distinction/NN </C>
<C>if/IN it/PRP were/AUX Bush/NNP ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ someone/NN can/MD make/VB a/DT mistake/NN </C>
<C>without/IN intending/VBG </C>
<C>to/TO mislead/VB ./. </C>
</S>
<S>
<C>He/PRP says/VBZ the/DT ATF/NN tracing/VBG center/NN has/AUX reported/VBN that/IN 90/CD %/NN of/IN guns/NNS traced/VBN came/VBD from/IN various/JJ sources/NNS within/IN the/DT United/NNP States/NNPS ./. </C>
</S>
</P>
</T>
