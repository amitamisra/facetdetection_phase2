<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP says/VBZ it/PRP 's/AUX not/RB a/DT requirement/NN of/IN a/DT government/NN under/IN the/DT Constitution/NNP for/IN all/DT states/NNS to/TO have/AUX the/DT same/JJ laws/NNS ./. </C>
</S>
<S>
<C>Article/NNP IV/NNP section/NN 1/CD says/VBZ that/IN they/PRP have/AUX to/TO give/VB faith/NN and/CC credit/NN to/TO laws/NNS of/IN other/JJ states/NNS ,/, not/RB adopt/VB them/PRP as/IN their/PRP$ own/JJ ./. </C>
</S>
<S>
<C>The/DT government/NN is/AUX a/DT republic/NN limited/VBN in/IN power/NN ,/, bound/VBN by/IN the/DT Constitution/NNP ./. </C>
</S>
<S>
<C>He/PRP says/VBZ state/NN rights/NNS was/AUX one/CD of/IN many/JJ issues/NNS of/IN the/DT war/NN ./. </C>
</S>
<S>
<C>The/DT constitution/NN allowed/VBD slavery/NN ./. </C>
</S>
<S>
<C>The/DT Bill/NNP of/IN Rights/NNPS gave/VBD states/NNS free/JJ will/NN ./. </C>
</S>
<S>
<C>He/PRP admits/VBZ </C>
<C>after/IN the/DT war/NN states/NNS had/AUX less/JJR autonomy/NN ./. </C>
</S>
<S>
<C>He/PRP admits/VBZ Lincoln/NNP 's/POS constitutionally/RB debatable/JJ reason/NN for/IN war/NN was/AUX to/TO save/VB the/DT union/NN from/IN being/AUXG broken/VBN ./. </C>
</S>
<S>
<C>Author2/NNP questions/VBZ how/WRB Author1/NNP 's/POS stance/NN on/IN the/DT war/NN applies/VBZ to/TO the/DT rights/NNS he/PRP says/VBZ only/RB the/DT states/NNS have/AUX ./. </C>
</S>
<S>
<C>He/PRP says/VBZ Author1/NNP has/AUX the/DT same/JJ position/NN as/IN the/DT South/NNP ./. </C>
</S>
<S>
<C>He/PRP questions/VBZ </C>
<C>whether/IN the/DT states/NNS had/AUX free/JJ will/NN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ the/DT South/NNP wanted/VBD to/TO leave/VB the/DT union/NN and/CC keep/VB their/PRP$ slaves/NNS and/CC autonomy/NN ./. </C>
</S>
<S>
<C>He/PRP said/VBD the/DT North/NNP had/AUX slaves/NNS too/RB ./. </C>
</S>
<S>
<C>He/PRP ask/VB what/WP was/AUX Lincoln/NNP 's/POS real/JJ reason/NN for/IN the/DT war/NN ./. </C>
</S>
</P>
</T>
