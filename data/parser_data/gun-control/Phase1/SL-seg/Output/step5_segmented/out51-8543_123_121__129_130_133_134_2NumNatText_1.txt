<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP says/VBZ injuries/NNS are/AUX reduced/VBN by/IN regulation/NN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ no/DT one/NN said/VBN anything/NN about/IN if/IN guns/NNS did/AUX not/RB exist/VB except/IN Author2/NNP ./. </C>
</S>
<S>
<C>He/PRP accuses/VBZ Author2/NNP of/IN misstating/VBG what/WP other/JJ people/NNS say/VBP ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ saying/VBG if/IN something/NN did/AUX not/RB exist/VB is/AUX not/RB realistic/JJ ./. </C>
</S>
<S>
<C>Guns/NNS are/AUX well/RB made/VBN and/CC last/JJ a/DT long/JJ time/NN ./. </C>
</S>
<S>
<C>He/PRP ask/VB what/WP Author1/NNP means/VBZ by/IN ``/`` sufficient/JJ regulation/NN ''/'' ./. </C>
</S>
<S>
<C>He/PRP says/VBZ regulations/NNS have/AUX not/RB stopped/VBN school/NN shootings/NNS ,/, </C>
<C>the/DT only/JJ thing/NN that/WDT has/AUX is/AUX when/WRB the/DT shooter/NN is/AUX killed/VBN or/CC injured/VBN ./. </C>
</S>
<S>
<C>He/PRP was/AUX simply/RB pointing/VBG out/RP how/WRB untrue/JJ it/PRP would/MD be/AUX to/TO imagine/VB if/IN guns/NNS did/AUX not/RB exist/VB ./. </C>
</S>
<S>
<C>He/PRP says/VBZ Author1/NNP DID/AUX make/VB a/DT reference/NN to/TO guns/NNS not/RB existing/VBG </C>
<C>when/WRB he/PRP said/VBD if/IN it/PRP was/AUX like/IN in/IN Australia/NNP and/CC the/DT UK/NNP where/WRB the/DT killer/NN could/MD not/RB have/AUX bought/VBN a/DT gun/NN ,/, </C>
<C>the/DT 32/CD victims/NNS would/MD be/AUX safe/JJ and/CC alive/JJ ./. </C>
</S>
<S>
<C>He/PRP says/VBZ this/DT proves/VBZ Author2/NNP thinks/VBZ there/EX should/MD be/AUX a/DT total/JJ ban/NN on/IN handguns/NNS like/IN in/IN the/DT UK/NNP ./. </C>
</S>
<S>
<C>He/PRP says/VBZ that/DT would/MD only/RB be/AUX possible/JJ if/IN all/DT handguns/NNS were/AUX destroyed/VBN ./. </C>
</S>
<S>
<C>He/PRP says/VBZ the/DT system/NN failed/VBD </C>
<C>because/IN the/DT person/NN was/AUX not/RB entered/VBN in/IN the/DT system/NN ./. </C>
</S>
</P>
</T>
