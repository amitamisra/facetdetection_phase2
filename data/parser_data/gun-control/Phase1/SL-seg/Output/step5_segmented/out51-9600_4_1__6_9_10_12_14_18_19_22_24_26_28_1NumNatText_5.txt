<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP thinks/VBZ rival/JJ gangs/NNS are/AUX forced/VBN to/TO share/VB guns/NNS ,/, </C>
<C>analysis/NN suggests/VBZ guns/NNS are/AUX so/RB scarce/JJ in/IN the/DT UK/NNP that/IN they/PRP are/AUX rented/VBN out/RP to/TO both/DT sides/NNS ./. </C>
</S>
<S>
<C>Author2/NNP thinks/VBZ this/DT is/AUX an/DT admission/NN of/IN failure/NN ./. </C>
</S>
<S>
<C>He/PRP asks/VBZ if/IN it/PRP 's/AUX against/IN the/DT law/NN to/TO possess/VB ,/, borrow/VB or/CC loan/VB a/DT firearm/NN in/IN the/DT UK/NNP ./. </C>
</S>
<S>
<C>Author1/NNP says/VBZ that/DT 's/AUX his/PRP$ point/NN ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ it/PRP 's/AUX his/PRP$ ./. </C>
</S>
<S>
<C>Author1/NNP 's/POS was/AUX just/RB admitting/VBG failure/NN ./. </C>
</S>
<S>
<C>Author1/NNP thinks/VBZ if/IN a/DT gang/NN member/NN wants/VBZ to/TO shoot/VB someone/NN ,/, </C>
<C>having/AUXG to/TO rent/VB a/DT gun/NN is/AUX not/RB a/DT failure/NN ./. </C>
</S>
<S>
<C>If/IN so/RB ,/, you/PRP should/MD be/AUX equally/RB lampooning/JJ the/DT recent/JJ DC/NNP statistics/NNS thread/NN </C>
<C>because/IN newly/RB allowed/VBN guns/NNS have/AUX not/RB dropped/VBN the/DT crime/NN rate/NN to/TO 0/CD ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ it/PRP must/MD be/AUX a/DT handicap/NN to/TO go/VB next/JJ door/NN to/TO get/VB one/NN ./. </C>
</S>
<S>
<C>Author1/NNP thinks/VBZ no/DT law/NN is/AUX 100/CD %/NN successful/JJ ./. </C>
</S>
<S>
<C>Author2/NNP says/VBZ if/IN you/PRP turn/VBP on/IN your/PRP$ allies/NNS you/PRP can/MD not/RB trust/VB them/PRP ./. </C>
</S>
<S>
<C>Author1/NNP claims/VBZ an/DT increase/NN in/IN crime/NN due/JJ purely/RB to/TO criminal/JJ refugees/NNS should/MD not/RB be/AUX treated/VBN as/IN due/JJ to/TO 2A/CD ./. </C>
</S>
<S>
<C>Author2/NNP thinks/VBZ that/IN Author1/NNP will/MD blame/VB citizens/NNS and/CC the/DT availability/NN of/IN guns/NNS for/IN criminal/JJ actions/NNS ./. </C>
</S>
</P>
</T>
