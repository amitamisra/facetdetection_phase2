<T>
<P>
</P>
<P>
<S>
<C>Author1/NNP is/AUX in/IN support/NN of/IN people/NNS owning/VBG guns/NNS ./. </C>
</S>
<S>
<C>He/PRP believes/VBZ it/PRP is/AUX a/DT person/NN 's/POS right/NN to/TO be/AUX allowed/VBN to/TO protect/VB himself/herself/JJ when/WRB necessary/JJ ./. </C>
</S>
<S>
<C>He/PRP calls/VBZ </C>
<C>to/TO question/VB the/DT different/JJ crimes/NNS ,/, including/VBG gang/NN rapes/NNS of/IN teenage/JJ girls/NNS which/WDT have/AUX happened/VBN in/IN Australia/NNP ,/, </C>
<C>where/WRB Author2/NNP is/AUX from/IN ./. </C>
</S>
<S>
<C>Author1/NNP responds/VBZ to/TO Author2/NNP 's/POS mention/NN of/IN Jeffery/NNP Dahmer/NNP and/CC Ted/NNP Bundy/NNP advising/VBG these/DT men/NNS did/AUX not/RB use/VB guns/NNS in/IN their/PRP$ killings/NNS ./. </C>
</S>
<S>
<C>He/PRP cites/VBZ a/DT few/JJ other/JJ crimes/NNS from/IN Australia/NNP advising/VBG guns/NNS are/AUX not/RB the/DT issue/NN ,/, </C>
<C>but/CC instead/RB crime/NN is/AUX ./. </C>
</S>
<S>
<C>Author2/NNP feels/VBZ guns/NNS should/MD be/AUX more/RBR regulated/JJ and/CC admits/VBZ he/PRP looks/VBZ down/RP on/IN those/DT who/WP choose/VBP to/TO own/VB them/PRP ./. </C>
</S>
<S>
<C>Author2/NNP brings/VBZ attention/NN to/TO some/DT different/JJ catastrophic/JJ events/NNS which/WDT have/AUX occurred/VBN in/IN the/DT United/NNP States/NNPS such/JJ as/IN the/DT Uni-bomber/NNP and/CC the/DT D.C./NNP ./. </C>
</S>
<S>
<C>Sniper./NNP He/PRP also/RB mentions/VBZ both/DT Bundy/NNP and/CC Dahmer/NNP ,/, serial/JJ killers/NNS from/IN the/DT US/NNP ./. </C>
</S>
<S>
<C>Author2/NNP is/AUX offended/VBN by/IN Author1/NNP 's/POS mention/NN of/IN the/DT gang/NN rapes/NNS in/IN Australia/NNP </C>
<C>as/IN he/PRP feels/VBZ Author1/NNP is/AUX blaming/VBG this/DT activity/NN on/IN a/DT lack/NN of/IN guns/NNS in/IN public/JJ hands/NNS ./. </C>
</S>
</P>
</T>
