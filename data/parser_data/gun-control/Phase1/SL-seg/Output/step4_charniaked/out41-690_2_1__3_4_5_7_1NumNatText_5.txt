(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ refers) (PP (TO to) (NP (NP (JJ previous) (NNS posts)) (PP (IN about) (S (VP (VBG carrying) (NP (NP (DT a) (NN gun)) (, ,) (CC and) (NP (NP (DT the) (NN lack)) (PP (IN of) (NP (NP (NNS replies)) (PP (IN as) (PP (TO to) (SBAR (IN whether) (S (NP (PRP it)) (VP (VP (VBN endangered) (NP (NN anyone))) (, ,) (VP (VBN increased) (NP (NN crime))) (, ,) (CC or) (VP (VBN violated) (NP (NNS rights)))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ asks) (SBAR (S (VP (AUX is) (S (NP (PRP it)) (ADJP (JJ okay)) (S (VP (TO to) (VP (VB carry) (NP (DT a) (NN gun)) (ADVP (RB then)))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ asks) (SBAR (IN if) (S (NP (PRP it)) (VP (AUX is) (ADJP (ADJP (JJ okay)) (SBAR (IN for) (S (NP (PRP him)) (VP (TO to) (VP (VB own) (NP (DT a) (JJ nuclear) (NN weapon))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (DT the) (NN question)) (VP (AUX is) (ADJP (JJ irrelevant)) (SBAR (IN since) (S (NP (JJ nuclear) (NNS weapons)) (VP (AUX are) (RB not) (PP (IN for) (NP (NN sale)))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ states) (SBAR (S (SBAR (IN if) (S (NP (DT the) (NN government)) (VP (AUX is) (VP (VBN allowed) (S (VP (TO to) (VP (VB own) (NP (DT a) (JJ nuclear) (NN weapon))))))))) (, ,) (NP (PRP he)) (VP (MD should) (VP (AUX be) (ADVP (RB too))))))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (PRP they)) (VP (AUX are) (VP (VBN banned))))) (, ,) (NP (PRP he)) (VP (MD should) (RB not) (VP (AUX be) (ADJP (JJ able) (S (VP (TO to) (VP (VB own) (NP (NN one)))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ argues) (SBAR (S (NP (EX there)) (VP (AUX is) (NP (DT no) (NN ban)) (SBAR (IN because) (S (NP (PRP they)) (VP (AUX are) (RB not) (PP (IN for) (NP (NN sale)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (EX there)) (VP (AUX is) (NP (NP (DT no) (NN seller)) (SBAR (WHNP (WP who)) (S (VP (MD would) (VP (VB sell) (NP (CD one))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ argues) (SBAR (S (NP (NNS sellers)) (VP (MD would) (VP (VB sell) (NP (PRP them)) (SBAR (IN if) (S (NP (PRP they)) (VP (AUX were) (VP (VBN allowed) (S (VP (TO to)))))))))))) (. .)))

