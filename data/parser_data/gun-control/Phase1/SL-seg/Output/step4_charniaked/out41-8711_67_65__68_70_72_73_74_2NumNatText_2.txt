(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ feels) (SBAR (S (NP (NNP NATO)) (VP (AUX is) (ADVP (RB unfairly)) (VP (VBG labelling) (NP (NP (NN him/her)) (PP (IN in) (NP (NP (NN favor)) (PP (IN of) (NP (JJ strict) (NN gun) (NN control)))))) (PP (IN because) (IN of) (NP (NP (NNS statements)) (VP (ADVP (RB he/she)) (VBN made) (PP (VBG regarding) (S (VP (VBG photographing) (CC and) (VBG fingerprinting) (PP (IN during) (NP (NP (DT the) (NN sale)) (PP (IN of) (NP (NN firearms)))))))))))))))) (. .)))

(S1 (S (NP (DT This)) (VP (MD could) (VP (AUX be) (ADJP (JJ useful) (PP (IN in) (NP (NP (NN crime) (NNS investigations)) (, ,) (NP (NP (NNS cases)) (PP (IN of) (NP (VBN stolen) (NNS weapons)))) (, ,) (CC and) (NP (NN fraud))))))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX is) (NP (NP (NP (DT a) (NN right)) (PP (TO to) (NP (NP (NP (JJ own) (NN firearms)) (PP (IN for) (NP (NN protection)))) (CC and) (NP (JJ personal) (NN use))))) (, ,) (CC but) (RB not) (NP (DT a) (ADJP (RB wholly) (JJ unlimited)) (NN right)))) (. .)))

(S1 (S (NP (NNP NATO)) (VP (AUX is) (NP (NP (NN ignorant)) (PP (IN of) (NP (NP (NN legislation)) (CC and) (NP (NP (NNS labels)) (ADJP (NP (DT all) (NN gun) (NNS laws)) (JJ unconstitutional))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (AUX is) (NP (NP (DT a) (JJ third-party) (NN commentator)) (PP (IN in) (NP (DT this) (NN conversation))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (AUX is) (VP (AUXG being) (ADJP (JJ aggressive) (PP (IN towards) (NP (NNP NATO)))) (PP (IN in) (NP (PRP$ his) (NN response))))) (. .)))

(S1 (SQ (AUX Does) (NP (NNP Author1)) (ADVP (RB only)) (VP (VB believe) (SBAR (IN as) (S (NP (NN he/she)) (VP (AUX does) (SBAR (ADVP (RB simply)) (IN because) (S (NP (PRP it)) (VP (AUX 's) (UCP (NP (DT a) (NN law)) (, ,) (CC or) (SBAR (IN because) (S (NP (PRP it)) (VP (AUX 's) (NP (DT a) (JJ personal) (NN belief))))))))))))) (. ?)))

(S1 (S (NP (JJ Other) (JJ anti-gun) (NNS people)) (VP (VBP claim) (S (VP (TO to) (VP (VB support) (NP (DT the) (JJ 2nd) (NN Amendment)) (ADVP (RB also)))))) (. .)))

(S1 (S (NP (NNS Guns)) (VP (VB save) (NP (NNS lives)) (, ,) (SBAR (IN as) (S (VP (VBN indicated) (PP (IN by) (NP (NP (NN law) (NNS enforcements)) ('' ') (VP (VBG carrying) (NP (PRP them))))))))) (. .)))

