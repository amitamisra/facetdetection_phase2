(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ responds) (PP (TO to) (NP (NP (NN someone) (POS 's)) (NN comment))) (PP (IN by) (S (VP (VBG joking) (SBAR (IN that) (S (NP (PRP he)) (VP (MD will) (VP (VB post) (NP (PRP it)) (PP (IN at) (NP (NN work))) (SBAR (RB so) (IN that) (S (NP (NNS people)) (VP (MD can) (VP (VB laugh) (PP (IN at) (SBAR (WHADJP (WRB how) (JJ absurd)) (S (NP (PRP it)) (VP (AUX is))))))))))))))))) (. .)))

(S1 (S (NP (DT The) (NN comment)) (VP (AUX is) (VP (VBN directed) (PP (IN at) (NP (NP (DT another) (NN poster)) (PP (IN on) (NP (NP (DT the) (NN message) (NN board)) (, ,) (RB not) (NP (NNP Author2)))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (NNP Author2)) (VP (AUX is) (ADVP (RB just)) (VP (AUXG being) (UCP (ADJP (JJ disruptive)) (CC and) (VP (VBG spamming) (NP (DT the) (NN board)) (PP (IN by) (S (VP (VBG saying) (NP (DT the) (JJ same) (NN thing)) (ADVP (RB over) (CC and) (RB over)))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ repeats) (NP (DT the) (JJ same) (NN argument)) (NP (JJ several) (NNS times)) (, ,) (SBAR (IN that) (S (S (NP (NNP Author1)) (VP (VBZ claims) (S (VP (TO to) (VP (VB believe) (PP (IN in) (NP (DT the) (NN right) (S (VP (TO to) (VP (VB bear) (NP (NNS arms)))))))))))) (, ,) (CC but) (S (ADVP (RB also)) (VP (VBZ believes) (SBAR (S (NP (NNS guns)) (VP (MD should) (VP (AUX be) (VP (VBN regulated))))))))))) (. .)))

(S1 (S (NP (DT This)) (VP (VBZ makes) (NP (NP (NNP Author1)) (NP (DT a) (NN gun) (NN hater))) (SBAR (IN because) (S (NP (DT those) (CD two) (NNS ideas)) (VP (AUX are) (ADJP (JJ contradictory)))))) (. .)))

(S1 (S (NP (NN Infringement)) (VP (AUX is) (VP (VBN defined) (PP (IN as) (NP (NP (NN encroachment)) (PP (IN on) (NP (NP (DT a) (NN right)) (CC or) (NP (NN privilege)))))))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (NNP Author1)) (VP (VBZ believes) (PP (IN in) (NP (DT the) (NN right) (S (VP (TO to) (VP (VB bear) (NP (NNS arms)))))))))) (, ,) (NP (PRP he) (CC or) (PRP she)) (VP (MD can) (RB not) (ADVP (RB also)) (VP (VB believe) (PP (IN in) (NP (NP (NP (DT the) (NN government) (POS 's)) (NN infringement)) (PP (IN on) (NP (DT that) (NN right))))))) (. .)))

