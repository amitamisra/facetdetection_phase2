(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ pro-gun) (NNS enthusiasts)) (VP (AUX do) (RB not) (VP (AUX have) (NP (DT a) (NN plan) (S (VP (TO to) (VP (VB combat) (NP (NP (JJ illegal) (NNS sales)) (PP (IN of) (NP (NP (NNS guns)) (PP (TO to) (NP (JJ other) (NNS people))))))))))))))) (. .)))

(S1 (S (NP (NP (NNS People)) (SBAR (WHNP (WP who)) (S (VP (VBP own) (NP (NNS guns)))))) (VP (MD can) (ADVP (RB legally)) (VP (VB sell) (NP (PRP$ their) (JJ own) (NNS guns)) (PP (TO to) (NP (NP (NN anyone)) (PP (IN without) (S (VP (VBN needed) (S (VP (TO to) (VP (VB go) (PP (IN through) (NP (NP (NN background) (NNS checks)) (CC or) (NP (JJ other) (NNS measures)))))))))))))) (. .)))

(S1 (S (NP (DT This) (NN law)) (VP (MD can) (VP (VB allow) (S (NP (NNS people)) (VP (TO to) (VP (VP (VB purchase) (NP (NP (NNS guns)) (PP (IN from) (NP (JJ reputable) (NNS stores))))) (, ,) (CC and) (VP (ADVP (RB then)) (VP (VB turn) (ADVP (RB around))) (CC and) (VP (VB sell) (NP (DT these) (NNS weapons)) (PP (TO to) (NP (NP (NNS people)) (SBAR (WHNP (WP who)) (S (MD should) (RB not) (VP (VBZ posses) (NP (NP (NNS weapons)) (PRN (-LRB- -LRB-) (NP (NP (DT those)) (SBAR (WHNP (WP who)) (S (VP (VP (AUX are) (ADJP (RB too) (JJ young))) (, ,) (VP (AUX have) (NP (DT a) (NN record))) (, ,) (CC or) (VP (AUX are) (PP (IN in) (NP (NP (NN part)) (PP (IN of) (NP (DT a) (NN gang)))))))))) (-RRB- -RRB-))))))))))))))) (. .)))

(S1 (S (NP (NP (DT The) (NN lack)) (PP (IN of) (NP (NN background) (NNS checks))) (PP (IN for) (NP (NP (NNS buyers)) (PP (IN after) (NP (DT the) (JJ first) (CD one)))))) (VP (VBZ opens) (PRT (RP up)) (NP (DT a) (JJ large) (NN hole)) (PP (IN in) (NP (NP (DT the) (NN control)) (PP (IN of) (NP (NN gun) (NNS sales)))))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ legal) (S (VP (TO to) (VP (VB check) (NP (DT the) (JJ first) (NNS purchasers) (NN ability)) (PP (TO to) (NP (JJ own) (NNS guns)))))))))) (, ,) (ADVP (RB then)) (NP (PRP it)) (VP (MD should) (VP (AUX be) (ADJP (JJ legal) (S (VP (TO to) (VP (VB check) (NP (NP (DT the) (NNS buyers)) (PP (IN of) (NP (DT that) (NN gun)))) (PP (IN for) (NP (JJ subsequent) (NNS purchases))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ illegal) (NNS sales)) (VP (VB decrease) (SBAR (WHADVP (WRB when)) (S (NP (JJR more) (NNS people)) (VP (AUX have) (VP (NNS guns) (S (VP (TO to) (VP (VB start) (PP (IN with))))))))))))) (. .)))

(S1 (S (NP (JJS Most) (NNS guns)) (VP (AUX are) (RB not) (VP (VBN sold) (PP (IN at) (NP (NNS guns) (NNS shows))))) (. .)))

(S1 (S (NP (NNS People)) (VP (AUX do) (RB not) (ADVP (RB usually)) (VP (VB buy) (NP (NP (NNS guns)) (PP (IN via) (NP (NN proxy)))))) (. .)))

(S1 (S (NP (NNS People)) (VP (MD should) (RB not) (VP (AUX need) (NP (NN background) (NNS checks)) (S (VP (TO to) (VP (VB sell) (NP (DT a) (NN gun)) (ADVP (RB individually))))))) (. .)))

