(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ states) (SBAR (IN that) (S (S (NP (NP (JJS most) (NNS firearms)) (VP (VBN used) (PP (IN in) (NP (NNS crimes))))) (VP (AUX are) (RB not) (VP (VBN recovered)))) (, ,) (CC and) (S (NP (NP (QP (RB approximately) (CD 63)) (NN %)) (PP (IN of) (NP (NP (DT the) (JJ traceable) (NNS handguns)) (VP (VBN recovered) (PP (IN in) (NP (NNP Toronto))))))) (VP (VBP originated) (PP (IN in) (NP (DT the) (NNP US)))))))) (. .)))

(S1 (S (S (NP (NP (DT A) (JJ large) (NN proportion)) (PP (IN of) (NP (NP (DT the) (NNS firearms)) (VP (VBN recovered) (PP (IN in) (NP (NNS crimes))))))) (VP (AUX have) (VP (AUX had) (NP (PRP$ their) (JJ serial) (NNS numbers) (VBN removed))))) (, ,) (CC but) (S (NP (NNP Author1)) (VP (VBZ wonders) (SBAR (WHADVP (WRB how)) (S (NP (PRP it)) (VP (AUX was) (VP (VBN determined) (SBAR (IN that) (S (NP (PRP they)) (VP (VBP originated) (SBAR (S (S (PP (IN in) (NP (DT the) (NNP U.S.))) (NP (DT The) (JJ serial) (NNS numbers)) (VP (AUX were) (VP (VBN removed)))) (, ,) (CC but) (S (NP (NNP Author2)) (VP (VBZ tells) (NP (NNP Author1)) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX were) (RB not) (NP (NP (NN part)) (PP (IN of) (NP (DT the) (JJ traceable) (NN group)))) (SBAR (IN unless) (S (NP (PRP it)) (VP (AUX was) (PP (IN through) (NP (NNS ballistics)))))))))))))))))))))) (. .)))

(S1 (S (S (NP (NNP Author2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (NNS ballistics) (NNS traces)) (VP (AUX are) (RB not) (ADJP (JJ possible))))))) (, ,) (CC and) (S (NP (PRP he)) (VP (VBZ ponders) (NP (NP (DT the) (NN usefulness)) (PP (IN of) (NP (DT a) (JJ long) (NN gun) (NN registry)))))) (. .)))

(S1 (S (S (NP (NNP Author1)) (VP (VBZ outlines) (NP (DT an) (NN article) (SBAR (IN that) (S (VP (NNS details) (NP (NP (DT a) (NN case)) (SBAR (WHADVP (WRB where)) (S (NP (NNP Ottawa) (NNS police)) (VP (VBD traced) (NP (DT a) (NN gun)))))))))))) (, ,) (CC and) (S (NP (PRP he)) (VP (VBZ states) (SBAR (IN that) (S (NP (DT a) (NN gun)) (VP (MD can) (VP (AUX be) (VP (VBN traced) (PP (IN without) (NP (DT the) (NN serial) (NNS numbers)))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ admits) (SBAR (IN that) (S (NP (PRP it)) (VP (MD may) (RB not) (VP (AUX have) (VP (AUX been) (NP (NP (NNS ballistics)) (, ,) (SBAR (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB support) (NP (DT the) (JJ long) (NN gun) (NN registry))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ tells) (NP (NNP Author1)) (SBAR (IN that) (S (NP (DT the) (NN article)) (VP (AUX did) (RB not) (VP (VB state) (SBAR (SBAR (IN that) (S (NP (NNS ballistics) (NNS databases)) (VP (VBD determined) (NP (NN anything))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NNS ballistics) (NNS databases)) (VP (VBP match) (NP (NNS rounds)) (PP (TO to) (NP (NP (DT the) (NN gun)) (SBAR (S (NP (PRP they)) (VP (VBD came) (PP (IN from)))))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ mentions) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX was) (VP (VBG referring) (PP (TO to) (NP (NNS forensics) (CONJP (RB instead) (IN of)) (NNS ballistics)))))))) (. .)))

