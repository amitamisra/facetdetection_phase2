(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (NP (JJS most) (NNS guns)) (VP (VBN used) (PP (IN for) (NP (NNS crimes))))) (VP (AUX are) (RB not) (VP (VBN recovered)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (PP (IN in) (NP (NNP Toronto))) (, ,) (NP (NP (CD 63) (NN %)) (PP (IN of) (NP (DT the) (NNS guns)))) (VP (VBD came) (PP (IN from) (NP (NNP America))))))) (. .)))

(S1 (S (S (NP (QP (IN Over) (CD 2600)) (NNS guns)) (VP (AUX were) (VP (VBN recovered) (PP (IN by) (NP (NP (NN police)) (PP (IN in) (NP (NNP Canada)))))))) (, ,) (CC and) (S (NP (CD 925)) (VP (VBD came) (PP (IN from) (NP (NP (DT a) (NN dealer)) (, ,) (SBAR (WHPP (IN of) (WHNP (WDT which))) (S (NP (CD 3/4)) (VP (VBD came) (PP (IN from) (NP (DT the) (NNP USA)))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ questions) (SBAR (WHADVP (WRB how)) (S (SBAR (IN if) (S (NP (DT the) (JJ serial) (NNS numbers)) (VP (AUX were) (VP (VBN removed))))) (, ,) (NP (PRP they)) (VP (AUX were) (ADJP (JJ sure) (SBAR (S (NP (DT those) (NNS guns)) (VP (AUX had) (VP (VBN come) (PP (IN from) (NP (DT the) (NNP USA)))))))))))) (. .)))

(S1 (S (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (PRP they)) (VP (AUX were) (PP (IN through) (NP (NNS ballistics)))))))) (, ,) (CC but) (S (NP (NNP Author2)) (VP (VBZ argues) (SBAR (S (NP (PRP it)) (VP (AUX is) (ADJP (JJ impossible) (S (VP (TO to) (VP (VB trace) (NP (NNS guns)))))) (NP (DT that) (NN way))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ questions) (SBAR (WHNP (WP what) (JJ good)) (S (VP (AUX is) (NP (DT the) (JJ long) (NN gun) (NN registry)))))) (. .)))

(S1 (NP (NP (CD Author1) (NNS details)) (NP (NP (DT a) (NN case)) (SBAR (WHPP (IN in) (WHNP (WDT which))) (S (NP (DT a) (NN gun)) (VP (AUX was) (VP (VBN used) (PP (IN in) (NP (DT a) (NN crime)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (SBAR (IN although) (S (NP (DT the) (JJ serial) (NNS numbers)) (VP (AUX were) (VP (VBG missing))))) (, ,) (NP (PRP they)) (VP (AUX were) (ADJP (JJ able) (S (VP (TO to) (VP (VB trace) (NP (DT the) (NN gun)) (PP (TO to) (NP (NP (DT a) (NN shop)) (PP (IN in) (NP (NNP Maine))))))))))))) (. .)))

(S1 (S (NP (DT A) (NN person)) (VP (VP (VBD bought) (NP (CD eight) (NNS guns))) (CC and) (VP (VBD sold) (NP (PRP them)) (PP (TO to) (NP (NP (DT a) (NNP Canadian)) (SBAR (WHNP (WP who)) (S (VP (VBD smuggled) (NP (PRP them))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBD said) (SBAR (S (NP (DT this)) (VP (VBD showed) (SBAR (S (NP (NNS guns)) (VP (MD could) (ADVP (RB still)) (VP (AUX be) (VP (VBN traced)))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (ADVP (RB still)) (VP (VBZ questions) (SBAR (WHADVP (WRB how)) (S (NP (NNS ballistics)) (VP (MD would) (VP (VB help) (VP (VB identify) (NP (DT the) (NNS guns)))))))) (. .)))

(S1 (S (S (NP (PRP He)) (VP (VBZ points) (PRT (RP out)) (NP (NP (DT the) (NNS specifics)) (PP (IN of) (NP (DT this)))))) (, ,) (CC and) (S (NP (NNP Author1)) (VP (VBZ admits) (SBAR (S (NP (PRP he)) (VP (MD should) (VP (AUX have) (VP (VBN said) (S (NP (NNS forensics) (CONJP (RB instead) (IN of)) (NNS ballistics)))))))))) (. .)))

