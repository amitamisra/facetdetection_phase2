(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (DT the) (NN government)) (VP (AUX is) (VP (VBN held) (PRT (RP back)) (PP (IN by) (NP (DT the) (NNP Constitution))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (PRP we)) (VP (AUX have) (VP (VBN set) (PRT (RP up)) (NP (NP (NNS limits)) (PP (IN on) (NP (PRP$ their) (NN power)))))))))) (. .)))

(S1 (S (SBAR (RB Even) (IN as) (S (NP (PRP we)) (VP (AUX are) (S (VP (VBG letting) (S (NP (PRP them)) (VP (VB break) (NP (DT those) (NNS limits)) (ADVP (RB relatively) (RB often))))))))) (, ,) (NP (NP (DT the) (JJ second) (NN amendment)) (CC and) (NP (NP (DT the) (NN right)) (PP (TO to) (NP (JJ own) (NNS guns))))) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN controlled) (ADVP (RB so) (RB heavily))))) (. .)))

(S1 (S (NP (JJ Many) (NNS states)) (ADVP (RB also)) (VP (AUX have) (NP (NP (PRP$ their) (JJ own) (NNS Bills)) (PP (IN of) (NP (NNPS Rights)))) (, ,) (PP (IN with) (S (NP (NP (DT a) (JJ vast) (NN majority)) (PP (IN of) (NP (PRP them)))) (ADVP (RB also)) (VP (VBG allowing) (NP (NN gun) (NN ownership)))))) (. .)))

(S1 (S (NP (NP (DT The) (NN government) (POS 's)) (NNS powers)) (VP (MD should) (VP (AUX be) (VP (VBN limited)))) (. .)))

(S1 (S (NP (NP (DT The) (NN power)) (PP (IN of) (NP (DT the) (NNS people))) (PP (TO to) (NP (JJ own) (NNS guns)))) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN limited)))) (. .)))

(S1 (S (NP (NP (NN Government) (NN regulation)) (PP (IN of) (NP (NNS guns)))) (VP (MD should) (ADVP (RB only)) (VP (AUX be) (VP (`` ``) (VBN regulated) ('' '') (VP (MD should) (VP (VB mean) (SBAR (IN that) (S (NP (NP (DT the) (NNS people) (POS 's)) (NNS powers)) (VP (AUX are) (VP (VBN maintained))))))) (, ,) (RB not) (VP (VBN reduced) (ADVP (RB further)))))) (. .)))

(S1 (S (S (NP (NNP Author2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (DT the) (NNP Constitution)) (VP (AUX does) (RB not) (VP (VB protect) (NP (NNS people)) (ADVP (RB equally)) (SBAR (IN until) (S (NP (DT the) (JJ 14th) (NN amendment)) (VP (AUX was) (VP (VBN passed))))))))))) (, ,) (CC and) (S (ADVP (RB even) (RB then)) (NP (DT the) (NN government)) (VP (VBZ gives) (NP (PRP itself)) (NP (JJ certain) (NNS exceptions)) (S (VP (TO to) (VP (VB get) (ADVP (RB out) (PP (IN of) (NP (PRP$ its) (NN regulation))))))))) (. .)))

(S1 (S (NP (DT The) (NNP Constitution)) (VP (ADVP (RB only)) (VBN applied) (PP (TO to) (NP (DT the) (JJ Federal) (NN government))) (, ,) (PP (RB not) (PP (TO to) (NP (DT the) (NNS states))))) (. .)))

(S1 (S (NP (NP (DT The) (NNP Constitution)) (CC and) (NP (NP (DT the) (NNP Bill)) (PP (IN of) (NP (NNPS Rights))))) (ADVP (RB only)) (VP (VBD applied) (PP (TO to) (NP (DT the) (NNS states))) (PP (IN after) (NP (DT the) (JJ 14th) (NN amendment)))) (. .)))

(S1 (S (NP (JJ Individual) (NNS states) (NNS constitutions)) (VP (AUX are) (RB not) (NP (NP (DT the) (JJ same)) (PP (IN as) (NP (NP (DT the) (NNP Federal) (NN Government) (POS 's)) (NNP Constitution))))) (. .)))

