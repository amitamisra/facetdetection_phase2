(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNP Author2)) (VP (AUX does) (RB not) (VP (AUX have) (NP (NP (DT an) (NN idea)) (SBAR (WHADVP (WRB where)) (S (NP (NP (DT a) (NN figure)) (SBAR (S (NP (PRP they)) (VP (VBD used))))) (VP (VBD came) (PP (IN from))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VP (VBZ calls) (S (NP (NNP Obama)) (NP (DT a) (NN liar)))) (, ,) (CC and) (VP (VBZ says) (SBAR (IN that) (S (NP (PRP we)) (VP (AUX need) (S (VP (TO to) (VP (VB seal) (NP (PRP$ our) (NNS borders)))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VP (VBZ believes) (SBAR (S (NP (NNP Obama)) (VP (AUX was) (ADJP (ADJP (RB intentionally) (JJ misleading)) (CC but) (RB not) (VBG lying)))))) (CC and) (VP (VBZ wants) (S (VP (TO to) (VP (VB open) (NP (DT the) (NN border))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNP Author1)) (VP (MD would) (RB not) (VP (VB make) (NP (NP (DT the) (JJ same) (NN argument)) (PP (IN for) (NP (NNP Bush))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (EX there)) (VP (AUX is) (NP (NP (DT a) (NN difference)) (PP (IN between) (S (VP (VBG misspeaking) (CC and) (VBG misleading))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VP (VBZ tells) (S (NP (NNP Author1)) (VP (TO to) (VP (VB heed) (NP (PRP$ their) (JJ own) (NN advice)))))) (CC and) (VP (VBZ asks) (S (NP (NNP Author1)) (VP (TO to) (VP (VB agree) (SBAR (IN that) (S (NP (NP (NNP Obama)) (CC and) (NP (PRP$ his) (NN administration))) (VP (AUX are) (NP (NNS liars)))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (IN that) (S (NP (NP (DT a) (ADJP (CD 90) (NN percent)) (NN figure)) (SBAR (S (VP (VBD came) (PP (IN from) (NP (DT the) (NNP DEA))))))) (VP (AUX was) (PP (IN for) (NP (NP (NNS guns)) (SBAR (WHNP (WDT that)) (S (VP (MD could) (VP (AUX be) (VP (VBN traced)))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (ADVP (RB also)) (VP (VBZ says) (SBAR (SBAR (IN that) (S (NP (NNS firearms)) (VP (AUX are) (VP (AUXG being) (VP (VBN transported) (PP (IN from) (NP (DT the) (NNP US))) (PP (IN into) (NP (NNP Mexico)))))))) (CC and) (SBAR (IN that) (S (NP (NP (CD 90) (NN %)) (PP (IN of) (NP (NNS weapons)))) (VP (AUX were) (NP (NNP American))))))) (. .)))

