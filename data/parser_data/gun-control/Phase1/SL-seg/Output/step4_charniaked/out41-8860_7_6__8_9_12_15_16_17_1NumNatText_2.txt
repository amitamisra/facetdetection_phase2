(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (JJ firearms) (NNS inspections)) (VP (AUX are) (ADVP (RB only)) (VP (VBN allowed) (S (VP (TO to) (VP (AUX be) (VP (VBN investigated) (PP (IN at) (NP (NP (DT any) (NN time)) (, ,) (SBAR (WHADVP (RB only) (WRB when)) (S (NP (DT the) (NN gun)) (VP (AUX is) (VP (VBN involved) (PP (IN in) (NP (DT a) (JJ criminal) (NN investigation)))))))))))))))))) (. .)))

(S1 (S (NP (NNS Guns)) (VP (AUX are) (RB not) (VP (VBN allowed) (S (VP (TO to) (VP (AUX be) (VP (VBN requested) (S (VP (TO to) (VP (AUX be) (VP (VBD searched) (PP (IN at) (NP (NP (DT any) (NN time)) (SBAR (WHADVP (WRB when)) (S (NP (PRP they)) (VP (AUX are) (RB not) (ADJP (VBN related) (PP (TO to) (NP (DT a) (JJ criminal) (NN investigation))))))))))))))))))) (. .)))

(S1 (S (SBAR (WHADVP (WRB When)) (S (RB not) (VP (VBN related) (PP (TO to) (NP (DT a) (JJ criminal) (NN investigation)))))) (, ,) (NP (NNS searches)) (VP (AUX are) (ADVP (RB only)) (ADJP (JJ legal)) (ADVP (RB not) (RBR more) (IN than) (RB once)) (PP (IN in) (NP (DT any) (ADJP (CD 12) (NN month)) (NN period)))) (. .)))

(S1 (S (NP (DT The) (JJ only) (NN exception)) (VP (AUX is) (SBAR (IN that) (S (NP (NNS records)) (VP (MD can) (VP (AUX be) (VP (VBD searched) (NP (NP (CD three) (NNS times)) (NP (DT each) (NN year))))))))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (PRP they)) (VP (AUX are) (VP (VBG investigating) (NP (NP (DT a) (NN firearm)) (PP (IN in) (NP (NP (DT a) (NN crime)) (SBAR (WHNP (WDT that)) (S (VP (VBZ belongs) (PP (TO to) (NP (DT a) (NN person))))))))))))) (, ,) (S (NP (PRP they)) (VP (MD can) (ADVP (RB still)) (VP (VB investigate) (NP (NN someone))))) (, ,) (CC but) (S (NP (EX there)) (VP (AUX is) (NP (DT a) (NN likelihood) (SBAR (IN that) (S (NP (PRP it)) (VP (MD would) (RB not) (VP (AUX be) (PP (IN in) (NP (NP (DT the) (NN middle)) (PP (IN of) (NP (DT the) (NN night)))))))))))) (. .)))

(S1 (S (S (NP (NNP Author2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (PRP they)) (VP (VBD added) (NP (NP (CD two) (JJ additional) (NNS chances)) (PP (IN for) (NP (JJ legal) (JJ unannounced) (NNS searches)))) (PP (IN in) (NP (DT each) (ADJP (CD 12) (NN month)) (NN period)))))))) (, ,) (CC and) (S (NP (PRP they)) (ADVP (RB also)) (VP (AUX are) (ADVP (RB now)) (VP (VBN allowed) (S (VP (TO to) (VP (VB search) (PP (IN at) (NP (NP (DT any) (NN time)) (PP (IN in) (NP (NP (NN regard)) (PP (TO to) (NP (NP (DT a) (NN firearm)) (PP (IN in) (NP (DT a) (JJ criminal) (NN investigation))))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (MD will) (RB not) (VP (VB announce) (NP (PRP$ their) (NNS searches)))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX is) (NP (NP (JJ fair) (NN game)) (PP (IN for) (NP (DT any) (NNS searches)))) (SBAR (IN because) (S (NP (PRP it)) (VP (AUX is) (RB not) (VP (VBN stated) (ADVP (RB otherwise))))))) (. .)))

