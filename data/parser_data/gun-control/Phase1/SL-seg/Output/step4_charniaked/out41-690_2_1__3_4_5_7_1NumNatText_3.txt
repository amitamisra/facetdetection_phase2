(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBD received) (NP (DT no) (NNS posts)) (PP (IN on) (NP (NP (NNS threads)) (PP (IN about) (NP (NP (NN gun)) (VP (VP (VBG carrying) (NP (VBG endangering) (NNS people))) (, ,) (VP (VBG increasing) (NP (NN crime))) (CC or) (VP (VBG violating) (NP (NNS rights))))))))) (. .)))

(S1 (S (RB So) (NP (PRP he)) (VP (VBZ asks) (SBAR (RB rhetorically) (IN if) (S (NP (PRP it)) (VP (AUX 's) (ADJP (JJ okay)) (S (VP (TO to) (VP (VB carry) (NP (NNS guns))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ answers) (SBAR (S (NP (PRP it)) (VP (AUX 's) (ADJP (ADJP (RB as) (JJ okay)) (PP (IN as) (NP (NP (DT a) (NN country)) (VP (VBG storing) (NP (NP (DT a) (JJ nuclear) (NN weapon)) (PP (IN in) (NP (DT a) (JJ foreign) (NN city)))))))))))) (. .)))

(S1 (S (S (VP (VBG Carrying) (NP (NNS guns)))) (VP (AUX is) (ADVP (RB inherently)) (ADJP (JJ dangerous))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ replies) (SBAR (IN that) (S (NP (NNS nukes)) (VP (AUX are) (VP (ADVP (RB effectively)) (VBN banned) (, ,) (S (VP (VBG invalidating) (NP (DT the) (NN comparison))))))))) (. .)))

(S1 (S (NP (PRP$ His) (JJ unanswered) (NNS posts)) (VP (VBP prove) (SBAR (S (NP (NNS guns)) (VP (MD need) (RB n't) (VP (AUX be) (VP (VBN banned))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (DT no) (NN citizen)) (VP (MD should) (VP (AUX have) (NP (NNS nukes))))))) (. .)))

(S1 (S (CC And) (NP (NNS guns)) (VP (AUX are) (ADJP (RB similarly) (JJ dangerous))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (NN nobody)) (VP (VBZ sells) (NP (NNS individuals) (NNS nukes)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (VBZ talks) (PP (PP (IN about) (NP (NP (NNS guns)) (PP (IN with) (NP (NP (NN respect)) (PP (TO to) (NP (NP (PRP$ his) (NN opponent) (POS 's)) (NN argument))))))) (: ;) (PP (IN about) (NP (JJ nuclear) (NNS weapons)))) (PP (IN with) (NP (NP (NN respect)) (PP (TO to) (NP (PRP$ his) (JJ own))))))))) (. .)))

(S1 (S (NP (NNS Makers)) (VP (MD would) (VP (VB sell) (NP (NNS nukes)) (, ,) (SBAR (IN if) (S (VP (VBN allowed)))) (, ,) (PP (IN for) (NP (NN profit) (CC and) (NN power))))) (. .)))

(S1 (SBARQ (RB So) (PRN (, ,) (S (NP (PRP he)) (VP (VBZ asks) (ADVP (RB rhetorically)))) (, ,)) (SQ (MD should) (RB not) (NP (NNS people)) (VP (AUX be) (VP (VBN allowed) (S (VP (TO to) (VP (VB buy) (NP (PRP them)))))))) (. ?)))

