(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (SBAR (NP (CD Author1) (NNS states)) (IN since) (S (NP (NNS guns)) (VP (AUX are) (VP (VBN banned))))) (, ,) (NP (NN confiscation) (CC or) (NN buy) (NNS backs)) (VP (MD will) (VP (VB happen))) (. .)))

(S1 (S (S (NP (NNS Author2)) (VP (VB ask) (FRAG (WHADVP (WRB why))))) (, ,) (CC and) (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (DT that)) (VP (AUX is) (SBAR (WHADVP (WRB how)) (S (NP (NP (DT the) (NNP UK)) (, ,) (NP (NNP Australia)) (, ,) (CC and) (NP (NNP Canada))) (VP (AUX did) (NP (PRP it)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (DT this)) (VP (AUX is) (NP (DT the) (JJ only) (NN way) (SBAR (IN for) (S (NP (NNS police)) (VP (TO to) (VP (VB get) (ADJP (JJ rid) (PP (IN of) (NP (NP (NNS guns)) (PP (IN on) (NP (DT the) (NNS streets))))))))))))))) (. .)))

(S1 (S (S (NP (CD One)) (VP (AUX is) (VP (VBG creating) (NP (DT a) (NN police) (NN state))))) (CC and) (S (NP (CD one)) (VP (AUX is) (VP (VBG burning) (NP (NN money)) (, ,) (SBAR (IN since) (S (NP (DT the) (NNS guns)) (VP (AUX are) (VP (VBN melted) (PRT (RP down))))))))) (. .)))

(S1 (S (S (NP (NNP Author2)) (VP (VBZ lives) (PP (IN in) (NP (NNP Canada))))) (, ,) (CC and) (S (VP (VBZ disagrees) (PP (IN with) (NP (DT this))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (PRP you)) (VP (VP (MD can) (VP (VB keep) (NP (PRP$ your) (JJ old) (NNS guns)))) (CC and) (VP (ADVP (RB just)) (MD can) (RB not) (VP (VB buy) (NP (JJ new) (NNS handguns)))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ agrees) (SBAR (S (S (NP (NNP Canada)) (VP (AUX is) (VP (VBG buying) (NP (NNS guns))))) (, ,) (CC but) (S (NP (PRP it)) (VP (AUX is) (RB not) (ADJP (JJ voluntary))))))) (. .)))

(S1 (S (NP (PRP They)) (ADVP (RB only)) (VP (VBP pay) (NP ($ $) (CD 50)) (ADVP (RB regardless) (PP (IN of) (NP (NN value))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (S (NP (NP (DT a) (JJ low) (NN number)) (PP (IN of) (NP (NNS owners)))) (VP (AUX have) (VP (VBN participated)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ gives) (NP (NP (DT an) (NN example)) (PP (IN of) (NP (NP (DT a) (NN boy)) (SBAR (WHNP (WP who)) (S (VP (VP (VBD sold) (NP (PRP$ his) (ADJP ($ $) (CD 900)) (NN gun))) (CC and) (VP (VB ask) (SBAR (IN if) (S (NP (DT the) (NN boy)) (VP (VBD felt) (VP (VBN forced) (S (VP (TO to) (VP (VB sell)))))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP ($ $) (CD 50)) (VP (AUX is) (PP (IN above) (NP (NP (DT the) (NN value)) (PP (IN of) (NP (DT the) (NN gun))))) (ADVP (RB now)))))) (. .)))

(S1 (S (SBAR (IN Since) (S (NP (DT the) (NN gun)) (VP (MD can) (RB not) (VP (AUX be) (ADVP (RB legally)) (VP (VBN sold) (PP (TO to) (NP (NP (NN someone)) (ADJP (RB else))))))))) (, ,) (NP (NP (PRP it) (POS 's)) (NN value)) (VP (AUX is) (NP (CD zero))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ insists) (SBAR (S (NP (DT the) (NN boy)) (VP (AUX was) (ADJP (JJ cheated)) (PP (IN out) (PP (IN of) (NP (NN property) (NN value)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ calls) (S (NP (DT this) (NN government) (NN theft)))) (. .)))

