(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NNS deaths)) (VP (MD can) (VP (AUX be) (VP (VBN reduced) (PP (IN by) (NP (NP (NN regulation)) (PP (IN of) (NP (NNS guns))))))))))) (. .)))

(S1 (S (NP (NNS Guns)) (VP (AUX do) (RB not) (VP (VBN become) (ADJP (RBR less) (JJ lethal)) (SBAR (WHADVP (WRB when)) (S (NP (PRP they)) (VP (AUX are) (RB not) (VP (VBN made) (ADVP (ADVP (RB as) (RB well)) (PP (IN as) (NP (JJ other) (NNS guns)))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ argues) (SBAR (SBAR (IN that) (S (NP (NP (NNS regulations)) (PP (IN of) (NP (NNS guns)))) (VP (MD will) (VP (VB decrease) (NP (NN violence)))))) (CC and) (SBAR (IN that) (S (NP (JJR more) (NN gun) (NN regulation)) (VP (AUX is) (ADJP (JJ necessary))))))) (. .)))

(S1 (S (NP (NN School) (NNS shootings)) (VP (MD would) (VP (VB happen) (ADVP (RBR less) (RB often)) (SBAR (IN if) (S (NP (EX there)) (VP (AUX were) (NP (NP (JJ sufficient) (NN regulation)) (PP (IN of) (NP (NNS guns))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NNS guns)) (VP (MD will) (ADVP (RB always)) (VP (VB exist) (, ,) (SBAR (IN because) (S (S (NP (JJ old) (NNS guns)) (VP (AUX are) (ADVP (RB still)) (ADVP (RB around)))) (CC and) (S (VP (VB work) (ADJP (RB just) (JJ fine))))))))))) (. .)))

(S1 (S (NP (JJ Modern) (NNS guns)) (VP (VP (AUX are) (VP (VBN made) (PP (IN of) (NP (JJR higher) (NN quality) (NNS materials))))) (CC and) (VP (MD will) (ADVP (RB still)) (VP (AUX be) (NP (NP (QP (RB around) (JJ many) (JJR more)) (NNS years)) (PP (IN from) (NP (RB now))))))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX is) (ADJP (JJ impossible) (S (VP (TO to) (VP (VB remove) (NP (DT all) (NNS guns)) (PP (IN from) (NP (NN society)))))))) (. .)))

(S1 (S (NP (NN Gun) (NN regulation)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN increased)))) (. .)))

(S1 (S (S (NP (NN Gun) (NN violence)) (VP (AUX has) (RB not) (VP (VBN decreased) (ADJP (JJ due) (PP (TO to) (NP (NNS regulations))))))) (, ,) (CC but) (S (ADVP (RB rather)) (NP (NNS guns)) (VP (AUX have) (VP (VBN helped) (S (VP (TO to) (VP (VB end) (NP (NN school) (NNS shootings)) (SBAR (IN while) (S (NP (PRP they)) (VP (AUX are) (PP (IN in) (NP (NN progress)))))))))))) (. .)))

(S1 (S (SBAR (IN Since) (S (NP (NNS guns)) (VP (AUX do) (VP (VB exist))))) (, ,) (NP (PRP we)) (VP (MD should) (RB not) (VP (AUX be) (ADJP (JJ worried) (PP (IN about) (NP (NP (JJ hypothetical) (NNS situations)) (SBAR (WHNP (WDT which)) (S (VP (VP (MD will) (RB not) (VP (VB occur))) (, ,) (CC and) (VP (MD should) (ADVP (RB instead)) (VP (VB provide) (NP (NNS guns)) (PP (TO to) (NP (NP (ADJP (RB as) (JJ many)) (NNS people)) (PP (IN as) (ADJP (JJ possible))))) (S (VP (TO to) (VP (VB protect) (NP (PRP ourselves))))))))))))))) (. .)))

(S1 (S (NP (NP (DT A) (JJ total) (NN ban)) (PP (IN on) (NP (NNS guns)))) (VP (AUX is) (ADJP (JJ impossible)) (PP (IN in) (NP (DT the) (NNP US)))) (. .)))

