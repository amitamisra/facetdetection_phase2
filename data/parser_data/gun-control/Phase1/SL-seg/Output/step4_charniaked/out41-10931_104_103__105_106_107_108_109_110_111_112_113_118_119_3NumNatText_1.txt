(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ believes) (SBAR (S (NP (DT the) (NN ATF)) (VP (AUX is) (VP (VBG abusing) (NP (DT the) (NN system)) (ADVP (ADVP (RB as) (RB well)) (SBAR (IN as) (S (NP (PRP$ their) (NNS acts)) (VP (AUX are) (ADJP (JJ unconstitutional))))))))))) (. .)))

(S1 (S (NP (EX There)) (VP (VBZ needs) (S (VP (TO to) (VP (AUX be) (NP (NP (DT a) (VB trace)) (PP (IN on) (NP (NP (NP (NN gun) (NNS smugglers)) (VP (VBG working) (PP (IN for) (NP (DT the) (NN ATF))))) (CONJP (RB as) (RB well) (IN as)) (NP (NP (NP (DT the) (NN ATF) (POS 's)) (NNS records)) (SBAR (S (NP (PRP they)) (VP (AUX are) (VP (VBG withholding) (ADVP (RB illegally)) (PP (IN from) (NP (NNP Congress))))))))))))))) (. .)))

(S1 (S (S (NP (NNP Congress)) (VP (VBZ makes) (NP (DT the) (NNS laws)))) (CC and) (S (NP (NNP ATF)) (VP (AUX is) (VP (AUXG being) (VP (VBN charged) (PP (IN with) (NP (NP (NN contempt)) (PP (IN of) (NP (NNP Congress))))))))) (. .)))

(S1 (S (S (NP (DT The) (NN law)) (VP (VBZ states) (NP (NNS approvals)) (PP (IN through) (NP (DT the) (JJ NICS) (NN check))) (VP (AUX be) (VP (VBN destroyed) (PP (IN in) (NP (CD 24) (NNS hours))))))) (CC and) (S (NP (DT no) (JJ permanent) (NNS records)) (VP (MD will) (VP (VB exist) (ADVP (RB anywhere)) (PP (IN except) (PP (IN in) (NP (NN gun) (NNS shops))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ sees) (SBAR (S (NP (NNP Author2)) (VP (AUX is) (RB not) (PP (IN up) (PP (IN with) (NP (JJ current) (NNS events)))))))) (. .)))

(S1 (S (NP (NP (JJ Author2) (JJ contests) (NNS protests)) (PP (IN against) (NP (DT the) (NN ATF)))) (VP (AUX is) (NP (NP (DT a) (NN conspiracy)) (CC and) (NP (NP (DT the) (JJ ATF) (NNS abides)) (PP (IN by) (NP (DT the) (NN law)))))) (. .)))

(S1 (S (NP (DT The) (NN ATF)) (VP (AUX was) (VP (VP (VBN asked) (PP (IN for) (NP (NNS copies)))) (, ,) (CONJP (CC but) (RB not)) (VP (VBN asked) (PP (IN for) (NP (DT the) (NNS books)))) (NP (PRP themselves)))) (. .)))

(S1 (S (NP (EX There)) (VP (AUX is) (NP (NP (DT a) (NN statute)) (SBAR (WHNP (WDT that)) (S (VP (VBZ states) (S (NP (NP (DT a) (JJ federal) (NN agency)) (PRN (-LRB- -LRB-) (NP (NNP ATF)) (-RRB- -RRB-))) (VP (ADVP (RB cannot)) (VB ask) (PP (IN for) (NP (NNS copies)))))))))) (. .)))

(S1 (S (NP (NNS Sellers)) (VP (AUX are) (VP (VBN bound) (PP (IN by) (NP (DT the) (NN law) (S (VP (TO to) (VP (VB produce) (NP (DT any) (NNS records)) (SBAR (WHADVP (WRB when)) (S (VP (VBN needed))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ keeps) (VP (VBG refusing) (S (VP (TO to) (VP (VB refute) (NP (DT the) (NN proof) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX have) (VP (VBN shown) (PP (IN for) (NP (DT the) (NN argument))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ claims) (SBAR (IN that) (S (NP (NNP Author1)) (VP (VBZ keeps) (S (VP (VBG diverting) (ADVP (RB away) (PP (IN from) (NP (NP (NNP Author2) (POS 's)) (NN argument)))))))))) (. .)))

