(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (AUX is) (VP (VBG talking) (PP (IN about) (NP (NP (NN someone)) (SBAR (SBAR (WHNP (WP who)) (S (VP (VBD left) (NP (DT a) (NN knife)) (PP (IN on) (NP (DT a) (NN counter)))))) (, ,) (CC and) (SBAR (WHNP (WRB how) (JJ obvious)) (S (NP (DT the) (NN intent) (S (VP (TO to) (VP (AUX do) (NP (NN harm)))))) (VP (AUX is))))))))) (. .)))

(S1 (NP (NP (CD Author2) (NNS posts)) (NP (NP (DT the) (JJ relevant) (NN headline)) (PP (IN of) (NP (NP (DT an) (JJ injured) (NN woman)) (VP (VBN stabbed) (PP (IN in) (NP (DT a) (NN store) (NN shooting))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (IN that) (S (SBAR (IN if) (S (NP (PRP it)) (VP (AUX had) (VP (AUX been) (NP (DT a) (NN gun)))))) (, ,) (NP (DT the) (NN headline)) (VP (MD would) (VP (AUX have) (VP (AUX been) (NP (QP (RB about) (CD seven)) (JJ dead) (NNS people)))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (NNS posts) (NP (NP (DT a) (NN headline)) (PP (IN about) (NP (NP (CD seven) (NNS people)) (VP (VBN stabbed) (PP (TO to) (NP (NP (NN death)) (PP (IN in) (NP (NNP Japan)))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ brings) (PRT (RP up)) (NP (DT the) (NNP Columbine) (NNP School) (NN shooting))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ argues) (SBAR (S (NP (PRP it)) (VP (AUX is) (RB not) (ADJP (JJ relevant)))))) (. .)))

(S1 (S (NP (DT The) (CD two) (NNS people)) (VP (VP (AUX were) (ADJP (JJ crazy))) (, ,) (CC and) (VP (VBD got) (NP (PRP$ their) (NNS guns)) (ADVP (RB illegally)))) (. .)))

(S1 (S (NP (PRP They)) (VP (VP (VBD took) (NP (PRP them)) (PP (TO to) (NP (DT a) (ADJP (RB no) (NNS guns)) (NN area)))) (, ,) (CC and) (VP (VBD killed) (PP (IN without) (NP (NP (DT any) (NN regard)) (PP (IN for) (NP (NNS people))))))) (. .)))

(S1 (S (S (NP (DT This)) (VP (AUX is) (ADJP (JJ different) (PP (IN than) (S (NP (NN someone)) (VP (VBG carrying) (NP (DT a) (NN gun)) (PP (IN for) (NP (NN protection))))))))) (, ,) (NP (PRP he)) (VP (VBZ says)) (. .)))

