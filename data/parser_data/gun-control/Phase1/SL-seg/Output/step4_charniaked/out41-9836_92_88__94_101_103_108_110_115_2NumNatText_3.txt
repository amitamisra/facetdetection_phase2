(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (S (NP (NNP Author1)) (VP (VBZ tells) (NP (NNP Author2)) (SBAR (IN that) (S (NP (NNS kids)) (VP (MD should) (VP (AUX be) (VP (VBN taught) (SBAR (WHADVP (WRB how)) (S (VP (TO to) (VP (VB handle) (NP (DT a) (NN gun))))))))))))) (, ,) (CC and) (S (NP (NNP Author2)) (VP (VBZ replies) (SBAR (IN that) (S (NP (NP (DT all)) (PP (IN of) (NP (PRP$ his) (NNS kids)))) (VP (AUX are) (NP (NN crack) (NNS shots))))))) (. .)))

(S1 (S (NP (NNP Author1)) (ADVP (RB sarcastically)) (VP (VBZ commends) (NP (NNP Author2)) (PP (IN for) (S (VP (AUXG being) (NP (NP (DT an) (NN example)) (PP (TO to) (NP (DT all) (ADJP (JJ responsible)) (NN law) (VBG abiding) (NNS parents)))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB then)) (VP (VBZ tells) (NP (NNP Author2)) (SBAR (IN that) (S (NP (PRP$ his) (NNS kids)) (VP (AUX are) (VP (VBN prepared) (PP (IN for) (NP (NP (DT a) (VBG killing) (NN spree)) (PP (IN at) (NP (NN school))) (, ,) (SBAR (WHNP (WDT which)) (S (VP (VBZ causes) (S (NP (NNP Author2)) (VP (TO to) (VP (NN insult) (NP (NNP Author1))))))))))))))) (. .)))

(S1 (S (S (NP (PRP He)) (VP (VBZ tells) (NP (NNP Author1)) (SBAR (IN that) (S (NP (PRP$ his) (NNS kids)) (VP (VB respect) (NP (NN firepower))))))) (, ,) (RB then) (S (NP (NNP Author2)) (VP (VBZ makes) (NP (NP (DT a) (NN joke)) (PP (IN about) (NP (PRP$ his) (NNS kids)))))) (. .)))

(S1 (S (S (NP (NNP Author1)) (VP (VBZ responds) (PP (IN by) (S (VP (VBG saying) (SBAR (IN that) (S (NP (DT the) (NN joke)) (VP (AUX was) (ADJP (JJ unforgivable)))))))))) (, ,) (RB then) (S (NP (NNP Author1)) (VP (VBZ makes) (NP (NP (JJ sarcastic) (NNS remarks)) (PP (IN about) (NP (NP (NNS kids)) (VP (VBG packing) (CC and) (VBG handling) (NP (NNS guns)))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ tells) (NP (NNP Author1)) (SBAR (SBAR (IN that) (S (NP (NNP America)) (VP (AUX is) (ADJP (JJ different) (PP (IN from) (NP (NNP Australia))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NNP Author1)) (VP (MD must) (VP (AUX be) (VP (VBG living) (PP (IN under) (NP (NP (DT a) (NN rock)) (PP (IN in) (NP (NNP Australia)))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ states) (SBAR (SBAR (IN that) (S (NP (NNPS Americans)) (VP (AUX are) (RB not) (ADJP (JJ paranoid) (PP (IN about) (NP (NNS guns))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (PRP they)) (VP (VBP understand) (NP (NN firearms))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ tells) (NP (NNP Author2)) (SBAR (IN that) (S (NP (NNP Australia)) (VP (AUX is) (NP (NP (DT the) (NN land)) (PP (IN of) (NP (DT no) (NNS guns)))) (, ,) (SBAR (IN while) (S (NP (NNP America)) (VP (AUX is) (NP (NP (NP (DT the) (NN land)) (PP (IN of) (NP (DT the) (NN gun)))) (CC and) (NP (NP (DT the) (NN home)) (PP (IN of) (NP (DT the) (JJ fearful)))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ corrects) (NP (NNP Author1)) (PP (IN by) (S (VP (VBG reading) (NP (NNS headlines)) (PP (IN from) (NP (NP (JJ Australian) (NN news) (NNS reports)) (PP (IN about) (NP (NN gun) (NN access) (CC and) (NN gun) (NNS crimes))) (SBAR (IN that) (S (NP (NNP Australia)) (VP (AUX is) (VP (VBG dealing) (PP (IN with)))))))))))) (. .)))

