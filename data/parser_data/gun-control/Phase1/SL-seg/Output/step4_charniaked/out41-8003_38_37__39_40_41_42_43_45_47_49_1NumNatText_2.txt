(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ points) (PRT (RP out)) (SBAR (IN that) (S (NP (NNP British) (NNP Canada)) (VP (VBD won) (NP (NP (DT the) (NN war)) (PP (IN of) (NP (CD 1812)))))))) (. .)))

(S1 (S (S (NP (NNS Guns)) (VP (AUX are) (RB not) (NP (NP (DT the) (JJ major) (NN issue)) (PP (IN in) (NP (DT the) (NNP US)))))) (, ,) (NP (PRP it)) (VP (AUX is) (NP (NP (DT the) (NNS people)) (SBAR (WHNP (WP who)) (S (VP (AUX do) (RB not) (VP (VB know) (SBAR (WHADVP (WRB how)) (S (VP (TO to) (VP (VB use) (NP (NNS guns)) (ADVP (RB safely)))))))))))) (. .)))

(S1 (S (PP (IN In) (NP (NNP Britain))) (NP (NNS people)) (VP (MD can) (VP (VB walk) (ADVP (RB safely)) (PP (IN through) (NP (JJ major) (NNS cities))) (PP (IN without) (NP (NP (NN fear)) (PP (IN of) (NP (NP (NN crime)) (CC or) (NP (NN murder)))))))) (. .)))

(S1 (S (S (NP (NNS Guns)) (VP (AUX are) (ADVP (RB rarely)) (VP (VBN used) (S (VP (TO to) (VP (VB commit) (NP (NP (NN murder)) (PP (IN in) (NP (NNP Britain)))))))))) (, ,) (CC and) (S (NP (NP (DT the) (NN majority)) (PP (IN of) (NP (NNS murders)))) (VP (AUX are) (NP (JJ drunken) (NNS altercations)))) (. .)))

(S1 (S (NP (NN Vigilantism)) (VP (AUX is) (RB not) (NP (NP (DT a) (JJR better) (NN alternative)) (PP (IN than) (NP (DT a) (VBN trained) (NN police) (NN force))))) (. .)))

(S1 (S (S (S (VP (VBG Taking) (NP (DT the) (NN law)) (PP (IN into) (NP (PRP$ your) (JJ own) (NNS hands))))) (VP (AUX is) (NP (NP (DT the) (NN definition)) (PP (IN of) (NP (NN vigilantism)))))) (, ,) (CC and) (S (NP (NP (DT the) (VBN claimed) (NN defense)) (PP (IN of) (NP (PRP US) (NNS rights)))) (VP (AUX is) (VP (AUXG being) (VP (VBN eroded) (ADVP (RB daily)) (PP (IN by) (NP (DT the) (NN Patriot) (NN Act))))))) (. .)))

(S1 (S (SBAR (IN Since) (S (NP (EX there)) (VP (AUX is) (NP (DT no) (JJ popular) (NN uprising))))) (, ,) (NP (PRP it)) (VP (AUX is) (ADJP (JJ clear)) (SBAR (IN that) (S (NP (NNS guns)) (VP (AUX do) (RB not) (VP (VB help) (VP (VB protect) (NP (NP (NN anyone) (POS 's)) (NNS rights)))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ argues) (SBAR (SBAR (IN that) (S (NP (DT the) (NNP US)) (VP (AUX did) (RB not) (VP (VB lose) (NP (NP (DT the) (NN War)) (PP (IN of) (NP (CD 1812)))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NP (DT the) (NNS issues)) (PP (IN with) (NP (NN gun) (NN ownership)))) (VP (VBP lie) (PP (IN in) (NP (NP (NNS criminals)) (CC and) (NP (DT a) (JJ weak) (NN justice) (NN system))))))))) (. .)))

(S1 (S (NP (NNPS Americans)) (VP (AUX do) (RB not) (VP (ADVP (RB blindly)) (VB surrender) (NP (PRP$ their) (NNS rights)))) (. .)))

(S1 (S (NP (NNP Britain)) (VP (AUX is) (NP (NP (DT a) (NN police) (NN state)) (PP (IN with) (NP (NP (PRP$ their) (JJ excessive) (NNS regulations)) (CC and) (NP (NN surveillance)))))) (. .)))

(S1 (S (NP (JJ Violent) (NNS criminals)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN left) (PP (IN on) (NP (DT the) (NNS streets)))))) (. .)))

