(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ believes) (PP (IN in) (S (VP (VBG fingerprinting) (CC and) (VBG photographing) (PP (IN for) (NP (NP (NN gun) (NNS purchases)) (PP (IN at) (NP (NP (DT the) (NN point)) (PP (IN of) (NP (NN sale))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (NNS licenses)) (VP (VP (AUX are) (ADJP (JJ unconstitutional))) (CC and) (VP (VBZ points) (PRT (RP out)) (SBAR (IN that) (S (NP (CD 200,000) (NNS guns)) (VP (AUX are) (VP (VBN stolen) (ADVP (RB annually))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ supports) (NP (NP (NN gun) (NN ownership)) (, ,) (CONJP (CC but) (RB not)) (NP (JJ absolute) (NNS rights)))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNP Author1)) (VP (MD will) (VP (AUX have) (S (VP (TO to) (VP (VB take) (NP (NP (DT every) (NN bit)) (PP (IN of) (NP (NN gun) (NN control)))) (SBAR (IN that) (S (S (NP (PRP he)) (VP (VBZ wants))) (CC and) (S (NP (PRP they)) (VP (VBP know) (SBAR (WHADVP (WRB where)) (S (NP (DT each)) (VP (VBZ stands)))))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (DT the) (NNP Heller) (NN decision)) (VP (AUX has) (VP (AUX been) (S (S (VP (VB explain) (NP (JJ many) (NNS times)))) (, ,) (CC and) (S (NP (JJ safe) (NN storage) (NNS laws)) (VP (AUX were) (RB not) (VP (VBN declared) (S (ADJP (JJ unconstitutional)))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (PRP they)) (VP (AUX are) (VP (VBG talking) (PP (IN about) (NP (DT the) (JJ 2nd) (NN amendment)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ questions) (SBAR (IN if) (S (NP (DT the) (NNP Supreme) (NNP Court)) (VP (VBD ruled) (SBAR (IN that) (S (NP (EX there)) (VP (AUX is) (NP (DT no) (NN right) (S (VP (TO to) (VP (VB keep) (CC and) (VB bear) (SBAR (MD would) (S (NP (DT the) (JJ 2nd) (NN amendment)) (ADVP (RB still)) (VP (AUX be) (VP (VBN supported)))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX is) (ADJP (JJ skeptical) (PP (IN of) (SBAR (WHNP (WP what)) (S (NP (NNS people)) (VP (AUX have) (S (VP (TO to) (VP (VB say)))))))))) (. .)))

(S1 (S (NP (NNP Author1) (CC and) (NNP Author2)) (VP (AUX do) (RB not) (VP (VB hate) (NP (DT each) (JJ other)))) (. .)))

