(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ claims) (SBAR (IN that) (S (PP (IN in) (NP (NNP Miami))) (NP (DT a) (NN PlayStation)) (VP (VBZ costs) (NP (NP (JJR more)) (PP (IN than) (NP (DT an) (NNP AK-47)))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ asks) (NP (NNP Author1)) (SBAR (WHNP (WP what)) (S (NP (NP (DT the) (NN street) (NN price)) (PP (IN of) (NP (DT an) (NNP AK-47)))) (VP (AUX is) (, ,) (FRAG (CC and) (ADVP (RB that)) (SBAR (IN if) (S (NP (PRP they)) (VP (AUX do) (RB not) (VP (VB know) (SBAR (S (NP (PRP they)) (VP (AUX 're) (RB not) (VP (VBG contributing)))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ responds) (NP (NP (JJR less)) (PP (IN than) (NP (DT a) (NN PlayStation))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ insists) (SBAR (S (NP (NNP Author1)) (VP (VP (VBZ backs) (PRT (RP up)) (NP (PRP$ their) (NN claim))) (CC and) (VP (VBZ calls) (S (NP (PRP them)) (NP (DT a) (NN liar)))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP ask) (ADVP (RB again)) (PP (IN for) (NP (NP (DT the) (NN price)) (PP (IN of) (NP (DT an) (NNP AK-47)))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (SBAR (S (NP (PRP they)) (VP (AUX 're) (RB not) (VP (VBG lying) (PP (IN with) (NP (PRP$ their) (NN assertion))))))) (CC and) (SBAR (IN that) (S (NP (PRP$ their) (NN claim)) (ADVP (RB also)) (VP (AUX is) (RB not) (ADJP (JJ false))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VP (VBZ lists) (NP (NP (DT the) (NNS prices)) (PP (IN of) (NP (NP (JJ various) (NNS models)) (PP (IN of) (NP (NNP PlayStations))))))) (CC and) (VP (VBZ says) (SBAR (S (NP (DT an) (NNP AK-47)) (VP (AUX is) (ADJP (RBR more) (JJ expensive))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (PRP they)) (VP (MD can) (RB not) (VP (VB back) (PRT (RP up)) (NP (DT that) (NN claim))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VP (VBZ places) (NP (NP (DT the) (NN burden)) (PP (IN of) (NP (NN proof)))) (PP (IN on) (NP (NNP Author1)))) (CC and) (VP (VBZ says) (SBAR (S (S (NP (PRP$ their) (JJ initial) (NN statement)) (VP (AUX had) (NP (DT no) (NN proof)))) (CC and) (S (NP (PRP$ their) (NNS claims)) (VP (AUX are) (ADVP (RB thus)) (ADJP (JJ invalid)))))))) (. .)))

