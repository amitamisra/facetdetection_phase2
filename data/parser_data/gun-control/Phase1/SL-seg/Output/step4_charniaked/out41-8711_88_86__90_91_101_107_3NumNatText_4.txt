(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ states) (SBAR (S (NP (NNP Canada)) (VP (AUX has) (NP (NP (ADJP (RB significantly) (JJR less)) (NN gun) (NN violence)) (PP (IN than) (NP (NNP America))) (ADJP (JJ due) (PP (TO to) (NP (JJR lesser) (NN gun) (NN control) (NN legislation))))))))) (. .)))

(S1 (S (S (NP (JJ Canadian) (NN gun) (NN ownership) (NNS numbers)) (VP (AUX are) (ADJP (JJ similar) (PP (TO to) (NP (NP (DT those)) (PP (IN of) (NP (DT the) (NNP U.S.)))))))) (, ,) (CC so) (S (PP (IN despite) (NP (JJ other) (NNS variables))) (, ,) (NP (PRP they)) (VP (AUX 're) (ADJP (JJ comparable)))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (AUX is) (ADVP (RB selfishly)) (VP (VBG denying) (NP (NP (DT all) (JJ statistical) (NN comparison)) (PP (IN between) (NP (NNP Canada) (CC and) (NNP America)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ feels) (SBAR (IN that) (S (S (VP (VBG comparing) (NP (NP (NP (NNP Canada) (POS 's)) (CC and) (NP (NNP America) (POS 's))) (NN gun) (NN violence) (NNS statistics)))) (VP (AUX is) (ADJP (JJ impossible) (PP (JJ due) (TO to) (NP (NP (JJ major) (NNS differences)) (PP (IN between) (NP (NP (NNP America)) (CC and) (NP (NNP Canada))))))))))) (. .)))

(S1 (S (NP (NNP Canada)) (VP (AUX has) (NP (NP (QP (RB roughly) (CD 1/10)) (DT the) (NN population)) (SBAR (WHNP (IN that)) (S (NP (NNP America)) (VP (AUX has)))))) (. .)))

(S1 (S (S (NP (NNP America)) (ADVP (RB also)) (VP (AUX has) (NP (NP (DT a) (JJ big) (NN problem)) (PP (IN with) (NP (NN gang) (NN member) (NN culture)))) (, ,) (PP (IN unlike) (NP (NNP Canada))))) (, ,) (CC and) (S (NP (NP (CD 80) (NN %)) (PP (IN of) (NP (NP (DT all) (NN crime)) (PP (IN in) (NP (NNP America)))))) (VP (VBZ stems) (PP (IN from) (NP (NN gang) (NN activity))))) (. .)))

