(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (S (VP (VBG owning) (NP (DT a) (NN gun)))) (VP (VBZ shows) (SBAR (S (NP (NN someone)) (VP (AUX does) (RB not) (VP (VB respect) (NP (NP (DT another) (NN person) (POS 's)) (NN right) (S (VP (TO to) (VP (VB life-) (SBAR (RB even) (IN if) (S (NP (DT that) (NN person)) (VP (AUX is) (VP (VBG attempting) (S (VP (TO to) (VP (VB take) (NP (PRP$ your) (NN life))))))))))))))))))))) (. .)))

(S1 (S (S (VP (VBG Denying) (NP (PRP them)))) (NP (DT that) (NN right)) (VP (VBZ says) (SBAR (S (NP (NNS rights)) (VP (AUX are) (RB not) (VP (VBN guaranteed)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (NNS criminals)) (VP (AUX do) (RB not) (VP (AUX have) (NP (DT a) (NN right) (S (VP (TO to) (VP (VB commit) (NP (DT a) (NN crime))))))))))) (. .)))

(S1 (S (S (NP (NNS Criminals)) (VP (AUX do) (RB not) (NP (NNP obey) (NNS laws)))) (, ,) (IN so) (S (NP (NN gun) (NN control)) (VP (AUX does) (RB not) (VP (VB affect) (NP (PRP them))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VP (VBZ questions) (SBAR (WHADVP (WRB why)) (S (VP (AUX have) (VP (NNS laws) (SBAR (IN if) (S (NP (PRP they)) (VP (AUX do) (RB not) (VP (AUX have) (NP (DT any) (NN effect))))))))))) (, ,) (CC and) (VP (VBZ says) (SBAR (S (NP (PRP they)) (VP (AUX do) (VP (VB affect) (NP (NNS criminals)))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ argues) (SBAR (S (NP (NN gun) (NN control) (NNS laws)) (VP (AUX do) (RB not) (VP (VB affect) (NP (NP (NNS criminals)) (SBAR (WHNP (WP who)) (S (VP (VP (AUX do) (RB not) (VP (NNP obey) (NP (PRP them)))) (, ,) (CC but) (VP (AUX do) (VP (VB affect) (NP (JJ law-abiding) (NNS citizens))))))))))))) (. .)))

(S1 (S (NP (JJ Author1) (NNS questions)) (ADVP (WRB why)) (VP (AUX does) (RB Author2) (VP (VB think) (SBAR (S (NP (PRP he)) (VP (AUX has) (NP (NP (DT a) (NN right)) (PP (TO to) (NP (DT a) (NN gun)))) (PP (ADVP (RB just)) (IN because) (IN of) (NP (NP (DT a) (NN government)) (VP (VBD made) (PRT (RP up)) (NP (NN law)))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VP (VBZ says) (SBAR (S (NP (DT the) (JJ second) (NN amendment)) (VP (AUX does) (RB not) (VP (VB grant) (NP (DT the) (JJ pre-existing) (NN right) (S (VP (TO to) (VP (VB bear) (NP (NNS arms))))))))))) (, ,) (CC but) (VP (VBZ preserves) (CC and) (VBZ protects) (NP (NP (DT a) (NN right)) (SBAR (WHNP (WDT that)) (S (ADVP (RB already)) (VP (VBD existed))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (SBAR (RB regardless) (IN if) (S (NP (PRP it)) (VP (AUX was) (ADJP (JJ pre-existing))))) (, ,) (NP (NN society)) (VP (MD can) (VP (VB change) (NP (PRP it)) (SBAR (IN if) (S (NP (NNS opinions)) (VP (VBP support) (S (VP (VBG doing) (ADVP (RB so)))))))))))) (. .)))

(S1 (S (S (NP (JJ Author2) (NNS states) (NNS laws)) (VP (AUX do) (RB not) (VP (VB prevent) (NP (NNS crimes))))) (, ,) (NP (PRP they)) (VP (VBP provide) (NP (NP (NN punishment)) (PP (IN for) (SBAR (WHADVP (WRB when)) (S (NP (NNS crimes)) (VP (VBP occur))))))) (. .)))

(S1 (S (NP (NN Gun) (NN control) (NNS laws)) (VP (VBP affect) (NP (NP (JJ law-abiding) (NNS citizens)) (, ,) (RB not) (NP (NNS criminals)))) (. .)))

