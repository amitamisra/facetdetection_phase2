(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (DT the) (NNP Obama) (NN administration)) (VP (AUX has) (VP (VBN made) (NP (NP (ADJP (RB unintentionally) (JJ misleading)) (NNS statements)) (SBAR (WHNP (WDT that)) (S (VP (MD should) (VP (AUX be) (VP (VBN analyzed) (PP (IN before) (S (VP (VBG arguing) (PP (IN about) (NP (PRP them))))))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ wants) (S (VP (TO to) (VP (VB remove) (NP (NN border) (NNS controls)) (ADVP (RB entirely)) (, ,) (PP (RB instead) (IN of) (S (VP (VBG sealing) (PP (IN off) (NP (DT the) (JJ US-Mexico) (NN border)))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ respects) (NP (DT the) (NNP Obama) (NN administration)) (, ,) (SBAR (RB even) (IN if) (S (NP (PRP he)) (VP (NN dislikes) (NP (DT the) (JJ current) (NN president))))) (, ,) (SBAR (IN because) (S (NP (PRP they)) (VP (AUX are) (NP (NP (DT the) (NN Commander)) (PP (IN in) (NP (NP (NN Chief)) (PP (IN of) (NP (DT the) (JJ military) (NNS forces)))))))))) (. .)))

(S1 (S (S (NP (NNPS Firearms)) (VP (AUX are) (ADVP (RB routinely)) (VP (VBN taken) (PP (IN over) (NP (DT the) (NN border))) (PP (IN into) (NP (NNP Mexico)))))) (, ,) (CC and) (S (NP (NP (CD 90) (NN percent)) (PP (IN of) (NP (DT those) (NNS firearms)))) (VP (AUX were) (VP (VBN made) (PP (IN in) (NP (DT the) (NNP US))) (PP (VBG according) (PP (TO to) (NP (DT the) (NNP Obama) (NN administration))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ wants) (S (NP (DT the) (NN border)) (VP (TO to) (VP (AUX be) (VP (VBN closed) (SBAR (IN in) (NN order) (S (VP (TO to) (VP (VB stop) (S (ADJP (JJ illegal)) (NP (NN trade) (, ,) (NN traffic) (, ,) (CC and) (NN immigration)))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (AUX does) (RB not) (VP (VB trust) (NP (DT the) (NNP Obama) (NN administration)) (, ,) (S (VP (VBG claiming) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX have) (RB continuously) (VP (VBD lied) (SBAR (IN in) (NN order) (S (VP (TO to) (VP (VB protect) (NP (PRP themselves)))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (AUX does) (RB not) (VP (VB trust) (NP (DT the) (NN government)))) (. .)))

