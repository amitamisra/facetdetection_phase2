(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ claims) (SBAR (IN that) (S (NP (NP (NNS people)) (SBAR (WHNP (WP who)) (S (VP (VBP kill) (NP (NN police) (NNS officers)))))) (VP (AUX are) (ADVP (RB commonly)) (VP (VBG using) (NP (NNS drugs))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ demands) (NP (NN proof) (SBAR (IN that) (S (NP (NNS militias)) (VP (AUX are) (ADJP (JJ responsible) (PP (IN for) (NP (DT any) (NN police) (NNS deaths))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (NP (DT a) (NN militia) (NN group)) (PP (IN in) (NP (DT the) (NNP Midwest)))) (VP (VBD planned) (S (VP (TO to) (VP (VP (VB kill) (NP (NNS police))) (, ,) (CC and) (VP (VB ask) (SBAR (IN if) (S (NP (NNP Author1)) (VP (AUX is) (S (VP (VBG claiming) (SBAR (S (NP (PRP they)) (VP (AUX were) (PP (IN on) (NP (NNS drugs))))))))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ argues) (SBAR (S (NP (PRP they)) (VP (VP (AUX did) (RB not) (VP (VB commit) (NP (DT the) (NN crime)))) (, ,) (CC but) (VP (AUX were) (VP (VBN accused) (PP (IN by) (NP (NP (NN someone) (RB else)) (SBAR (WHNP (WP who)) (S (VP (AUX had) (NP (DT an) (JJ ulterior) (NN motive))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VP (VBZ mentions) (NP (JJ other) (NNS conspiracies))) (, ,) (CC and) (VP (VBZ claims) (SBAR (S (NP (DT this)) (VP (AUX is) (NP (DT another) (NN conspiracy))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (NNP Author2)) (VP (AUX has) (RB not) (VP (VB read) (SBAR (WHADVP (WRB how)) (S (VP (VBD accused) (SBAR (S (NP (NP (NNS criminals)) (SBAR (WHNP (WP who)) (S (VP (VBD killed) (NP (NNS police)))))) (VP (VBD blamed) (NP (NNS drugs)) (PP (IN for) (NP (PRP$ their) (NN behavior)))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (ADVP (RB never)) (VP (VBD said) (SBAR (S (NP (NN militia)) (VP (AUX were) (ADJP (JJ responsible) (PP (IN for) (NP (NN police) (NNS deaths))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (S (NP (NNP Author2) (NNS judges)) (ADJP (ADJP (NP (DT a) (NN group)) (JJ guilty)) (SBAR (IN before) (S (NP (PRP they)) (VP (AUX are) (VP (VBN convicted)))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBD said) (SBAR (S (NP (PRP he)) (VP (AUX was) (VP (VBG posting) (PP (VBN based) (PP (IN on) (SBAR (WHNP (WP what)) (S (NP (JJ federal) (NNS prosecutors)) (VP (AUX had) (VP (VBN said)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ accuses) (NP (NNP Author1)) (PP (IN of) (S (VP (VBG accusing) (NP (NNS prosecutors)) (PP (IN of) (NP (NP (JJ false) (NN reporting)) (PP (IN of) (NP (NNS facts))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ points) (PRT (RP out)) (SBAR (WHADVP (WRB how)) (S (NP (JJ federal) (NNS prosecutors)) (VP (AUX have) (VP (VBN tried) (S (VP (TO to) (VP (VB prove) (NP (JJ false) (NNS cases)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ accuses) (NP (PRP them)) (PP (IN of) (S (VP (VBG trying) (S (VP (TO to) (VP (VB influence) (NP (NN opinion))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ accuses) (NP (NNP Author2)) (PP (IN of) (S (VP (VBG making) (NP (NP (NNS comments)) (PP (IN about) (NP (NNS militias)))) (PP (IN in) (NP (NP (DT the) (JJ same) (NN way)) (SBAR (S (VP (TO to) (VP (VB influence) (NP (NP (NNS opinions)) (PP (IN of) (NP (PRP them)))))))))))))) (. .)))

