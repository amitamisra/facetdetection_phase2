(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ argues) (SBAR (IN that) (S (NP (NN self) (NN defense)) (VP (AUX is) (ADVP (RB only)) (ADJP (JJ applicable) (PP (IN as) (NP (DT a) (NN court) (NN plea)))) (SBAR (IN if) (S (NP (PRP you)) (AUX are) (VP (AUX do) (RB not) (VP (VB want) (S (VP (TO to) (VP (AUX be) (PP (IN in) (NP (DT the) (NN fight)))))))))))))) (. .)))

(S1 (S (NP (NN Self) (NN defense)) (VP (VBZ works) (SBAR (SBAR (IN if) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT an) (JJ immediate) (NN threat)) (PP (TO to) (NP (PRP$ your) (NN safety))))))) (, ,) (CONJP (CC but) (RB not)) (SBAR (IN if) (S (NP (EX there)) (VP (AUX is) (RB not) (NP (DT an) (JJ immediate) (NN threat))))))) (. .)))

(S1 (S (S (VP (VBG Fleeing) (PP (IN from) (NP (DT an) (NN attacker))))) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN required) (PP (IN before) (NP (NN self) (NN defense))) (, ,) (SBAR (IN as) (S (ADVP (RB sometimes)) (NP (PRP it)) (VP (AUX is) (RB not) (ADJP (JJ possible)))))))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (PRP you)) (VP (VP (AUX have) (NP (DT a) (NN chance) (S (VP (TO to) (VP (VB flee)))))) (CC and) (VP (AUX do) (RB not) (VP (VB take) (NP (PRP it))))))) (, ,) (NP (DT that)) (VP (AUX is) (ADVP (RB no) (RB longer)) (NP (NN self) (NN defense))) (. .)))

(S1 (S (NP (EX There)) (VP (AUX is) (NP (NP (DT a) (JJ clear) (NN difference)) (PP (IN between) (NP (NN manslaughter) (CC and) (NN murder))))) (. .)))

(S1 (S (SBAR (IN If) (S (S (NP (PRP you)) (VP (AUX are) (ADJP (JJ able) (S (VP (TO to) (VP (VB flee) (NP (DT a) (NN situation)))))))) (CC and) (S (NP (PRP you)) (ADVP (RB instead)) (VP (VBP kill) (NP (DT a) (NN man)))))) (, ,) (NP (PRP you)) (VP (MD will) (VP (AUX be) (VP (VBN charged) (PP (IN with) (NP (DT a) (NN crime)))))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (VBG fleeing)) (VP (AUX is) (ADJP (JJ dangerous))))) (, ,) (ADVP (RB then)) (NP (PRP you)) (VP (MD can) (ADVP (RB still)) (VP (VB act) (PP (IN in) (NP (NN self) (NN defense))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ argues) (SBAR (IN that) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT no) (NN requirement)) (PP (IN in) (NP (DT the) (NN law))) (SBAR (IN that) (S (NP (PRP you)) (VP (AUX need) (S (VP (TO to) (VP (VB flee) (SBAR (IN in) (NN order) (S (VP (TO to) (VP (VB plead) (NP (NN self) (NN defense))))))))))))))))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (PRP you)) (VP (AUX are) (VP (VBN threatened) (PP (IN with) (NP (NP (DT a) (JJ dangerous) (NN situation)) (, ,) (SBAR (S (NP (PRP you)) (VP (AUX do) (RB not) (VP (AUX need) (S (VP (TO to) (VP (VB attempt) (S (VP (TO to) (VP (VB flee))))))))))))))))) (, ,) (NP (PRP you)) (VP (MD can) (ADVP (RB instead) (RB immediately)) (VP (VB respond) (PP (IN with) (NP (NN force))))) (. .)))

(S1 (S (S (VP (VBG Defending) (NP (PRP yourself)))) (VP (AUX is) (ADJP (JJ legal) (PP (IN as) (NP (NN self) (NN defense))))) (. .)))

