(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (S (VP (VB stay) (PRT (RP open)) (S (VP (VBN minded)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (NNS people)) (VP (AUX have) (NP (DT a) (NN right) (S (VP (TO to) (VP (VB make) (NP (PRP$ their) (JJ own) (NN defense)) (SBAR (WHADVP (WRB when)) (S (PP (IN under) (NP (NN attack))))))))))))) (. .)))

(S1 (S (NP (NP (JJ Only) (NNS criminals)) (CC and) (NP (NP (DT those)) (VP (VBN declared) (S (ADJP (JJ insane)))))) (VP (MD should) (VP (VB lose) (NP (DT the) (NN right) (S (VP (TO to) (VP (VB defend) (NP (PRP themselves)))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (ADJP (VBN trained) (CC and) (VBN determined)) (NNS terrorists)) (VP (MD would) (VP (VB overwhelm) (NP (JJ armed) (NNS citizens))))))) (. .)))

(S1 (S (CC And) (NP (NP (DT a) (NN plane)) (ADJP (JJ full) (PP (IN of) (NP (NP (NNS people)) (PP (IN with) (NP (NNS guns))))))) (VP (MD would) (RB not) (VP (AUX be) (ADJP (JJ safe)))) (. .)))

(S1 (S (VP (VB Let) (S (NP (DT the) (NN airline)) (VP (AUX do) (NP (NN security))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (NNP Author1)) (VP (AUX is) (VP (VBG giving) (PRT (RP up)) (NP (PRP$ his) (NNP Second) (NNP Amendment) (NNS rights))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (NP (NNS citizens)) (PP (IN with) (NP (NNS guns)))) (VP (MD would) (VP (AUX be) (ADJP (RBR more) (JJ dangerous) (PP (IN than) (NP (NNS terrorists))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (S (VP (VBG giving) (PRT (RP up)) (NP (NN gun) (NNS rights)))) (VP (AUX is) (S (VP (VBG letting) (S (NP (DT the) (NNS terrorists)) (VP (VB win))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (S (VP (VBG carrying) (NP (NNS guns)))) (VP (VBZ puts) (NP (NNS thousands)) (PP (IN in) (NP (NN danger))))))) (. .)))

(S1 (S (S (NP (DT The) (NN world)) (VP (AUX has) (VP (VBN changed)))) (, ,) (CC and) (S (NP (DT this) (NN extremism)) (VP (AUX is) (RB not) (SBAR (WHNP (WP what)) (S (NP (DT the) (VBG founding) (NNS fathers)) (VP (AUX had) (PP (IN in) (NP (NN mind)))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (IN so) (IN if) (S (NP (DT an) (NN airline)) (VP (VBZ allows) (SBAR (S (NP (NNS guns)) (, ,) (ADVP (RB just)) (VP (VB choose) (NP (DT another))))))))) (. .)))

