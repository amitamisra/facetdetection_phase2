(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (NNP Author2)) (VP (AUX is) (VP (VBG proposing) (ADVP (RB again)) (SBAR (IN that) (S (NP (DT the) (NNP U.S.)) (VP (VB adopt) (NP (NP (NNP California) (POS 's)) (NNS standards)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ cites) (SBAR (S (NP (NNS others)) (VP (VBP lack) (PP (IN of) (NP (NN support))) (, ,) (SBAR (IN because) (S (NP (CD one) (NN state)) (VP (MD can) (RB not) (VP (VB determine) (SBAR (WHADVP (WRB how)) (S (NP (DT another)) (VP (MD should) (VP (VB act))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (NNS states)) (VP (MD can) (RB not) (VP (VB ignore) (NP (NP (NNS issues)) (PP (IN of) (NP (PRP$ their) (JJ own))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (NP (NNS states)) (VP (VBG adopting) (NP (NNS controls)))) (VP (MD would) (VP (VB depend) (PP (IN on) (NP (NP (DT the) (NNS voters)) (, ,) (CC and) (NP (DT the) (NNS states))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ states) (SBAR (IN that) (S (NP (NNP California)) (VP (AUX has) (NP (NP (JJ large) (ADJP (RB densely) (VBN populated)) (NNS cities)) (PP (IN with) (NP (JJ high) (NN crime) (NNS rates)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ gives) (NP (DT a) (JJ recent) (NN example) (SBAR (IN that) (S (NP (CD four) (NNS cops)) (VP (AUX were) (VP (VBN killed) (PP (IN by) (NP (NP (DT a) (NN criminal)) (PP (IN with) (NP (DT an) (NNP AK-47))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ questions) (SBAR (WHADVP (WRB how)) (S (NP (NP (DT an) (NN example)) (PP (IN of) (NP (NP (DT a) (VBN prohibited) (NN weapon)) (VP (AUXG being) (VP (VBN used)))))) (VP (VBZ serves) (PP (IN as) (NP (NP (DT a) (JJ good) (NN reason)) (PP (IN for) (NP (DT the) (NN ban))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (PRP it)) (VP (VBZ shows) (SBAR (WHADVP (WRB why)) (S (NP (EX there)) (VP (AUX is) (NP (DT a) (NN ban))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (SBAR (IN if) (S (NP (DT the) (NN gun)) (VP (AUX had) (RB not) (VP (AUX been) (ADJP (JJ available)))))) (, ,) (NP (DT the) (NN damage)) (VP (MD would) (RB not) (VP (AUX be) (ADJP (RB so) (JJ great))))))) (. .)))

