(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ asks) (SBAR (IN if) (S (NP (EX there)) (VP (AUX was) (NP (NP (DT a) (NN case)) (PP (IN of) (NP (NP (NN something)) (SBAR (WHNP (WDT that)) (S (ADVP (RB ever)) (VP (VBD happened))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP say) (SBAR (IN that) (S (NP (NNS people)) (VP (VBP like) (S (VP (TO to) (VP (VB use) (NP (IN that) (NN quote)) (SBAR (WHADVP (WRB when)) (S (VP (VBG talking) (PP (IN about) (NP (JJ unarmed) (NNS passengers))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP think) (SBAR (S (NP (NNS people)) (VP (AUX are) (VP (VBG vilifying) (NP (NP (NNS people)) (VP (VBG defending) (NP (PRP themselves))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (EX there)) (VP (AUX is) (NP (NP (DT no) (NN difference)) (PP (IN between) (NP (S (VP (VBG dying) (PP (IN in) (NP (DT a) (NN crash))))) (CC and) (S (VP (VBG dying) (PP (IN during) (NP (NN defense)))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP say) (SBAR (IN that) (S (NP (DT the) (NN danger)) (VP (AUX is) (PP (IN from) (NP (NP (NNS mistakes)) (PP (IN of) (NP (NN intent))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (EX there)) (VP (AUX are) (NP (NP (DT no) (NNS incidents)) (PP (IN of) (NP (NP (NP (NNP Author2) (POS 's)) (NNS claims)) (, ,) (SBAR (WHNP (IN that)) (S (NP (PRP they)) (VP (AUX are) (ADVP (RB just)) (VP (VBG chanting) (NP (JJ anti) (NN gun) (NNS ideas))))))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP say) (SBAR (S (NP (NNP Author2)) (VP (VBZ wants) (S (VP (TO to) (VP (VB ban) (NP (NP (NN right)) (PP (TO to) (NP (JJ own) (NNS guns))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (PRP they)) (VP (AUX do) (RB not) (VP (VB want) (S (VP (TO to) (VP (VB ban) (, ,) (S (RB just) (VP (VB restrict))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ accuses) (NP (NNP Author2)) (PP (IN of) (S (VP (VBG using) (NP (JJ undocumented) (NNS statements)) (S (VP (TO to) (VP (VB limit) (CC or) (VB ban) (NP (DT the) (NN right) (S (VP (TO to) (VP (VB bear) (NP (NNS arms))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (SBAR (S (NP (PRP they)) (VP (VBP think)))) (CC and) (SBAR (IN that) (S (NP (JJ regular) (NNS people)) (VP (MD can) (RB not) (VP (VB defend) (NP (NNS planes)) (ADVP (RB anyway)))))))) (. .)))

