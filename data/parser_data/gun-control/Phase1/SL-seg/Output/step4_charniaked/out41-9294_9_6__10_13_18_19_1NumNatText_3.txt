(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ states) (SBAR (IN that) (S (NP (NP (VB trace) (NNS data)) (PP (IN in) (NP (NP (DT the) (NN sale)) (PP (IN of) (NP (NNS firearms))) (PP (TO to) (NP (NP (DT a) (NN person)) (VP (VBG operating) (PP (IN with) (NP (DT a) (NNP Federal) (NNP Firearm) (NN License))))))))) (VP (MD could) (VP (AUX be) (ADJP (JJ available) (PP (IN for) (NP (NN law) (NN enforcement)))) (SBAR (IN if) (S (VP (VBN needed))))))))) (. .)))

(S1 (S (NP (DT This)) (VP (MD would) (VP (AUX be) (ADJP (JJ legal)) (PP (IN in) (CC and) (IN around) (NP (NP (DT the) (NNS confines)) (PP (IN of) (NP (NP (DT the) (JJ 2nd) (NN Amendment)) (CC and) (NP (JJ private) (NN seller) (NNS freedoms)))))))) (. .)))

(S1 (S (NP (NN Law) (NN enforcement)) (VP (MD would) (VP (AUX have) (S (VP (TO to) (VP (VB rely) (PP (IN on) (NP (NP (NN information)) (VP (VBN provided) (NP (PRP them)) (PP (IN by) (NP (NP (DT the) (JJ last) (JJ legal) (NN source)) (PP (IN of) (NP (NP (NN information)) (, ,) (NP (DT the) (NN manufacturer)))))))))))))) (. .)))

(S1 (S (NP (DT This)) (VP (MD could) (VP (AUX be) (ADJP (JJ helpful) (PP (IN in) (S (VP (VBG solving) (NP (NN gun) (NNS crimes)))))) (PP (IN by) (S (VP (ADVP (RB significantly)) (VBG narrowing) (NP (DT the) (JJ suspect) (NN pool))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ feels) (SBAR (S (NP (NP (NNP Author1) (POS 's)) (NN statement)) (VP (AUX is) (ADVP (RB unethically)) (VP (VBG involving) (NP (NP (DT the) (NNS manufacturers)) (PP (IN of) (NP (NP (NNS firearms)) (PP (IN in) (NP (NN law) (NN enforcement))))))))))) (. .)))

(S1 (S (NP (PRP It)) (VP (MD 'd) (VP (AUX be) (ADJP (RB similarly) (JJ ludicrous) (S (VP (TO to) (VP (VB investigate) (NP (JJ major) (NN car) (NNS manufacturers)))))) (SBAR (IN if) (S (NP (DT a) (NN vehicle)) (VP (AUX was) (VP (VBN involved) (PP (IN in) (NP (DT a) (JJ vehicular) (NN crime))))))))) (. .)))

(S1 (S (NP (NNS Manufacturers)) (VP (MD could) (ADVP (RB only)) (VP (VB link) (PP (TO to) (NP (NNS distributors))) (, ,) (PP (RB not) (PP (TO to) (NP (NNS dealers)))))) (. .)))

(S1 (S (NP (DT This)) (ADVP (RB falsely)) (VP (VBZ casts) (NP (NN doubt)) (PP (IN on) (NP (JJ legal) (NN firearm) (NNS owners)))) (. .)))

