(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VP (VBZ asks) (S (NP (NNP Author1)) (VP (TO to) (VP (VB view) (NP (NP (NNS threads)) (SBAR (WHNP (WDT that)) (S (VP (VBP start) (SBAR (IN with) (`` ``) (S (NP (PRP I)) (VP (VBD carried) (NP (DT a) (NN gun)))) ('' '')))))))))) (CC and) (VP (VBZ says) (SBAR (IN that) (WHADVP (WRB how)) (S (NP (PRP$ their) (NNS actions)) (VP (AUX did) (RB not) (VP (VP (VB endanger) (NP (NN anyone))) (, ,) (VP (VB increase) (NP (NN crime))) (, ,) (CC or) (VP (VB violate) (NP (NNS rights))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VB ask) (NP (NNP Author2)) (SBAR (IN if) (S (NP (PRP it)) (VP (AUX 's) (ADJP (JJ okay)) (S (VP (TO to) (VP (VB carry) (NP (NNS guns))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ asks) (SBAR (IN if) (S (NP (PRP it)) (VP (AUX 's) (ADJP (JJ okay)) (S (VP (TO to) (VP (VB carry) (NP (DT a) (JJ nuclear) (NN weapon))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ challenges) (S (NP (NNP Author2)) (VP (TO to) (VP (VB find) (NP (NP (DT a) (JJ nuclear) (NN weapon)) (PP (IN for) (NP (NN sale)))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ asks) (SBAR (WHADVP (WRB how)) (S (NP (PRP they)) (VP (MD could) (VP (VB get) (NP (DT a) (NN gun)) (SBAR (IN if) (S (NP (PRP they)) (VP (AUX were) (VP (VBN banned)))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (IN that) (S (SBAR (IN since) (S (NP (DT the) (NNP US)) (VP (AUX has) (NP (NNS nukes))))) (, ,) (NP (PRP they)) (VP (MD should) (VP (AUX be) (VP (VBN allowed) (S (NP (CD one) (RB too))))))))) (. .)))

(S1 (S (NP (PRP They)) (VP (VBP say) (SBAR (IN that) (S (NP (DT the) (NN goal)) (VP (MD should) (VP (AUX be) (S (VP (TO to) (VP (VP (VB repeal) (NP (DT a) (NN ban))) (, ,) (RB not) (VP (VB break) (NP (PRP it))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ implies) (SBAR (S (NP (NNS nukes)) (VP (AUX are) (ADJP (RB not) (JJ comparable)) (SBAR (IN since) (S (NP (EX there)) (VP (AUX are) (NP (JJR fewer) (NNS vendors))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (IN that) (S (NP (DT the) (JJ free) (NN market)) (VP (MD may) (VP (VB allow) (S (NP (JJ nuclear) (NNS weapons)) (VP (TO to) (VP (AUX be) (VP (VBN bought) (SBAR (IN if) (S (NP (EX there)) (VP (AUX was) (NP (DT a) (NN demand)))))))))))))) (. .)))

