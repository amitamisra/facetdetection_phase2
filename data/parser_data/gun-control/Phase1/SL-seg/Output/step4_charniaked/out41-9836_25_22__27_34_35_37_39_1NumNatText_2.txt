(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ believes) (SBAR (IN that) (S (NP (JJ illegal) (NNS drugs)) (VP (AUX are) (ADVP (RB almost) (RB always)) (NP (NP (DT the) (NN root) (NN cause)) (SBAR (WHADVP (WRB when)) (S (NP (DT a) (NN police) (NN officer)) (VP (VBZ gets) (VP (VBN killed) (PP (IN by) (NP (NN violence)))))))))))) (. .)))

(S1 (S (NP (NP (DT Those)) (SBAR (WHNP (WP who)) (S (NP (NN murder) (NNS policemen)) (VP (AUX do) (RB not))))) (VP (VBP tend) (S (VP (TO to) (VP (AUX be) (NP (NP (DT the) (NNS members)) (PP (IN of) (NP (NP (NNS militias) (CC or) (NNS adherents)) (PP (IN of) (NP (JJ religious) (NNS cults)))))))))) (. .)))

(S1 (S (NP (NNPS Presidents) (NNP Clinton) (CC and) (NNP Obama)) (VP (AUX have) (VP (VBD lied) (PP (IN about) (NP (JJ such) (NNS things))) (PP (IN in) (NP (DT the) (NN past) (S (VP (TO to) (VP (VP (VB cover) (NP (PRP$ their) (JJ illicit) (NN involvement))) (CC or) (VP (VB hide) (NP (PRP$ their) (JJ shady) (NNS connections)))))))))) (. .)))

(S1 (S (PP (IN In) (NP (JJ other) (NNS cases))) (, ,) (NP (NNS people)) (VP (AUX have) (VP (VBD lied) (S (VP (TO to) (VP (VB get) (NP (NP (NN funding)) (PP (IN for) (NP (PRP$ their) (NNS groups))))))))) (. .)))

(S1 (S (CC But) (PP (IN in) (NP (NN reality))) (, ,) (NP (EX there)) (VP (AUX is) (NP (NP (DT a) (JJ strong) (NN correlation)) (PP (IN between) (NP (NP (NN drug) (NN use)) (CC and) (NP (NN police) (NNS deaths)))))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX is) (ADJP (JJ wrong) (S (VP (TO to) (VP (VB blame) (NP (DT the) (NNS militias))))))) (. .)))

(S1 (S (S (NP (NNS Juries)) (VP (AUX are) (ADJP (JJ likely) (S (VP (TO to) (VP (VB proclaim) (NP (NP (JJ such) (NNS victims)) (PP (IN of) (NP (DT the) (NN system)))) (S (VP (TO to) (VP (AUX be) (ADJP (JJ innocent))))))))))) (CC and) (S (NP (PRP it)) (VP (AUX is) (NP (NP (JJS best)) (SBAR (S (VP (TO to) (VP (VB wait) (PP (IN for) (NP (PRP$ their) (NN verdict)))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ believes) (SBAR (IN that) (S (NP (DT a) (JJ particular) (NN militia) (NN group)) (VP (AUX did) (VP (NN plan) (S (VP (TO to) (VP (AUX do) (NP (JJ deadly) (NN violence)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX does) (RB not) (VP (VB say) (SBAR (S (NP (PRP they)) (ADVP (RB actually)) (VP (VBD killed) (NP (NN anyone)))) (, ,) (CC but) (SBAR (IN that) (S (NP (PRP they)) (VP (VBD planned) (S (VP (TO to) (VP (AUX do) (ADVP (RB so))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (S (NP (PRP he)) (VP (AUX is) (RB not) (NP (NP (DT the) (NN one)) (SBAR (WHNP (WP who)) (S (VP (VBZ sees) (NP (NNS conspiracies)) (ADVP (RB everywhere)))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB also)) (VP (VBZ believes) (SBAR (SBAR (IN that) (S (NP (JJ Federal) (NNS prosecutors)) (VP (AUX are) (NP (JJ reliable) (NNS sources))))) (, ,) (CC and) (SBAR (IN that) (S (S (VP (VBG quoting) (NP (PRP them)))) (VP (VBZ provides) (NP (NP (JJ reliable) (NN information)) (PP (IN on) (NP (DT the) (NN topic)))) (PP (IN under) (NP (NN discussion)))))))) (. .)))

