(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ states) (SBAR (S (NP (EX there)) (VP (AUX are) (NP (NP (DT no) (NNS statistics)) (PP (VBG proving) (S (VP (VBG owning) (NP (NP (DT a) (NN gun)) (SBAR (S (VP (VBZ makes) (S (NP (NNS people)) (ADJP (JJR safer))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNS numbers)) (VP (MD can) (VP (AUX be) (VP (VBG deceiving) (PP (IN as) (PP (TO to) (SBAR (WHNP (WP what)) (S (VP (VP (VBZ creates) (NP (DT a) (NN problem)) (, ,) (S (VP (VBG citing) (NP (NP (DT the) (NN number)) (PP (IN of) (NP (NP (NNS people)) (VP (VBN killed) (PP (IN in) (NP (CD 9-11))))))) (PP (IN as) (NP (NP (DT a) (JJ small) (NN number)) (PP (TO to) (NP (DT some)))))))) (CC and) (VP (NP (DT a) (JJ large) (CD one)) (PP (TO to) (NP (NNS others))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (SBAR (IN if) (S (NP (NNP Author1)) (VP (AUX had) (NP (NP (DT a) (NN personal) (CC or) (NN family) (NN encounter)) (PP (IN with) (NP (NN gun) (NN violence))))))) (, ,) (NP (PRP he)) (VP (MD would) (VP (VB feel) (ADVP (RB differently))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (SBAR (IN if) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT no) (NN proof) (CD one) (NN way)) (CC or) (NP (DT the) (JJ other)))))) (, ,) (RB then) (ADVP (WRB why)) (VP (VB infringe) (PP (IN on) (NP (PRP$ his) (NNS rights))))))) (. ?)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (PRP it)) (VP (VP (AUX is) (ADJP (JJ provable))) (, ,) (CC but) (VP (ADVP (RB just)) (AUX has) (RB not) (VP (AUX been) (VP (AUX done)))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (JJ illogical) (NNS statistics)) (VP (AUX are) (VP (VBN used) (S (VP (TO to) (VP (VB violate) (NP (PRP$ his) (NNS rights)))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ replies) (SBAR (S (S (VP (VBG making) (NP (NP (DT a) (NN debate)) (PP (IN on) (NP (NN something)))))) (VP (AUX is) (RB not) (VP (VBG violating) (NP (NP (NN someone) (POS 's)) (NNS rights))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ restates) (SBAR (S (NP (PRP$ his) (NNS rights)) (VP (AUX are) (VP (AUXG being) (VP (VBN violated))))))) (. .)))

