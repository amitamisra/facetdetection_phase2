(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ mentions) (S (NP (NP (DT an) (NN article)) (PP (IN about) (NP (DT the) (NNP Racine) (NN mayor)))) (VP (AUXG being) (VP (VBN arrested) (PP (IN in) (NP (DT a) (NN sex) (NN sting))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ asks) (SBAR (WHNP (WP what)) (S (NP (DT that)) (VP (AUX has) (S (VP (TO to) (VP (AUX do) (PP (IN with) (NP (NNP Ted) (NNP Nugent)))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (AUX 's) (NP (NP (DT a) (JJ former) (NN mayor)) (SBAR (WHNP (WDT that)) (S (VP (VBD told) (NP (NNS people)) (SBAR (S (NP (PRP they)) (VP (MD should) (RB 't) (VP (AUX have) (VP (VP (VP (NNS guns)) (CC and) (VP (AUX was) (VP (VBN busted) (PP (IN on) (NP (NN child) (NN sex) (NNS charges)))))) (CC and) (VP (AUX is) (ADVP (RB now)) (NP (DT a) (VBN convicted) (NN felon)))))))))))))))) (. .)))

(S1 (S (ADVP (RB Simply)) (VP (VB put) (, ,) (ADVP (DT no) (NN matter) (SBAR (WHNP (WP who)) (S (VP (AUX is) (VP (VBN put) (PRT (RP up)) (PP (IN as) (NP (NP (DT an) (NN example)) (PP (IN of) (NP (NP (NN evil)) (SBAR (S (NP (PRP he)) (VP (AUX has) (NP (NP (NN someone)) (ADJP (ADJP (JJR worse)) (SBAR (S (VP (TO to) (VP (VB counter)))))))))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VP (VBZ asks) (SBAR (WHADVP (WRB why)) (S (NP (NNP Author1)) (VP (AUX is) (VP (VBG coming) (PRT (RP up)) (PP (IN with) (NP (DT this)))))))) (CC and) (VP (VBZ asks) (SBAR (WHNP (WP who)) (S (VP (AUX 's) (VP (VBG talking) (PP (IN about) (S (VP (VBG taking) (PRT (RP away)) (NP (NNS guns))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (AUX 's) (VP (VBG talking) (PP (IN about) (NP (NP (DT a) (NN coward)) (SBAR (WHNP (WDT that)) (S (VP (VBZ supports) (NP (DT the) (NN NRA)))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (DT the) (NN mayor)) (VP (AUX is) (NP (NP (DT a) (NN child) (NN rapist)) (SBAR (WHNP (WDT that)) (S (VP (VP (VBZ wants) (S (VP (TO to) (VP (VB take) (PRT (RP away)) (NP (NNS guns)))))) (CC and) (VP (VBZ asks) (SBAR (IN if) (S (NP (PRP it)) (VP (AUX 's) (ADJP (JJ nice)) (S (VP (TO to) (VP (AUX have) (NP (RB so) (RB much)) (PP (IN in) (NP (NP (NN common)) (PP (IN with) (NP (NP (NN someone)) (PP (IN like) (NP (DT that)))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ asks) (SBAR (IN if) (S (NP (PRP he)) (VP (VBZ knows) (SBAR (WHNP (WP what)) (S (NP (PRP he)) (VP (AUX 's) (VP (VBG talking) (PP (IN about)))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VP (VBZ says) (SBAR (S (NP (DT no) (NN party)) (VP (AUX is) (ADJP (JJ perfect)))))) (CC but) (VP (TO to) (VP (VB hear) (S (NP (NNP Author2)) (VP (NN talk) (SBAR (S (NP (PRP you)) (VP (MD would) (VP (VB think) (SBAR (S (NP (NNS liberals)) (VP (AUX are) (ADJP (JJ holy)))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ asks) (SBAR (IN if) (S (NP (NNP Author1)) (ADVP (RB really)) (VP (VBZ thinks) (SBAR (S (NP (NNP Obama)) (VP (AUX is) (ADJP (JJ liberal))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ thinks) (SBAR (S (S (NP (PRP he)) (VP (AUX is))) (CC and) (S (NP (NNS liberals)) (VP (AUX have) (VP (VP (VBN reduced) (NP (JJ moral) (NNS standards))) (CC and) (VP (NN worship) (NP (DT the) (NN government))))))))) (. .)))

