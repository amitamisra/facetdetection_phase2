(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (IN that) (S (NP (PRP you)) (VP (AUX need) (NP (DT a) (NN warrant)) (S (VP (TO to) (VP (VB search) (NP (DT a) (NN home) (CC or) (NN car)) (, ,) (SBAR (IN unless) (S (NP (PRP you)) (VP (VB consent) (PP (TO to) (NP (DT a) (NN search))))))))))))) (. .)))

(S1 (S (SBAR (IN Because) (S (NP (DT the) (NN man)) (VP (AUX was) (SBAR (IN within) (S (VP (AUX was) (PP (IN within) (NP (NP (CD 1000) (NNS feet)) (PP (IN of) (NP (DT a) (NN school))))))))))) (, ,) (NP (PRP he)) (VP (AUX was) (VP (VBN expelled) (PP (IN for) (S (VP (AUXG having) (NP (NNS shotguns)) (PP (IN in) (NP (PRP$ his) (NN trunk)))))))) (. .)))

(S1 (S (NP (EX There)) (VP (AUX are) (NP (DT no) (NNS exceptions) (SBAR (IN that) (S (NP (NP (JJ loaded) (NNS firarms)) (PP (IN near) (NP (NNS schools)))) (VP (AUX are) (ADJP (JJ illegal))))))) (. .)))

(S1 (S (NP (DT The) (NN student)) (VP (AUX was) (RB not) (VP (VP (VBN charged) (PP (IN with) (NP (DT a) (NN crime)))) (, ,) (CONJP (CC but) (RB rather)) (VP (VBN expelled) (PP (IN from) (NP (NN school))) (SBAR (IN after) (S (NP (DT the) (NN school)) (VP (VBD searched) (NP (PRP$ his) (NN car)))))))) (. .)))

(S1 (S (NP (DT The) (NN school)) (VP (VBD asked) (NP (DT the) (NNS cops)) (S (VP (TO to) (VP (VB search) (NP (DT the) (NN car)))))) (. .)))

(S1 (S (NP (DT The) (NN law)) (VP (VBD allowed) (S (NP (DT the) (NN school)) (VP (TO to) (VP (VB search) (NP (PRP$ his) (NN car)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (IN that) (S (NP (DT the) (NN student)) (VP (AUX was) (RB not) (S (VP (VBG breaking) (NP (DT any) (NNS laws)))))))) (. .)))

(S1 (S (S (NP (PRP$ His) (NN shotgun)) (VP (AUX was) (UCP (ADJP (JJ unloaded)) (CC and) (PP (IN in) (NP (DT the) (NN car)))))) (, ,) (CC and) (S (NP (PRP he)) (VP (AUX was) (RB not) (S (VP (VBG breaking) (NP (NP (DT any) (NN law)) (CC or) (RB else)) (SBAR (S (NP (PRP he)) (VP (MD would) (VP (AUX have) (VP (AUX been) (VP (VBN arrested))))))))))) (. .)))

(S1 (S (NP (EX There)) (VP (AUX is) (NP (NP (DT an) (NN exception)) (PP (IN in) (NP (NNP California) (NN law))) (SBAR (WHNP (WDT that)) (S (VP (VBZ says) (SBAR (S (NP (NP (JJ unloaded) (JJ long) (NNS guns)) (PP (IN in) (NP (NP (DT the) (NN trunk)) (PP (IN of) (NP (DT a) (NN car)))))) (VP (AUX are) (ADJP (JJ acceptable)))))))))) (. .)))

(S1 (S (NP (DT The) (NN school) (NN district)) (VP (MD should) (RB not) (VP (AUX have) (VP (VBD searched) (NP (PRP$ his) (NN car))))) (. .)))

(S1 (S (S (NP (DT The) (NN police)) (VP (MD should) (RB not) (VP (AUX have) (VP (VBD searched) (NP (PRP$ his) (NN car)))))) (, ,) (CC and) (S (NP (DT the) (NN school)) (VP (AUX was) (ADJP (JJ wrong) (S (VP (TO to) (VP (VB expel) (NP (PRP him)) (PP (IN for) (S (RB not) (VP (VBG breaking) (NP (NP (DT any) (NNS laws)) (ADVP (IN at) (DT all)))))))))))) (. .)))

