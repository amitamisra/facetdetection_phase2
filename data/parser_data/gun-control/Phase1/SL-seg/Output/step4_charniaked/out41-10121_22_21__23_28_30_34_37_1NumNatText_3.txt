(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ states) (SBAR (S (NP (NNP Heller)) (VP (VBD found) (SBAR (IN that) (S (NP (NP (DT the) (NNP English) (NNP Bill)) (PP (IN of) (NP (NNPS Rights)))) (VP (VBD allowed) (S (NP (NNS people)) (VP (TO to) (VP (VB keep) (NP (NP (NNS guns)) (PP (IN for) (NP (NN self) (NN defense)))))))))))))) (. .)))

(S1 (S (NP (DT A) (NN court) (NN brief)) (VP (VBD argued) (SBAR (S (NP (PRP he)) (VP (AUX was) (ADJP (JJ wrong)))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (DT the) (NN right) (S (VP (TO to) (VP (VB bear) (NP (NNS arms)))))) (VP (AUX was) (VP (VBN connected) (PP (IN with) (NP (NN militia))) (SBAR (IN so) (S (NP (DT the) (JJ second) (NN amendment)) (VP (AUX was) (RB not) (VP (VBG protecting) (NP (JJ ordinary) (NN citizen) (NNS rights))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (ADVP (RB just)) (IN because) (S (NP (NNS people)) (VP (VBD owned) (SBAR (S (NP (NNS guns)) (VP (AUX does) (RB not) (VP (VB prove) (SBAR (S (NP (PRP they)) (VP (AUX were) (VP (VBN protected) (S (VP (TO to) (VP (AUX do) (ADVP (RB so))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (AUX was) (RB not) (VP (VBG saying) (SBAR (S (NP (EX there)) (VP (AUX was) (NP (NP (DT a) (NN ban)) (PP (IN on) (NP (NP (NNS people)) (PP (IN from) (S (VP (VBG owning) (NP (NNS guns)))))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (S (VP (TO to) (VP (VB prove) (SBAR (S (NP (NP (NNS people)) (SBAR (WHNP (WP who)) (S (VP (AUX were) (RB not) (ADJP (JJ able) (S (VP (TO to) (VP (VB serve) (PP (IN in) (NP (NNS militias))))))))))) (VP (AUX were) (RB not) (VP (VBN allowed) (S (VP (TO to) (VP (VB own) (NP (NNS weapons))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (S (NP (NNP Author1)) (VP (AUX is) (VP (VBG arguing) (SBAR (IN that) (S (NP (RB only) (DT the) (NN militia)) (VP (AUX were) (VP (VBN protected) (PP (IN in) (S (VP (VBG owning) (NP (NNS guns)))))))))))) (, ,) (CC and) (S (NP (DT that)) (VP (VBZ means) (SBAR (S (NP (NP (NN anyone)) (ADJP (JJ unable) (S (VP (TO to) (VP (VB serve) (PP (IN in) (NP (DT a) (NN militia)))))))) (VP (MD would) (ADVP (RB then)) (VP (AUX be) (VP (VBN excluded) (PP (IN from) (S (VP (VBG owning) (NP (NNS guns))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (DT the) (NN fact) (SBAR (S (NP (NNS people)) (VP (AUX were) (VP (VBN allowed) (S (VP (TO to) (VP (VB own) (NP (NNS guns)))))))))) (VP (VBZ shows) (SBAR (SBAR (IN that) (S (NP (PRP they)) (VP (AUX were) (VP (VBN protected) (S (VP (TO to) (VP (AUX do) (ADVP (RB so))))))))) (, ,) (CC or) (SBAR (S (NP (NP (NNS people)) (ADVP (JJ outside) (PP (IN of) (NP (NN militia) (NNS members))))) (VP (MD would) (RB not) (VP (AUX have) (VP (AUX been) (VP (VBN allowed) (S (VP (TO to)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (DT this)) (VP (VBZ shows) (SBAR (S (NP (NP (NNP Author1) (POS 's)) (NNS statements) (SBAR (IN that) (S (NP (DT the) (JJ second) (NN amendment)) (VP (AUX does) (RB not) (VP (VB protect) (NP (NP (NN citizen) (NNS rights)) (PP (TO to) (NP (NN bear) (NNS arms))))))))) (VP (AUX are) (ADJP (JJ false))))))))) (. .)))

