(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (PP (IN before) (S (VP (VBG infringing) (PP (IN on) (NP (DT the) (NNP Constitution)))))) (, ,) (NP (PRP it)) (VP (MD should) (VP (AUX be) (VP (VBN determined) (SBAR (IN if) (S (NP (NN gun) (NNS controls)) (VP (AUX are) (ADJP (JJ effective))))))))))) (. .)))

(S1 (S (S (NP (PRP He)) (VP (VBZ cites) (NP (NP (CD two) (JJ other) (NNS countries)) (SBAR (WHNP (WP who)) (S (VP (AUX have) (NP (NN gun) (NN control) (NNS laws)))))))) (, ,) (CC and) (S (NP (NNS guns)) (VP (AUX are) (ADVP (RB still)) (ADJP (JJ cheap)) (ADVP (RB there) (PP (IN for) (NP (NNS criminals)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (S (NP (NN gun) (NN control)) (VP (AUX does) (RB not) (VP (VB reduce) (NP (NN crime))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (DT the) (NN purpose)) (VP (AUX is) (S (VP (TO to) (VP (VB reduce) (NP (NP (DT the) (NN supply)) (PP (IN of) (NP (NNS guns))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ cites) (NP (NP (JJR higher) (NNS prices)) (PP (IN for) (NP (NNS guns))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ mocks) (SBAR (IN that) (S (NP (NN money)) (VP (MD would) (VP (AUX be) (NP (NP (DT an) (NN issue)) (PP (IN for) (NP (NNS criminals))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ cites) (NP (NP (DT a) (NN case)) (SBAR (WHADVP (WRB where)) (S (NP (DT a) (ADJP (NP (CD 13) (NN year)) (JJ old))) (VP (AUX had) (NP (DT a) (JJ sub) (NN machine) (NN gun))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (S (NP (DT the) (NN gun)) (VP (AUX was) (NP (DT a) (NN replica)))) (, ,) (CC and) (S (NP (NNS police)) (VP (AUX are) (ADVP (RB still)) (VP (VBG examining) (SBAR (IN if) (S (NP (PRP it)) (VP (VBD fired) (NP (NNS bullets))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ argues) (NP (NP (DT another) (NN case)) (SBAR (S (VP (VBZ says) (SBAR (S (NP (NP (NNS weapons)) (VP (VBN preferred) (PP (IN by) (NP (NN gang) (NNS members))))) (VP (AUX were) (VP (VBN stolen) (PP (IN from) (NP (DT the) (NN military)))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ cites) (NP (NP (DT a) (NN report)) (PP (IN of) (NP (NP (DT a) (ADJP (NP (CD 15) (NN year)) (JJ old))) (VP (VBN killed) (PP (IN by) (NP (DT a) (NN machine) (NN gun)))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (DT no) (NNS laws)) (VP (AUX are) (ADJP (JJ capable) (PP (IN of) (S (VP (VBG preventing) (NP (NNS people)) (PP (IN from) (S (VP (VBG breaking) (NP (PRP them))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ argues) (SBAR (IN that) (S (PP (IN before) (S (VP (VP (VBG taking) (PRT (RP away)) (NP (NP (NNS people) (POS 's)) (NNS rights) (CC and) (NN property))) (CC and) (VP (VBG infringing) (PP (IN on) (NP (DT the) (NNP Constitution))))))) (, ,) (NP (EX there)) (VP (MD should) (VP (AUX be) (NP (JJ good) (NNS benefits))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (NNPS Americans)) (VP (AUX have) (VP (VP (VBN fought) (ADVP (RB hard))) (CC and) (VP (VBN given) (NP (PRP$ their) (NNS lives)) (S (VP (TO to) (VP (VB protect) (NP (DT those) (NNS rights))))))))))) (. .)))

