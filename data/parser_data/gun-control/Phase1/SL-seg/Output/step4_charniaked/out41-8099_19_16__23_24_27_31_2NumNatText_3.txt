(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ questions) (SBAR (IN whether) (CC or) (RB not) (S (S (VP (VBG allowing) (NP (NP (NNS guns)) (PP (IN on) (NP (NN school) (NNS campuses)))))) (VP (AUX is) (NP (DT a) (JJ good) (NN idea)))))) (. .)))

(S1 (S (S (NP (NNP Author2) (POS 's)) (VP (VBG referencing) (NP (NP (DT the) (NN unlikelihood)) (PP (IN of) (NP (NP (DT a) (VBG shooting) (NN range)) (VP (AUXG being) (NP (NP (DT the) (NN site)) (PP (IN of) (NP (NN gun) (NN violence)))) (PP (IN in) (NP (NP (NN response)) (PP (TO to) (NP (NP (NNS guns)) (PP (IN on) (NP (NN school) (NNS campuses)))))))))) (VP (VBG preventing) (NP (NN gun) (NN violence)) (PP (IN in) (NP (DT the) (JJ same) (NN way))))))) (VP (AUX is) (ADJP (RB completely) (JJ unrelated))) (. .)))

(S1 (S (NP (NN Gun) (NN club) (NNS members)) (VP (AUX are) (ADJP (JJ responsible) (PP (IN around) (NP (NN firearms))))) (. .)))

(S1 (S (NP (NP (DT Every) (NN person)) (PP (IN in) (NP (DT a) (NN school)))) (VP (MD will) (RB not) (ADVP (RB necessarily)) (VP (AUX be) (ADJP (RB as) (JJ experienced)))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VP (AUX is) (VP (VBG challenging) (NP (NP (NNP Author1) (POS 's)) (NNS statements)) (S (ADVP (RB just)) (VP (TO to) (VP (AUX be) (ADJP (JJ argumentative))))))) (CC and) (VP (AUX is) (VP (VBG providing) (NP (DT no) (JJ legitimate) (NN input)) (PP (TO to) (NP (DT the) (NN conversation)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ mentions) (NP (NP (DT the) (NN lack)) (PP (IN of) (NP (NN mass) (NNS shootings) (CC or) (NNS killings)))) (PP (IN at) (NP (VBG shooting) (NNS ranges)))) (. .)))

(S1 (S (NP (NP (DT A) (NN room)) (ADJP (JJ full) (PP (IN of) (NP (JJ fully-armed) (NNS citizens))))) (VP (AUX is) (RB not) (ADJP (JJ likely) (S (VP (TO to) (VP (AUX be) (NP (NP (DT the) (NN site)) (PP (IN of) (NP (NN gun) (NN violence))))))))) (. .)))

(S1 (S (S (VP (VBG Allowing) (NP (NP (NNS guns)) (PP (IN on) (NP (NN school) (NNS campuses)))))) (VP (MD would) (VP (AUX have) (NP (DT the) (JJ same) (NN effect)))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ provides) (NP (NP (DT no) (NNS statistics)) (VP (VBG validating) (NP (NN position))))) (. .)))

