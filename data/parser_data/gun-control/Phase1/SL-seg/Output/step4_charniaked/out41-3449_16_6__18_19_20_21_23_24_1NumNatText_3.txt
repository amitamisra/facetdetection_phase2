(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ asserts) (SBAR (SBAR (IN that) (S (NP (PRP he)) (VP (AUX has) (NP (NP (NNS years)) (PP (IN of) (NP (JJ tactical) (NNS weapons) (NN training))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX has) (VP (AUX been) (NP (DT a) (JJ Christian)) (PP (IN for) (NP (CD 29) (NNS years))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ responds) (PP (TO to) (NP (NNP Author1))) (PP (IN with) (NP (NP (DT a) (NN litany)) (PP (IN of) (NP (NP (NNS insults)) (, ,) (SBAR (WHNP (WDT which)) (S (VP (VBZ causes) (S (NP (NNP Author1)) (VP (TO to) (VP (VB state) (SBAR (IN that) (S (NP (PRP he)) (VP (MD will) (RB not) (VP (VB waste) (NP (PRP$ his) (NN time)) (S (VP (VBG refuting) (NP (DT the) (NNS insults))))))))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ tells) (NP (NNP Author2)) (SBAR (SBAR (IN that) (S (NP (PRP he)) (VP (VBD asked) (PP (IN for) (NP (PRP$ his) (NN opinion))) (PP (IN on) (NP (NP (DT another) (NN scripture)) (SBAR (WHNP (WDT that)) (S (VP (VBZ mentions) (NP (NNS weapons)))))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (RB not) (PP (IN against) (NP (NNP Author2)))))))) (. .)))

(S1 (S (S (NP (NNP Author2)) (VP (VBZ continues) (S (VP (TO to) (VP (VB berate) (NP (NNP Author1))))))) (, ,) (RB then) (S (NP (PRP he)) (VP (VBZ tells) (NP (NNP Author1)) (SBAR (IN that) (S (NP (PRP he)) (VP (VBD cannot) (S (VP (VB produce) (NP (DT a) (JJ single) (NN argument)) (S (VP (TO to) (VP (VB refute) (NP (PRP$ his) (NN stance)))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VP (VBZ calls) (S (NP (NNP Author2)) (ADJP (JJ arrogant)))) (, ,) (CC and) (VP (VBZ tells) (NP (PRP him)) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX is) (RB not) (VP (VBG trying) (S (VP (TO to) (VP (VB refute) (NP (PRP$ his) (NN stance))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VP (VBZ insults) (NP (NNP Author2))) (, ,) (CC and) (VP (VBZ tells) (NP (PRP him)) (SBAR (IN that) (S (NP (PRP he)) (VP (AUX has) (NP (NP (DT no) (NN idea)) (SBAR (WHADVP (WRB how)) (S (VP (TO to) (VP (VB debate))))))))))) (. .)))

(S1 (S (S (NP (NNP Author2)) (VP (VBZ expresses) (NP (NN disbelief)) (PP (IN over) (NP (DT the) (NN statement) (SBAR (IN that) (S (NP (NNP Author1)) (VP (VBD made) (SBAR (WHADVP (WRB where)) (S (NP (PRP he)) (VP (VBD claimed) (S (VP (TO to) (VP (VB agree) (PP (IN with) (NP (NNP Author2)))))))))))))))) (, ,) (CC and) (S (NP (PRP he)) (VP (VBZ tells) (NP (NNP Author1)) (SBAR (IN that) (S (NP (PRP they)) (ADVP (RB probably)) (VP (MD will) (RB not) (VP (VB agree) (PP (IN on) (NP (JJ other) (NNS issues))))))))) (. .)))

(S1 (S (S (NP (NNP Author2)) (VP (VBZ mentions) (NP (DT those) (NNS issues)))) (, ,) (CC and) (S (NP (NP (PRP they)) (NP (DT all))) (VP (VBP involve) (NP (NP (DT some) (NN form)) (PP (IN of) (NP (NN gun) (NN regulation)))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ reassures) (NP (NNP Author2)) (SBAR (IN that) (S (NP (PRP they)) (VP (MD will) (VP (VB agree))))) (, ,) (SBAR (IN because) (S (NP (PRP he)) (VP (NN dislikes) (NP (NP (NN gun) (NNS regulations)) (SBAR (WHNP (WDT that)) (S (VP (VB infringe) (PP (IN on) (NP (PRP$ his) (NNS rights))))))))))) (. .)))

