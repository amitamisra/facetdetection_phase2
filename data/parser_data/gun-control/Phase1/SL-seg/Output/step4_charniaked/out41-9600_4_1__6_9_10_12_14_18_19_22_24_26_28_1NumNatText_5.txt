(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (S (NP (NNP Author1)) (VP (VBZ thinks) (SBAR (S (NP (JJ rival) (NNS gangs)) (VP (AUX are) (VP (VBN forced) (S (VP (TO to) (VP (VB share) (NP (NNS guns))))))))))) (, ,) (NP (NN analysis)) (VP (VBZ suggests) (SBAR (S (NP (NNS guns)) (VP (AUX are) (ADJP (RB so) (JJ scarce)) (PP (IN in) (NP (DT the) (NNP UK))) (SBAR (IN that) (S (NP (PRP they)) (VP (AUX are) (VP (VBN rented) (PRT (RP out)) (PP (TO to) (NP (DT both) (NNS sides))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ thinks) (SBAR (S (NP (DT this)) (VP (AUX is) (NP (NP (DT an) (NN admission)) (PP (IN of) (NP (NN failure)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ asks) (SBAR (IN if) (S (NP (PRP it)) (VP (AUX 's) (PP (IN against) (NP (DT the) (NN law))) (S (VP (TO to) (VP (VB possess) (, ,) (VB borrow) (CC or) (VB loan) (NP (NP (DT a) (NN firearm)) (PP (IN in) (NP (DT the) (NNP UK))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (DT that)) (VP (AUX 's) (NP (PRP$ his) (NN point)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (PRP it)) (VP (AUX 's) (NP (PRP$ his)))))) (. .)))

(S1 (S (NP (NNP Author1) (POS 's)) (VP (AUX was) (ADVP (RB just)) (VP (VBG admitting) (NP (NN failure)))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ thinks) (SBAR (S (SBAR (IN if) (S (NP (DT a) (NN gang) (NN member)) (VP (VBZ wants) (S (VP (TO to) (VP (VB shoot) (NP (NN someone)))))))) (, ,) (S (VP (AUXG having) (S (VP (TO to) (VP (VB rent) (NP (DT a) (NN gun))))))) (VP (AUX is) (RB not) (NP (DT a) (NN failure)))))) (. .)))

(S1 (S (SBAR (IN If) (FRAG (ADVP (RB so)))) (, ,) (NP (PRP you)) (VP (MD should) (VP (AUX be) (ADJP (RB equally) (JJ lampooning)) (NP (DT the) (JJ recent) (NNP DC) (NNS statistics) (NN thread)) (SBAR (IN because) (S (NP (ADJP (RB newly) (VBN allowed)) (NNS guns)) (VP (AUX have) (RB not) (VP (VBN dropped) (NP (DT the) (NN crime) (NN rate)) (PP (TO to) (NP (CD 0))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (PRP it)) (VP (MD must) (VP (AUX be) (NP (NP (DT a) (NN handicap)) (SBAR (S (VP (TO to) (VP (VB go) (NP (JJ next) (NN door)) (S (VP (TO to) (VP (VB get) (NP (NN one))))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ thinks) (SBAR (S (NP (DT no) (NN law)) (VP (AUX is) (ADJP (NP (CD 100) (NN %)) (JJ successful)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (IN if) (S (NP (PRP you)) (VP (VBP turn) (PP (IN on) (NP (NP (PRP$ your) (NNS allies)) (SBAR (S (NP (PRP you)) (VP (MD can) (RB not) (VP (VB trust) (NP (PRP them)))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ claims) (SBAR (S (NP (NP (DT an) (NN increase)) (PP (IN in) (NP (NN crime))) (PP (JJ due) (ADVP (RB purely)) (TO to) (NP (JJ criminal) (NNS refugees)))) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN treated) (PP (IN as) (ADJP (JJ due) (PP (TO to) (NP (CD 2A))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (NNP Author1)) (VP (MD will) (VP (VB blame) (NP (NP (NNS citizens)) (CC and) (NP (NP (DT the) (NN availability)) (PP (IN of) (NP (NNS guns))))) (PP (IN for) (NP (JJ criminal) (NNS actions)))))))) (. .)))

