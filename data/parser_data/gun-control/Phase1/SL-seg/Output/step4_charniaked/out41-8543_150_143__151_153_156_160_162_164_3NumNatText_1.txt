(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ states) (SBAR (IN that) (S (NP (RB since) (DT the) (NNS airports)) (VP (AUX are) (ADJP (JJ able) (S (VP (TO to) (VP (VB carry) (PRT (RP out)) (NP (NP (NN security) (NNS measures)) (PP (JJ such) (IN as) (NP (NP (NNS searches)) (, ,) (NP (NN metal) (NNS detectors)) (, ,) (NP (NNS x-rays)) (CC and) (NP (NP (NNS limits)) (PP (IN on) (SBAR (WHNP (WP what)) (S (VP (MD can) (VP (AUX be) (VP (VBN carried) (PP (IN on) (SBAR (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB see) (SBAR (WHADVP (WRB why)) (S (NP (NNS DC)) (VP (MD can) (VP (AUX do) (NP (DT the) (JJ same)) (PP (IN with) (NP (PRP$ its) (NNS commuters))) (PP (IN on) (NP (DT a) (JJ daily) (NN basis)))))))))))))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ goes) (PP (IN on) (S (VP (VBG asking) (PP (IN for) (NP (NN proof) (SBAR (SBAR (IN that) (S (NP (NN gun) (NNS owners)) (VP (VBP want) (S (VP (TO to) (VP (VB repeal) (NP (NP (DT the) (JJ federal) (NN age) (NN limit)) (SBAR (WHNP (WDT that)) (S (VP (VBZ covers) (NP (NP (DT the) (NN purchase)) (PP (IN of) (NP (NN firearms)))))))))))))) (CC and) (SBAR (IN that) (S (NP (NP (NNS people)) (PP (IN on) (NP (DT the) (VBN prohibited) (NN list)))) (VP (TO to) (VP (AUX be) (VP (VBN allowed))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB then)) (VP (VBZ asks) (SBAR (WHNP (WP what) (NN evidence)) (S (NP (EX there)) (VP (AUX is) (SBAR (DT the) (S (NP (NP (DT no) (NN gun) (NN law)) (PP (IN in) (NP (NNP Great) (NNP Britain)))) (VP (AUX has) (VP (VBN worked))))))))) (. ?)))

(S1 (S (NP (NNP Author2)) (VP (VBZ states) (SBAR (IN that) (S (SBAR (IN because) (S (NP (NNS airports)) (VP (AUX are) (NP (JJ private) (NN property))))) (NP (PRP they)) (VP (MD can) (VP (AUX have) (NP (DT that) (NN security)) (, ,) (PP (IN for) (NP (NP (DT a) (NN city)) (SBAR (S (NP (NP (DT the) (NN size)) (PP (IN of) (S (VP (VBG DC) (NP (PRP it)))))) (VP (MD would) (VP (AUX be) (ADJP (ADJP (JJ expensive)) (CC and) (ADJP (JJ impossible)))))))))))))) (. .)))

(S1 (S (ADVP (RB Also)) (VP (MD would) (VP (AUX have) (S (VP (TO to) (VP (VB convince) (NP (NNS people)) (PP (IN of) (NP (NP (DT the) (NN constitutionality)) (PP (IN of) (NP (PRP it)))))))))) (. .)))

(S1 (S (NP (NNS Airports)) (VP (MD can) (VP (PP (IN because) (IN of) (NP (NP (DT the) (JJ unfounded) (NN fear)) (PP (IN of) (NP (NP (NN death)) (PP (IN by) (NP (JJ terrorist))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (IN that) (S (NP (DT no) (NN one)) (VP (VBZ believes) (SBAR (IN that) (S (S (NP (NN gun) (NN control)) (VP (MD will) (VP (VB stop) (NP (DT all) (NN gun) (NN crime))))) (CC but) (S (NP (PRP it)) (VP (MD will) (VP (VB slow) (NP (PRP it)) (PRT (RP down))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (AUX does) (VP (VB concede) (SBAR (IN that) (S (NP (DT both) (NNS sides)) (VP (AUX have) (NP (DT no) (JJ actual) (NN data)) (PP (IN for) (NP (NN proof)))))))) (. .)))

