(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NP (JJ Author1) (NNS states)) (ADJP (RBS most) (JJ firearms))) (VP (AUX are) (RB not) (VP (VBN recovered))) (. .)))

(S1 (S (NP (NP (QP (RB About) (CD 63)) (NN %)) (PP (IN of) (NP (NP (NNS guns)) (VP (VBN recovered) (PP (IN in) (NP (NNP Toronto)))))) (VP (VBN originated) (PP (IN in) (NP (DT the) (NP (PRP US)) (CC and) (NP (DT a) (JJ large) (NN number)))))) (VP (AUX have) (S (NP (PRP$ their) (JJ serial) (NNS numbers)) (VP (VBN removed)))) (. .)))

(S1 (S (S (PP (VBG According) (PP (TO to) (NP (NNP Royal) (VBN Mounted) (NN Police)))) (NP (CD 2,637) (NNS guns)) (VP (AUX were) (VP (VBN reported)))) (CC and) (S (NP (CD 925)) (VP (AUX were) (VP (VBN linked) (PP (TO to) (NP (NP (DT a) (NN dealer)) (, ,) (CC and) (NP (NP (CD 3/4)) (PP (IN of) (NP (NP (DT those)) (VP (VBN originated) (PP (IN in) (NP (DT the) (NNP US)))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ asks) (SBAR (WHADVP (WRB how)) (S (NP (PRP they)) (VP (AUX were) (VP (VBN linked) (PP (TO to) (NP (DT the) (PRP US))) (SBAR (IN if) (S (NP (DT the) (JJ serial) (NNS numbers)) (VP (AUX had) (VP (AUX been) (VP (VBN removed))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (DT those)) (VP (AUX were) (RB not) (NP (NP (NN part)) (PP (IN of) (NP (DT the) (JJ traceable) (NN group)))) (SBAR (IN unless) (S (VP (VBN related) (PP (TO to) (NP (NNS ballistics)))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ thinks) (SBAR (S (NP (DT this)) (VP (AUX is) (ADJP (JJ ridiculous)))))) (. .)))

(S1 (S (NP (VBN Failed) (NNS ballistics) (NNS tests)) (VP (AUX are) (RB not) (ADJP (JJ traceable))) (. .)))

(S1 (S (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (JJ comprehensive) (NNS statistics)) (VP (AUX are) (ADJP (JJ scarce))))))) (, ,) (CC but) (S (NP (JJ recent) (NNS cases)) (VP (VBP point) (PP (TO to) (NP (DT the) (NN problem))))) (. .)))

(S1 (S (NP (PRP$ His) (NN point)) (VP (AUX is) (SBAR (RB even) (IN with) (S (NP (DT the) (JJ serial) (NNS numbers)) (VP (VBD removed) (, ,) (S (NP (DT some)) (VP (MD can) (ADVP (RB still)) (VP (AUX be) (VP (VBN traced))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (AUX does) (RB not) (VP (VB see) (NP (NP (NN anything)) (SBAR (WHNP (WDT that)) (S (VP (VBZ suggests) (SBAR (S (S (NP (NNS ballistics)) (VP (VBD determined) (NP (NN anything)))) (CC and) (S (NP (EX there)) (VP (AUX 's) (NP (NP (DT no) (NN mention)) (PP (IN of) (S (VP (VBG testing) (NP (DT the) (NNS slugs))))))))))))))))))) (. .)))

(S1 (S (ADVP (RB Furthermore)) (, ,) (NP (NNP BD)) (VP (MD can) (ADVP (RB only)) (VP (VB compare) (NP (NP (NNS rounds)) (VP (VBN linked) (PP (TO to) (NP (DT a) (NN crime))))) (, ,) (SBAR (IN so) (S (NP (DT no) (JJ random) (NN match)) (VP (AUX is) (ADJP (JJ possible))))))) (. .)))

(S1 (S (NP (RB Even) (DT that)) (VP (AUX is) (ADJP (JJ negligible))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (ADVP (RB never)) (VP (VBD suggested) (NP (NP (NN anything)) (PP (IN about) (NP (NP (JJ ballistics) (NNS databases)) (, ,) (SBAR (S (NP (NNP Author2)) (VP (AUX did))))))))))) (. .)))

(S1 (S (NP (PRP He)) (ADVP (RB initially)) (VP (VBD used) (NP (NP (DT the) (NN term) (NNS ballistics)) (PP (RB rather) (IN than) (NP (NNS forensics))))) (. .)))

