(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ states) (SBAR (IN that) (S (SBAR (IN since) (S (NP (DT the) (NNP US)) (VP (AUX is) (ADVP (RB now)) (VP (VBG banning) (NP (NNS guns)))))) (, ,) (NP (NP (DT the) (JJ next) (JJ natural) (NN step)) (, ,) (VP (ADVP (RB as)) (VBN seen) (PP (IN in) (NP (NP (NNP Canada)) (, ,) (NP (NNP England)) (CC and) (NP (NNP Australia))))) (, ,)) (VP (MD may) (VP (AUX be) (NP (NP (NN confiscation)) (CC or) (NP (DT a) (NN government) (NN buy-back)))))))) (. .)))

(S1 (S (NP (DT These)) (VP (AUX are) (NP (NP (DT the) (QP (RB only) (CD two)) (NNS ways)) (SBAR (S (VP (TO to) (VP (VB get) (NP (NNS guns)) (PP (IN off) (IN of) (NP (DT the) (NN street))))))))) (. .)))

(S1 (S (NP (DT A) (NNS confiscations)) (VP (AUX is) (ADVP (RB essentially)) (VP (VBG creating) (NP (DT a) (NN police) (NN state)))) (. .)))

(S1 (S (NP (DT A) (NN buy-back)) (VP (MD will) (ADVP (RB significantly)) (VP (VB damage) (NP (DT the) (NN economy)))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (PRP they)) (VP (VB melt) (PRT (RP down)) (NP (DT the) (NNS guns)) (ADVP (RB down))))) (NP (PRP it)) (VP (AUX 's) (VP (VBG burning) (NP (NN money) (CC and) (NNS resources)))) (. .)))

(S1 (S (PP (IN With) (NP (DT the) (NNP US) (NN economy) (SBAR (IN as) (S (NP (PRP it)) (VP (AUX is)))))) (, ,) (NP (PRP we)) (VP (AUX need) (S (VP (TO to) (VP (AUX be) (VP (VP (VBG creating) (NP (NNS jobs))) (CC and) (RB not) (VP (VBG wasting/throwing) (PRT (RP away)) (NP (NN money)))))))) (. .)))

(S1 (S (S (NP (NNP Author1)) (VP (VBZ acknowledges) (SBAR (S (NP (DT the) (JJ Canadian) (NN government)) (VP (VP (AUX has) (NP (DT a) (JJ buy-back) (NN program))) (CC and) (VP (AUX does) (RB not) (VP (VB seize) (NP (PRP them))))))))) (, ,) (CC but) (S (NP (NP (PRP they)) (NP (NP (NN trade-in) (NNS rates)) (VP (VBN compared) (PP (TO to) (NP (NP (DT the) (NN FMV)) (PP (IN of) (NP (DT the) (NNS guns)))))))) (VP (AUX is) (ADJP (JJ abysmal)))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VP (AUX is) (PP (IN from) (NP (NNP Canada)))) (CC and) (VP (VBZ argues) (SBAR (IN that) (S (NP (NNP Canada)) (VP (AUX has) (RB not) (ADVP (RB simply)) (VP (VBN confiscated) (NP (PRP$ its) (NNS guns)) (, ,) (SBAR (IN as) (S (NP (NNP Author1)) (VP (VBD suggested) (, ,) (S (NP (PRP it)) (ADVP (RB merely)) (VP (VB put) (NP (DT a) (NN restriction)) (PP (IN on) (S (VP (VBG importing) (NP (NN and/or)) (S (VP (VBG buying) (NP (JJ new) (NNS ones)))))))))))))))))) (. .)))

(S1 (S (NP (PRP You)) (VP (AUX are) (VP (VBN allowed) (S (VP (TO to) (VP (VB keep) (NP (NP (DT the) (NNS weapons)) (SBAR (S (NP (PRP you)) (VP (VBP own)))))))))) (. .)))

(S1 (S (S (NP (DT This)) (ADVP (RB all) (RB only)) (VP (VBZ applies) (PP (TO to) (NP (NNS handguns))) (ADVP (RB anyway)))) (, ,) (NP (NN hunting) (NNS weapons)) (VP (AUX are) (RB not) (NP (NP (DT a) (NN part)) (PP (IN of) (NP (DT the) (NN restriction))))) (. .)))

(S1 (S (NP (JJ Illegal) (NNS weapons)) (VP (AUX have) (NP (DT no) (NN FMV))) (. .)))

(S1 (S (NP (NN Compensation)) (VP (AUX is) (ADJP (RB sometimes) (JJ generous))) (. .)))

