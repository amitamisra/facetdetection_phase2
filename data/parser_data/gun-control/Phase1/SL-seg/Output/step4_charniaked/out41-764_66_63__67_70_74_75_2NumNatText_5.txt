(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ believes) (SBAR (S (NP (DT the) (NN right) (S (VP (TO to) (VP (VB carry) (NP (DT a) (NN gun)))))) (VP (AUX is) (RB not) (NP (NP (NP (DT a) (JJ natural) (NN right)) (PP (IN for) (NP (NNS citizens)))) (CONJP (CC but) (RB rather)) (NP (NP (DT a) (NN tool)) (PP (IN of) (NP (NP (NN protection)) (PP (IN for) (NP (NNS states))) (PP (IN against) (NP (NN tyranny))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ believes) (SBAR (S (NP (NP (DT the) (NNP US) (NN constitution/) (NN Bill)) (PP (IN of) (NP (NNPS Rights)))) (VP (AUX is) (NP (NP (DT a) (NN contract)) (PP (IN with) (NP (PRP US) (NNS citizens)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ implies) (SBAR (IN that) (S (NP (NNP Author2)) (VP (AUX does) (RB not) (VP (VB know) (NP (NP (JJ constitutional) (NN history)) (CC or) (NP (PRP$ its) (NN application) (S (VP (TO to) (VP (VB 2A) (NP (CC and) (NNP Bill) (CC or) (NNP Rights) (NNS laws)))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ reminds) (NP (NNP Author1)) (SBAR (IN that) (S (NP (DT the) (NNP US)) (VP (VBD made) (NP (PRP$ their) (NX (NX (JJ own) (NNS laws)) (, ,) (NX (NN constitution)) (CC and) (NX (NX (NNP Bill)) (PP (IN of) (NP (NNPS Rights)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ argues) (SBAR (S (NP (PRP US) (NNS citizens)) (VP (AUX have) (NP (NP (JJ natural) (NNS rights)) (VP (VBN guaranteed) (PP (IN by) (NP (NP (DT the) (JJ constitution/) (NNP Bill)) (PP (IN of) (NP (NNPS Rights))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ insults) (NP (NNP Author1)) (PP (IN by) (S (VP (VBG suggesting) (SBAR (S (NP (PRP he)) (VP (VB take) (NP (PRP$ his) (NN thinking)) (ADVP (RB elsewhere)) (SBAR (IN because) (S (NP (PRP$ his) (NN thinking)) (VP (MD would) (VP (VB throw) (NP (DT the) (NN world)) (PP (IN into) (NP (NN slavery)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ reminds) (NP (NNP Author1)) (SBAR (S (NP (NP (DT the) (NNS creators)) (PP (IN of) (NP (NP (DT the) (NN constitution/Bill)) (PP (IN of) (NP (NNPS Rights)))))) (VP (VBD knew) (SBAR (WHNP (RB exactly) (WP what)) (S (NP (PRP they)) (VP (AUX were) (VP (VBG doing))))))))) (. .)))

