(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ sees) (NP (DT the) (NN situation)) (PP (IN as) (NP (NP (DT a) (JJ clear) (NN argument)) (PP (IN for) (NP (NN gun) (NN ownership)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (AUX does) (RB not) (VP (VB agree) (SBAR (IN because) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT no) (NN fairness)) (PP (IN in) (S (VP (VBG dealing) (PP (IN with) (NP (DT a) (JJ terrorist)))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (AUX does) (RB not) (VP (VB believe) (SBAR (IN that) (S (NP (NNS rights)) (VP (MD should) (VP (AUX be) (VP (VBN restricted) (PP (VBN based) (PP (IN on) (NP (NN trust)))) (SBAR (IN as) (S (NP (NP (DT the) (NN responsibility)) (PP (IN for) (NP (NP (CD one) (POS 's)) (NN destiny)))) (VP (VBZ lies) (PP (IN with) (NP (DT the) (NN individual) (PRP himself))))))))))))) (. .)))

(S1 (S (PP (VBG According) (PP (TO to) (NP (NNP Author2)))) (, ,) (NP (NNS rights)) (VP (AUX are) (VP (VBN waived) (PP (ADVP (RB only)) (IN for) (NP (VBN convicted) (NNS criminals))))) (. .)))

(S1 (S (S (NP (NNP Author1)) (VP (VBZ disagrees) (SBAR (IN as) (S (NP (DT the) (NNS planes)) (VP (AUX are) (PP (IN for) (NP (NN business) (NNS purposes)))))))) (, ,) (CC and) (S (NP (NNS guns)) (VP (MD should) (RB not) (VP (AUX be) (VP (VBN allowed))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (AUX is) (VP (VBN offended) (PP (IN by) (NP (NP (DT the) (NN acceptance)) (PP (IN of) (NP (NN gun) (NN ownership) (NN restriction))))) (SBAR (WHADVP (WRB when)) (S (NP (JJ many) (NNS ancestors)) (VP (VBD perished) (SBAR (IN if) (RB only) (S (VP (TO to) (VP (VB ensure) (SBAR (IN that) (S (NP (DT the) (JJ 2nd) (NN Amendment)) (VP (VBZ stands))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ disapproves) (S (NP (DT the) (NN claim)) (ADVP (RB once) (RB again)) (PP (IN by) (S (VP (VBG pointing) (PRT (RP out)) (SBAR (IN that) (S (NP (NP (DT the) (NN presence)) (PP (IN of) (NP (NNS weapons)))) (ADVP (RB only)) (VP (VBZ increases) (NP (NN instability)) (PP (RB instead) (IN of) (S (VP (VBG providing) (NP (NN security))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ sees) (NP (DT this)) (PP (IN as) (NP (NP (DT a) (NN restriction)) (PP (IN of) (NP (DT an) (JJ individual) (NN right)))))) (. .)))

