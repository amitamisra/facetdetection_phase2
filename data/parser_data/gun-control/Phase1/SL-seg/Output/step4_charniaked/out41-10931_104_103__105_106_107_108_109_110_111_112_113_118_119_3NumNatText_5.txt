(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VP (VBZ quotes) (NP (NP (DT a) (NN headline)) (PP (IN about) (NP (QP (RB about) (CD AK)) (NNS records) (NNS checks))))) (CC and) (VP (VBZ thinks) (SBAR (S (NP (JJ illegal) (, ,) (JJ unlawful) (CC and) (JJ unconstitutional) (NNS acts)) (VP (AUX are) (ADVP (RB still)) (ADJP (JJ ongoing))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ thinks) (SBAR (S (NP (DT this)) (VP (VP (AUX is) (NP (JJ right-wing) (NN conspiracy))) (CC and) (VP (VBZ asks) (SBAR (WHNP (WP what)) (S (VP (AUX is) (NP (NP (PRP$ his) (NN responsibility)) (SBAR (WHADVP (WRB when)) (S (VP (VBD asked) (S (VP (TO to) (VP (VB trace) (NP (DT a) (NN gun))))))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (PRP they)) (VP (AUX are) (RB not) (VP (VBG tracing) (NP (NNS guns)))))) (, ,) (SBAR (S (NP (PRP they)) (VP (AUX are) (VP (VBG trying) (S (VP (TO to) (VP (VB seize) (NP (NP (DT the) (JJ entire) (NNS dealers) (POS ')) (NNS records)))))))))) (. .)))

(S1 (S (NP (VBN Bound) (NNS volumes)) (ADVP (RB always)) (VP (VP (VBP stay) (PP (IN with) (NP (DT the) (NN dealer)))) (CC and) (VP (ADVP (RB never)) (VBP leave) (NP (DT the) (NN shop)))) (. .)))

(S1 (S (S (NP (PRP They)) (VP (MD can) (VP (AUX be) (VP (VBN examined) (PP (IN on) (NP (NNS premises))))))) (CC but) (S (NP (NN cannot)) (VP (VP (AUX be) (VP (VBN taken) (PP (IN from) (NP (DT the) (NN shop))) (PP (IN under) (NP (DT any) (NN circumstance))))) (CC and) (VP (AUX is) (NP (NP (DT a) (NN violation)) (PP (IN of) (NP (NN law))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ disagrees) (PP (IN with) (NP (NP (NNP Author1) (POS 's)) (NNS assertions)))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (NNS posts) (NP (NP (DT a) (NN link)) (PP (IN about) (NP (NP (NNS atf) (POS 's)) (NNS records)))) (SBAR (S (NP (PRP they)) (VP (AUX are) (ADVP (RB illegally)) (VP (VBG withholding) (PP (IN from) (NP (NNP congress)))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ thinks) (SBAR (S (NP (DT this)) (VP (AUX is) (ADJP (RBR more) (JJ nonsense)))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (NNP Author2)) (VP (AUX is) (VP (VBG defending) (NP (JJ unconstitutional) (NNS acts))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ asks) (SBAR (WHADVP (WRB how)) (S (S (VP (VBG asking) (PP (IN for) (NP (NNS copies))))) (VP (VBZ turns) (PP (IN into) (NP (NP (DT an) (JJ illegal) (NN confiscation)) (PP (IN of) (NP (VBN bound) (NNS ledgers))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (PRP it)) (VP (AUX 's) (PP (IN in) (NP (NP (NN contempt)) (PP (IN of) (NP (NNP congress))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (DT no) (JJ such) (NNS statues)) (VP (AUX have) (VP (AUX been) (VP (VBN quoted))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (VBD quoted) (NP (NNP congress)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (S (NP (DT the) (NN ATF)) (VP (VBD asked) (PP (IN for) (NP (NP (NNS copies)) (, ,) (RB not) (NP (DT the) (NNS volumes)))))) (, ,) (CC and) (S (NP (DT the) (NNS sellers)) (VP (AUX are) (VP (VBN bound) (PP (IN by) (NP (NN law))) (S (VP (TO to) (VP (VB comply)))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (AUX does) (RB not) (VP (VB agree))) (. .)))

