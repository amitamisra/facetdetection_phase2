(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (PRP we)) (VP (VBD investigated) (NP (NP (DT the) (JJ possible) (NN relationship)) (PP (IN between) (S (VP (AUXG being) (NP (NP (NN shot)) (CC and) (NP (NP (NN possession)) (PP (IN of) (NP (DT a) (NN gun))))))))))))) (. .)))

(S1 (S (NP (PRP We)) (VP (VBD enrolled) (NP (CD 677) (NNS participants)) (SBAR (IN that) (S (VP (AUX had) (VP (AUX been) (VP (VBN shot) (PP (IN in) (NP (NP (DT an) (NN assault)) (CC and) (NP (CD 684) (JJ population-based) (NN control) (NNS participants)))))))))) (. .)))

(S1 (S (NP (PRP We)) (VP (VBD adjusted) (NP (NP (NNS odds) (NNS ratios)) (PP (IN for) (NP (VBG confounding) (NNS variables))))) (. .)))

(S1 (S (NP (NP (NNS Individuals)) (PP (IN with) (NP (DT a) (NN gun)))) (VP (AUX were) (ADJP (ADJP (QP (CD 4.46) (NNS times)) (RBR more) (JJ likely)) (SBAR (S (VP (TO to) (VP (AUX be) (ADJP (VBN shot) (PP (IN than) (NP (DT those)))) (PP (IN without) (NP (CD one))))))))) (. .)))

(S1 (S (SBAR (WHADVP (WRB When)) (S (NP (DT the) (NN victim)) (VP (MD could) (VP (VB resist))))) (, ,) (NP (DT this) (NN ratio)) (VP (VBN increased) (PP (TO to) (NP (CD 5.45)))) (. .)))

(S1 (S (NP (NNS Guns)) (VP (AUX did) (RB not) (VP (VB protect) (NP (NNS people)) (PP (IN from) (S (VP (AUXG being) (VP (VBD shot))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (NNS men)) (VP (AUX are) (ADJP (RBR more) (JJ likely) (S (VP (TO to) (VP (AUX be) (VP (VBN struck) (PP (IN by) (S (VP (VBG lightening))))))))))))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX 's) (ADJP (JJ fatal)) (S (VP (TO to) (VP (VB think) (SBAR (S (NP (NNS criminals)) (VP (MD will) (VP (AUX be) (ADJP (JJ sympathetic)) (SBAR (WHADVP (WRB when)) (S (NP (NNS guns)) (VP (AUX are) (VP (VBN removed))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ thinks) (SBAR (S (NP (DT this) (NN conclusion)) (VP (AUX is) (ADJP (VBN flawed)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ thinks) (SBAR (S (NP (NNP Author1) (POS 's)) (VP (AUX are) (ADJP (JJ flawed)))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ asks) (S (VP (TO to) (VP (VB see) (S (NP (DT a) (NN study)) (ADVP (WRB where)) (VP (AUXG having) (NP (NP (DT a) (NN gun)) (SBAR (S (VP (VBZ produces) (NP (DT a) (JJ favorable) (NN outcome)))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (AUX 's) (VP (VBN proven) (S (NP (NNS millions)) (VP (VB defend) (NP (PRP themselves)))) (PP (IN with) (NP (NNS guns)))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (DT that)) (VP (MD would) (VP (VB include) (NP (NP (NNS people)) (SBAR (WHNP (WDT that)) (S (VP (VBP get) (NP (NP (DT a) (NN gun)) (PP (IN for) (NP (DT a) (NN noise))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (AUX does) (RB not) (VP (VB see) (NP (DT the) (NN point)))) (. .)))

(S1 (S (ADVP (RB Author1)) (NP (PRP it)) (VP (AUX does) (RB not) (VP (VB count) (SBAR (IN if) (S (NP (DT no) (NN one)) (VP (AUX 's) (ADVP (RB there))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (NN someone)) (VP (MD could) (VP (AUX have) (VP (AUX been) (ADVP (RB there)))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (DT the) (JJ annual) (NNP DGU) (NN claim)) (VP (AUX is) (VP (VBN based) (PP (IN on) (NP (NP (DT a) (NN tele-survey)) (PP (IN of) (NP (CD 1500) (NNPS Americans)))))))))) (. .)))

