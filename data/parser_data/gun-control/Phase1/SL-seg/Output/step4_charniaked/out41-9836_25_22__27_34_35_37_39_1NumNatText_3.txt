(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1) (CC and) (NNP Author2)) (VP (AUX are) (VP (VBG discussing) (NP (NP (NN death)) (PP (IN by) (NP (NP (NN perpetrator) (NNS fatalities)) (PP (IN of) (NP (NN police) (NNS officers)))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ feels) (SBAR (S (NP (NP (JJS most)) (PP (IN of) (NP (DT these) (NNS incidents)))) (VP (VBP stem) (PP (IN from) (NP (NN drug) (NN use))) (, ,) (SBAR (IN while) (S (NP (NNP Author2)) (VP (VBZ believes) (SBAR (S (NP (NP (JJ many)) (PP (IN of) (NP (DT these) (NNS acts)))) (VP (AUX are) (NP (JJ planned) (NNS attacks)))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ cites) (NP (NP (DT an) (NN investigation)) (PP (IN into) (NP (NP (DT a) (NNP Midwest) (NN militia) (NN group)) (SBAR (WHNP (WDT which)) (S (VP (AUX was) (VP (VBN alleged) (S (VP (TO to) (VP (AUX have) (VP (VBN planned) (S (VP (TO to) (VP (VB kill) (NP (NNS officers))))))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ feels) (SBAR (S (S (VP (VBG blaming) (NP (NN militia) (NNS groups)))) (VP (AUX is) (NP (NP (DT the) (NN attempt)) (PP (IN at) (NP (NP (DT a) (NN cover)) (UCP (ADVP (RB up)) (CC and) (S (VP (TO to) (VP (VB draw) (NP (NN attention)) (ADVP (RB away) (PP (IN from) (NP (NNP Islamic) (NNS connections))))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ advises) (SBAR (S (NP (PRP he)) (VP (VP (AUX did) (RB not) (VP (VB blame) (NP (DT a) (NN militia) (NN group)))) (, ,) (CC but) (VP (AUX did) (VP (VB cite) (NP (NP (DT the) (NN investigation)) (VP (ADVP (RB currently)) (VBG taking) (NP (NN place)))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ compares) (NP (DT this)) (PP (TO to) (NP (NP (DT the) (NNP Branch) (NNP Davidian) (NN case)) (SBAR (WHPP (IN in) (WHNP (WDT which))) (S (NP (DT a) (NN prosecutor)) (VP (MD would) (RB not) (VP (VB allow) (NP (JJ armed) (JJ federal) (NNS investigators)) (PP (IN in) (NP (DT the) (NNS proceedings)))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ advises) (SBAR (S (NP (DT a) (NN person)) (VP (AUX is) (S (VP (TO to) (VP (AUX be) (VP (VBN presumed) (S (ADJP (JJ innocent))) (SBAR (IN until) (S (VP (VBN proven) (S (S (ADJP (JJ guilty)) (PP (IN beyond) (NP (NN doubt)))) (CC and) (S (NP (PRP it)) (VP (AUX is) (ADVP (RB up) (PP (TO to) (NP (DT a) (NN jury)))) (S (VP (TO to) (VP (VB make) (NP (DT the) (NN decision)) (PP (IN as) (PP (TO to) (SBAR (IN whether) (CC or) (RB not) (S (NP (EX there)) (VP (AUX is) (NP (NP (DT a) (NN shadow)) (PP (IN of) (NP (NP (DT a) (NN doubt)) (VP (VBN related) (PP (TO to) (NP (NN guilt))))))))))))))))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ advises) (SBAR (IN that) (S (NP (NN shadow)) (VP (MD must) (VP (AUX be) (VP (VBN found) (PP (IN by) (NP (NP (DT a) (NN jury)) (PP (IN of) (NP (NP (DT the) (NN person) (POS 's)) (NNS peers))))) (PP (RB not) (IN by) (NP (NP (DT a) (NN judge)) (CC or) (NP (NN prosecutor)))))))))) (. .)))

