(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ argues) (SBAR (IN that) (S (S (NP (NP (DT the) (NN Preamble)) (PP (IN of) (NP (DT the) (NNP Constitution)))) (VP (AUX is) (RB not) (NP (NP (DT a) (NN law)) (NP (PRP itself))))) (, ,) (CC and) (S (NP (NP (DT the) (NNS purposes)) (VP (VBN stated) (PP (IN in) (NP (DT the) (NN Preamble))))) (VP (AUX are) (ADJP (ADJP (RB not) (JJ enough)) (SBAR (IN for) (S (NP (NNS people)) (VP (TO to) (VP (VB enact) (NP (NP (JJ social) (NNS changes)) (VP (VBN based) (PP (IN upon)))))))))))))) (. .)))

(S1 (S (NP (NNS Liberals)) (VP (VBP like) (S (VP (TO to) (VP (VB use) (NP (DT the) (NN Preamble)) (S (VP (TO to) (VP (VB defend) (NP (NP (PRP$ their) (NNS ideas)) (VP (VP (VBG relating) (PP (TO to) (NP (JJ social) (NN engineering)))) (CC and) (VP (VBG giving) (NP (JJ monetary) (NNS rewards)) (PRT (RP out)) (PP (TO to) (NP (NNS people)))))) (PP (IN for) (NP (NP (RB little)) (PP (TO to) (NP (DT no) (NN work)))))))))))) (. .)))

(S1 (S (SBAR (IN Because) (S (NP (NP (DT the) (NN Preamble)) (PP (TO to) (NP (DT the) (NNP Constitution)))) (VP (ADVP (RB only)) (VBZ introduces) (NP (DT the) (NNP Constitution))))) (, ,) (NP (PRP it)) (VP (AUX is) (RB not) (ADJP (JJ enough) (S (VP (TO to) (VP (VB cite) (NP (DT the) (NN Preamble)) (PP (IN as) (NP (NP (NN justification)) (PP (IN for) (NP (NP (DT a) (JJ new) (NN law)) (CC or) (NP (NN idea))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ argues) (SBAR (SBAR (IN that) (S (NP (DT the) (NN Preamble)) (VP (VBZ defines) (SBAR (WHADVP (WRB why)) (S (NP (DT the) (NN document)) (VP (VBZ exists))))))) (, ,) (CC and) (SBAR (IN that) (S (NP (NP (DT the) (NNS definitions)) (PP (IN for) (SBAR (WHADVP (WRB why)) (S (NP (DT the) (NNP Constitution)) (VP (VBZ exists)))))) (VP (AUX are) (ADJP (ADJP (RB just) (RB as) (JJ important)) (PP (IN as) (NP (NP (DT the) (NN text)) (PP (IN within) (NP (DT the) (NNP Constitution))))))))))) (. .)))

(S1 (S (NP (DT The) (NN Preamble)) (VP (AUX does) (VP (VB provide) (NP (DT a) (JJ statutory) (NN precedent)) (SBAR (IN because) (S (NP (DT those) (NNS ideas)) (VP (VBP cannot) (VP (AUX be) (VP (VBN removed) (PP (IN by) (NP (NP (DT any) (JJ other) (NN branch)) (PP (IN of) (NP (NN government)))))))))))) (. .)))

(S1 (S (ADVP (RB Also)) (, ,) (NP (DT the) (NN Preamble)) (VP (AUX is) (NP (NP (DT a) (NN part)) (PP (IN of) (NP (NP (DT the) (NNP Declaration)) (PP (IN of) (NP (NP (NNP Independence)) (, ,) (NP (RB not) (DT the) (NNP Constitution)))))))) (. .)))

(S1 (S (NP (NP (NNP Congress) (POS 's)) (NNS powers)) (VP (AUX are) (VP (ADVP (RB specifically)) (VBN defined) (PP (IN in) (NP (NP (NN article) (CD 1)) (PP (IN of) (NP (DT the) (NNP Constitution))))) (, ,) (ADVP (RB not) (RB anywhere) (RB else)))) (. .)))

(S1 (S (SBAR (IN If) (S (NP (DT the) (NN power)) (VP (AUX is) (RB not) (VP (VBN listed) (ADVP (RB there)))))) (NP (PRP they)) (ADVP (RB technically)) (VP (AUX do) (RB not) (VP (AUX have) (NP (PRP it)))) (. .)))

