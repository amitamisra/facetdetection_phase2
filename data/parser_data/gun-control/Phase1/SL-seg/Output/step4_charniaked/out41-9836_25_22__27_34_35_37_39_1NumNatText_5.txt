(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (VP (VBD got) (NP (NP (CD one) (NN link)) (VP (VBG showing) (NP (NP (DT the) (JJ common) (NN thing)) (PP (IN with) (NP (NP (DT those)) (SBAR (WHNP (WDT that)) (S (VP (VP (VBP kill) (NP (NN police) (NNS officers))) (, ,) (NP (NNS drugs)))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (IN if) (S (NP (NNP Author2)) (VP (MD can) (VP (VP (VB show) (S (NP (NP (DT an) (NN officer)) (VP (VBN killed) (PP (IN by) (NP (NN militia))))) (VP (TO to) (VP (AUX do) (NP (PRP it)))))) (CC or) (VP (VB get) (PP (IN off) (NP (DT the) (NN point))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VP (VBZ asks) (SBAR (IN if) (S (NP (NP (NNS members)) (PP (IN of) (NP (NP (DT the) (NN militia)) (PP (IN in) (NP (NP (DT the) (NN mid-west)) (SBAR (WHNP (WDT that)) (S (VP (VBD planned) (S (VP (TO to) (VP (VB kill) (NP (NNS officers))))))))))))) (VP (AUX were) (PP (IN on) (NP (NNS drugs))))))) (CC and) (VP (VBZ asks) (SBAR (IN if) (S (NP (NNP Author1)) (VP (AUX is) (VP (VBG defending) (NP (PRP them)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (NNP Author1)) (VP (AUX is) (PP (IN into) (NP (NN conspiracy) (NNS theories))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (SBAR (S (NP (PRP they)) (VP (AUX were) (VP (VBN accused) (PP (IN by) (NP (DT an) (NN insider))))))) (CC and) (SBAR (S (NP (PRP he)) (VP (MD will) (VP (VB wait) (S (VP (TO to) (VP (VB see) (SBAR (WHNP (WP what)) (S (NP (DT a) (NN jury)) (VP (AUX has) (S (VP (TO to) (VP (VB say)))))))))))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (S (NP (NNP Author2)) (VP (VP (AUX is) (VP (VBG making) (NP (JJ false) (NNS claims)))) (CC and) (VP (VBD failed) (S (VP (TO to) (VP (VB read) (NP (NP (DT the) (NN officer) (NN death) (NN synopsis)) (SBAR (WHADVP (WRB where)) (S (NP (NNS drugs)) (VP (AUX are) (VP (VBN mentioned) (PP (IN in) (NP (ADJP (RB almost) (DT every)) (NN death)))))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (PRP he)) (ADVP (RB never)) (VP (VBD claimed) (SBAR (S (NP (NN police) (NNS officers)) (VP (AUX were) (VP (VBN killed) (PP (IN by) (NP (NN militia) (NN group) (NNS members))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ asks) (SBAR (WHADVP (WRB why)) (S (VP (VBP mention) (NP (NNS militias)) (ADVP (RB then)))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ says) (SBAR (S (NP (NNP Author2)) (VP (VBD made) (NP (DT the) (NN claim)) (SBAR (S (NP (DT some)) (VP (VP (AUX were) (VP (VBN saved) (PP (IN from) (NP (NP (JJ maoist) (NN slaughter)) (PP (IN in) (NP (NNP India))))))) (CC but) (VP (MD can) (RB not) (VP (VB back) (NP (PRP it)) (PRT (RP up))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ thinks) (SBAR (S (S (NP (NNP Author1)) (VP (AUX is) (VP (AUXG being) (ADJP (JJ hypocritical))))) (CC and) (S (NP (PRP$ his) (NN post)) (VP (AUX was) (VP (VBN based) (PP (IN on) (SBAR (WHNP (WP what)) (S (NP (JJ federal) (NNS prosecutors)) (VP (AUX had) (S (VP (TO to) (VP (VB say)))))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (PRP you)) (VP (AUX 're) (ADJP (JJ innocent) (SBAR (IN until) (S (VP (VBN proven) (S (ADJP (JJ guilty))))))))))) (. .)))

