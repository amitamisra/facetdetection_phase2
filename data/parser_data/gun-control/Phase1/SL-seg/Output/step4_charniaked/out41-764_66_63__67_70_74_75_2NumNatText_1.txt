(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (DT the) (NN right) (S (VP (TO to) (VP (VB carry) (NP (DT a) (NN gun)))))) (VP (AUX is) (RB not) (NP (DT a) (JJ natural) (NN right)))))) (. .)))

(S1 (S (NP (PRP It)) (VP (AUX is) (NP (DT a) (JJ granted) (NN right))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (IN that) (S (NP (NNP Author1)) (VP (AUX is) (VP (VBG forgetting) (SBAR (IN that) (S (NP (NN nobody)) (VP (VBD gave) (SBAR (S (S (NP (NNP United) (NNP States)) (VP (NNS citizens) (NP (NNS rights)))) (, ,) (NP (PRP they)) (VP (AUX had) (S (VP (TO to) (VP (VB take) (NP (PRP them)))))))))))))))) (. .)))

(S1 (S (ADVP (RB Furthermore)) (, ,) (NP (NP (DT the) (NNP Bill)) (PP (IN of) (NP (NNPS Rights)))) (VP (AUX is) (NP (NP (DT a) (NN statement)) (PP (IN of) (SBAR (WHNP (WP what)) (S (NP (DT those) (NNS rights)) (VP (AUX are))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ thinks) (SBAR (S (NP (NNP Author1)) (VP (AUX has) (NP (NP (DT a) (ADJP (RB very) (JJ silly)) (NN notion)) (PP (IN of) (NP (DT the) (NN law)))))))) (. .)))

(S1 (S (NP (PRP He)) (VP (VBZ thinks) (SBAR (S (NP (NNP Author2)) (VP (VBZ believes) (PP (IN in) (NP (NP (JJ natural) (NNS laws)) (SBAR (WHNP (WDT that)) (S (VP (AUX are) (RB not) (ADJP (JJ natural))))) (, ,) (PP (JJ such) (IN as) (NP (NP (NNS laws)) (VP (VBG allowing) (S (NP (PRP him)) (VP (TO to) (VP (VB defend) (NP (NP (PRP$ his) (NN state)) (VP (AUXG being) (VP (VBN used) (S (VP (TO to) (VP (VB defend) (NP (PRP himself)) (PP (IN from) (NP (DT a) (NN mugger))))))))))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ states) (SBAR (DT the) (S (S (NP (NP (DT the) (NN Bill)) (PP (IN of) (NP (NNS rights)))) (VP (AUX is) (RB not) (NP (DT a) (NN contract)))) (, ,) (CC and) (S (NP (NP (DT the) (NNS rights)) (PP (IN in) (NP (NP (DT the) (NNP Bill)) (PP (IN of) (NP (NNPS Rights)))))) (VP (AUX are) (NP (JJ natural) (NNS rights))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ questions) (SBAR (WHADVP (WRB how)) (S (NP (DT the) (NNS rights)) (VP (AUX are) (ADJP (JJ natural)) (SBAR (IN if) (S (NP (PRP they)) (VP (AUX were) (VP (VBN taken))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ thinks) (SBAR (IN that) (S (NP (NNP Author1)) (VP (AUX is) (ADJP (JJ anti-American)))))) (. .)))

