(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ states) (SBAR (IN that) (S (S (SBAR (IN if) (S (NP (NP (DT the) (NN record)) (PP (IN of) (NP (JJ private) (NN party) (NNS sales)))) (VP (AUX is) (VP (VBN kept) (PP (IN with) (NP (NP (DT a) (NN FFL)) (VP (VBG doing) (NP (DT the) (NN background) (NN check))))))))) (, ,) (ADVP (RB then)) (VP (VB trace) (NP (NNS data)))) (VP (AUX is) (ADJP (JJ available) (PP (TO to) (NP (NN law) (NN enforcement)))))))) (. .)))

(S1 (S (NP (DT The) (NN manufacturer)) (VP (AUX has) (S (VP (TO to) (VP (VB know) (SBAR (WHADVP (WRB where)) (S (VP (TO to) (VP (VB point) (NP (NN law) (NN enforcement)) (, ,) (SBAR (IN since) (S (NP (NP (DT the) (NNS FFL)) (SBAR (WHNP (WDT that)) (S (VP (VBZ corresponds) (PP (TO to) (NP (DT the) (JJ serial) (NN number))))))) (VP (MD would) (RB not) (VP (AUX have) (NP (NP (DT a) (NN record)) (PP (IN of) (NP (DT the) (JJ current) (NN owner)))))))))))))))) (. .)))

(S1 (S (S (NP (NNP Author2)) (VP (VBZ tells) (NP (NNP Author1)) (SBAR (IN that) (S (NP (PRP he)) (VP (VBZ wants) (S (NP (DT the) (NN manufacturer)) (VP (TO to) (VP (VB get) (ADJP (VBN involved) (PP (IN with) (NP (DT the) (NN police)))))))))))) (, ,) (CC but) (S (NP (JJ firearm) (NNS manufacturers)) (VP (AUX do) (RB not) (VP (VB sell) (PP (TO to) (NP (NNS dealers)))))) (. .)))

(S1 (S (S (NP (DT The) (NNS distributors)) (VP (VBP sell) (PP (TO to) (NP (DT the) (NNS dealers))))) (, ,) (RB then) (S (NP (NNP Author1)) (VP (VBZ asks) (NP (NNP Author2)) (SBAR (IN if) (S (NP (NN law) (NN enforcement)) (VP (VBZ finds) (NP (DT the) (JJ original) (NN owner)) (PP (IN through) (NP (NP (DT the) (NN distributor)) (PP (IN with) (NP (NP (DT the) (NN manufacturer) (POS 's)) (NN help)))))))))) (. .)))

(S1 (S (S (NP (NNP Author2)) (VP (VBZ answers) (SBAR (IN that) (S (NP (DT the) (NN manufacturer)) (VP (VBZ leads) (NP (PRP them)) (PP (TO to) (NP (DT the) (NN distributor)))))))) (, ,) (RB then) (S (NP (DT the) (NN distributor)) (VP (VBZ tells) (NP (NP (NN law) (NN enforcement)) (SBAR (WHNP (WDT which)) (S (NP (JJ FFL) (NN holder)) (VP (AUX has) (NP (PRP it)))))))) (. .)))

(S1 (NP (NP (CD Author1) (NNS thanks)) (NP (NNP Author2)) (. .)))

