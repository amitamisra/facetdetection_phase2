(S1 (NP (NNP <PARAGRAPH>)))

(S1 (S (NP (NNP Author1)) (VP (VBZ thinks) (SBAR (S (NP (DT a) (JJ new) (NN gun) (NN law)) (VP (AUX is) (NP (DT a) (NN gun) (NN buyback)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (SBAR (IN if) (S (NP (PRP you)) (VP (AUX do) (RB not) (VP (VB sell) (NP (PRP$ your) (NN gun)))))) (NP (PRP they)) (VP (MD will) (VP (VB take) (NP (PRP it))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (PRP they)) (VP (AUX are) (RB not) (VP (VBG taking) (NP (DT all) (NNS guns)) (ADVP (RB away))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ compares) (NP (PRP it)) (PP (TO to) (S (NP (NNP WW2) (NNP Germany)) (RB not) (VP (VBG taking) (PRT (RP away)) ('' ') (NP (PDT all) (DT the) (NNPS Jews) (POS ')))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (NNP Author2)) (VP (VP (VBZ loves) (S (VP (VBG bringing) (NP (NNPS Jews)) (PP (IN into) (NP (NNS conversations)))))) (CC and) (VP (VBZ asks) (SBAR (WHNP (WHADJP (WRB how) (JJ many)) (NNS countries)) (S (VP (VBD wanted) (S (VP (TO to) (VP (VB eliminate) (NP (NNPS Jews))))))))))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ provides) (NP (DT a) (JJ few) (NNS answers))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (S (VP (VBG comparing) (NP (NNPS Jews)) (PP (TO to) (NP (NNS guns))))) (VP (AUX is) (ADJP (JJ ridiculous) (PP (IN for) (NP (JJ multiple) (NNS reasons)))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VP (AUX has) (NP (NP (DT no) (NNS problems)) (PP (IN with) (NP (NP (JJ Australian) (NNS guns)) (VP (AUXG being) (VP (VBN taken) (PRT (RP away)))))))) (CC and) (VP (VBZ asks) (SBAR (WHNP (WDT what) (NN harm)) (S (VP (MD will) (VP (VB come) (PP (IN from) (NP (PRP it))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (IN that) (S (PP (ADVP (RB even)) (IN with) (NP (DT no) (NN gun) (NN control))) (, ,) (NP (NNS governments)) (VP (MD can) (ADVP (RB still)) (VP (VB oppress)))))) (. .)))

(S1 (S (NP (NNP Author2)) (VP (VBZ says) (SBAR (S (NP (PRP it)) (VP (AUX 's) (ADJP (JJ fair)) (SBAR (IN because) (S (NP (NNS guns)) (VP (AUX were) (VP (VBN taken) (PP (IN from) (NP (NNPS Jews))))))))))) (. .)))

(S1 (S (NP (NNP Author1)) (VP (VBZ says) (SBAR (S (NP (NNS governments)) (VP (MD could) (VP (VB take) (NP (NP (NN control)) (PP (IN of) (NP (PRP us)))) (ADVP (RB regardless))))))) (. .)))

