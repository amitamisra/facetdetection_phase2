key 1-10121_22_21__23_28_30_34_37_1

New Cluster0
Author1 says Heller explored the rights origins, the 1689 English Bill of Rights explicitly protected a right to keep arms for self-defense.
By 1765, Blackstone was able to assert the right to keep and bear arms.
The English Historians Brief says that Heller was wrong.
The RKBA was connected with militia service, so pointing out that the RKBA is a preexisting right doesn't support Author2's argument.
Author2 asks where it's been established that any person who was incapable of taking part in the militia was barred from owning a firearm.
Author1 asks how people owning guns outside the militia would prove it's protected by the RKBA.
Author2 says Author1 argues that the right to keep and bear arms exists to guarantee that only the militia may be armed, so he should prove it.
Author1 replies that a lack of ban on firearms by those outside of the militia does not mean that ownership was considered to be protected.
Author2 thinks it does.
Author1 says he never said there was a weapons ban for non-military purposes.
Author1 argues that the Magna Carta was approved in 1215 and that people had typically been allowed to keep weapons in the UK with their typical laws.
The second amendment is claimed to protect a preexisting right, but instead the right to keep and bear arms (RKBA) was only ever connected with military service.
The RKBA is not a preexisting condition as presented in the second amendment, and although there is no ban against owning weapons outside a militia, the right is only guaranteed for the purpose of having a militia.
There has never been a complete ban on firearms for people not in a militia, but rather weapons can be reasonably regulated by the states.
Author2 argues that there is no proof that weapons were only guaranteed for militia members and not for all citizens.
Since the RKBA was not guaranteed for anyone other than milita members, it is reasonable to assume that is should be illegal for anyone not in a militia should not own guns.
But there is no ban like that.
Author1 states Heller found that the English Bill of Rights allowed people to keep guns for self defense.
A court brief argued he was wrong.
Author1 says the right to bear arms was connected with militia so the second amendment wasn't protecting ordinary citizen rights.
He says just because people owned guns doesn't prove they were protected to do so.
He says he wasn't saying there was a ban on people from owning guns.
Author2 says to prove people who were not able to serve in militias were not allowed to own weapons.
He says Author1 is arguing that only the militia were protected in owning guns, and that means anyone unable to serve in a militia would then be excluded from owning guns.
He says the fact people were allowed to own guns shows that they were protected to do so, or people outside of militia members wouldn't have been allowed to.
He says this shows Author1's statements that the second amendment doesn't protect citizen rights to bear arms are false.
Author1 sites some information eluding to the fact the 2nd Amendment was not designed to protect a preexisting right to bear arms.
He advises the basis of the amendment was in support of people who had been involved with the militia in the sixteen and seventeen hundreds and that weapons owned by people who were not members were subject to reasonable regulations.
He sites several books to support his theory.
Author2 does not support the argument that the right to keep and bear arms was established for the militia or militia ready persons only and requests proof of this fact.
He supports his ideas by citing the fact that people who were considered unable to serve the militia would have lost their weapons had this been the case.
He uses retired militia members as an example advising if the RKBA were only to protect militia groups, then those retired would no longer be able to hold weapons as they would no longer be able to serve the state if needed.
Author1 tells Author2 that the Second Amendment protects a preexisting right, and that the RKBA used to be connected with the militia service.
Author2 wants to know when a physically incapable person that could not take part in the militia was ever barred from owning a firearm.
Author2 believes that Author1 thinks regular people should not own firearms if they cannot serve the state.
Author1 asks why ownership of weapons by those outside the militia would prove that such ownership is protected by the RKBA, and Author2 answers that Author1 has argued that the right to keep and bear arms only applies to the militia.
Author2 states that Author1 believes that certain females were barred from owning firearms because it was not in the interest of the government.
Author2 asserts that Author1 needs to prove that women and those over 46 were not allowed to be armed because they could not serve in the militia.
Author1 clarifies for Author2 that he never argued that there was a complete ban on firearms used for non-militia purposes.

 New Key
key 1-10145_4_3__5_9_21_29_1

New Cluster0
Author1 argues that firearms should not be unregulated, and supports the recent Supreme Court rulings, but also feels that the laws will not be changed.
The majority of people want gun regulations stricter or at the least kept the same.
Repealing gun regulations will not make individuals safer.
Guns should be regulated and protected, because they are dangerous tools.
Author2 argues that guns should be less regulated.
Author2 points out that, while a majority of people want the gun laws not to change, very few people want stricter gun control than exists right now.
Author2 feels that Author1 is in the minority, and that the right to own guns should be protected.
Additional gun control is taking away the guaranteed rights of the people.
Author1 supports gun ownership laws because they are Constitutional even if Author2 disagrees.
Author1 interprets Author2's point of view as a complete disregard for any regulation and a position of support for removing guns from the hands of private owners.
Author2 defends himself by providing poll results obtained by various corporations which support his claim of American preference for looser restrictions.
Author2 does not argue in favor of complete removal of laws, but instead for their minimization.
Author1 informs Author2 that he is simply misinterpreting the data.
According to Author1, the diminishing support of the people for strict gun laws is not the same as support of removal of all barriers to ownership.
Author2 disagrees with Author1,thinking his position to be even more radical.
Author1 thinks that Author2 is attempting to demonize anyone that doesn't agree with his view that firearms should be totally unregulated, and suggests a hidden agenda that leads to the confiscation of firearms.
He thinks that Author2 is a radical and among the minority of Americans.
Author1 agrees with the SCOTUS rulings, but he maintains that neither Heller or McDonald will lead to much repeal of existing gun regulations because firearm regulation is constitutional.
Author2 says he isn't calling for repeal, but few Americans support strict gun control laws according to CNN polls.
Author1 thinks that doesn't mean that a decline in favor of strict gun control means people want laws lifted.
46 percent want no change in the current law, Author2 is being dishonest.
Author1 and Author2 are talking about something being imaginary.
Author1 says Author2 is trying to demonize anyone who wants firearms to be regulated.
They say that Author2's position places them in a small radical minority.
Author1 says they support recent rulings but do not believe they will repeal existing gun regulations.
They believe firearm regulation is constitutional.
Author2 says that they do not want laws to be repealed and cites a recent Gallup Poll that claims fewer Americans support stricter gun control laws.
Author1 implies they are reading selectively, and that 85% of Americans want gun regulations to stay the same or be stricter.
Author2 says that Author1 wants to steal rights, and that the second amendment rights are inalienable and whatnot.
Author1 is chastising Author2 as putting people down just because they don't agree with him.
He mocks Author2's claims of a hidden agenda to  enable authorities to seize weapons.
He calls Author2 a radical, and in the minority with those beliefs.
He supports recent rulings, but does not believe they will cause any changes, because of the constitution.
Author2 says  he is not asking for repeals, but citizens do not support stricter laws.
Author1 points out that people want the laws to stay the same, and only 15% want laws repealed, making Author2 in a minority.
He accuses Author2 of being dishonest.
Author2 says that gun laws steal rights from people, and he is defending his rights as a citizen.

 New Key
key 1-10756_37_35__39_42_43_44_1

New Cluster0
Author1 notes that Author2 has repeatedly suggested that other states should enact gun control laws similar to California's.
He points out that one state can't regulate the rules of another, and that Author2's proposal has gained little support.
Author1 asks Author2 twice if he's really saying that the deaths of four Oakland police officers killed by a (banned) AK-47 shows why California's gun laws are a good idea.
Author2 asserts that voters in other states could choose to adopt California's gun policies, but concedes that such a move is unlikely in some states.
Author2, who says he is a gun owner, believes the killing of the officers in Oakland supports the case for a continued ban on weapons like the one used.
Author1 says the argument that the United States should adopt California's standards as national standards has been made several times, but support has been lacking.
A single state can't regulate the activities of others, but that doesn't mean a state should ignore home rule issues that have created problems.
Author2 says states could adopt controls like California if they wanted, but it would depend on voters.
He says that people miss that California is a large state with large crime rates, and this is one of the reasons for such restrictions.
He gives an example of four officers being gunned down with an AK-47.
Author1 questions an example of a prohibited weapon being used illegally as an example.
Author2 thinks the weapon shouldn't have been available.
Author1 states that Author2 made the argument that California should set the standard for the United States, but one state cannot regulate the activities of another state.
A state should not ignore home rule issues that may have created a problem that they cannot respond to.
Author2 believes that other states could adopt controls like California, and he further believes that the reform will not take place because other states do not like the California laws.
Four Oakland police officers were gunned down by a hood with an AK-47, which serves as a valid reason for some restrictions in areas like California has.
Author1 believes that this is not a good example, but Author2 believes the cop murder proves that it is a good measure.
Author1 says Author2 is proposing again that the U.S. adopt California's standards.
He cites others lack of support, because one state can not determine how another should act.
He says states can not ignore issues of their own.
Author2 says states adopting controls would depend on the voters, and the states.
He states that California has large densely populated cities with high crime rates.
He gives a recent example that four cops were killed by a criminal with an AK-47.
Author1 questions how an example of a prohibited weapon being used serves as a good reason for the ban.
Author2 says it shows why there is a ban.
Author2 says if the gun hadn't been available, the damage wouldn't be so great.
Author1 is against the controls on guns that California has put in place.
They argue that guns should not be so strictly controlled and regulated as they are in California.
Other states cannot and should not be controlled by the rulings of a different state.
If other states wanted to, they could adopt similar laws.
However, gun control does not apply well in real life.
If gun laws are stricter, then illegal guns will still manage to bypass those laws, because the guns are illegal.
Author2 argues that stricter gun control is necessary and would have saved four policeman's lives.
Because of the high crime rates in Californian cities, it is a good idea for there to be more gun control in place.

 New Key

 Features used are 
['AdVerb_Sent', 'Adj_Sent', 'Noun_Sent', 'Verb_Sent']
eps value is     5 
minsample value is 2
metric used is euclidean