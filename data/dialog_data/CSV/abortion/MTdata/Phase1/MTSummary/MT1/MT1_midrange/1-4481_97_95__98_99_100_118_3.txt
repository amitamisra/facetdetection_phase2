
----------
D0
----------
Author1 accuses Author2 of engaging in 'atheistic liberal propaganda.' Author2 asks how women are exploited by having abortions, since it is their choice to do so. Author1 denies that women are given a choice. In Author1's view, abortion clinics pressure women into getting abortions for profit. Author1 contrasts crisis pregnancy centers, which Author1 claims support women if they want to have their child. Author2 asks for evidence. In Author2's experience, the decision to get an abortion was the woman's. Author2 also denies that abortion clinics are lucrative: many offer low-cost abortions. Author2 asks if Author1 would feel the same way about a clinic that lied to keep women from getting abortions. Author1 claims abortion clinics lie and offers a citation. Author2 questions its credibility.


----------
D1
----------
Author1 called it atheistic liberal propaganda. He denies they have a choice, and abortion clinics say it's the only option, and push for a quick choice so they won't change their mind. Pregnancy crisis centers offer many options like housing, food, clothing, counseling, medical care, and ministering. 

Author2 asked how women are exploited by being given a choice. He asked for proof, and said most women have already decided before they go to an abortion clinic. They do not all make money off it since poor are given a sliding scale. He asked if it was reversed, if Author1 would support a clinic not giving abortion as an option, and insisting pregnancy was the only choice. He derides Author1's source as biased.


----------
D2
----------
Author1 believes women are not given options at clinics, or "abortion mills."  There, women are told that abortion is their only option, and that it must happen right away.  The clinics are motivated by lucrative profits.  In contrast, crisis pregnancy centers help the woman and her child with everything from housing to medical care to ministry.  Author2 disagrees that abortion is a "lucrative business."  Planned Parenthood has a sliding scale for fees, so that the poor don't have to pay more than they can afford.  Author2 asks if Author1 opposes clinics that don't offer the option to abort, and tell women that having the child is their only option.  Author2 says that abortionfacts.com has no credibility.


----------
D3
----------
Author1 claims that abortion clinics exploit women by lying to them. They claim that abortionists tell women that abortion is their only option, and that it is all done for the sake of profit. They also believe that crisis centers are dedicated to the well being of women, and accuse other debaters of trying to brainwash them with liberal propaganda. Author2 doubts all of Author1s claims. They point out that a woman having a choice to get an abortion is not exploitation, and refute claims that abortions are lucrative and that women are forced in to them. They ask Author1 whether they would take issue with a place that told women carrying to term was their only option, and denies the credibility of Author1's sources.


----------
D4
----------
Author1 says his mind won't be corrupted with atheistic liberal propaganda. Author2 asks how women are exploited. They have a chance to get an abortion and that's the point. Author1 says women aren't given options in these clinics. They are told abortion is the only option and it must be done quickly. That's how the abortion industry makes its business. Crisis pregnancy centers help women with housing, food, clothing, counseling, medical care, and more. Author2 asks for evidence of this. In his experience women that had abortions had decided before they went in. He asks if he would be equally appalled with an organization that didn't offer abortion as an option. Author1 provides a link to support his claims. Author2 says that it isn't credible.

