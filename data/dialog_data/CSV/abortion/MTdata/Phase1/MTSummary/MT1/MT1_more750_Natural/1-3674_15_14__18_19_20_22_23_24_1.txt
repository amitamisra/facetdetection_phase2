
----------
D0
----------
Author1 and Author2 are discussing the subject of abortion related to the UN.  Author1 feels the use of the phrase, "begging the question," was incorrect in describing his/her statement.  Author1 seems to be on the pro-abortion side noting a license from the UN is not needed to make decisions.  He/she feels a personal right to autonomy is above a right of life, regardless of whether or not the person believes a fetus to be a person at conception.  He/she feels the statement made in regard to a license from the UN is not the basis of his/her argument and that Author2 has latched onto  a piece of information having nothing to do with the conversation.
Author2 advises the comment to which he/she was referring was not in regard to the UN, it was the phrase "Killing someone."  He/she feels this suggests Author1 acknowledges a fetus as a "someone" and begs the question as to his/her stance on the issue at hand.  He/she also feels he used the phrase correctly.


----------
D1
----------
Author1 has posted a response to Author2's opinion on abortion and Author2 is disputing the language used in said response. Author1 sarcastically used the analogy that, "you don't need a license from to UN to determine whether you can or can not abort." Author2 is arguing that since Author1 chose a side in the natural sticking point of whether a fetus is a person or a thing, in his original response, that it has invalidated his point. Author2 goes on to explain the details of why Author1's response was an assumption in the from of "begging the question" where by circumventing the real question which is, is the fetus to be morally considered a person. Author1 then attempts to resumes his dictation by stating that some people believe they have the right to personal autonomy in regards to the persons life growing inside them, which is to say that he agrees that the fetus is considered a person. Author2 then resumes arguing over the semantics of whether or not Author1 was accusing them of making an assumption and in closing whether Author1 was opposed to Author2's original opinion.


----------
D2
----------
Author1 said his conclusion wasn't the basis for his premise. Just because he believed a fetus was a someone and not a something, doesn't mean his conclusion that abortion is wrong is incorrect. Author1 accuses Author2 of doing the same thing and making a conclusion on whether a fetus is a someone or something when saying abortion is okay. Author1 said that it is not an important part of the debate because some people DO believe it is a someone, but that the right to personal autonomy supersedes the right to life. 

Author2 said the phrase Author1 used, "killing someone," is the main debate over abortion. Whether it is a someone or something is the central argument of the debate. For Author1 to make the debate that abortion is wrong, without addressing whether it is wrong really depends on whether a fetus is a person or thing, is not properly addressing the main argument of the debate. Author1 should address that first before trying to make the argument that abortion is wrong.


----------
D3
----------
Author1 tells him that by his standards, Author2 begs the question all of the time. Author1 tells him that he assumes the sticking point is whatever Author2 wants it to be. Author1 states that some believe a right to personal autonomy takes precedence over a right to life. He believes that the phrase begging the question is being incorrectly used, and Author1 insists that he will not call him out for incorrectly using it.

Author2 tells him that he accused him of begging the question for using the phrase, killing someone. The sticking point of abortion is whether or not someone is being killed. He states that the critical sticking point between the two sides of the debate is whether the fetus is a person or not. Author2 tells him that he assumed that a fetus was a person when he tried to refute a point that is related to the abortion debate's sticking point, Author1 begged the question by doing that. He believes that Author1 cannot tell him what the actual sticking point is.


----------
D4
----------
Author1 thinks any reasonable person would know you don't need a UN license to make a decision. Author2 says he was talking about the phrase "killing someone." Author1 says it was his mistake. Author2 is confused. He asks if a fetus is a morally considerable thing, or a person. He says Author1 assumed it was when making his argument. Author1 says Author2 begged the question himself. Author2 asks what the actual sticking point is. Author1 replies some believe a right to personal autonomy over a right to life, so they believe it is a human but below the right to autonomy. He thinks Author2 is using the term begging the question incorrectly. Author2 says this is a good point, but it is marginal at best. He says making an assumption is not the same as begging the question, which is a type of assumption. He suggests Author1 reread the entry and says he didn't answer his question. He says Author1 assumed he thought X was true, but the problem is when X is under dispute.

