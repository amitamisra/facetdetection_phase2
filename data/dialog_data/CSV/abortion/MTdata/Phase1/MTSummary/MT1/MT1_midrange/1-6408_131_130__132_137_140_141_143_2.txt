
----------
D0
----------
Author1 sarcastically consoles Author2 for conceding the argument. Author2 disputes the notion that Author2 conceded, but merely refused to debate. To Author1, these are the same thing. Author2 accuses Author1 of claiming that women purposely become pregnant. Author2 claims that Author1 conceded the argument, since Author1 refuses to answer a question. Author1 counters that his question has not been answered: how does he force women to bear their unborn children? Author2 notes that the question wasn't asked by Author2, but is willing to answer. Author2 states that Author1 cannot force women to carry pregnancies to term. But by campaigning to deny them the opportunity for abortions, Author1 is in effect forcing them. However, Author2 says women would then seek illegal abortions. Author1 thanks Author2.


----------
D1
----------
Author1 said Author2 is conceding he's wrong, and he believes refusing to answer does signify that. His question was asked first, and he wanted to know how he forces women to proceed with a pregnancy and how he physically forces them.

Author2 states refusing to debate is not the same thing as conceding to being wrong. The incorrect interpretation explains why Author1 incorrectly interprets women as getting themselves pregnant. He claims Author1 refuses to answer questions which is the same thing. Author2 refutes the question was asked first, but answers it by saying Author1 can't force women, although he would like to by removing the option of abortion, because women would still find a way to have abortions even if they were illegal.


----------
D2
----------
Author1 and Author2 each say the other one is conceding, by refusing to answer any debate questions.  Author1's question hadn't previously been asked of Author2, and Author2 is willing to answer it. Author1's question is: how is he (Author1) forcing women to carry pregnancies to term?  What method of force does he use?  Author2 responds that Author1 can't force women to carry to term, but he would like to try.  Removing the option of terminating a pregnancy is by default forcing them to continue it.  However, even if abortion is illegal, women will still seek and obtain abortions.  Author1 thanks Author2 for agreeing that what Author1 is advocating does not involve forcing women to do anything.


----------
D3
----------
Author1 thinks it is too bad that Author2 feels the need to concede. Author2 replies that refusing to debate with Author1 is not conceding. He replies it's no wonder Author1 thinks women get themselves pregnant. He lives by a double standard. Author1 replies that in his opinion, it is. Author2 says his lack of willingness to answer a straight question seems to indicate a lack of willingness to debate and therefore he conceded first. Author1says he asked the question first, how does he force women to carry a pregnancy to term and what does he do to force them. Author2 replies removing the possibility of abortion is by default force, but women will still get them. Author1 says that Author2 agrees he doesn't force.


----------
D4
----------
Author1 says Author2 has conceded the argument because he won't respond. His question is how Author2 feels he is forcing women to carry their pregnancies to term or what action he's taken to make this happen. After Author2 responds, he maintains he won the argument because he asked a question first, and Author2 admits he doesn't force women to do anything. Author2 states Author1 won't answer the question he asked before him. He believes Author1 would force women to carry to term but can't. So, while he hasn't taken direct action, not letting abortion be an option means that women would have to give birth. However, women will find a way to have an illegal abortion if there's no way to do it legally.

