
----------
D0
----------
Author2 argues that fetuses have the right to life because they are alive. Author2 considers it irrelevant whether fetuses are mindless or not. Author2 points out that the Constitution does not mention that thinking or feeling is necessary for a person to possess rights. According to Author2, biological life begins at the moment of conception. Personhood is conferred as soon as one is a living human being. Author2 contends that this is a biological rather than metaphysical truth.

Author1 states that personhood cannot exist without thought, and fetuses don't think. A fetus may be alive, but it is not sentient. If personhood requires thought, and fetuses don't think, a fetus does not have any rights. Author1 claims that Author2 is making claims without supporting evidence.


----------
D1
----------
Author1 believes abortion because of malformation is reasonable. A fetus doesn't have rights like a person. A fertilized ovum is not a person. He argues not all biology is sentient, and humans are. Since a fetus isn't sentient, it isn't a human being. Author2 hasn't shown a fetus to be sentient.

Author2 asked if people with deformities don't have rights to life. Fetuses are alive, so they have rights. The Constitution doesn't base rights on a person being able to feel or think. He claims a person's life begins at conception.  The Constitution recognizes persons as human, which a fetus is. He questions how beliefs in separation of church and state can then say that personhood should be based on metaphysical instead of biological.


----------
D2
----------
Author1 feels that it is reasonable to abort a fetus known to have significant deformations or abnormalities.  People have rights; a mindless fetus does not.  Cogito, ergo sum.  Our rights only apply to persons, and a fertilized egg is not a sentient entity, and therefore, is not a person.  Author2 counters that people with genetic maladies have a right to live.  A fetus is already alive, and should be able to continue the life it's already living.  The only requirement for personhood is that a person be a human being.  The Constitution recognizes all biological persons equally, regardless of whether they are sentient or handicapped. Personhood is not based on the metaphysical, but on the biological.


----------
D3
----------
Author1 thinks it's reasonable to abort because of an identified significant malformation. Author2 questions people with genetic malformations not having rights. Author1 says people have right, a fetus doesn't. Author2 thinks a fetus does have rights. Author1 says if there is no mind there is no person. Rights only apply to people. Author2 challenges Author1 to demonstrate his own personhood. He thinks there is plenty of evidence that life begins at conception. The only requirement for being a person is being human. Author1 replies it isn't a being if it isn't sentient. Personhood is a concept that requires biological structures to even be possible. Not all beings are sentient. He thinks that Author2 is admitting that he can't prove his case or make others agree.


----------
D4
----------
Author1 had a personal experience with high-risk pregnancy, and supports abortion in the case of diagnosed genetic abnormality. He feels fetuses should not have the same rights as people because they're mindless. A fertilized ovum is not a person. Author1 distinguishes personhood by sentience. Since a fetus isn't sentient, it has no personhood. He accuses Author2 of making a claim without evidence. Author2 insists on the right of every human life, regardless of genetic malady. He argues that fetuses are people because they are living, and are therefore protected by the Constitution. Author2 believes that biological life begins at conception and challenges Author1 to prove his own personhood. Since a fetus is a human being, and human beings are persons, a fetus is a person.

