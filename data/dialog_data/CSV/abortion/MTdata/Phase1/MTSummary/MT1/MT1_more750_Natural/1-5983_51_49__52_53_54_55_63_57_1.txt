
----------
D0
----------
Author1 asked if Author2 thinks a fetus can over-ride a woman's right to her body. He says ants have a life, and no one protests squishing them. He says humans have a right to their body, and a fetus growing in them doesn't have the right to make decisions for them. He questions Author2's opinions on jail time and if a fetus is a human. 

Author2 questions if a right to a body exists. Assuming so, he says the right to life should be more important. He says laws support life being more important, such as in capital punishment cases for murder. He says ants are not humans, and only human life is sacred. He thinks there should be consequences for abortion such as jail time for doctors. He says a fetus is a real human. He says pro-choice thinks the mother has rights, but pro-life thinks they both have rights. He says you can't kill people that are unwanted, so why should you be able to kill a baby that is unwanted?


----------
D1
----------
Author1 asks Author2 why he feels that the rights of a fetus override the right a woman has over her body, and he states that both a fetus and an ant are a life. Real humans have the right to make decisions about their own bodies, not the fetus that is growing in the stomach. He points out a contradiction in a statement that Author2 made, where he said reproductive health doctors should be jailed. He then contradicted that by saying a fetus is a real human. 

Author2 believes that in a conflict between the right to your own body and the right to life, life will always win. The taking of the right to life is a worse punishment than taking away the right to your body. Author2 tells Author1 that they are talking about humans, not ants. He believes that doctors who perform abortions should be jailed, and that a fetus is a real human. A two month old unwanted baby cannot be killed without punishment, while an unwanted fetus can be killed.


----------
D2
----------
Author1 is Pro-Choice and believe a woman has a right to choose what is done or not done to her own body.  Author1 compares the want of illegalizing abortion due to "right of life" to also making squishing an ant illegal because it is a life as well.  Author1 feels life begins at birth.    

Author2 is Pro-Life and feels the life a fetus overrides a woman's right to her own body.  Author2 challenges Author1's ant squishing comparison advising the discussion is regarding humans, not insects.  He then moves on to say if abortion is legal, then he could shoot people on site and get away with it because of a lack of right to life.  Author2 feels reproductive doctors should be jailed.  Author2 feels life begins at conception.  Author2 also feels contraception should be illegal and not available to people at all.  Author2 disagrees with Author1's statement that he wants contraception outlawed.  Author2 feels abortion is for selfish personal gain reasons.


----------
D3
----------
Author1 asks why Steve thinks a fetus overrides a woman's right to her own body. Author2 asks how one defines the right to your own body. The right to life wins over the right to your body. Author1 says both a fetus and an ant are life so should we make killing ants illegal. Ants have a right to life. He asks Author2 to elaborate. Author2 says they are talking about humans but fine, you have no right to life or a body. Author1 says yes, they are talking about humans, not a fetus. Author2 says a fetus is a human. Author1 doesn't agree a fetus is a real human. Author2 says pro-choicers think the mother has rights and pro-lifers think both the mother and fetus have rights. He asks if we should kill everyone or everything that is unwanted. He asks if Author1 is unwanted should we be able to kill him without punishment, or a 2 month old baby if it's unwanted. He thinks it's selfish to kill for your own personal gain.


----------
D4
----------
Author1 argues that a fetus is life, but so is an ant, so killing an ant should be just as much of an issue as killing a fetus: that is to say, not at all. Life, in general, does not have value, unless we put value on it. Real humans have a right to their bodies, and should not be forced to have them taken over by a fetus that forces them into certain decisions. A fetus is not a real human, and reproductive health doctors should not be put in jail.
Author2 argues that the right to life is always more important than the right to your own body, and so people value life more than your own body. There should be consequences for people who have an abortion, possibly including jail time. Condoms and birth control should not be outlawed, and not all people who are unwanted should be killed, because that is merely selfish and for personal gain. The fetus does not have a choice, so it shouldn't be killed.

