
----------
D0
----------
Author1 has presented the story of Prudence, an African woman who died from sepsis after her fetus died, because the family couldn't afford to have the fetus medically removed.  Her death is relevant to abortion because it was caused by Bush's cutting funding for international maternal healthcare in an effort to block abortion funding.  Worldwide, one woman per minute dies because of pregnancy.  So-called "pro-life" people are anti-women, because they oppose contraception and reproductive healthcare.

Author2 says that Prudence's story is sad, but has nothing to do with abortion.  Internationally, women's healthcare needs to be improved, and virtually all pro-lifers would agree with that.  Author2 would support a policy in which Bush earmarked more aid for women's international healthcare for everything other than abortion, but there's no reason to think that changing abortion policy would have affected Prudence.  Author1 is using rhetoric, and using a story of a woman who did not choose to abort to argue for the right to abortion, and that's an invalid argument.


----------
D1
----------
Author1 asks how how others would feel if they died the same way Prudence did. Author2 says he wouldn't want to die at all but doesn't see how this relates to abortion. He doesn't think the article deals with abortion as there's no evidence she wanted an abortion. It deals with medical care. Furthermore, it seems to be more about inequality of the sexes in Africa. He thinks medical care should be good for everyone, everywhere. Author1 says her death is related to abortion because Bush drastically cut funding for international healthcare using the excuse he might pay for abortions. Catholics get upset anytime reproductive healthcare is talked about. Women are considered dirt in the world especially by republicans and religions. Author2 thinks this is a big stretch. The problem should be with Bush, not with pro-lifers. Author2 wouldn't have a problem with more money being sent for healthcare. He still doesn't think the story has to do with abortion. Author1 says women die every minute from pregnancy. Author2 doesn't agree with Author1's statements.


----------
D2
----------
Author1 argues that Prudence died because of the abortion issue, because international maternal healthcare funding in the US was cut severely by President Bush. The Roman Catholic Church does not like reproductive healthcare. Women, including Prudence, die because women are not valued by religious and right leaning politicians. One women each women dies due to pregnancy.
Author2 argues that people do not want to die. The article is not about abortion, so relating Prudence to the article is irrelevant and off topic. The family did not have money to pay for the removal of a fetus, which caused her to die, but that is not directly related to healthcare. Instead, the general medical care in third world countries needs to be improved. Bush should have given more money for women's health. The pro-life stance does not mean that women are not worth anything. The story in question had nothing to do with abortion and should not have been used as an argument. Prudence may have died whether or not abortion was legal.


----------
D3
----------
Author1 asked if Author2 would like to die in the same manner as a martyr to influence abortions being stopped. The death of the woman is tied to abortion because President Bush cut funding for international maternal healthcare over fears it paid for abortion. The Catholic church disapproves of reproductive healthcare. Author1 blames religion and Republicans for women being treated badly. Author1 says how can people claim to be pro-life when women are allowed to die still from pregnancy.


Author2 said the story doesn't mention if the woman wanted an abortion, just that the woman couldn't afford to pay to have a dead fetus removed. The story is about women in third world countries not having access to medical care. He agrees women it would be better if they had better medical care and didn't use midwives. The fetus was already dead which isn't the same as abortion where a fetus is still alive. Author2 says pro-lifers shouldn't be blamed for funding decisions. Women not being valued is a separate issue from abortion.


----------
D4
----------
Author1 is Pro-Choice and believes women are suffering due to President Bush's decision to cut spending on maternal healthcare.  Author1 feels women are looked down upon in many countries and as a result are not considered good enough for quality healthcare.  Author1 feels one woman around the world dies every minute because of pregnancy, whether wanted or unwanted.  Author1 used an article about a girl named Prudence to establish her point.  Prudence died as a result of lacking good medical care during her pregnancy.  Author1 feels life begins at birth.

Author2 is Pro-Life and disagrees with the remark about President Bush advising he would have no issue with government funded healthcare for women as long as it didn't include abortion.  Author2 feels Author1's statement in regard to a woman dying a minute is unfounded.  Author2 disputes the use of the Prudence article because nowhere in the article does it speculate Prudence wanted an abortion or died from a botched abortion.  Author2 feels life begins at conception.

