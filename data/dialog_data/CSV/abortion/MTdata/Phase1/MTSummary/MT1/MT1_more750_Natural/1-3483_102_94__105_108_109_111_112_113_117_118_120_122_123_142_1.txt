
----------
D0
----------
Author1 states that he cares more for a living human being than a clump of cells. He gives several examples of what being a living human requires, such as breathing and having a heart. A fertilized egg has no brain or heart, which means that a comparison between humans and fertilized eggs is not viable. Author1 makes a distinction between the word fetus and fertilized egg, because Author2 used that word in his argument.

Author2 tells him that he is made of a clump of cells, then he questions whether you need a heart or brain to be considered human. He wonders who set this rule, and he believes that it is a nonsensical unproven standard. Author2 mentions the fact that a fetus has a brain and a heart, but does not have to breathe to live. He asks Author1 who decided that a fertilized egg is inhuman. If it is human, that would make it immoral and illegal to kill it. Author2 tells Author1 that he used the word fetus because he wanted to.


----------
D1
----------
Author1 feels a distinction can be made between a living breathing human being with a heart and brain, and "a clump of cells" that isn't viable on its own.  Author1 feels that Author2 is making absurd comparisons, and denies that his (Author1's) logic would lead one to say a patient undergoing heart surgery or someone holding their breath is temporarily not human.  No human can survive for more than a brief period without a beating heart, or without breathing; whereas a fertilized egg couldn't survive even with those things.  He objects to Author2 substiuting the word "fetus" in for "fertilized egg."

Author2 points out that all human beings can be described as "clumps of cells."  He disagrees with the criteria set out by Author1 for defining "human," saying we each get to decide for ourselves what makes us human; and since a fertilized egg can't speak, we're obliged to "play it safe" and protect its interests. If human, then it is both morally and legally impermissible to destroy it.


----------
D2
----------
Author1 argues that we care more for a human being, more than we do for a bunch of cells. Are the brain and heart enough to make us human, and how are we special compared to other animals? Other animals have brains too, but don't have the same consideration as humans. A fertilized egg is not comparable to to a transplant person, because they are alive without one part, and a fertilized egg is just not the same. A fertilized egg does not have a brain. 
Author2 argues that we are all just bunches of cells, but that is not a compliment or an insult. A heart transplant is a human without a heart, so maybe they aren't. A person does not just need to be brains and hearts and cells. A fetus does not have a brain, heart, or breath, but it is still alive, so we should not kill it. It is a human/person, so one should not legally be able to kill it.


----------
D3
----------
Author1 said that they care more for a living human being than a clump of cells. You have to have a brain, heart, and breathe, or else you will die. A fertilized egg does not have these things. It can't survive outside of a womb. Author1 said they only used the term fertilized egg, and Author2 suddenly started talking about a fetus, which is a different thing. 

Author2 asked do you have to have heart, brain, and be breathing to be a human. The standard for what a person must have to be human can't be proven. A fetus has a brain, heart, and doesn't need to breathe to live. With a fertilized egg, there comes the debate of when it is human. Since you can't ask a fertilized egg, it is better to be safe and protect it as a human, not make a life-ending decision for it. Morally, most people would consider killing an innocent wrong, and only in Roe vs Wade is killing legalized.


----------
D4
----------
Author1 thinks the people on his side care more for living humans than a clump of cells. Author2 says that Author1 is a clump of cells. Author1 takes this as a compliment. Author2 says this was neither a compliment or insult. The question is if he has a heart. Author1 says a heart, brain, or lungs are not comparable to a fertilized egg because the egg doesn't have these things. Author2 says a fetus has these things and talking about an egg still brings you back to the question of what defines a human being. Just because an environment is required to live doesn't mean they are any less a human. The killing of innocents is wrong, so it's better to play it safe. Author1 says he didn't mention the word fetus. He was speaking of an egg. Author2 asks if Author1 is offended. Author1 says his question is still unanswered and again asks why they are talking about a fetus when he was talking about a fertilized egg. Author2 replies because he felt like it.

