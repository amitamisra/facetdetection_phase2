
----------
D0
----------
Author2 argues that life begins when a cell can form a new organism. In other words, Author2 argues that life begins at conception. Author2 disdains the idea that human life begins only when human beings are fully formed. When, Author2 asks, can that determination be made? Author2 says that abortions out of medical need occur very rarely. All other abortions are abortions of want rather than need. Necessary abortions wouldn't be affected if elective abortions were made illegal.

Author1 argues that women would put their lives in jeopardy if elective abortion was illegal. They would potentially need to get abortions from 'quacks and butchers.' Author1 states that women need abortions whenever they're in desperate situations without support, not only when it's necessary from a medical standpoint.


----------
D1
----------
Author1 said making abortion illegal will put women's lives in jeopardy. All medical procedures have a risk of jeopardy, and should be done by professionals, not quacks. Some women need abortion because they don't have support. 

Author2 said legal abortions carry a risk. He defines when an egg is fertilized it becomes a person, because that's when it's capable of becoming a person. Whether it's formed or not doesn't matter because development continues all the way through childhood to adulthood. Only 2.8% of abortions are needed because the mother's health is at risk, and she would receive the operation. It's the other elective abortions women want rather than need he is against. He argues all women could find adoption help to support them.


----------
D2
----------
Author1 states that making abortion illegal will put women's lives at risk. Author2 responds that legal abortions also put women's lives at risk.  Author1 says that all medical procedures carry some risk, which is why they need to be done by medical professionals.  Author2 says that only 2.8% of abortions are a matter of NEED, meaning the woman's life is in jeopardy.  The rest are about want, not need, and do not override the right of the fetus to live.  Author1 responds that abortion can be a need to women in desperate situations.  Not all women have friends and family to support them.  Author2 disagrees, saying the every woman has a friend and family who'll help with adoption.


----------
D3
----------
Author1 believes that abortion is a necessary procedure and that making it illegal will result in women having to risk their lives in order to obtain one. They point out that without qualified doctors, many women will be forced to rely on "quacks and butchers" in order to get abortions. They site that abortions are in fact a need due to lack of financial means to support a child. Author2 feels that most abortions are actually a want, not a need. They point out that fetuses are alive as compared to other cells in the body, and that abortions are only a need in life threatening situations. The also point out the availability of families willing and able to adopt unwanted children.


----------
D4
----------
Author1 asks how making incest illegal puts a woman's life in danger. Making abortion illegal does. Author2 replies making abortion legal does as well. He says a human becomes a person when a cell gains the ability to form into a new organism. By using the logic that we can kill a fetus because it isn't fully formed you can argue we can kill children up to puberty because they aren't fully developed. Author1 says abortion is a medical procedure and all procedures carry a risk. By Author2's logic all medical procedures should be outlawed. Author2 replies he never said that. Also, abortion is an elective procedure. Author1 says when women are in a desperate situation it becomes a need. Author2 replies it is a want.

