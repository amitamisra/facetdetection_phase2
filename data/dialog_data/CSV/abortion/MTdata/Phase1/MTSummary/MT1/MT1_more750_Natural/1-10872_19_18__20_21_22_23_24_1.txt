
----------
D0
----------
Author1 believes that abortion is a failure unlike the pro-lifers who see it as a solution. That even if it is a pregnancy by rape the fact that a potential child is destroyed means someone somewhere along the way failed. He goes on to say that part of the problem is men like Author2 who tend to see an unborn child as a "problem when it suits them" and label the fetus as a "non-entity or parasite to justify getting rid of it." He believes the unborn has a right to be heard and can't understand how destroying it could ever be ok.

Author2 disagrees stating that the pro-lifers see a choice not solution. He states that he doesn't know every situation, but pregnancy from rape would be one that abortion might be called for and not considered a failure. Author2 goes on to say that the"non-entity" can't be denied anything because it can't possess anything. He believes that how men see abortion is of little or "no consequence" and that it is up to the woman.


----------
D1
----------
Author1 is Pro-Life.  He feels aborting a fetus is the same as killing an infant after birth.  He feels every terminated pregnancy is a failure.  He feels abortion should not be used, even in the case of rape and feels all children should be given the chance to realize their potential.  Author1 feels those in favor of abortion basically label a fetus as a parasite to justify the killing of a fetus.  He feels life begins at conception and abortion is just murder.

Author2 is Pro-Choice.  He feels each situation is different and all cannot be handled in the same manner.  He feels too many men attempt to speak for women.  He also feels conception does not begin life and the unborn do not have a voice as they are not yet alive.   Author2 feels no man can truly understand the process of an abortion and; therefore, is unable to make a true statement in regard to the right or wrong of the situation.


----------
D2
----------
Author1's problem with pro choicers is they see abortion as a solution rather than a failure. Killing a fetus before it gets a brain isn't better than killing it later, and termination should be seen as a failure. Author2 thought they saw it as a choice. He doesn't agree choices are solutions. Author1 thinks denying a child a chance live isn't ok. He questions if a rape resulting in pregnancy that results in abortion doesn't indicate there was a failure. He doesn't agree with abortion. Author2 says you can't deny something to a non-entity and he doesn't think it's ok for Author1 to speak for all women. The unborn has no standing. Author1 thinks men like Author2 see an unborn child as a problem when it fits them, but a fetus is a victim when aborted. The unborn is being dehumanized. Author2 thinks what any man thinks doesn't matter. The reasons for abortion are no one's business but the woman's. Author1 says more than just the woman is involved. The fetus is killed.


----------
D3
----------
Author1 argues that pro choicers who think abortion is a solution to a problem is a bad thing, and that instead abortion should be seen as a failure of the motherhood process. Abortion is killing a baby, or a fetus, and the timing doesn't affect how it is morally wrong. A potential child should be given the chance to live, and getting rid of that chance is a failure. Even unborn children who come from rapes should not be ended, because that child should be given a chance. Unborn children should not be treated as problems. The only person that is destroyed is a fetus, and that means that there is more than just the woman's choice involved.

Author2 argues that a choice is a choice, and abortion should be able to be a choice for mothers. There is nothing that is possessed or has rights, other than the mother, in these situations. The unborn doesn't, and shouldn't have a voice or rights. The pregnant woman's rights are very important and should be respected.


----------
D4
----------
Author1 said pro-lifer's see abortion as a solution, whereas he sees it as a failure. He doubts the woman thinks it's okay and the fetus certainly doesn't. He says men are the problem because they treat the fetus like a problem. They dehumanize it to justify destroying it. Cases of rape are very rare, but used as justification for legal abortions. He doesn't think there is ever any justification. He said he doesn't speak for women like Author2 tries to. He doesn't agree with the majority of women that a fetus has no standing or voice in the matter. 

Author2 said they see it as a choice, not a solution. He asked what about as a result of rape. He says the fetus is a non-entity, which means it has no rights or isn't being denied because it isn't a person. He says men don't have a right to speak for women. Author1's opinion isn't shared by many women. He doesn't see the best decision of a number of bad ones as a failure.

