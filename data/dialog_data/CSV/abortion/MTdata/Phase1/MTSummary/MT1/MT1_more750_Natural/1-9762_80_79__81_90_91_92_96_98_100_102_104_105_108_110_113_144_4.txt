
----------
D0
----------
Author1 argued that there are liberals who disliked the presidential order that forbid federal funding for abortion that Obama passed. All men who reject Jesus are liars and not to be trusted. The Bible is the word of god, Adam was the first man created by god, and the reincarnation of Christ happened. The resurrection is true, and if it is false then so is everything else, because it is the core of what makes Christianity. The bible says that the world is 6000 years old and anyone who says otherwise is stupid.
Author2 argues that religious people have been bigoted and silly, because they have done nasty things in the past and not owned up for it. There is no proof to the bible, because the only real evidence for the bible is withing the bible at all. The resurrection has nothing to do with the issues at hand, and the bible wasn't written down until after a very long time after the events apparently happened.


----------
D1
----------
Author1 wonders how the pro-infanticide liberals liked Obama's Presidential order that says no federal funding for abortion. Author2 says when backed into a corner he never answers questions, he just repeats the same thing over and over again. Author1 thinks any man that rejects Jesus is a liar. Author2 thinks that it's very difficult to have an intelligent discussion with Author1. He doesn't believe in the Bible and asks for proof. Author1 asks when Author2 is going to get saved. Author2 takes this as proof he doesn't answer questions and asks again for it. Author1 talks about the resurrection, and the age of the Earth. Author2 thinks Author1 fails to take into account that the Bible was passed down through many generations and is likely distorted before ever being written down. He asks if Author2 actually believes in talking snakes and virgin birth, or if these truths could have been distorted through the years of oral tradition. He says that once Author2 can prove that these things are true then they can talk about them.


----------
D2
----------
Author1 asks whether "pro-infanticide liberals" liked Obama's Presidential order disallowing federal funding for abortions.  The conversation becomes a volley of insults, and arguing over the Bible, with nothing more said on abortion.  Author1 challenges Author2 to "bust [the truth of] the resurrection," which he says is the crux of Christianity, and proof of God's existence.  He also claims that Jesus made reference to the first man being Adam, and says that anyone who doesn't believe in Jesus Christ is a liar, and shouldn't be trusted.

Author2 doesn't comment on Obama's decision regarding federal funding for abortion.  He accuses Author1 of not answering questions once he's backed into a corner.  He compares Jesus to Santa Claus, and asks what proof Author1 has that the Bible is the word of God.  He points out that the stories in the Bible were passed down by oral tradition for generations, and surely changed over time.  He mocks Author1 for believing in talking snakes and virgin births.


----------
D3
----------
Author1 is Pro-Life and seems to base the feelings on religion.  The actual topic of abortion is not discussed in this thread.  Author1 answers each question with a supportive statement about Christianity and the Bible.  Author1 challenges Author2 to refute the story of the resurrection of Jesus.  Author1 feels Author2 needs to repent and be saved or he will go to hell.  Author1 does note that President Barack Obama cut public funding for abortions.  

Author2 appears to be Pro-Choice but does not verify this in the thread.  He repeatedly challenges Author1 to provide proof of his religious statements, to which Author1 replies with more religion.  Author2 compares belief in the Bible to belief in Santa Claus.  Author2 feels it is not right to base your entire existence on a book which was written based upon interpretations from hundreds of years ago.  He feels so many years of having been spoken of before being written down could have distorted what he feels to be stories.


----------
D4
----------
Author1 asked how liberals felt after Obama issued an order of no federal funding for abortion. He said any man who rejects Jesus is a liar. The first man alive was Adam and his God is the God of the Bible. God himself mentioned Adam in the Bible. To prove their is no God, Author2 would have to prove there was no resurrection. He said the resurrection is the crux of Christianity, because Jesus said he would rise in 3 days and he did.

Author2 said he never responds to questions, and tries to distract people from arguments. He said Author1 still hasn't answered his questions. He asked Author1 if he was serious about believing. He asked where is Author1's proof the resurrection did happen, or that the Bible is the word of God, or Jesus spoke of Adam. He said the stories of the Bible were passed down verbally for hundreds of years before being written down, and it would be doubtful they would remain accurate after all that time.

