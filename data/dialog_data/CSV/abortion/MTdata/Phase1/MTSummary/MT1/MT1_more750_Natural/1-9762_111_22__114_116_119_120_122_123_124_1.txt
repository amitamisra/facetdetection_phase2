
----------
D0
----------
Author1 believe the bigger picture is following the Bible and living God's laws, therefore killing an unborn baby is wrong.  Author1 wonders what divine insight the pro-abortion crowd to believe as they do.  Author1 feels that most Americans share his opinions.  Author1 is angry that Author2 can defend the ideals of infanticide of innocent unborn.  That one day Author2 will be held accountable for his beliefs by God.  Author1 feels that murder is murder, whether a person is old or unborn. 

Author2 feels that each person should act as they believe within their own families.  Not all Americans share the same beliefs to metaphysical questions.  Author2 wants to know why Author1 beliefs should be the law in the universe.  Author2 wants to know why people like Author1 attack those whose opinions are different from theirs.  Author2 doesn't equate abortion with infanticide.  Author2 states we do not live in a theocracy.  Maybe Author1 might be better suited to live in Iran instead of America.


----------
D1
----------
Author1 argues that the biblical verses he quotes show that people are real people before they are born. They are real people well before they were born. The pro-abortion crowd believes that you can destroy what god creates just because it hasn't been born yet, and that is not good. Abortion is the infanticide of the innocent unborn. Abortionists want to hide the dead very quickly, and that is very much against the will of God. The US is not a theocracy, and abortion is the same thing as murder. Murdering the innocent people is wrong no matter where one lives.
Author2 believes that action must come from within a family structure, because the question is not black and white, but rather a metaphysical one. Anger implies that your opinions are not very strong, and not everyone believes that abortion is the same thing as infanticide. The US is not a theocracy, but Iran is a theocracy.


----------
D2
----------
Author1 quotes the Bible and asks how people think they have the right to destroy life. Author2 says not all Americans believe as Author1, or in God. He asks why his way of life should be law. People should be able to make their own choices. Author1 again quotes the Bible and says not everyone shares Author2's beliefs either. Author2 thinks Author1 is angry and asks why Author1 thinks it is fine to attack others that don't agree with him. Author1 thinks people that support abortion should have to clean after it and bury the unborn. Author2 thinks Author1 is way off base and irrelevant to those that don't equate abortion with infanticide. Author1 thinks Author2's opinion is contrary to God's will and one day he will have to account for it. He wishes he could legally deport Author2. Author2 questions how he dares to claim to know the will of God. We aren't in a theocracy and if Author1 wants one he should move to Iran. Author1 says murder is murder and it's wrong.


----------
D3
----------
Author1 quotes Bible passages about how God created someone in the womb, and asked why pro-lifers think they have the right to destroy it. Author1 believes they are in the majority of Americans who view killing unborn children as wrong. Pro-choice people should have to clean up after an abortion to see what they are supporting. Abortion is contrary to God's will, and they will have to answer for it. Murder is murder, whether it is young or old. Godly women don't kill their unborn. 

Author2 said if Author1 believes that and wants to practice that in their family, that is fine, but they have no right to make their views a law for everyone else. He condemns Author1 for attacking instead of debating. Name calling is irrelevant to the debate that some people don't see abortion as infanticide. He questions how Author1 can be sure he knows the will of God. He trusts the women in his family to make the best choice for them.


----------
D4
----------
Author1 quotes the bible as saying that God created the child in the womb, and asks what divine insight the pro-choice side has that gives them the right to interfere.  A belief that abortion is not infanticide is contrary to God's will.  Murder is murder, whether  the victim is old or young.  A religious state shouldn't even be necessary to know that killing someone is wrong.  Pro-choicers don't value life very much.  Godly women nurture their young, they don't poison them.

Author2 says that Author1's beliefs should be practiced within his family only.  Author1 can make his own personal decision, but why should that be the law of the land?  Author2 calls people like himself "Springsteen-Americans," and says he's just as American as is Author1. Author2's view is not that it is okay to commit infanticide, because abortion is not infanticide.  Author2 has more confidence in woman to make up their own minds than does Author1.

