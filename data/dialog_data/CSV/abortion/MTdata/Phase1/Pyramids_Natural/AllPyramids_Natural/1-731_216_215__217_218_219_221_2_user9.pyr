<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>Author1 argues that women should not be counseled into going into abortion clinics because that is lying to innocent people. There are no disabled people that are born with tails, and all successful births do not look like embryos. The cellular structure of a human is the same once it is born, so once that cellular structure is completed there shouldn't be any abortions because then you are killing a human and not just a mass of cells. The claims about emergency contraception, birth control, and other medicines need to be analyzed for the validity of those claims. </line>
  <line>Author2 argues that toddlers don't visually appear to be the same thing as adult humans, and that children who are deformed also do not look similar, and that we shouldn't be able to kill things/have an abortion just based on looks. There are people who have been born (such as the elephant man) who are treated as less than human, and that's wrong, because humans are humans and shouldn't be killed or aborted.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>Author1 says embryos are more similar to mollusks than humans and thinks it is wrong to counsel women going into abortion clinics. Author2 says that a toddler is underdeveloped. It still has 16 years to go. He asks about deformed children and if we can kill them. Just because someone doesn't look human doesn't mean they aren't. Author1 says he is talking about cell structure, not looks. Author2 says everyone has tails. He asks if the only way a zygote will be human, or a toddler, is if someone looks at it. Author1 again says he is talking about cellular structure, not looks. He asks if Author2 really thinks birth control is unsafe and an abortifacient. He asks if he's researched the claims. Author2 says this doesn't matter. The infant only weights a few pounds and adults weigh over a hundred. People with Down's Syndrome have too many or few chromosomes. These things don't mean they don't have a right to live. A defective cell structure isn't a reason to kill someone in Author2's opinion.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>Author1 believes that an unborn fetus is not the same as a fully formed human.  He emphasizes that is not about what the fetus looks like, but instead about its basic structure. Because fetuses don't have fully developed brains or bodies, it is wrong to say that abortion is the same as killing a human. He also believes it is morally wrong to harass people going into abortion clinics. Claims that abortions or contraception cause cancer and other harm to women are incorrect.</line>
  <line>Author2 believes that it is wrong to draw a distinction between fetuses and people who are already born. By saying that a fetus is not yet human, it could raise other questions about how to define what a human is. For example, some people might think that disabled people are not human or that toddlers are not yet fully grown. Allowing abortions is on the same moral ground as allowing disabled or deformed people to be killed. He also suggests that abortion can cause lasting damage to a woman's body and mind.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>Author1 asserts that fetuses do not look human, and the American Life League is wrong to counsel women going into abortion clinics. He believes that fetuses are like organisms, and he stresses that he is referring to their cellular structure. He asks Author2 if he believes their claim that emergency contraceptives are unsafe abortifacients, abortion causes breast cancer &amp; severe psychological trauma. He questions Author2 on the validity of those claims, and Author1 asks him if he researched it to see if the claims were true or not.</line>
  <line></line>
  <line>Author2 points out that deformed humans do not look human, and that toddlers are not fully developed. The American Life League offers post abortion treatment centers as well, and he believes that these organisms are human. It does not matter that a zygote does not look human, or that a toddler does not look like an adult. They are both human, and our cellular structure is always different in the cycle of life. He does not know what the effects of abortion are, but abortions are dangerous.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>Author1 said one is a toddler and the other is undeveloped, similar to a simple organism like a mollusk than a born human. It is wrong to try and dissuade women from using an abortion clinic. Author2 clarifies that they are not talking about looks, but cellular structure. Author1 chides Author2 for telling lies to people going into abortion clinics. They question if Author2 believes the claims that contraception is unsafe, causes cancer, and psychological trauma, and asked if Author2 has researched the truth about those claims.</line>
  <line></line>
  <line>Author2 said the toddler is undeveloped, and still has years to grow. Some deformed children don't look human. Just because someone doesn't look human doesn't mean they aren't human. Author2 cites the differences of an infant to an adult human, and asked if that makes them somehow not human. Author2 questions if cellular structure is different such as in Down's Syndrome, if that means people with different cellular structure can be killed. The reason abortion is so dangerous is because the uterus is opened prematurely.</line>
 </text>
 <scu uid="11" label="Children who are deformed do not look similar">
  <contributor label="and that children who are deformed also do not look similar">
   <part label="and that children who are deformed also do not look similar" start="687" end="746"/>
  </contributor>
  <contributor label="Author2 points out that deformed humans do not look human">
   <part label="Author2 points out that deformed humans do not look human" start="3624" end="3681"/>
  </contributor>
  <contributor label="He asks about deformed children">
   <part label="He asks about deformed children" start="1244" end="1275"/>
  </contributor>
  <contributor label="Some deformed children don't look human">
   <part label="Some deformed children don't look human" start="4750" end="4789"/>
  </contributor>
 </scu>
 <scu uid="31" label="Claims that abortions or contraception cause cancer and other harm to women are incorrect">
  <contributor label="Claims that abortions or contraception cause cancer and other harm to women are incorrect">
   <part label="Claims that abortions or contraception cause cancer and other harm to women are incorrect" start="2454" end="2543"/>
  </contributor>
  <contributor label="He asks if Author2 really thinks birth control is unsafe and an abortifacient">
   <part label="He asks if Author2 really thinks birth control is unsafe and an abortifacient" start="1619" end="1696"/>
  </contributor>
  <contributor label="He asks Author2 if he believes their claim that emergency contraceptives are unsafe abortifacients, abortion causes breast cancer &amp; severe psychological trauma">
   <part label="He asks Author2 if he believes their claim that emergency contraceptives are unsafe abortifacients, abortion causes breast cancer &amp; severe psychological trauma" start="3328" end="3487"/>
  </contributor>
  <contributor label="They question if Author2 believes the claims that contraception is unsafe, causes cancer, and psychological trauma">
   <part label="They question if Author2 believes the claims that contraception is unsafe, causes cancer, and psychological trauma" start="4497" end="4611"/>
  </contributor>
 </scu>
 <scu uid="16" label="Embryos are not fully formed humans">
  <contributor label="Author1 says embryos are more similar to mollusks than humans">
   <part label="Author1 says embryos are more similar to mollusks than humans" start="1037" end="1098"/>
  </contributor>
  <contributor label="Author1 believes that an unborn fetus is not the same as a fully formed human...Because fetuses don't have fully developed brains or bodies">
   <part label="Author1 believes that an unborn fetus is not the same as a fully formed human" start="2067" end="2144"/>
   <part label="Because fetuses don't have fully developed brains or bodies" start="2245" end="2304"/>
  </contributor>
  <contributor label="Author1 asserts that fetuses do not look human">
   <part label="Author1 asserts that fetuses do not look human" start="3085" end="3131"/>
  </contributor>
  <contributor label="Author1 said one is a toddler and the other is undeveloped, similar to a simple organism like a mollusk than a born human">
   <part label="Author1 said one is a toddler and the other is undeveloped, similar to a simple organism like a mollusk than a born human" start="4145" end="4266"/>
  </contributor>
 </scu>
 <scu uid="17" label="It is wrong to counsel women going into abortion clinics">
  <contributor label="and thinks it is wrong to counsel women going into abortion clinics">
   <part label="and thinks it is wrong to counsel women going into abortion clinics" start="1099" end="1166"/>
  </contributor>
  <contributor label="He also believes it is morally wrong to harass people going into abortion clinics">
   <part label="He also believes it is morally wrong to harass people going into abortion clinics" start="2371" end="2452"/>
  </contributor>
  <contributor label="and the American Life League is wrong to counsel women going into abortion clinics">
   <part label="and the American Life League is wrong to counsel women going into abortion clinics" start="3133" end="3215"/>
  </contributor>
  <contributor label="It is wrong to try and dissuade women from using an abortion clinic">
   <part label="It is wrong to try and dissuade women from using an abortion clinic" start="4268" end="4335"/>
  </contributor>
 </scu>
 <scu uid="39" label="Question the claims about emergency contraception and birth control">
  <contributor label="He questions Author2 on the validity of those claims, and Author1 asks him if he researched it to see if the claims were true or not">
   <part label="He questions Author2 on the validity of those claims, and Author1 asks him if he researched it to see if the claims were true or not" start="3489" end="3621"/>
  </contributor>
  <contributor label="He asks if he's researched the claims">
   <part label="He asks if he's researched the claims" start="1698" end="1735"/>
  </contributor>
  <contributor label="and asked if Author2 has researched the truth about those claims">
   <part label="and asked if Author2 has researched the truth about those claims" start="4613" end="4677"/>
  </contributor>
  <contributor label="The claims about emergency contraception, birth control, and other medicines need to be analyzed for the validity of those claims">
   <part label="The claims about emergency contraception, birth control, and other medicines need to be analyzed for the validity of those claims" start="466" end="595"/>
  </contributor>
 </scu>
 <scu uid="19" label="Refer to cell structure">
  <contributor label="Author1 says he is talking about cell structure, not looks...Author1 again says he is talking about cellular structure, not looks">
   <part label="Author1 says he is talking about cell structure, not looks" start="1367" end="1425"/>
   <part label="Author1 again says he is talking about cellular structure, not looks" start="1549" end="1617"/>
  </contributor>
  <contributor label="He emphasizes that is not about what the fetus looks like, but instead about its basic structure">
   <part label="He emphasizes that is not about what the fetus looks like, but instead about its basic structure" start="2147" end="2243"/>
  </contributor>
  <contributor label="and he stresses that he is referring to their cellular structure">
   <part label="and he stresses that he is referring to their cellular structure" start="3262" end="3326"/>
  </contributor>
  <contributor label="Author2 clarifies that they are not talking about looks, but cellular structure">
   <part label="Author2 clarifies that they are not talking about looks, but cellular structure" start="4337" end="4416"/>
  </contributor>
 </scu>
 <scu uid="10" label="Toddlers don't look like adult humans">
  <contributor label="Author2 argues that toddlers don't visually appear to be the same thing as adult humans">
   <part label="Author2 argues that toddlers don't visually appear to be the same thing as adult humans" start="598" end="685"/>
  </contributor>
  <contributor label="Author2 says that a toddler is underdeveloped. It still has 16 years to go">
   <part label="Author2 says that a toddler is underdeveloped. It still has 16 years to go" start="1168" end="1242"/>
  </contributor>
  <contributor label="and that toddlers are not fully developed...or that a toddler does not look like an adult">
   <part label="and that toddlers are not fully developed" start="3683" end="3724"/>
   <part label="or that a toddler does not look like an adult" start="3901" end="3946"/>
  </contributor>
  <contributor label="Author2 said the toddler is undeveloped, and still has years to grow...Author2 cites the differences of an infant to an adult human">
   <part label="Author2 said the toddler is undeveloped, and still has years to grow" start="4680" end="4748"/>
   <part label="Author2 cites the differences of an infant to an adult human" start="4863" end="4923"/>
  </contributor>
 </scu>
 <scu uid="21" label="A zygote is human despite it does not look human">
  <contributor label="He asks if the only way a zygote will be human, or a toddler, is if someone looks at it">
   <part label="He asks if the only way a zygote will be human, or a toddler, is if someone looks at it" start="1460" end="1547"/>
  </contributor>
  <contributor label="and he believes that these organisms are human...It does not matter that a zygote does not look human">
   <part label="It does not matter that a zygote does not look human" start="3847" end="3899"/>
   <part label="and he believes that these organisms are human" start="3799" end="3845"/>
  </contributor>
  <contributor label="because humans are humans">
   <part label="because humans are humans" start="948" end="973"/>
  </contributor>
 </scu>
 <scu uid="34" label="Abortion can cause damage to a woman">
  <contributor label="He also suggests that abortion can cause lasting damage to a woman's body and mind">
   <part label="He also suggests that abortion can cause lasting damage to a woman's body and mind" start="2974" end="3056"/>
  </contributor>
  <contributor label="He does not know what the effects of abortion are, but abortions are dangerous">
   <part label="He does not know what the effects of abortion are, but abortions are dangerous" start="4038" end="4116"/>
  </contributor>
 </scu>
 <scu uid="2" label="Counseling women to go to abortion clinics is lying to innocent people">
  <contributor label="because that is lying to innocent people">
   <part label="because that is lying to innocent people" start="109" end="149"/>
  </contributor>
  <contributor label="Author1 chides Author2 for telling lies to people going into abortion clinics">
   <part label="Author1 chides Author2 for telling lies to people going into abortion clinics" start="4418" end="4495"/>
  </contributor>
 </scu>
 <scu uid="18" label="Just because someone doesn't look human doesn't mean they aren't">
  <contributor label="Just because someone doesn't look human doesn't mean they aren't">
   <part label="Just because someone doesn't look human doesn't mean they aren't" start="1301" end="1365"/>
  </contributor>
  <contributor label="Just because someone doesn't look human doesn't mean they aren't human...and asked if that makes them somehow not human">
   <part label="Just because someone doesn't look human doesn't mean they aren't human" start="4791" end="4861"/>
   <part label="and asked if that makes them somehow not human" start="4925" end="4971"/>
  </contributor>
 </scu>
 <scu uid="12" label="We shouldn't be able to kill things just based on looks">
  <contributor label="and that we shouldn't be able to kill things/have an abortion just based on looks">
   <part label="and that we shouldn't be able to kill things/have an abortion just based on looks" start="748" end="829"/>
  </contributor>
  <contributor label="and if we can kill them">
   <part label="and if we can kill them" start="1276" end="1299"/>
  </contributor>
 </scu>
 <scu uid="28" label="A defective cell structure isn't a reason to kill someone">
  <contributor label="A defective cell structure isn't a reason to kill someone in Author2's opinion">
   <part label="A defective cell structure isn't a reason to kill someone in Author2's opinion" start="1960" end="2038"/>
  </contributor>
  <contributor label="Author2 questions if cellular structure is different such as in Down's Syndrome, if that means people with different cellular structure can be killed">
   <part label="Author2 questions if cellular structure is different such as in Down's Syndrome, if that means people with different cellular structure can be killed" start="4973" end="5122"/>
  </contributor>
 </scu>
 <scu uid="30" label="Abortion is not the same as killing a human">
  <contributor label="it is wrong to say that abortion is the same as killing a human">
   <part label="it is wrong to say that abortion is the same as killing a human" start="2306" end="2369"/>
  </contributor>
 </scu>
 <scu uid="37" label="Allowing abortions is like allowing disabled or deformed people to be killed">
  <contributor label="Allowing abortions is on the same moral ground as allowing disabled or deformed people to be killed">
   <part label="Allowing abortions is on the same moral ground as allowing disabled or deformed people to be killed" start="2873" end="2972"/>
  </contributor>
 </scu>
 <scu uid="22" label="Ask if he really thinks birth control is unsafe and an abortifacient">
  <contributor label="Claims that abortions or contraception cause cancer and other harm to women are incorrect">
   <part label="Claims that abortions or contraception cause cancer and other harm to women are incorrect" start="2454" end="2543"/>
  </contributor>
 </scu>
 <scu uid="20" label="Everyone has tails">
  <contributor label="Author2 says everyone has tails">
   <part label="Author2 says everyone has tails" start="1427" end="1458"/>
  </contributor>
 </scu>
 <scu uid="38" label="Fetuses are like organisms">
  <contributor label="He believes that fetuses are like organisms">
   <part label="He believes that fetuses are like organisms" start="3217" end="3260"/>
  </contributor>
 </scu>
 <scu uid="32" label="It is wrong to draw a distinction between fetuses and people who are born">
  <contributor label="Author2 believes that it is wrong to draw a distinction between fetuses and people who are already born">
   <part label="Author2 believes that it is wrong to draw a distinction between fetuses and people who are already born" start="2545" end="2648"/>
  </contributor>
 </scu>
 <scu uid="8" label="Once that cellular structure is completed an abortion is killing a human">
  <contributor label="because then you are killing a human and not just a mass of cells">
   <part label="because then you are killing a human and not just a mass of cells" start="399" end="464"/>
  </contributor>
 </scu>
 <scu uid="7" label="Once that cellular structure is completed there shouldn't be any abortions">
  <contributor label="so once that cellular structure is completed there shouldn't be any abortions">
   <part label="so once that cellular structure is completed there shouldn't be any abortions" start="321" end="398"/>
  </contributor>
 </scu>
 <scu uid="42" label="Our cellular structure is always different in the cycle of life">
  <contributor label="They are both human, and our cellular structure is always different in the cycle of life">
   <part label="They are both human, and our cellular structure is always different in the cycle of life" start="3948" end="4036"/>
  </contributor>
 </scu>
 <scu uid="26" label="People with Down's Syndrome have too many or few chromosomes">
  <contributor label="People with Down's Syndrome have too many or few chromosomes">
   <part label="People with Down's Syndrome have too many or few chromosomes" start="1841" end="1901"/>
  </contributor>
 </scu>
 <scu uid="33" label="Saying that a fetus is not human could raise other questions about how to define a human">
  <contributor label="By saying that a fetus is not yet human, it could raise other questions about how to define what a human is">
   <part label="By saying that a fetus is not yet human, it could raise other questions about how to define what a human is" start="2650" end="2757"/>
  </contributor>
 </scu>
 <scu uid="35" label="Some people might think that disabled people are not human">
  <contributor label="For example, some people might think that disabled people are not human">
   <part label="For example, some people might think that disabled people are not human" start="2759" end="2830"/>
  </contributor>
 </scu>
 <scu uid="36" label="Some people might think that toddlers are not yet fully grown">
  <contributor label="or that toddlers are not yet fully grown">
   <part label="or that toddlers are not yet fully grown" start="2831" end="2871"/>
  </contributor>
 </scu>
 <scu uid="4" label="Successful births do not look like embryos">
  <contributor label="and all successful births do not look like embryos">
   <part label="and all successful births do not look like embryos" start="206" end="256"/>
  </contributor>
 </scu>
 <scu uid="41" label="The American Life League offers post abortion treatment centers">
  <contributor label="The American Life League offers post abortion treatment centers as well">
   <part label="The American Life League offers post abortion treatment centers as well" start="3726" end="3797"/>
  </contributor>
 </scu>
 <scu uid="5" label="The cellular structure of a human is the same once it is born">
  <contributor label="The cellular structure of a human is the same once it is born">
   <part label="The cellular structure of a human is the same once it is born" start="258" end="319"/>
  </contributor>
 </scu>
 <scu uid="40" label="The infant only weights a few pounds and adults weigh over a hundred">
  <contributor label="The infant only weights a few pounds and adults weigh over a hundred">
   <part label="The infant only weights a few pounds and adults weigh over a hundred" start="1771" end="1839"/>
  </contributor>
 </scu>
 <scu uid="44" label="The reason abortion is so dangerous is because the uterus is opened prematurely">
  <contributor label="The reason abortion is so dangerous is because the uterus is opened prematurely">
   <part label="The reason abortion is so dangerous is because the uterus is opened prematurely" start="5124" end="5203"/>
  </contributor>
 </scu>
 <scu uid="3" label="There are no disabled people that are born with tails">
  <contributor label="There are no disabled people that are born with tails">
   <part label="There are no disabled people that are born with tails" start="151" end="204"/>
  </contributor>
 </scu>
 <scu uid="13" label="There are people who were treated as less than human">
  <contributor label="There are people who have been born (such as the elephant man) who are treated as less than human">
   <part label="There are people who have been born (such as the elephant man) who are treated as less than human" start="831" end="928"/>
  </contributor>
 </scu>
 <scu uid="27" label="These things don't mean they don't have a right to live">
  <contributor label="These things don't mean they don't have a right to live">
   <part label="These things don't mean they don't have a right to live" start="1903" end="1958"/>
  </contributor>
 </scu>
 <scu uid="24" label="This doesn't matter">
  <contributor label="Author2 says this doesn't matter">
   <part label="Author2 says this doesn't matter" start="1737" end="1769"/>
  </contributor>
 </scu>
 <scu uid="14" label="Treating people as less than human is wrong">
  <contributor label="and that's wrong">
   <part label="and that's wrong" start="930" end="946"/>
  </contributor>
 </scu>
 <scu uid="1" label="Women should not be counseled into going into abortion clinics">
  <contributor label="Author1 argues that women should not be counseled into going into abortion clinics">
   <part label="Author1 argues that women should not be counseled into going into abortion clinics" start="26" end="108"/>
  </contributor>
 </scu>
 <scu uid="45" label="Embryos shouldn't be aborted">
  <contributor label="and shouldn't be killed or aborted">
   <part label="and shouldn't be killed or aborted" start="974" end="1008"/>
  </contributor>
 </scu>
</pyramid>
