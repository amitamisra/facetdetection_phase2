<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>Author1 says Author2 stated he would force a pregnancy to term. This means they both use force. Author2 was talking about abortion in the early trimesters being a responsible answer to unwanted pregnancy and it being an advantage of letting the woman choose whereas Author1 would force her. Author1 thinks this is a double standard. Author2 thinks he gave an acceptable compromise and his view is being misrepresented. Author1 replies he isn't misrepresenting it. Author2 said he would use force after claiming that the use of force was wrong. He thinks this is a double standard. Author2 states he said Author1 was wrong for using force in that situation, but that doesn't mean that it is wrong to use it in any situation. He also didn't claim it was alright when he did it, but not when others did. He doesn't see any hope of resolving the issue, so he thinks it is best to drop it and move on to other issues that aren't inane. Author1 asks how he was wrong in his argument.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>Author1 argues that, in order to make an argument that using force is wrong, you can't make the argument in the other direction too: that your use of force is right. Double standards are bad and should not be allowed in a debate. Force cannot be good for one person's argument and bad for another persons argument. </line>
  <line>Author2 argues that abortion in the early trimesters is acceptable, and that is good because a woman can then make choices about her body and that is a good thing. The use of force by the government, in order to stop a woman from being able to make choices about her body, especially concerning abortions. The government should not be able to stop a woman from choosing what to do with her body. Force is no wrong in all instances, though, and sometimes force is appropriate. There are times when force is justifiable in order to keep order and protect the typical life of people.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>Author1 said that Author2 said he would force someone to continue pregnancy later in term, yet said Author1 would be wrong to force someone to pregnancy in earlier terms. Author1 said saying Author1 is wrong to force someone then is wrong. Author1 continues to argue that use of force in one situation means use of force in another isn't wrong. </line>
  <line></line>
  <line>Author2 clarifies that abortion in early term is a responsible solution for unwanted pregnancy, and gives the woman the choice. He would allow abortion to be outlawed in the third trimester. Author2 assumed that just because he said force was wrong early on, that he meant it is wrong to force someone. He feels force has to be justified through law. He supports freedom unless there are justifications for the use of force. If Author1 could show justification for force in early trimesters, he would be willing to change his view. Author1 only has his views, not justification, for use of force. They both have to have justification for their uses of force.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>Author1 starts out by pointing out how Author2 uses force in their argument and then poses the question of why did Author2 bother to state how Author1 uses &quot;force&quot; if not to state it was wrong. Author2 believes abortion to be a viable option for dealing with an unwanted pregnancy especially in the first trimester, but in the third in which case a pregnancy should be forced. Author1 exposes Author2's double standard on the use of force in comparison between a forced birth at term vs. a termination of the pregnancy. It is at this point where Author2 starts to reject continuing the debate, to which Author1 restates Author2's argument in the form of a double standard. Author2 continues to reply to these jabs claiming that they want their argument to be properly understood before ending the discussion. Author1 continues to defend their view of the pace of the debate up to this point and Author2 concludes that their debate has degenerated into a bias argument of semantics and they will agree to disagree.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>Author1 claims to have been condemned by Author2 for wanting to use force to make women carry their fetuses to term, and yet Author2 supports such force for women in their third trimester.  Author1 calls this a double-standard, where force is acceptable for Author2, but not Author1.  Author1 does not agree that he misinterpreted anything, or jumped to any conclusions.  He says that Author2 has not given any other argument for why Author1 is wrong, but is now acknowledging that the use of force is acceptable, as a way to make a woman carry to term.</line>
  <line></line>
  <line>Author2 says that his views are being misrepresented, and that he did not argue that it was always wrong to force a woman to complete a pregnancy.  Any such force would have to have good justification, which he believes would not apply to early pregnancies.  He suggests they focus on what justifications there might be to force a woman to complete her pregnancy, and not on the issue of force being the main difference.</line>
 </text>
 <scu uid="171" label="Abortion in the early trimesters is a responsible answer to unwanted pregnancy">
  <contributor label="Author2 was talking about abortion in the early trimesters being a responsible answer to unwanted pregnancy">
   <part label="Author2 was talking about abortion in the early trimesters being a responsible answer to unwanted pregnancy" start="122" end="229"/>
  </contributor>
  <contributor label="Author2 argues that abortion in the early trimesters is acceptable">
   <part label="Author2 argues that abortion in the early trimesters is acceptable" start="1347" end="1413"/>
  </contributor>
  <contributor label="Author2 believes abortion to be a viable option for dealing with an unwanted pregnancy especially in the first trimester">
   <part label="Author2 believes abortion to be a viable option for dealing with an unwanted pregnancy especially in the first trimester" start="3182" end="3302"/>
  </contributor>
  <contributor label="Author2 clarifies that abortion in early term is a responsible solution for unwanted pregnancy">
   <part label="Author2 clarifies that abortion in early term is a responsible solution for unwanted pregnancy" start="2302" end="2396"/>
  </contributor>
 </scu>
 <scu uid="205" label="He would force a pregnancy in late term">
  <contributor label="Author1 says Author2 stated he would force a pregnancy to term">
   <part label="Author1 says Author2 stated he would force a pregnancy to term" start="26" end="88"/>
  </contributor>
  <contributor label="Author1 said that Author2 said he would force someone to continue pregnancy later in term...He would allow abortion to be outlawed in the third trimester">
   <part label="Author1 said that Author2 said he would force someone to continue pregnancy later in term" start="1955" end="2044"/>
   <part label="He would allow abortion to be outlawed in the third trimester" start="2430" end="2491"/>
  </contributor>
  <contributor label="and yet Author2 supports such force for women in their third trimester...He says that Author2 has not given any other argument for why Author1 is wrong, but is now acknowledging that the use of force is acceptable, as a way to make a woman carry to term">
   <part label="and yet Author2 supports such force for women in their third trimester" start="4146" end="4216"/>
   <part label="He says that Author2 has not given any other argument for why Author1 is wrong, but is now acknowledging that the use of force is acceptable, as a way to make a woman carry to term" start="4401" end="4581"/>
  </contributor>
  <contributor label="but in the third in which case a pregnancy should be forced">
   <part label="but in the third in which case a pregnancy should be forced" start="3304" end="3363"/>
  </contributor>
 </scu>
 <scu uid="174" label="This is a double standard">
  <contributor label="Author1 thinks this is a double standard...He thinks this is a double standard">
   <part label="Author1 thinks this is a double standard" start="317" end="357"/>
   <part label="He thinks this is a double standard" start="570" end="605"/>
  </contributor>
  <contributor label="you can't make the argument in the other direction too: that your use of force is right">
   <part label="you can't make the argument in the other direction too: that your use of force is right" start="1108" end="1195"/>
  </contributor>
  <contributor label="Author1 exposes Author2's double standard on the use of force in comparison between a forced birth at term vs. a termination of the pregnancy...to which Author1 restates Author2's argument in the form of a double standard">
   <part label="Author1 exposes Author2's double standard on the use of force in comparison between a forced birth at term vs. a termination of the pregnancy" start="3365" end="3506"/>
   <part label="to which Author1 restates Author2's argument in the form of a double standard" start="3582" end="3659"/>
  </contributor>
  <contributor label="Author1 calls this a double-standard">
   <part label="Author1 calls this a double-standard" start="4219" end="4255"/>
  </contributor>
 </scu>
 <scu uid="181" label="Use of force is not wrong in every situation">
  <contributor label="Author2 states he said Author1 was wrong for using force in that situation, but that doesn't mean that it is wrong to use it in any situation">
   <part label="Author2 states he said Author1 was wrong for using force in that situation, but that doesn't mean that it is wrong to use it in any situation" start="607" end="748"/>
  </contributor>
  <contributor label="Force is no wrong in all instances, though, and sometimes force is appropriate">
   <part label="Force is no wrong in all instances, though, and sometimes force is appropriate" start="1743" end="1821"/>
  </contributor>
  <contributor label="and that he did not argue that it was always wrong to force a woman to complete a pregnancy">
   <part label="and that he did not argue that it was always wrong to force a woman to complete a pregnancy" start="4638" end="4729"/>
  </contributor>
  <contributor label="Author1 continues to argue that use of force in one situation means use of force in another isn't wrong">
   <part label="Author1 continues to argue that use of force in one situation means use of force in another isn't wrong" start="2195" end="2298"/>
  </contributor>
 </scu>
 <scu uid="196" label="Force would have to have good justification">
  <contributor label="Any such force would have to have good justification">
   <part label="Any such force would have to have good justification" start="4732" end="4784"/>
  </contributor>
  <contributor label="He supports freedom unless there are justifications for the use of force...They both have to have justification for their uses of force">
   <part label="He supports freedom unless there are justifications for the use of force" start="2653" end="2725"/>
   <part label="They both have to have justification for their uses of force" start="2899" end="2959"/>
  </contributor>
  <contributor label="There are times when force is justifiable in order to keep order and protect the typical life of people">
   <part label="There are times when force is justifiable in order to keep order and protect the typical life of people" start="1823" end="1926"/>
  </contributor>
 </scu>
 <scu uid="173" label="He would force the woman in earlier terms">
  <contributor label="whereas Author1 would force her">
   <part label="whereas Author1 would force her" start="284" end="315"/>
  </contributor>
  <contributor label="yet said Author1 would be wrong to force someone to pregnancy in earlier terms">
   <part label="yet said Author1 would be wrong to force someone to pregnancy in earlier terms" start="2046" end="2124"/>
  </contributor>
  <contributor label="Author1 claims to have been condemned by Author2 for wanting to use force to make women carry their fetuses to term">
   <part label="Author1 claims to have been condemned by Author2 for wanting to use force to make women carry their fetuses to term" start="4029" end="4144"/>
  </contributor>
 </scu>
 <scu uid="172" label="It is an advantage to let the woman choose">
  <contributor label="and it being an advantage of letting the woman choose">
   <part label="and it being an advantage of letting the woman choose" start="230" end="283"/>
  </contributor>
  <contributor label="and that is good because a woman can then make choices about her body and that is a good thing">
   <part label="and that is good because a woman can then make choices about her body and that is a good thing" start="1415" end="1509"/>
  </contributor>
  <contributor label="and gives the woman the choice">
   <part label="and gives the woman the choice" start="2398" end="2428"/>
  </contributor>
 </scu>
 <scu uid="200" label="The use of force is wrong">
  <contributor label="Author1 said saying Author1 is wrong to force someone then is wrong...that he meant it is wrong to force someone">
   <part label="that he meant it is wrong to force someone" start="2561" end="2603"/>
   <part label="Author1 said saying Author1 is wrong to force someone then is wrong" start="2126" end="2193"/>
  </contributor>
  <contributor label="after claiming that the use of force was wrong">
   <part label="after claiming that the use of force was wrong" start="522" end="568"/>
  </contributor>
  <contributor label="Author1 argues that, in order to make an argument that using force is wrong">
   <part label="Author1 argues that, in order to make an argument that using force is wrong" start="1031" end="1106"/>
  </contributor>
 </scu>
 <scu uid="170" label="They both use force">
  <contributor label="This means they both use force">
   <part label="This means they both use force" start="90" end="120"/>
  </contributor>
  <contributor label="Author1 starts out by pointing out how Author2 uses force in their argument and then poses the question of why did Author2 bother to state how Author1 uses &quot;force&quot; if not to state it was wrong">
   <part label="Author1 starts out by pointing out how Author2 uses force in their argument and then poses the question of why did Author2 bother to state how Author1 uses &quot;force&quot; if not to state it was wrong" start="2988" end="3180"/>
  </contributor>
  <contributor label="where force is acceptable for Author2, but not Author1">
   <part label="where force is acceptable for Author2, but not Author1" start="4257" end="4311"/>
  </contributor>
 </scu>
 <scu uid="177" label="He isn't misrepresenting it">
  <contributor label="Author1 replies he isn't misrepresenting it">
   <part label="Author1 replies he isn't misrepresenting it" start="445" end="488"/>
  </contributor>
  <contributor label="Author1 does not agree that he misinterpreted anything">
   <part label="Author1 does not agree that he misinterpreted anything" start="4314" end="4368"/>
  </contributor>
 </scu>
 <scu uid="176" label="His view is being misrepresented">
  <contributor label="and his view is being misrepresented">
   <part label="and his view is being misrepresented" start="407" end="443"/>
  </contributor>
  <contributor label="Author2 says that his views are being misrepresented">
   <part label="Author2 says that his views are being misrepresented" start="4584" end="4636"/>
  </contributor>
 </scu>
 <scu uid="184" label="It is best to drop the issue and move on">
  <contributor label="so he thinks it is best to drop it and move on to other issues that aren't inane">
   <part label="so he thinks it is best to drop it and move on to other issues that aren't inane" start="875" end="955"/>
  </contributor>
  <contributor label="It is at this point where Author2 starts to reject continuing the debate">
   <part label="It is at this point where Author2 starts to reject continuing the debate" start="3508" end="3580"/>
  </contributor>
 </scu>
 <scu uid="186" label="Double standards should not be allowed in a debate">
  <contributor label="Double standards are bad and should not be allowed in a debate">
   <part label="Double standards are bad and should not be allowed in a debate" start="1197" end="1259"/>
  </contributor>
 </scu>
 <scu uid="197" label="Early pregnancies would not justify force">
  <contributor label="which he believes would not apply to early pregnancies">
   <part label="which he believes would not apply to early pregnancies" start="4786" end="4840"/>
  </contributor>
 </scu>
 <scu uid="187" label="Force cannot be good for one person's argument and bad for another persons argument">
  <contributor label="Force cannot be good for one person's argument and bad for another persons argument">
   <part label="Force cannot be good for one person's argument and bad for another persons argument" start="1261" end="1344"/>
  </contributor>
 </scu>
 <scu uid="201" label="Force has to be justified through law">
  <contributor label="He feels force has to be justified through law">
   <part label="He feels force has to be justified through law" start="2605" end="2651"/>
  </contributor>
 </scu>
 <scu uid="199" label="Force is not the main difference">
  <contributor label="and not on the issue of force being the main difference">
   <part label="and not on the issue of force being the main difference" start="4948" end="5003"/>
  </contributor>
 </scu>
 <scu uid="182" label="He also didn't claim it was alright when he did it but not when others did">
  <contributor label="He also didn't claim it was alright when he did it, but not when others did">
   <part label="He also didn't claim it was alright when he did it, but not when others did" start="750" end="825"/>
  </contributor>
 </scu>
 <scu uid="192" label="He defends his view of the pace of the debate">
  <contributor label="Author1 continues to defend their view of the pace of the debate up to this point">
   <part label="Author1 continues to defend their view of the pace of the debate up to this point" start="3797" end="3878"/>
  </contributor>
 </scu>
 <scu uid="195" label="He did not jump to conclusions">
  <contributor label="or jumped to any conclusions">
   <part label="or jumped to any conclusions" start="4370" end="4398"/>
  </contributor>
 </scu>
 <scu uid="183" label="He doesn't see any hope of resolving the issue">
  <contributor label="He doesn't see any hope of resolving the issue">
   <part label="He doesn't see any hope of resolving the issue" start="827" end="873"/>
  </contributor>
 </scu>
 <scu uid="175" label="He gave an acceptable compromise">
  <contributor label="Author2 thinks he gave an acceptable compromise">
   <part label="Author2 thinks he gave an acceptable compromise" start="359" end="406"/>
  </contributor>
 </scu>
 <scu uid="203" label="He has no justification for use of force">
  <contributor label="Author1 only has his views, not justification, for use of force">
   <part label="Author1 only has his views, not justification, for use of force" start="2834" end="2897"/>
  </contributor>
 </scu>
 <scu uid="190" label="He wants his argument to be properly understood before ending the discussion">
  <contributor label="Author2 continues to reply to these jabs claiming that they want their argument to be properly understood before ending the discussion">
   <part label="Author2 continues to reply to these jabs claiming that they want their argument to be properly understood before ending the discussion" start="3661" end="3795"/>
  </contributor>
 </scu>
 <scu uid="185" label="He was not wrong in his argument">
  <contributor label="Author1 asks how he was wrong in his argument">
   <part label="Author1 asks how he was wrong in his argument" start="957" end="1002"/>
  </contributor>
 </scu>
 <scu uid="202" label="He would change his view if he could show justification for force in early trimesters">
  <contributor label="If Author1 could show justification for force in early trimesters, he would be willing to change his view">
   <part label="If Author1 could show justification for force in early trimesters, he would be willing to change his view" start="2727" end="2832"/>
  </contributor>
 </scu>
 <scu uid="207" label="He would use force">
  <contributor label="Author2 said he would use force">
   <part label="Author2 said he would use force" start="490" end="521"/>
  </contributor>
 </scu>
 <scu uid="188" label="The government should not be able to stop a woman from choosing what to do with her body">
  <contributor label="The use of force by the government, in order to stop a woman from being able to make choices about her body, especially concerning abortions. The government should not be able to stop a woman from choosing what to do with her body">
   <part label="The use of force by the government, in order to stop a woman from being able to make choices about her body, especially concerning abortions. The government should not be able to stop a woman from choosing what to do with her body" start="1511" end="1741"/>
  </contributor>
 </scu>
 <scu uid="180" label="The use of force is wrong early on">
  <contributor label="Author2 assumed that just because he said force was wrong early on">
   <part label="Author2 assumed that just because he said force was wrong early on" start="2493" end="2559"/>
  </contributor>
 </scu>
 <scu uid="193" label="Their debate has degnerated into semantics">
  <contributor label="and Author2 concludes that their debate has degenerated into a bias argument of semantics">
   <part label="and Author2 concludes that their debate has degenerated into a bias argument of semantics" start="3879" end="3968"/>
  </contributor>
 </scu>
 <scu uid="198" label="They should ask what justifications there might be to force a woman to complete her pregnancy">
  <contributor label="He suggests they focus on what justifications there might be to force a woman to complete her pregnancy">
   <part label="He suggests they focus on what justifications there might be to force a woman to complete her pregnancy" start="4843" end="4946"/>
  </contributor>
 </scu>
 <scu uid="194" label="They will agree to disagree">
  <contributor label="and they will agree to disagree">
   <part label="and they will agree to disagree" start="3969" end="4000"/>
  </contributor>
 </scu>
</pyramid>
