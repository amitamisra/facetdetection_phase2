
----------
D0
----------
Author1 believes either the universe always existed or was created, but the bang theory is evidence it didn't. Creationism covers all scenarios, but it can't be debated against science. He thinks Author2 believes in evolution because he is mistaken into thinking a hypothesis is evidence. What we know about things is inconsistent with a random beginning. He believes something always existed and we aren't aware of it because it's not part of our universe.


Author2 said emergent properties such as intelligence do occur, and according to theories there wasn't nuclear forces. This part of the universe did not exist until after the big bang theory, although quantum loop gravity suggests things happened elsewhere in the universe. He said science provides explanations based on demonstrable evidence, not supernatural forces. He discusses whether the term incontrovertible is appropriate, how the bang theory was a quantum event. and general relativity breaks down at a point. Because quantum physics can be contradictory, he would avoid conclusions, but quantum mechanics is the most accurate theory of physics.


----------
D1
----------
Author1 says the big bang is the sole piece of evidence there is something besides a random universe. Either the universe always existed or something created it. The big bang is direct evidence that it did not always exist and is not infinite. Creationism accounts for all possible scenarios, that is why it is foolish to attempt disprove it or pit it against science. Author2 thinks intelligence is an attribute of the current universe, but humans are currently the only known instance of intelligence. Prior to humans intelligence was possibly not an attribute of the universe. The big bang is evidence that our local region of the universe did not always exist. A creator can account for anything but explanations nothing. Author1 believes if the universe existed for an infinite amount of time then the property of intelligence has emerged an infinite number of times. Author2 thinks the big bang was most likely a quantum event and doesn't agree with using the word incontrovertible. Author1 thinks it's incontrovertible. Author1 explains the universe using quantum mechanics.


----------
D2
----------
Author1 asserts that the Big Bang theory provides incontrovertible proof that Creationism is correct. Because the universe did not always exist and is not infinite, some force that exists outside the universe (God) must have sparked its creation. Furthermore, because intelligence is an attribute of the universe, this force must also have created intelligent life.

Author2 points out that while the Big Bang provides evidence that our local region of the universe did not always exist, there are several cosmological hypotheses that strongly suggest events prior to the Big Bang. He also objects to Author1's statement about "incontrovertible proof," as there are many unknowns concerning t=0 (the point at which the Big Bang occurred) and that much of it falls within the contradictory realm of quantum physics. When Author1 challenges the notion of something appearing from nothing, which he believes to be an impossibility, Author2 cautions him against drawing philosophical conclusions based on macro-world experiences.


----------
D3
----------
Author1 believes that since we are intelligent, and we are contained within the universe, that intelligence must be a property of the universe. They also believe that many people believe the world was created some thousands of years ago and any evidence to the contrary was placed there by God to test our faith. Author2 believes that intelligence developed within the universe, and that science seeks to provide a naturalistic explanation for phenomena. An unconstrained supernatural force can provide an answer for anything, but an explanation for nothing. Author1 doesn't believe that anything in existence hasn't existed before, and their belief's come from non contradictory observations on reality. Author2 admits that hypothesis do change in science and that quantum physics models often appear to contradict each other. Since the big bang was a quantum even, one should not attempt to observe it from our macro-world experiences. Author1 doesn't believe something can come from nothing, and Author2 believes that the quantum physics models are correct. Author2 goes on to list some entry-level quantum physics resources.


----------
D4
----------
Author1 believes that the Big Bang is evidence that the universe is not random. He claims that consciousness is an intrinsic part of the universe, and the Big Bang shows that the universe was created, since it is not infinite in time or space. He contends that the universe springing from nothingness is in itself sufficient evidence that it was created by something outside of and apart from the universe, saying that all the evidence our perception gives us access to point to that obvious conclusion.
Author2 contends that before humans became self-aware, consciousness may not have been an attribute of the universe. He believes that the Big Bang is only evidence that our particular part of the universe has not existed forever, but beyond that the origins of the universe are unknowable. Quantum physics has shown that the laws of nature are chaotic and not easily understood by human minds, and just because our perception doesn't give us access to a particular piece of information, that isn't sufficient evidence that said event did not happen.

