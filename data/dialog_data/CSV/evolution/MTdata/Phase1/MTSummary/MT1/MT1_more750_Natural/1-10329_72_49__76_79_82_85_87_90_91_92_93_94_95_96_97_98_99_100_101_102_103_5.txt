
----------
D0
----------
Author1 clarified that he isn't insinuating Buddhism could be involved in intelligent design, but that intelligent design could fit in with Buddhist karma and samsara, but it still wouldn't be creationism. Author2 presumes ID is creationist, but that isn't true. Buddhism is a religion, has theological teachings, doesn't recognize a creator, and ID fits into it's teachings. Clearly ID wouldn't belong to just creationism. He accuses Author2 of constantly changing opinions. He asked when he defended ID. He claims Author2 hasn't shown evidence of that. His other posts did not contain ID defense. 


Author2 questions if what he has also supported, that ID is a theological assumption, how would it be assumed to be a part of a non-theological religion. He rejects the notion that ID isn't creationism. He claims Author1 has posted in other posts defending ID. He claims Author1 keeps posting conflicting beliefs by saying he believes in something and then turning around and saying he doesn't believe in it. Author2 posted some of those quotes in another post.


----------
D1
----------
Author2 argues that Intelligent Design is an explicitly theological understanding of the creation of life. In Author2Os view, Intelligent Design is functionally identical to creationism, with creationism understood specifically as the creation of life by the God of the Bible. Intelligent Design is therefore incompatible with secular worldviews or religious worldviews that lack a godhead, such as Buddhism. Author2 also claims that Author1 is a supporter of the Intelligent Design position.  Author1 believes that Intelligent Design can be an acceptable framework for the creation of life outside of a specifically creationist, i.e. Judeo-Christian, worldview. Specifically, Author1 asserts that Intelligent Design would be compatible with the Buddhist faith through the concepts of karma and samsara. Furthermore, Author1 contests the idea that Buddhism is a non-theological religion. Regardless of this, however, Author1 believes that Intelligent Design is compatible with any worldview, philosophy or faith, as Author1 does not consider Intelligent Design to be synonymous with creationism. However, Author1 also takes issue with the claim that Author1 is a defender of Intelligent Design.


----------
D2
----------
Author1 and Author2 are discussing creationism versus intelligent design.  Author1 feels intelligent design could be accommodated within Buddhism.  Author1 does not feel creationism is related to Buddhism.  He advises although Buddhism does not recognize a creator God, its teachings do support ID.  He does not feel ID is a creationist theory either.  Author1 challenges Author2 to provide proof of where he defended ID's being creationism.  Author1 does not actually state which theory is in complete support of.

Author2 questions Author1's assertion questioning how ID could be involved in a non-theological religion if it's a theological concept.   Author2 feels ID is creationism.  Author2 feels Author1 is changing his position in the argument back and forth because he doesn't really understand the concept of ID.  Author2 advises he did post quotes in another thread which showed Author1's support of ID being creationism.  He does not re-quote those posts in this thread.  Author2 does not fully state which theory he is in support of.  No agreement.


----------
D3
----------
Author1 isn't asserting they could be involved with intelligent design, his assertion is that intelligent design could be accommodated within Buddism by karma and samsara. 
Author2 questions if intelligent design is a theological assumption, how could it be involved in a non-theological religion. Author1 replies Buddhism is a religion and can be seen as theological. Buddhism does not recognize a creator god but its teachings would accommodate intelligent design. Author1 says intelligent design is creationism and Author2 hasn't refuted anything. Author2 thinks Author1 doesn't understand the point. Each thinks the other is foolish and Author1 asks for Author2 to prove his point. Author2 thinks that Author1 is confused and keeps changing his mind about what he thinks and contradicting himself. Author1 asks for the evidence that he defended intelligent design and says there is no indecision there. Author2 replies that he posted a few quotes of it in another thread and asks if Author1 can recognizes his own ignorance. Author1 says that none of the posts showed defense of intelligent design just his confusion.


----------
D4
----------
Author1 believes that intelligent design could exist within Buddhism. Author2 holds to that intelligent design is a theological belief and Buddhism is not a theological belief. Author2 doesn't believe that Buddhism's beliefs are not based outside of what could be considered theological. The over-reaching point is that intelligent design doesn't belong solely to creationism. Author2 states plainly that Creationism and Intelligent Design are one in the same. At this point the debate degrades into name calling and insulting. There are references made to another thread where the possibility of Author1 defending intelligent design, which he denies. Author2 finds that to be a contradictory stance as to what is stated here. The adversarial name calling continues as Author1 suggest Author2 may have been thrown out of the military for being homosexual. A point which Author2 denies completely. Author1 asks for proof in reference that they defended intelligent design in this other discussion, Author2 posted quotes to the thread in response. Finally Author1 doesn't feel that Author2 understood the meaning in those quotes.

