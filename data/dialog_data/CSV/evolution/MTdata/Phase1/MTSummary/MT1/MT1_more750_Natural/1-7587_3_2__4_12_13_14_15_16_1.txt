
----------
D0
----------
Author1, responding to a prediction that a certain religious website would soon go belly up, says that the same thing was predicted about www.answersingenesis.org, which is still going strong.  When questioned, he admits that he's making an assumption, and doesn't know for certain that such predictions were made.  He points out that the matter is irrelevant to the debate.  Author2 is a hypocrite for demanding links in support of Author1's statements, but not asking the other people involved for links in support of their statements.

Author2 asks for evidence that supports the claim Author1 has made, that people were predicting that www.answersingenesis.org would fail when it first came out.  He says that Author1 has presented imaginary to refute someone else's claim, making the issue a legitimate point of debate.  If Author1 has integrity, he should bow out of the debate gracefully over this issue.  Author1 is using the word "hypocrite" incorrectly, as Author2 is not himself doing the thing that he's condemning Author1 for doing.


----------
D1
----------
Another forum member (Ripskar) had previously made a comment about the high probability of a Christian apologetics website failing in the near future. Author1 contends that this same attitude was directed toward Answers In Genesis, a similarly themed website, and that the site is still active. Author2 demands that Author1 provide evidence that people were indeed betting on the failure of Answers In Genesis. He also comments that websites such as these can be active for a long time if they have sufficient resources and monetary support from their followers, whom he believes are gullible. Author2 responds that he does not need to provide evidence or links to support his betting claim, as he was simply making a common sense deduction, and also criticizes Author2 for not demanding evidence from other forum members who made similar comments about betting. Author2 again points out that Author1 has not provided concrete evidence and accuses Author1 of making up the unfavorable predictions about Answers in Genesis to refute forum member Ripskar's original comment.


----------
D2
----------
Author1 claimed bets were made about how long a website would last years ago. He claims to have just responded to another commenter about the very same bet. He said he was sure when the site came online that others on the internet made bets about it too. He claims Author2 is unfairly attacking him on his comment when two other commenters on the site made the same comment and he didn't question their evidence. He accuses him of being a hypocrite. 


Author2 questions if it's true bets were made about the website. He asked for evidence. He thinks they have enough money from supporters to last awhile. He mocks Author1 as having just made something up about people betting. He said it is different because the other commenter claimed the website would fail. That was just his prediction. But Author1 claimed that others had previously made bets, and had no evidence of it, and then admitted he assumed it. He defines the word hypocrite, and shows Author1 used the word incorrectly.


----------
D3
----------
Author1 suggest the website 'answers in genesis' wont last. Author2 adds that it depends on how well they spend their cash resources, suggesting that they probably have a large some from the people they have tricked up to this point. Author1 explains that he was just adding on to a previous post that their new website would not last. Author2 was under the impression there was a physical bet in place on whether they would fail or not, and is now reassured that it was not a real bet. Author1 questions the importance of arguing over the issue of making statements about betting on the websites failure. Author2 reiterates their point by saying that Author1 made the assumption that people bet that the old website would fail, whereas other posters were only making that statement in respect to the new website. Author1 doesn't feel that they've been fairly judged when other posters have been guilty of the same offence yet not called out on it at all. Author2 places the blame squarely with Author1.


----------
D4
----------
Author1 gives a link about answers in Genesis. Author2 asks for a useful link As for how long this will last, he thinks it depends on how they decide to allocate their resources. Author1 says he was responding to a question asking how long this insanity will take to go belly up. Author2 says he knows what Author1 was saying and he was right that Author1 has nothing to back up his claim that people were betting it would fail. Author1 replies that giving an opinion isn't the same as making something up. He questions why he's being attacked when others said the same thing. Author2 says Author1 presented it as fact and presented data. He made up something in a debate and got caught. Author1 thinks he is being one sided and a hypocrite. Author2 thinks Author1 doesn't have the integrity to admit that he is wrong and is ignorant of the word hypocrite. He isn't required to comment on every person that doesn't provide links. Common sense dictates efforts made to worst offenders.

