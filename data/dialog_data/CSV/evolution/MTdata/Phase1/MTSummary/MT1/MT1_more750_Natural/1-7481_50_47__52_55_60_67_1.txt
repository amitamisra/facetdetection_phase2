
----------
D0
----------
Author1 states we have done more than rely on telescopes in our search for life. He offers the example of our use of the Voyager. Scientists have used it to search our solar system and beyond. Scientists have used the speed of light to date the universe. They have calculated it to be 14 billion years old.  They shouldn't, however, state that age with confidence. Effects on the speed of light such as the different densities of space can not be compiled statistically.

Author2 argues there are some factors that we can't pin down exactly so scientists have added a small degree of uncertainty . They have taken factors such as the different densities of space into consideration when calculating the age of the universe.  The age of 14 billion years is an estimate. He explains that the age of the universe is not measured by the speed of life. The measurements are based on the red shift of light


----------
D1
----------
Author1 says scientists have dated the universe by looking with telescopes and extrapolating its age based on the speed that light travels. He asks if they accounted for the effect of solar winds or the varrying densities of the space. They didn't as that information is impossible to compile because it's unknown, so they can't confidently state the universe is 14 billion years old. Author2 says they can and do take those things into account. Author1 replies his example is close but wrong and asks if it's wrong why doesn't Author2 correct it. Author2 replies measurments are based on the red shift of light, not on the speed of light. He thinks Author1 simply doesn't want to understand and is making false claims. He thinks if Author1 cared about learning he would ask questions. Author1 says he is adding nothing of substance and is just being argumentative and unresponsive. He asks for a link on the red light shift and what false claims he made. Author2 replies says he is attacking behavior which describes himself.


----------
D2
----------
Author1 points out that in our search for life we observe information from a great distance. They personally believe that distance can cause flawed information to be received which invalidates scientific conclusions based on that information. Author2 agrees there is a slight degree of uncertainty, but that uncertainty does not effect the amount of change needed to justify the belief that the universe is only six thousand years old. Author1 doesn't feel that Author2 has provided ample evidence to disprove his original point and requests more proof. Author2 assumed Author1 had an elementary knowledge of the sciences he was attempting to disprove, which Author2 now feels is not the case. Author2 does however offer a term to degree the perceived rate of change in distance, the red shift of light. Author1 feels the skirting of his question to be a lack in strength of argument on the case of Author2. Finally in closing Author2 turns his response to the crowd of this debate, inquiring if they can see Author1 for the things that Author2 sees.


----------
D3
----------
Author1 argues that the age of the universe cannot be determined with any confidence.  There are many unmeasurable or unknowable factors that could impact the measurements that scientists have taken for this purpose.  No conclusions can be drawn with confidence about the universe's age, and yet scientists do it anyways.  Many assumptions must be made and held onto in order to accept what the scientists assert, and these assumptions can not be proven "beyond a reasonable doubt."  To believe the age estimate of 14 billion years takes "a tremendous amount of faith."

Author2 says the factors Author1 mentions have been taken into account, and a degree of uncertainty is incorporated into the estimate of the age of the universe.  This is the reason the number is given as "14 billion years" and not a more precise figure.  Author1 doesn't understand enough about the science involved to evaluate the assumptions being made or to assess how they're taken into consideration, and Author2 thinks Author1 doesn't want to understand the science.


----------
D4
----------
Author1 points out scientists rely on more modern equipment than just telescopes now. He questions how scientists can accurately estimate Earth's age based on telescopes, when solar winds could affect speed, and the effects of denseness in space could affect speed. Author1 argues that Author2 makes assertions he can't prove because he makes such big assumptions. He asked for a link on the red shift of light instead of just accepting claims. He said he has not made any false claims, he only asked questions. 


Author2 said they do consider those things, which is why they say a whole number instead of decimals. He admits there is uncertainty on some factors, but that the amount would not be a huge sum that would make it significant, and certainly wouldn't reduce the number given now. Author2 states age isn't based on speed of light, but red shift of light. This shows Author1 doesn't know about space. He said Author1 attacks behavior, but then imitates the behavior.

