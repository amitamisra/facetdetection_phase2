
----------
D0
----------
Author1 believes, depending on your level of faith, that god will punish you for not believing in him, because he loves you. Author1 feels the information is so obvious and doesn't understand how Author2 doesn't get it. Author2 briefly describes the story of Jesus's life from the theological point of view as it poses more questions than it answers in his mind. Author1 explains that Jesus suffered for us, and that God allowed it to happen shows how much he loves us. Author2 explains that if Jesus was God than he wouldn't have suffered at all, since God has the power to simply take his own pain away. Author1 explains that since Jesus was human, he would have endured the pain just as any normal human would have had to. Author2 goes on to say that if Jesus was human than he wasn't God. Author2 says that the effects of Jesus's sacrifice were purely economical. Also if he was human, than he couldn't have had the powers. If he was human, than hes dead.


----------
D1
----------
Author1 argues that God loves humanity, since he sent his son (Jesus) to suffer and die for mankind's salvation. Author1 explains that, due to the nature of the Trinity, Jesus was both God and human at the same time. Any suffering that he experienced while human was just as painful and horrible as any other human would have experienced.

Author2 takes issues with the divinity of Jesus and with the Christian Trinity. Author2 argues that, if Jesus was God, he didn't really suffer when he was killed. If he didn't suffer, his sacrifice was not significant. Author2 points to the various miracles and supernatural events surrounding Jesus, like walking on water. If he did these things, how could he be a human? But if Jesus wasn't God, he was just a man. Therefore he's dead and his death was of no value. Author2 also finds the notion that God impregnated a virgin to become human and die nonsensical.


----------
D2
----------
Author1 thinks it's our human nature to go against everything God has for us. He believes God sent his only son to die for us. Author2 asks if Jesus is God or God's son. If he is God then God is dead. Author1 replies they, the trinity, are all one, the father, son, and holy spirit, and yet they are separate. He talks about the suffering on the cross. Author2 asks if they are the same then what was the sacrifice because no one really died or suffered. He questions how much love it takes to not really suffer and die. Author1 thinks Author2 is being ignorant. He says that God came in a human form and died. Author2 says if Jesus was a human then he wasn't God. He says his dilemma is God gave himself a choice and if he had decided not to save humans there would have been no purpose. According to religion Jesus wasn't ordinary and any suffering done by God wasn't suffering. If Jesus died then he is dead.


----------
D3
----------
Author1 is a religious believer.  He feels too many people in the world are too full of themselves to recognize religion and God.  He feels God loves all and sent his son, which was himself in human form, to die for everyone's sins.  He feels anyone who does not believe in God and his sending his son to earth is stupid.

Author2 is not a religious believer.  He questions what kind of God would impregnate a virgin and then send his son to die.  He questions how God really made a sacrifice if Jesus is actually him in human form.  He feels with that type of infinite power, there was no sacrifice made.  He feels the statement of Jesus' being God's human form is incorrect as if he were a typical human, he wouldn't have been able to heal the sick or turn water into wine.  He feels if Jesus actually was sent to die, then he is dead but according to most religions he is still alive.


----------
D4
----------
Author1 thinks people's egos cause them to ignore God until something bad happens and they realize they need him. God loves people so much he sent his son to die for them and show them they need God. He said clearly Author2 doesn't understand the trinity and how the three are separate yet one. He describes the torture and pain Jesus went through to show his love. He came in human form and suffered pain. 

Author2 asked if Jesus was a God or God's son. If he was a son then Author1 just said he died, so he was dead. He mocks what the story of God and the virgin birth of a god who was then killed and went back to Heaven had to do with human sin or loving people. if they were the same, then no one really suffered or died. But if he came as human, then he wasn't a God at that time, yet still was performing god-like miracles. This means he was a God and didn't suffer.

