
----------
D0
----------
The debate revolves around the incident of the coelacanth, which had previously been believed to be extinct for millions of years, being discovered to be alive in the present day. Author1 is positing that assumptions that the coelacanth had gone extinct were made based on a school of thought rooted in the belief in evolution. Author1 continues by stating that since scientists were drastically wrong about the fish being extinct, that this reflected on their methodology (rooted in evolutionary science), and that this further reflected possible flaws with the theory of evolution itself.

Author2 argues that this incident is completely disconnected from the theory of evolution, and that they theory is irrelevant to the discussion. Author2 argues that the assumption made my scientists were perfectly reasonable, saying that the conclusion of extinction was reached upon observing that fossils had stopped appearing and that there were no observed living specimens. Author2 argues that the conclusion did not factor evolution into it at all, and new evidence debunking the old prediction does not reflect poorly on evolution itself.


----------
D1
----------
Author1 mentions a fish that was found that evolutionists mistakenly believed had been extinct. He disagrees the fish is different from fossils. He said the common ancestor theory and fossil records were used to decide it's status. He claims the gap in the fossil record led scientists to believe in extinction. He emphasizes this was based on the theory of evolution and it was wrong. 

Author2 claims the fish found was different from the fossils. They were declared extinct because of evidence suggesting they were no longer around, but that was changed with new evidence of a captured fish. This is not related to the theory of evolution, which just explains how allele frequencies change over time in populations. He argued extinction was determined because there had been no sightings. He asserts science is always changing with new or better data. He says a decision was made on data, not on the theory of evolution. He clarifies he was speaking of the fish when he said it evolved instead of the fossil.


----------
D2
----------
Author1 says the coelacanth, according to fossil records, and according to evolutionists, allegedly went extinct millions and millions of years ago. Until they were found alive. He thinks this is evidence the evolution mindset contradicts observable science. Author2 says the coelacanths found are different from those in the fossil record. All evidence pointed to them being extinct, until we found other evidence, and we changed it. This has absolutely nothing to do with the theory of evolution, which explains the mechanisms responsible for the change of allele frequencies in populations over time. Author1 replies they don't differ in a significant way. The prediction was made based on the mindset that everything evolved from a common ancestor over billions of years. Furthermore, it was based upon the fossil record, which is supposedly shows how everything evolved. Author2 says it was based on not seeing them but seeing fossils. Author1 doesn't agree. Author2 questions if Author1 thinks dinosaurs are still alive. Author1 questions if Author2 believes fossils evolve into models. Author2 thinks that animals evolve, not fossils.


----------
D3
----------
Author1 argues that the unexpected existence of the coelacanth casts doubt on the correctness of the theory of evolution. Evolutionary scientists made claims regarding the fossil record on the basis of an extinct coelacanth. Since the coelacanth was found not to be extinct, Author1 claims that the evolutionary position has been demonstrated to be flawed. In Author1's view, this shows how evolution is rooted in unscientific abstraction rather than empirical data. Author1 also claims that the fossil coelacanth and modern coelacanth are identical, which is another blow to evolution.

Author2 contends that evolutionary scientists correctly followed the scientific method. When the coelacanth was perceived to be extinct, conclusions were drawn on the basis of that data. When that data was demonstrated to be flawed, and the coelacanth was proven to be extant, new hypotheses were advanced on the basis of the new data. Author2 also states that the modern coelacanth, compared to the fossil coelacanth, shows evolution over time.


----------
D4
----------
Author1 states that the coelacanth was thought to have went extinct and yet they were found alive today, which contradicts evolution theory. Author2 doesn't think it disagrees with evolution theory, as evolution deals with the change in allele frequency over time. Author2 also points out that the living coelacanths are fundamentally different from the ones in the fossil record. Author1 doesn't believe the coelacanth differ in any significant way from their ancestors, the prediction that they were extinct was a failing on evolution's part. Author2 states that since they are different, and they are descendants of the originals they still fit nicely into evolution theory. Author1 feels that since the prediction that they were extinct was based on evolution, and they are found to exist today, that evolution is wrong. Author2 elaborates on how evolution was not factored into the equation of determining whether they were extinct or not, there were fossils up to a certain point, and none after that. Author1 uses Author2's idea of evolution to question why the coelacanth didn't change much.

