
----------
D0
----------
Author1 thinks if future decisions can be foreknown, then free will cannot exist. Free will means that at the instant of the decision, any of all options can be taken. It doesn't matter if god is directing, if he knows what's chosen before it is chosen, then there is no choice, and no free will. Author2 disagrees. His example was supposed to emphasize an observer can know the end while watching the middle but have no control over what's going on. Author1 says nobody is saying god is the director, we are all agreeing for the sake of discussion he's the observer. Even in this case free will is taken away from the ones being observed. Author2 doesn't agree. Just because someone knows his actions doesn't mean that he doesn't have free will. Author1 says it means his choices were predetermined and there was no choice to begin with. Author2 understands but doesn't agree. He speaks of omniscience. Author1 defines omniscience differently. Knowledge of the future is not a requirement for omniscience in his opinion.


----------
D1
----------
Author1 believes that free will is incompatible with the existence of an omniscient God. In Author1's view, if God knows that certain choices will be made, or certain events take place, then actors are bound to make those choices or engage in those events. All other options are illusory. Their free will is constrained by being bound to choose the path that God has observed taking place. They cannot alter what God has forseen.

In Author2's view, God's omniscience is not a constraint on an individual's free will. As Author2 sees it, provided God merely observes an individual rather than directing their actions, that individual is free to choose whatever action they wish. Author2 believes that Author1 is too preoccupied with the concept of time, and the idea that foreknowledge somehow equals control. God, as a being that is omniscient and outside of time, has no 'foreknowledge,' merely knowledge of events unconstrained by time. And both parties agree that God does not directly control actions.


----------
D2
----------
Author1 believes that if god truly knows what you're going to do before you do it, than you did not make a choice. Author2 uses the example of a person watching a movie, having knowledge of the end, but not of the process to get there. Author2 also points out that stepping into the movie would change the outcome, thus the observer is not in control. Author1 believes that even in that case free will does not exist, if one were to fast-forward or rewind the choices being made would still lead to the same ending.  Author2 simplifies the idea saying that you made the choices, the observer only sees the consequences. Author1 still remains on their line of thinking that if there are three choices, and someone knows 100% which one you will pick, than you weren't in control of that choice, something else was. Author2 feels that they have adequately outlined a situation where an omniscient being and free will exist together. Author1 defines omniscience in the present, not the future.


----------
D3
----------
Author1 claims that fee will is nonexistent if future decisions are known; you have to be able to choose. If the choice is known, then  there isn't a choice when making one, because  the choice that's already known has to be chosen. Whether God influences it or not, if he knows what your choice is, there isn't free will because you can't pick the others. He is only proposing God as an observer. Even in a movie the actor's decisions are already decided so he doesn't have free will while doing them. 

Author2 disagrees with Author1's claims, and said the movie example was supposed to show how someone watching it can know the ending but not influence it because it was created in a different time.  He describes how if he is making choices, even if someone observes and knows them, that he is still making the choices so he has free will at that moment. He proposes the timeline is different for both the person and the observer.


----------
D4
----------
Author1 is arguing that if an observer may know an observed person's decisions before they are made, then the observed never truly had a choice to begin with, this presents the illusion of free will in the presence of an omniscient being. Author1 argues that if an observed person is given a set of choices, and an observer already knows the outcome of this decision, then it would be the same as if the observed only had the one choice, hence a lack of true free will.

Author2 argues that the above stated idea stands on poor logic, and that free will and an omniscient observer may coexist. Author2 argues that the fact that an atemporal observer knowing the outcome of a decision does not rob an observed person of free will. Just because which option an observed person may choose is known does not mean that the other options weren't still possible to take. The act of choosing a different option would simply lead to a new timeline, in which the new choice is known.

