
----------
D0
----------
Author1 suggests that science, even though it can be bias at times, requires you to remain objective by recognizing that bias. Author2 suggests that there are different practices of science depending on where in the world you learned them, illustrating the difference between being educated in the UK vs. the USA. Author1 disagrees and instead makes the point that all those different kinds of science are really just different ways of using the same thing, which is science. Author2 concedes that point yet suggests that anything Author1 believes to be false will be considered non-objective science. Author1 resents the presupposition but concedes that he believes evolution is true science. Author2 points out that Evolution is dominated by the masculine view, when taken from the feminist view the points are no longer objective. Author1 counters that by saying that Author2 is searching for a divide in science that doesn't actually exist. Author1 is also asserting that this line of thinking is out of a convenience to prove Author2's point, not based in reality.


----------
D1
----------
Author1 believes that science, by definition, must be objective and based on mathematical and empirical. This is distinct from what he refers to as philosophies or interpretations of science. Although some people or groups may twist and distort scientific fact to suit their own opinions and beliefs, this does not mean that the underlying saying is false or debated. He emphasizes that despite the fact that people may apply their own subjective viewpoints to science, there is only one core scientific truth. A feminist, Marxist, or capitalist interpretation of science is not science in and of itself.

Author2 argues that Author1 views the scientific interpretations of groups like the Nazis as subjective and unscientific simply because he does not agree with them. He argues that because different groups interpret science differently, there may not actually be any core objective scientific "truths". Instead, belief in concepts like evolution may simply be a result of a male-dominated Western or capitalist interpretation of science. Different groups, such as feminists, may actually disprove these beliefs.


----------
D2
----------
Author2's implied argument is a rebuttal to the notion that so-called 'creation science' isn't science. Author2 argues that there is no one monolithic science. Author2 points out that the Nazis had a complex system of science that they studied seriously, but their work is ignored by mainstream Western science. Author2 also uses the example of Darwin supposedly arguing for female physical weakness, and that being a tenet of Darwinian evolutionary science; modern feminist science would take issue with that claim. The fundamental argument is that creation science can't be dismissed as 'not science' because there is no one conception of science.



Author1 argues that Author2 isn't distinguishing types of science, but philosophies of science. Science is using the scientific method to make falsifiable claims about material reality. To the extent that Nazis, evolutionists, feminists and creation scientists operate in that manner, they are doing science. When they operate outside that framework, when they allow non-scientific philosophy to taint their work, they are not doing science. And creation science, Author1 contends, always operates outside that framework.


----------
D3
----------
Author1 thinks people are subjective, even scientists. The latter use a tool of objectivity to minimise that subjectivity, but remain subjective. Science can be debased by bad scientists. Author2 thinks he says they aren't science because he knows Nazi science to be false. Darwin believed women were meek, in line with the beliefs of his century. Male dominated science described the characteristics of women, in evolutionary terms, but through the male perspective. Now feminist scientists discovered that the ovum actually snare a sperm, so it's not passive. Author1 states if he says they are philosophies of science and not different types, it's because that's what he means. Author2 can repeat the idea that there are multiple types of science, but is wrong. Author2 thinks he is missing the point and calling Nazi science non-objective because he thinks it's false. Author1 thinks evolution is science and asks how he knows what he thinks of Nazi science. Author2 thinks materialist science has contradictions and what Author1 holds as true, isn't. Author1 says science is evidence not opinion.


----------
D4
----------
Author1 clarifies this isn't science itself, but a philosophy of science. Science is objective in order to avoid being subjective, which would be bad scientific procedures. He reiterates that subject science is a philosophy of, not a type of science. He explains they are "different ways of applying subjectivity to the objectivity of science." He classifies the theory of evolution as science. He says what makes something science is it's basis on math and evidence and not on feelings or opinions. 

Author2 cites Darwin's belief that women were meek. This stereotype was continued throughout history, and later dis-proven with the work of feminist scientists. He questions if Author1 still believes that they are philosophies if the subject is evolution. He maintains that it is a philosophy which has contradictions and benefits from its evidence. He has proven it's flawed science: evolutionists believed women were weaker than men, evolutionary science showed it years later, and then feminist scientists proved it false. The one philosophy Author1 says IS science is actually a flawed philosophy.

