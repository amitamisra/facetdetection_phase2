
----------
D0
----------
Author1 takes the Creationist view, and believes the formation of things (i.e. calcium carbonate deposits on cliffs) is hard to explain in a non-Creationist theory.  He/She questions how sea creatures collected there in a way that filtered out contaminates, to form these pure calcium carbonate deposits?  Author1 states that Science is not an exact science, and that facts change as new information is known.  He/She cites the Great Barrier Reef as an example; regarding how long it took for the coral to form, and how the opinion has changed from millions of years to 3800).

Author2 argues that other things (such as shales and other clastic/carbonate rocks) form in the same way as the calcium carbonate deposits on the cliffs, through a process called "deposition".  He/She gives a website ((http://www.geologyshop.co.uk/chalk.htm)as an example of how this happens.He/She believes that it did not require millions of years to form the deposits.  Author2 asks for a purely scientific explanation of Author1's theories, or at least some evidence supporting their stance.


----------
D1
----------
Author1 thinks Author2 ignored the part that seems to conflict with his theory. He thinks Author2 is missing the point. Interpretation not scientific fact. Author1 has given evidence that supports creation. Author2 says white sections of the cliffs are composed of almost pure calcium carbonate. Author1 questions why they are so pure that they are not discolored by contaminates. Author2 references a website explaining it. Author1 is aware of the information but disagrees that these amazingly clear seas could have remained unchanged for millions of years. He thinks it's impossible for to some people see that their is more than one way to look at evidence. Interpretation is an educated guess not fact. No one was there to watch it form. Just because he disagrees does not mean he am right. It means they disagree. Author2 says it requires millions of years to produce chalk. He asks for Author1's reasoning why his explanation is more valid or as valid. Author1 says there's no way to know, but people have different ideas and that is okay.


----------
D2
----------
Author1 is a believer of creationism.  He feels Author2 is asking for irrefutable proof of the theory but does not feel that exists for either creationism or evolution.  Author1 wants to know why the cliffs are white and have never been contaminated.  Author1 conquers it was a much more tropical climate when the chalks were formed; however, he does not agree the seas could have reminded untouched for millions of years.  He feels there is more than one way to view evidence for either theory.  He advises there is not a way for anyone to know how it happened.

Author2 uses examples of cliffs which are white in color and seem to have not been contaminated by other compounds or organisms.  He refers to them as chalk or soapstone.  Author2 provides the link www.geologyshop.co.uk as proof of his theories and asks that Author1 read that information for the answer to his questions.  He does not feel it took millions of years for the organisms to evolve into chalk.


----------
D3
----------
Author1 and Author2 are having a debate over geological formations, and how long it took them to be created. The science on these formations is fairly straight forward and Author1 quickly links to some geological resources. Author2 believes that given recent changes in geological thinking that those straight forward answers may need a more objective look. Author1 would like to have an answer explaining the purity of the stone in those areas, and how the sea life captured in those stones filtered out the impurities. Author2 believes their linked material elaborated well enough to cover those questions in depth. Author1 thinks the science needs more examining, pointing out how only a few years ago the great coral reef was thought to have formed over a very long period of time. Until they recognized similar coral structures developing on new structures built in only  the last hundred years, drastically changing their timeline. Author2 would like the bible not to be brought up, and Author1 thinks that not bringing up the bible would be closed minded.


----------
D4
----------
Author1 questioned why they're white. He knows what they're made of, his point was why aren't  they discolored, and how did they collect in such a way with such a high degree of purity. He's heard the explanations, but still finds it hard to believe seas could remain unchanged for so long. He points out no one was there when it formed; people can only guess how it happened. He cites the example of the Great Barrier Reef, which was believed to have taken millions to form, but has now been calculated to 3,800 years.

Author2 explains they are made of a white stone called chalk or soapstone. He provides a link that details how they were formed, and mentions other rocks that form in a similar manner. How they form depends on speed of movement, erosion rates, and water transport and depth. He thought Author1 understood it didn't take millions of years to make the chalk. He asked Author1 to explain his theory without the Bible and using scientific evidence.

