
----------
D0
----------
Author1 feels that any evidence against the Big Bang theory would also be evidence against evolution.  To believe a naturalistic explanation for the origin of life, one must accept a naturalistic explanation for everything leading up to it, too.  Evolutionary cosmology is a necessary part of the theory of evolution.  If the universe is only a million years old, then biological evolution could not have occurred as described.  Cosmology, geology, and paleontology must agree, or evolution is a dead idea.

Author2 states that evolution has nothing to do with the Big Bang.  Evolution is the change in biological organisms over time.  It is not necessary for the Big Bang to be true for evolution to be true.  Evolutionary theory does not address how the universe first arose, or how the first life form arose.  Evolution itself could be a God-driven process.  The theory of evolution is one of the best understood and most detailed of all sciences, and is at the core of all modern biology.


----------
D1
----------
Author1 believes that naturalism is a worldview, not just one specific subject. The god of time and chance must exist for evolution to be true. He believes that evolutionary cosmology is essential to the evolution theory. Evolutionists define evolution to fit a particular argument at the time. Evolution is based on the god of time and chance. Evolution could be a completely God-driven mechanism. The science of evolution can only view it from a naturalistic perspective.

Author2 stresses the point that others have made. The big bang has nothing to do with evolution. Evolution involves the change in form of organisms. He feels that a God could have created the first life fully formed and the Big Bang. Neither has any affect upon the theory of evolution. Evolution could be a completely God-driven mechanism for all we know. The theory of evolution is one of the best understood and most detailed of all sciences. It is at the core of all modern biology. He talks about all of the peer-reviewed documents that relate to evolution.


----------
D2
----------
Author1 thinks there are lots of miracles in evolution. The big bang, for example.
Author2 replies here is the creationist in its natural habitat, the lie. He says the handlers have pointed out that the big bang has nothing to do with evolution, but creationist's continue to wallow in this lie. Author1 disagrees with the handlers. In order to have a naturalistic explanation for the origin of life he thinks you must have a naturalistic explanation for the origin of the universe. Naturalism is a worldview not one specific subject. God must exist for evolution to be true. Therefore evolutionary cosmology is a necessary part of the evolution theory. Author2 says evolution is concerned with the change in form, not with how the universe came to be. God could have created the first life fully formed, or the big bang. Author1 thinks evolution is based on time and change. Author2 doesn't agree. Evolution could be God driven. Author2 replies evolution is the change in allele frequency over time and at the core of all modern biology.


----------
D3
----------
Author1 has an over-reaching world view that encompasses many different theories, so it is natural for him to assume that his opposition views the world in a similar way. A mistake that is pointed out by Author2, who does not group evolution and the big bang theory into one. Author1 favors the naturalistic explanation for the meaning of life, and the possibility that God is the driving force in evolution, like how God is the driving force in the evolution of the universe. Author2 disagrees with that point stating that evolution is just the change in organisms, by definition. Author1 states that science can only see evolution from a naturalistic point of view, Author2 appears to agree on that point but not on the science of evolution. Author2 further elaborates that the science of evolution is concerned with the change in allele frequency over time. Author2 does concede that there are other uses for evolution in areas other than biological evolution and would like to know if Author1 has a specific form in mind.


----------
D4
----------
Author1 said there are miracles in evolution. He starts to talk about the big bang theory and is interrupted. He disagrees with the denial posts. In order to explain how life began, you have to explain how it's environment was created first. He advocates evolutionary cosmology, and claims naturalism is a worldwide view. He says Author2's description is useless. Evolution based on random choice doesn't explain all the sciences and forms created at the same time. 


Author2 accuses Author1 of lying. He notes previous posts denying the big bang theory was involved in evolution. He clarifies that evolution is only concerned with the change in form of organisms. Questions about universe origin or first life origin don't have anything to do with that, and don't have any impact on the evolution theory. Evolution could even be a tool of God. He defines the theory of evolution as "the change in allele frequency over time." It is a detailed science that is the core of biology and the best understood science.

