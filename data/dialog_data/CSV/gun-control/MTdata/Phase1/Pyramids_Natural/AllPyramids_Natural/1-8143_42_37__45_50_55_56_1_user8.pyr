<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>S1 takes issue with S2's interpretation of results from a poll that S2 created about gun control. S1 believes S2 is judging others by S2's own standards, preconceptions and life experiences. S1 is trying to convince S2 that respondents provided truthful answers and that the results prove that S1's pole hypothesis was simply wrong.  </line>
  <line></line>
  <line>S2 believes his life experience and personal standards support his conclusion that gun owners will not be huge advocates for gun control, regardless of what the poll results showed. He thinks respondents could have lied and he refuses to derive any conclusions from the results of his own poll. He thinks his poll's accuracy is questionable because the poll results do not match what logic tells him.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>S1 thinks logic suggests others are being judged by your own standards, and if answers don't fit your preconceptions it doesn't mean the answers are lies, it means your preconceptions are sub-optimal. S2 thinks it's best to judge everything by the same set of standards rather than have two sets. Reason dictates gun owners will not be advocates for gun control. He knows this from experience and can trust experience or online polls. S1 thinks everyone's standards need to match and the implication the poll may not be honest simply suggests a lack of experience. S2 asks how he is biased. If logic says something different the poll may not be accurate. S1 thinks S2 is wrong. S2 thinks that theory can't be definitive.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>S1 says S2 is not logical judging others according to his standards. Just because answers are not what S2 wants doesn't mean the answers are wrong, but that S2's beliefs are. S2 says it is better to use one set of standards to judge. He says it's reasonable gun owners wouldn't support gun control, and his experience supports that. S1 says S2 is accusing people of lying because their experiences do not agree with S2's, when it is S2's experience that is insufficient. S2 denies his poll was biased. Logic on issues makes him question it.  S1 points out the poll results were completely against S2's hypothesis. S2 says that is because a poll is just theory and not definitive.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>S1 thinks S2 is judging by their own standards and not being logical. They say that S2 thinks things are lies because they don't fit S2's preconceptions. S2 thinks all things should be judged by the same standards. S2 thinks it is reasonable that gun owners won't support gun control, and has anecdotal evidence to back it up. They insist anecdotal evidence is more important. S1 says S2 needs to have their standards match other people and that S2 was implying that poll respondents weren't lying just because it doesn't match S2's anecdotal evidence. S2 says their poll is not biased and logic dictates that poll results other than expected may indicate a problem with the poll. S1 accuses S2 of ignoring contrary evidence.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>S1 tells S2 he's being illogical in the way he interprets his polling data.  He says that S2 is rejecting the results of his own poll because he doesn't like the answers he's getting.  But if the poll results don't match the hypothesis, it means the hypothesis is wrong, not that everyone who took the poll is lying.</line>
  <line></line>
  <line>S2 considers what he does to be valid, saying he's using a single set of standards for everyone.  If his life experience tells him one thing, and a poll tells him the opposite, he's not going to assume the poll is accurate.  S2 believes the fact that the poll results don't match his own experience casts doubt on the poll's accuracy.</line>
 </text>
 <scu uid="1" label="Gun owners will not advocate for gun control.">
  <contributor label="Reason dictates gun owners will not be advocates for gun control">
   <part label="Reason dictates gun owners will not be advocates for gun control" start="1087" end="1151"/>
  </contributor>
  <contributor label="He says it's reasonable gun owners wouldn't support gun control">
   <part label="He says it's reasonable gun owners wouldn't support gun control" start="1772" end="1835"/>
  </contributor>
  <contributor label="S2 thinks it is reasonable that gun owners won't support gun control">
   <part label="S2 thinks it is reasonable that gun owners won't support gun control" start="2460" end="2528"/>
  </contributor>
  <contributor label="that gun owners will not be huge advocates for gun control, regardless of what the poll results showed">
   <part label="that gun owners will not be huge advocates for gun control, regardless of what the poll results showed" start="440" end="542"/>
  </contributor>
 </scu>
 <scu uid="2" label="Life experience support his conclusion.">
  <contributor label="S2 believes his life experience and personal standards support his conclusion">
   <part label="S2 believes his life experience and personal standards support his conclusion" start="362" end="439"/>
  </contributor>
  <contributor label="and his experience supports that">
   <part label="and his experience supports that" start="1837" end="1869"/>
  </contributor>
  <contributor label="has anecdotal evidence to back it up...They insist anecdotal evidence is more important">
   <part label="has anecdotal evidence to back it up" start="2534" end="2570"/>
   <part label="They insist anecdotal evidence is more important" start="2572" end="2620"/>
  </contributor>
  <contributor label="He knows this from experience and can trust experience or online polls">
   <part label="He knows this from experience and can trust experience or online polls" start="1153" end="1223"/>
  </contributor>
 </scu>
 <scu uid="5" label="Results prove the hypothesis was simply wrong.">
  <contributor label="that the results prove that S1's pole hypothesis was simply wrong">
   <part label="that the results prove that S1's pole hypothesis was simply wrong" start="292" end="357"/>
  </contributor>
  <contributor label="it means your preconceptions are sub-optimal...S1 thinks S2 is wrong">
   <part label="it means your preconceptions are sub-optimal" start="945" end="989"/>
   <part label="S1 thinks S2 is wrong" start="1445" end="1466"/>
  </contributor>
  <contributor label="but that S2's beliefs are...when it is S2's experience that is insufficient">
   <part label="but that S2's beliefs are" start="1686" end="1711"/>
   <part label="when it is S2's experience that is insufficient" start="1960" end="2007"/>
  </contributor>
  <contributor label="it means the hypothesis is wrong">
   <part label="it means the hypothesis is wrong" start="3235" end="3267"/>
  </contributor>
 </scu>
 <scu uid="6" label="Respondents provided truthful answers.">
  <contributor label="S1 is trying to convince S2 that respondents provided truthful answers">
   <part label="S1 is trying to convince S2 that respondents provided truthful answers" start="217" end="287"/>
  </contributor>
  <contributor label="not that everyone who took the poll is lying">
   <part label="not that everyone who took the poll is lying" start="3269" end="3313"/>
  </contributor>
 </scu>
 <scu uid="7" label="He is judging others by his own standards.">
  <contributor label="S1 believes S2 is judging others by S2's own standards, preconceptions and life experiences">
   <part label="S1 believes S2 is judging others by S2's own standards, preconceptions and life experiences" start="124" end="215"/>
  </contributor>
  <contributor label="S1 thinks logic suggests others are being judged by your own standards">
   <part label="S1 thinks logic suggests others are being judged by your own standards" start="790" end="860"/>
  </contributor>
  <contributor label="S1 says S2 is not logical judging others according to his standards">
   <part label="S1 says S2 is not logical judging others according to his standards" start="1538" end="1605"/>
  </contributor>
  <contributor label="S1 thinks S2 is judging by their own standards and not being logical">
   <part label="S1 thinks S2 is judging by their own standards and not being logical" start="2245" end="2313"/>
  </contributor>
  <contributor label="S1 tells S2 he's being illogical in the way he interprets his polling data">
   <part label="S1 tells S2 he's being illogical in the way he interprets his polling data" start="2998" end="3072"/>
  </contributor>
 </scu>
 <scu uid="8" label="It's best to judge everything by the same set of standards.">
  <contributor label="S2 thinks it's best to judge everything by the same set of standards rather than have two sets...S1 thinks everyone's standards need to match">
   <part label="S2 thinks it's best to judge everything by the same set of standards rather than have two sets" start="991" end="1085"/>
   <part label="S1 thinks everyone's standards need to match" start="1225" end="1269"/>
  </contributor>
  <contributor label="S2 says it is better to use one set of standards to judge">
   <part label="S2 says it is better to use one set of standards to judge" start="1713" end="1770"/>
  </contributor>
  <contributor label="S2 thinks all things should be judged by the same standards...S1 says S2 needs to have their standards match other people">
   <part label="S2 thinks all things should be judged by the same standards" start="2399" end="2458"/>
   <part label="S1 says S2 needs to have their standards match other people" start="2622" end="2681"/>
  </contributor>
  <contributor label="saying he's using a single set of standards for everyone">
   <part label="saying he's using a single set of standards for everyone" start="3355" end="3411"/>
  </contributor>
 </scu>
 <scu uid="9" label="Denies his poll is biased.">
  <contributor label="S2 denies his poll was biased">
   <part label="S2 denies his poll was biased" start="2009" end="2038"/>
  </contributor>
  <contributor label="S2 asks how he is biased">
   <part label="S2 asks how he is biased" start="1355" end="1379"/>
  </contributor>
  <contributor label="S2 says their poll is not biased">
   <part label="S2 says their poll is not biased" start="2798" end="2830"/>
  </contributor>
  <contributor label="S2 considers what he does to be valid">
   <part label="S2 considers what he does to be valid" start="3316" end="3353"/>
  </contributor>
 </scu>
 <scu uid="10" label="Theory can't be definitive.">
  <contributor label="S2 thinks that theory can't be definitive">
   <part label="S2 thinks that theory can't be definitive" start="1468" end="1509"/>
  </contributor>
  <contributor label="S2 says that is because a poll is just theory and not definitive">
   <part label="S2 says that is because a poll is just theory and not definitive" start="2152" end="2216"/>
  </contributor>
 </scu>
 <scu uid="11" label="Respondents could have lied.">
  <contributor label="He thinks respondents could have lied">
   <part label="He thinks respondents could have lied" start="544" end="581"/>
  </contributor>
  <contributor label="S1 says S2 is accusing people of lying">
   <part label="S1 says S2 is accusing people of lying" start="1871" end="1909"/>
  </contributor>
  <contributor label="They say that S2 thinks things are lies...that S2 was implying that poll respondents weren't lying">
   <part label="They say that S2 thinks things are lies" start="2315" end="2354"/>
   <part label="that S2 was implying that poll respondents weren't lying" start="2686" end="2742"/>
  </contributor>
 </scu>
 <scu uid="12" label="Poll results do not match his preconception.">
  <contributor label="the poll results do not match what logic tells him">
   <part label="the poll results do not match what logic tells him" start="711" end="761"/>
  </contributor>
  <contributor label="because their experiences do not agree with S2's...S1 points out the poll results were completely against S2's hypothesis">
   <part label="because their experiences do not agree with S2's" start="1910" end="1958"/>
   <part label="S1 points out the poll results were completely against S2's hypothesis" start="2080" end="2150"/>
  </contributor>
  <contributor label="because they don't fit S2's preconceptions...because it doesn't match S2's anecdotal evidence">
   <part label="because they don't fit S2's preconceptions" start="2355" end="2397"/>
   <part label="because it doesn't match S2's anecdotal evidence" start="2748" end="2796"/>
  </contributor>
  <contributor label="But if the poll results don't match the hypothesis...If his life experience tells him one thing, and a poll tells him the opposite...S2 believes the fact that the poll results don't match his own experience casts">
   <part label="S2 believes the fact that the poll results don't match his own experience casts" start="3541" end="3620"/>
   <part label="If his life experience tells him one thing, and a poll tells him the opposite" start="3414" end="3491"/>
   <part label="But if the poll results don't match the hypothesis" start="3183" end="3233"/>
  </contributor>
 </scu>
 <scu uid="13" label="Poll's accuracy is questionable.">
  <contributor label="He thinks his poll's accuracy is questionable">
   <part label="He thinks his poll's accuracy is questionable" start="657" end="702"/>
  </contributor>
  <contributor label="the implication the poll may not be honest simply suggests a lack of experience...If logic says something different the poll may not be accurate">
   <part label="the implication the poll may not be honest simply suggests a lack of experience" start="1274" end="1353"/>
   <part label="If logic says something different the poll may not be accurate" start="1381" end="1443"/>
  </contributor>
  <contributor label="Logic on issues makes him question it">
   <part label="Logic on issues makes him question it" start="2040" end="2077"/>
  </contributor>
  <contributor label="logic dictates that poll results other than expected may indicate a problem with the poll">
   <part label="logic dictates that poll results other than expected may indicate a problem with the poll" start="2835" end="2924"/>
  </contributor>
  <contributor label="doubt on the poll's accuracy">
   <part label="doubt on the poll's accuracy" start="3621" end="3649"/>
  </contributor>
 </scu>
 <scu uid="14" label="If answers don't fit preconception it doesn't mean the answes are lies.">
  <contributor label="Just because answers are not what S2 wants doesn't mean the answers are wrong">
   <part label="Just because answers are not what S2 wants doesn't mean the answers are wrong" start="1607" end="1684"/>
  </contributor>
  <contributor label="if answers don't fit your preconceptions it doesn't mean the answers are lies">
   <part label="if answers don't fit your preconceptions it doesn't mean the answers are lies" start="866" end="943"/>
  </contributor>
 </scu>
 <scu uid="15" label="Ignoring contrary evidence.">
  <contributor label="S1 accuses S2 of ignoring contrary evidence">
   <part label="S1 accuses S2 of ignoring contrary evidence" start="2926" end="2969"/>
  </contributor>
 </scu>
 <scu uid="16" label="Refuses to derive any conclusions from the results of his own poll.">
  <contributor label="he refuses to derive any conclusions from the results of his own poll">
   <part label="he refuses to derive any conclusions from the results of his own poll" start="586" end="655"/>
  </contributor>
  <contributor label="He says that S2 is rejecting the results of his own poll...he's not going to assume the poll is accurate">
   <part label="He says that S2 is rejecting the results of his own poll" start="3075" end="3131"/>
   <part label="he's not going to assume the poll is accurate" start="3493" end="3538"/>
  </contributor>
 </scu>
 <scu uid="17" label="Does not like the answers he is getting.">
  <contributor label="he doesn't like the answers he's getting">
   <part label="he doesn't like the answers he's getting" start="3140" end="3180"/>
  </contributor>
 </scu>
 <scu uid="18" label="Takes issue with interpretation of results from a poll about gun control.">
  <contributor label="S1 takes issue with S2's interpretation of results from a poll that S2 created about gun control">
   <part label="S1 takes issue with S2's interpretation of results from a poll that S2 created about gun control" start="26" end="122"/>
  </contributor>
 </scu>
</pyramid>
