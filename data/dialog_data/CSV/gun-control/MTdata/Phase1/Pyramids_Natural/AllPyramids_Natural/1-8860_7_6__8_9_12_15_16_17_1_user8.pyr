<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>Author1 asks what part of the Brady II authorizes unannounced searches. Author2 cites section 203. Author1 says the &quot;at any time&quot; section only applies to guns used in a criminal investigation, so this scenario is nothing but a scare tactic. Author2 asks if Author1 missed the part that says &quot;or&quot;. He says there are no scare tactics involved. Author1 replies that he didn't miss anything and quotes the section following the &quot;or&quot;. He then asks Author2 when most inspections take place as he thinks it is during business hours. Author2 replies Author1 keeps skipping over the part that says or any time they investigate. He thinks this means at any hours and not just during normal business hours. Author1 says that this still applies to criminal investigations, so it is still a scare tactic. Author2 thinks Author1 is being obtuse on purpose and he says they may investigate up to three times per year. Furthermore, it doesn't state only during business hours, so it could be done at any time. He doesn't trust the ATF.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>Author1 argues that firearms inspections are only allowed to be investigated at any time, only when the gun is involved in a criminal investigation. Guns are not allowed to be requested to be searched at any time when they are not related to a criminal investigation. When not related to a criminal investigation, searches are only legal not more than once in any 12 month period. The only exception is that records can be searched three times each year. If they are investigating a firearm in a crime that belongs to a person, they can still investigate someone, but there is a likelihood that it would not be in the middle of the night. </line>
  <line>Author2 argues that they added two additional chances for legal unannounced searches in each 12 month period, and they also are now allowed to search at any time in regard to a firearm in a criminal investigation. They will not announce their searches. It is fair game for any searches because it is not stated otherwise.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>Author1 asks where the authorization of &quot;unannounced searches&quot; of the holders of arsenal license comes from. Upon learning that the &quot;at anytime&quot; pertains to the records relating to a firearm involved in a criminal investigation and is traced back to the owner of the  arsenal license. He claims that Author2 is just trying to scare people.  </line>
  <line>Author2 disagrees and says that he is not trying to scare anyone and that Author1 must think that the Bureau of Alcohol, Tobacco, and Firearms send out written invitations when the want to come by and inspect. Author1 says he didn't think that - but on the other hand - the BATF doesn't just show up in the middle of the night, right? Author2 tries to set Author1 straight and explains that when they are investigating a firearm the BATF is not limited to the stores hours, but that they can show up anytime they want up to 3 times in a year.  He goes on to say that not all the agents are &quot;nice guys&quot;.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>Author1 wants Author2 to cite the portion of the Brady II that would authorize these unannounced searches, and Author2 obliges him by quoting a section. It deals with the arsenal license that requires its holders to adhere to all obligations and requirements. Author1 thanks Author2 for the quote, and then proceeds to quote another section. Author1 emphasizes the portion that mentions a firearm involved in a criminal investigation, then he states his belief that it is mentioned to cause fear. Author2 argues that the BATFE's track record proves that there is no scare mongering, then Author1 tells Author2 that most gun dealers do not have their licenses inspected at midnight. Author2 believes that a gun dealer can be investigated outside of business hours. Author1 agrees that happens when they are investigating the owner of a firearm used in a crime, but Author2 states that gun dealers can be inspected three times a year. It does not state only during business hours, and then Author2 asserts that it is well over three visits in criminal investigations.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>Author1 ask what part of the Brady law covers unannounced searches. He points out the section of the code saying &quot;at any time&quot; only pertains to when the gun is involved in a criminal investigation. He says gun dealers records are rarely searched in the middle of the night. He says after hours inspections would only be during an investigation. </line>
  <line></line>
  <line></line>
  <line>Author2 cites US code Section 923 of title 18 covering Arsenal License. It says the licensee is subject to all obligations and requirements pertaining to dealers. It was changed from once every 12 months to 3 times every 12 months, or any time related to a firearm involved in a criminal investigation. He notes the &quot;or at any time&quot; part of the code, and mocks they make convenient appointments. He implies the &quot;or any time during an investigation&quot; means they could just say they were doing an investigation if they wanted to search more than 3 times a year. He says Author1 should know police aren't restricted to investigating only during business hours.</line>
 </text>
 <scu uid="2" label="Criminal investigators can search three times each year.">
  <contributor label="The only exception is that records can be searched three times each year">
   <part label="The only exception is that records can be searched three times each year" start="1454" end="1526"/>
  </contributor>
  <contributor label="he says they may investigate up to three times per year">
   <part label="he says they may investigate up to three times per year" start="872" end="927"/>
  </contributor>
  <contributor label="that they can show up anytime they want up to 3 times in a year">
   <part label="that they can show up anytime they want up to 3 times in a year" start="2882" end="2945"/>
  </contributor>
  <contributor label="Author2 states that gun dealers can be inspected three times a year">
   <part label="Author2 states that gun dealers can be inspected three times a year" start="3898" end="3965"/>
  </contributor>
  <contributor label="It was changed from once every 12 months to 3 times every 12 months">
   <part label="It was changed from once every 12 months to 3 times every 12 months" start="4638" end="4705"/>
  </contributor>
 </scu>
 <scu uid="4" label="Inspections can only be done during an investigation.">
  <contributor label="&quot;at any time&quot; only pertains to when the gun is involved in a criminal investigation...He says after hours inspections would only be during an investigation...or any time related to a firearm involved in a criminal investigation">
   <part label="He says after hours inspections would only be during an investigation" start="4401" end="4470"/>
   <part label="&quot;at any time&quot; only pertains to when the gun is involved in a criminal investigation" start="4240" end="4323"/>
   <part label="or any time related to a firearm involved in a criminal investigation" start="4707" end="4776"/>
  </contributor>
  <contributor label="Author1 agrees that happens when they are investigating the owner of a firearm used in a crime">
   <part label="Author1 agrees that happens when they are investigating the owner of a firearm used in a crime" start="3798" end="3892"/>
  </contributor>
  <contributor label="Author2 tries to set Author1 straight and explains that when they are investigating a firearm the BATF is not limited to the stores hours">
   <part label="Author2 tries to set Author1 straight and explains that when they are investigating a firearm the BATF is not limited to the stores hours" start="2739" end="2876"/>
  </contributor>
  <contributor label="Author1 argues that firearms inspections are only allowed to be investigated at any time, only when the gun is involved in a criminal investigation...If they are investigating a firearm in a crime that belongs to a person, they can still investigate someone">
   <part label="If they are investigating a firearm in a crime that belongs to a person, they can still investigate someone" start="1528" end="1635"/>
   <part label="Author1 argues that firearms inspections are only allowed to be investigated at any time, only when the gun is involved in a criminal investigation" start="1073" end="1220"/>
  </contributor>
  <contributor label="Author1 says the &quot;at any time&quot; section only applies to guns used in a criminal investigation...Author1 says that this still applies to criminal investigations">
   <part label="Author1 says the &quot;at any time&quot; section only applies to guns used in a criminal investigation" start="125" end="217"/>
   <part label="Author1 says that this still applies to criminal investigations" start="722" end="785"/>
  </contributor>
 </scu>
 <scu uid="13" label="Investigation can be done at anytime.">
  <contributor label="He thinks this means at any hours and not just during normal business hours...Furthermore, it doesn't state only during business hours, so it could be done at any time">
   <part label="Furthermore, it doesn't state only during business hours, so it could be done at any time" start="929" end="1018"/>
   <part label="He thinks this means at any hours and not just during normal business hours" start="645" end="720"/>
  </contributor>
  <contributor label="they also are now allowed to search at any time in regard to a firearm in a criminal investigation">
   <part label="they also are now allowed to search at any time in regard to a firearm in a criminal investigation" start="1827" end="1925"/>
  </contributor>
  <contributor label="Author2 believes that a gun dealer can be investigated outside of business hours...It does not state only during business hours">
   <part label="Author2 believes that a gun dealer can be investigated outside of business hours" start="3716" end="3796"/>
   <part label="It does not state only during business hours" start="3967" end="4011"/>
  </contributor>
  <contributor label="He notes the &quot;or at any time&quot; part of the code...He implies the &quot;or any time during an investigation&quot; means they could just say they were doing an investigation if they wanted to search more than 3 times a year...He says Author1 should know police aren't restricted to investigating only during business hours">
   <part label="He notes the &quot;or at any time&quot; part of the code" start="4778" end="4824"/>
   <part label="He implies the &quot;or any time during an investigation&quot; means they could just say they were doing an investigation if they wanted to search more than 3 times a year" start="4871" end="5032"/>
   <part label="He says Author1 should know police aren't restricted to investigating only during business hours" start="5034" end="5130"/>
  </contributor>
  <contributor label="Upon learning that the &quot;at anytime&quot; pertains to the records relating to a firearm involved in a criminal investigation">
   <part label="Upon learning that the &quot;at anytime&quot; pertains to the records relating to a firearm involved in a criminal investigation" start="2171" end="2289"/>
  </contributor>
 </scu>
 <scu uid="3" label="Gun dealers are rarely searched in the middle of the night.">
  <contributor label="He says gun dealers records are rarely searched in the middle of the night">
   <part label="He says gun dealers records are rarely searched in the middle of the night" start="4325" end="4399"/>
  </contributor>
  <contributor label="but there is a likelihood that it would not be in the middle of the night">
   <part label="but there is a likelihood that it would not be in the middle of the night" start="1637" end="1710"/>
  </contributor>
  <contributor label="then Author1 tells Author2 that most gun dealers do not have their licenses inspected at midnight">
   <part label="then Author1 tells Author2 that most gun dealers do not have their licenses inspected at midnight" start="3617" end="3714"/>
  </contributor>
  <contributor label="the BATF doesn't just show up in the middle of the night, right?">
   <part label="the BATF doesn't just show up in the middle of the night, right?" start="2674" end="2738"/>
  </contributor>
 </scu>
 <scu uid="1" label="what part of the Brady law covers unannounced searches.">
  <contributor label="Author1 ask what part of the Brady law covers unannounced searches">
   <part label="Author1 ask what part of the Brady law covers unannounced searches" start="4127" end="4193"/>
  </contributor>
  <contributor label="Author1 wants Author2 to cite the portion of the Brady II that would authorize these unannounced searches">
   <part label="Author1 wants Author2 to cite the portion of the Brady II that would authorize these unannounced searches" start="3034" end="3139"/>
  </contributor>
  <contributor label="Author1 asks what part of the Brady II authorizes unannounced searches">
   <part label="Author1 asks what part of the Brady II authorizes unannounced searches" start="26" end="96"/>
  </contributor>
  <contributor label="Author1 asks where the authorization of &quot;unannounced searches&quot; of the holders of arsenal license comes from">
   <part label="Author1 asks where the authorization of &quot;unannounced searches&quot; of the holders of arsenal license comes from" start="2062" end="2169"/>
  </contributor>
 </scu>
 <scu uid="5" label="After hour inspections are a scare tactic.">
  <contributor label="so this scenario is nothing but a scare tactic...so it is still a scare tactic">
   <part label="so this scenario is nothing but a scare tactic" start="219" end="265"/>
   <part label="so it is still a scare tactic" start="787" end="816"/>
  </contributor>
  <contributor label="He claims that Author2 is just trying to scare people">
   <part label="He claims that Author2 is just trying to scare people" start="2347" end="2400"/>
  </contributor>
  <contributor label="then he states his belief that it is mentioned to cause fear">
   <part label="then he states his belief that it is mentioned to cause fear" start="3469" end="3529"/>
  </contributor>
 </scu>
 <scu uid="6" label="After hour inspections are not a scare tactic.">
  <contributor label="Author2 argues that the BATFE's track record proves that there is no scare mongering">
   <part label="Author2 argues that the BATFE's track record proves that there is no scare mongering" start="3531" end="3615"/>
  </contributor>
  <contributor label="He says there are no scare tactics involved">
   <part label="He says there are no scare tactics involved" start="323" end="366"/>
  </contributor>
  <contributor label="Author2 disagrees and says that he is not trying to scare anyone">
   <part label="Author2 disagrees and says that he is not trying to scare anyone" start="2404" end="2468"/>
  </contributor>
 </scu>
 <scu uid="7" label="He cites code section.">
  <contributor label="Author2 cites section 203...quotes the section following the &quot;or&quot;">
   <part label="Author2 cites section 203" start="98" end="123"/>
   <part label="quotes the section following the &quot;or&quot;" start="417" end="454"/>
  </contributor>
  <contributor label="He points out the section of the code saying...Author2 cites US code Section 923 of title 18 covering Arsenal License">
   <part label="Author2 cites US code Section 923 of title 18 covering Arsenal License" start="4475" end="4545"/>
   <part label="He points out the section of the code saying" start="4195" end="4239"/>
  </contributor>
  <contributor label="Author2 obliges him by quoting a section...then proceeds to quote another section">
   <part label="Author2 obliges him by quoting a section" start="3145" end="3185"/>
   <part label="then proceeds to quote another section" start="3336" end="3374"/>
  </contributor>
 </scu>
 <scu uid="21" label="Firearm searches are not announced.">
  <contributor label="They will not announce their searches">
   <part label="They will not announce their searches" start="1927" end="1964"/>
  </contributor>
  <contributor label="that Author1 must think that the Bureau of Alcohol, Tobacco, and Firearms send out written invitations when the want to come by and inspect...Author1 says he didn't think that - but on the other hand">
   <part label="that Author1 must think that the Bureau of Alcohol, Tobacco, and Firearms send out written invitations when the want to come by and inspect" start="2473" end="2612"/>
   <part label="Author1 says he didn't think that - but on the other hand" start="2614" end="2671"/>
  </contributor>
  <contributor label="mocks they make convenient appointments">
   <part label="mocks they make convenient appointments" start="4830" end="4869"/>
  </contributor>
 </scu>
 <scu uid="17" label="Arsenal license requires its holder to adhere to all requirements.">
  <contributor label="It says the licensee is subject to all obligations and requirements pertaining to dealers">
   <part label="It says the licensee is subject to all obligations and requirements pertaining to dealers" start="4547" end="4636"/>
  </contributor>
  <contributor label="It deals with the arsenal license that requires its holders to adhere to all obligations and requirements">
   <part label="It deals with the arsenal license that requires its holders to adhere to all obligations and requirements" start="3187" end="3292"/>
  </contributor>
 </scu>
 <scu uid="18" label="Guns can not be searched at any time when guns are not related to a criminal investigation.">
  <contributor label="Guns are not allowed to be requested to be searched at any time when they are not related to a criminal investigation">
   <part label="Guns are not allowed to be requested to be searched at any time when they are not related to a criminal investigation" start="1222" end="1339"/>
  </contributor>
  <contributor label="Author1 emphasizes the portion that mentions a firearm involved in a criminal investigation">
   <part label="Author1 emphasizes the portion that mentions a firearm involved in a criminal investigation" start="3376" end="3467"/>
  </contributor>
 </scu>
 <scu uid="20" label="Two additional searches have been added to legal unannounced searches in each 12 month period.">
  <contributor label="Author2 argues that they added two additional chances for legal unannounced searches in each 12 month period">
   <part label="Author2 argues that they added two additional chances for legal unannounced searches in each 12 month period" start="1713" end="1821"/>
  </contributor>
  <contributor label="then Author2 asserts that it is well over three visits in criminal investigations">
   <part label="then Author2 asserts that it is well over three visits in criminal investigations" start="4017" end="4098"/>
  </contributor>
 </scu>
 <scu uid="11" label="Ask when most inspections take place.">
  <contributor label="He then asks Author2 when most inspections take place as">
   <part label="He then asks Author2 when most inspections take place as" start="456" end="512"/>
  </contributor>
 </scu>
 <scu uid="23" label="Guns are traced back to the owner of the arsenal license.">
  <contributor label="is traced back to the owner of the  arsenal license">
   <part label="is traced back to the owner of the  arsenal license" start="2294" end="2345"/>
  </contributor>
 </scu>
 <scu uid="10" label="He didn't miss anything.">
  <contributor label="Author1 replies that he didn't miss anything">
   <part label="Author1 replies that he didn't miss anything" start="368" end="412"/>
  </contributor>
 </scu>
 <scu uid="15" label="He is being obtuse on purpose.">
  <contributor label="Author2 thinks Author1 is being obtuse on purpose">
   <part label="Author2 thinks Author1 is being obtuse on purpose" start="818" end="867"/>
  </contributor>
 </scu>
 <scu uid="25" label="He is thankful for the quote.">
  <contributor label="Author1 thanks Author2 for the quote">
   <part label="Author1 thanks Author2 for the quote" start="3294" end="3330"/>
  </contributor>
 </scu>
 <scu uid="9" label="Investigations can only be done suring business hours.">
  <contributor label="he thinks it is during business hours">
   <part label="he thinks it is during business hours" start="513" end="550"/>
  </contributor>
 </scu>
 <scu uid="19" label="Legal searches can not be done more than once in a 12 month period.">
  <contributor label="When not related to a criminal investigation, searches are only legal not more than once in any 12 month period">
   <part label="When not related to a criminal investigation, searches are only legal not more than once in any 12 month period" start="1341" end="1452"/>
  </contributor>
 </scu>
 <scu uid="8" label="Missed reading section.">
  <contributor label="Author2 asks if Author1 missed the part that says &quot;or&quot;...Author2 replies Author1 keeps skipping over the part that says or any time they investigate">
   <part label="Author2 asks if Author1 missed the part that says &quot;or&quot;" start="267" end="321"/>
   <part label="Author2 replies Author1 keeps skipping over the part that says or any time they investigate" start="552" end="643"/>
  </contributor>
 </scu>
 <scu uid="12" label="Not all the agents are nice guys.">
  <contributor label="He goes on to say that not all the agents are &quot;nice guys&quot;">
   <part label="He goes on to say that not all the agents are &quot;nice guys&quot;" start="2948" end="3005"/>
  </contributor>
 </scu>
 <scu uid="16" label="The AFT is not trusted.">
  <contributor label="He doesn't trust the ATF">
   <part label="He doesn't trust the ATF" start="1020" end="1044"/>
  </contributor>
 </scu>
 <scu uid="22" label="Unannounced searches are fair game.">
  <contributor label="It is fair game for any searches because it is not stated otherwise">
   <part label="It is fair game for any searches because it is not stated otherwise" start="1966" end="2033"/>
  </contributor>
 </scu>
</pyramid>
