
----------
D0
----------
Author1 is against gun control. California has strict gun controls. Other states do not need to copy California. Other issues should be addressed by states. Gun control does not apply to real life well. Illegal guns do not need to follow regulations. There will still be dangerous guns available to criminals. Citizens should be able to protect themselves. Author2 argues that gun control is needed. Gun control would have saved four policeman's lives. High crime rates require stricter laws. Gun control reduces dangerous weapons on the streets. Both Author1 and Author2 are in favor of guns. Author2 wants more gun control. If gun control works it will save lives. AK-47's are too dangerous for typical citizens.


----------
D1
----------
Author1 says Author2 thinks everyone should adopt California's laws. He says there is no support. He argues one state shouldn't be able to tell others what to do. States need to take care of their issues however. Author2 says adopting laws would depend on the voters and states. He states that California has large densely populated cities with high crime rates. He gives a recent example. Four cops were killed by a criminal with an AK-47. Author1 questions how this is a good example. Using a gun that is banned doesn't prove the ban is good. Author2 says it shows why there is a ban. Author2 says injuries wouldn't be so severe if the gun hadn't been available.


----------
D2
----------
Author1 believes Author2 wants the USA to adopt California's laws. One state cannot regulate the activities of another state. A state should not ignore any home rule issues that they cannot respond to. Author2 asserts that states could adopt the controls that California has. Some states dislike California's laws. There will be no reform. Four Oakland police officers were murdered by a hood with an AK-47. This is proof that California has the right restrictions. Author1 questions Author2 about using this murder as an example of why this prohibition is good. Author2 asserts that it provides a good example. These measures are good. Author1 questions Author2 about his assertion again. Author2 stresses that the availability of these weapons increased the seriousness of the criminal act.


----------
D3
----------
Author1 says the argument that the United States should adopt California's standards as national ones has been made several times. Support for it has been lacking. States can't regulate the activities of others. That doesn't mean the state should ignore home rule issues that created problems. Author2 says states could adopt control like California but it would depend on the voters. He says that people miss the fact that California is a large state. It also has large crime rates. This is part of the reason for the restrictions. He gives an example of four officers that were gunned down with an AK-47. Author1 questions the example because the gun was an illegal one being used illegally. Author2 thinks the gun shouldn't have been available.


----------
D4
----------
Author1 notes that Author2 has argued for his current proposal before.  Author2's proposal is for other states to enact gun laws similar to California's.  These ideas have not received much support.  One state can't regulate the rules of another.  Author1 asks Author2 twice if he really believes that a crime committed with a banned gun demonstrates why that ban is a good idea.

Author2 says that voters in other states could choose to adopt laws like California's.  He acknowledges that some states are unlikely to do so.  The Oakland police shooting cases shows the value of banning weapons like AK-47s.  The ban will make it harder for criminals to find such weapons.  Author2 is a gun owner.

