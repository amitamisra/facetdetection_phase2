
----------
D0
----------
Author1 says that carrying guns isn't bad. They ask Author2 if it's okay to carry guns. Author2 asks if it's okay to carry a nuke. Author1 wants Author2 to find a nuke for sale. Author1 asks how people would get guns with a ban. Author2 says that they should be allowed nukes. Author2 the US has nukes so they should too. Author2 says Author1 should try to repeal a ban instead of breaking it. Author1 doesn't think nukes are comparable to guns. Author2 says nukes could be bought if there was a market for it.


----------
D1
----------
Author1 states that nuclear weapons are banned in the US. Author2 responds by stating that citizens should have the same right to own them since the US has the right. The goal should be to repeal the ban instead of trying to break it. Author1 states that the ban cannot be repealed because nuclear weapons cannot be sold to citizens. Vendors sell to the government. No vendor would sell to a citizen. Author2 clarifies that he was referring to bans on guns and gun ownership as it applies to Author1. He is using an example to illustrate the ban on owning and bearing nuclear weapons. He imagines a situation in which it is both legal and profitable for people to own and sell nuclear weapons.


----------
D2
----------
Author1 says nobody answered his threads about the dangers of gun carrying. That shows gun carrying is safe. Author2 says it's as safe as an unsupervised nuclear weapon. Carrying guns is inherently dangerous. Author1 says nukes are banned, so it's a bad analogy. Anyway, his unanswered posts prove that guns are safe and need not be banned. Author2 says no person should have access to an unregulated nuke. Guns should similarly be regulated. Author1 says nobody can buy nukes. Author2 says he mentions guns with respect to his opponent's argument, and nukes with respect to his own. If it were possible to sell people nukes, some maker would. So, by his opponent's argument, people should be able to buy them.


----------
D3
----------
Author1 asks if it's ok that he has a gun. No one has said how it endangers anyone, increases crime, or violate's anyone's rights. Author2 asks if it's ok for him to have a nuclear weapon. He asks if France can have one stored in Chicago. Author1 says France has the same rights as a US citizen. He thinks finding a nuclear weapon for sale would be more relevant. He asks why ban guns if the same conditions apply. Author1 asks what ban he's talking about. He says the only vendor is happy to have it's only customer be the Department of Defense. He agrees with the logic. It doesn't make sense, though. Author2 says he is talking about the ban to own nuclear weapons.


----------
D4
----------
Author1 refers to previous posts. He says no one replied as to being harmed by him carrying a gun. He asks if it is okay then to carry a gun. Author2 asks if it is okay for him to own a nuclear weapon. Author1 says the question is irrelevant. He says nuclear weapons aren't for sale. Author1 states the government is allowed to own a nuclear weapon. He feels he should be allowed to own one. He shouldn't be allowed to own one if they are banned. Author1 argues there is no ban. He says they are not for sale. He says no seller would sell one. Author2 argues sellers would sell them. He says they are not allowed to sell them.

