
----------
D0
----------
Author1 takes issue with Author2's interpretation of results from a poll that Author2 created about gun control. Author1 believes Author2 is judging others by Author2's own standards. Author1 also thinks Author2 is being influenced by his own preconceptions and life experiences. Author1 is trying to convince Author2 that the poll respondents provided truthful answers. Author2 also thinks Author1's pole hypothesis was simply wrong. 
 
Author2 believes his life experience and personal standards lead him to believe gun owners will not be huge advocates for gun control. He does not believe the results from his own poll. He thinks poll respondents could have lied. He thinks his own poll's accuracy is questionable. Logic tells him the results could not be accurate.


----------
D1
----------
Author1 says logic suggests others are being judged by your own standards. If answers don't fit your preconceptions it doesn't mean the answers are lies. It means your preconceptions are sub-optimal. Author2 thinks it's best to judge everything by the same set of standards instead of having two sets. Reason dictates gun owners will not be advocates for gun control. He knows this from experience. He can trust experience or online polls. Author1 thinks everyone's standards need to match. Implying that the poll may not be honest simply suggests a lack of experience. Author2 asks how he is biased. If logic says something different then the poll may not be accurate. Author1 thinks that Author2 is wrong. Author2 thinks that theory can't be definitive.


----------
D2
----------
Author1 believes Author2 is not logical. He is judging others on HIS standards. Just because answers don't agree with Author2 doesn't make them wrong. Author2's beliefs are wrong. Author2 says it is better to use one set of standards to judge. He says it's reasonable gun owners don't support gun control. His experience supports that. Author1 says Author2 falsely accuses people of lying who do not have the same experiences. He says Author2's experiences are insufficient. Author2 denies his poll was biased. Logic on issues makes him question it. Author1 says the poll results disprove Author2's hypothesis. Author2 blames that on a poll being a theory. A theory is not definitive.


----------
D3
----------
Author1 thinks Author2 is being unfair. Author1 thinks Author2 ignores evidence. Author2 uses one set of standards to judge things. Author2 thinks gun owners won't support gun control. Author2 says their experience confirms that. Author2 thinks their experience is more important than polls. Author1 thinks Author2 needs to look at evidence. Author1 says Author2 ignores evidence that disagrees with them. Author2 doesn't think their poll was biased. Author2 says their poll was faulty. Author2 thinks that because their poll disagrees with logic. Author1 thinks Author2 is ignoring evidence. Author2 does not hink online polls are definitive.


----------
D4
----------
Author1 thinks that Author2 is being illogical.  Author2 dismisses polling data that doesn't support his own preconception.  But If the polls don't support the hypothesis, it's the hypothesis that's wrong.  Author2 is wrong to conclude that the people polled are lying.  Author2's bias causes him to misinterpret the polls.

Author2 thinks his logic about polls is valid.  If poll results conflict with Author2's experience, it casts doubt on the poll's accuracy.  Author2's logic tells him that gun-owners are not in favor of gun control.  His experience supports this logic.  Online polls may or may not be accurate.  If poll results don't match up with Author2's logic, Author2 will not draw conclusions from those polls.

