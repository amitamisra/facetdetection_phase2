
----------
D0
----------
Author1 believes in state's rights. To him the US government is a constitutional republic. It follows a constitution, and elects its head of state. He also believes state's rights means not every individual state in the republic must have the same laws and liberties. Slavery was legal before the Fourteenth Amendment, when state laws were free from the restraints of the Bill of Rights. Once the Confederacy seceded, the US constitution was irrelevant. But after the war, the Fourteenth Amendment took away some of states' autonomy and freedom of action. Lincoln fought to preserve the Union, though his legal justification for that was questionable. Slavery was legal too, though many loathed it. Author2 believes state's rights is a disguise. He says talk of state's rights covers up views like those of the Civil War South. Before the Civil War they had free will, autonomy and slaves, and they seceded to keep them. But some Northerners also had slaves. So he would actually like to understand why Lincoln felt justified making war on the South.


----------
D1
----------
Author1 argues that a constitutional republic is defined because it is governed by a constitution and its form of government is a republic type, with democratic voting. The Civil War regarded issues including states rights, and the Bill of Rights was not enforced upon the states until the fourteenth amendment. States not in the Union would not be concerned with the Union's Constitution. The individual states lost power due to the passage of the fourteenth amendment. The point of the Civil War was to preserve the Union, and slavery was not the main issue, it was a moral issue. Moral issues are not decided upon by the legal or Constitutional system before the amendments made as a result of the civil War. Lincoln was not fully supported by the Constitution when he declared war on the south.

Author2 argues that states had more free will before the Civil War, and the South broke away to keep that free will as well as their choice to have slaves. States had less free will after the war.


----------
D2
----------
Author1 sees no sense in the claim that every state should protect the same liberties, and Author1 tells Author2 that he is misquoting an article of the Constitution. Author2 tells Author1 that he is actually misquoting him, and he asks Author1 to explain the Civil War. Author1 apologizes for the misquotation, then he asks Author2 what kind of explanation he should offer concerning the Civil War. Author2 wants Author1 to apply his stance in terms of the Civil War, because Author1 is claiming State's rights just like the South. Author1 states that the Civil War covers State rights, and he asks Author2 if he is referring to slavery being allowed before the Fourteenth Amendment existed. Author2 reminds him that the South had slaves and autonomy, they wanted to keep their free will. Author1 believes the Fourteenth Amendment caused the states to have less autonomy, but Author2 disagrees. Author2 states that the North also had slaves, and Lincoln never justified Northern aggression. Author1 counters by stating that Lincoln justified Northern aggression when he preserved the Union.


----------
D3
----------
Author1 says it's not a requirement of a government under the Constitution for all states to have the same laws. Article IV section 1  says that they have to give faith and credit to laws of other states, not adopt them as their own. The government is a republic limited in power, bound by the Constitution. He says state rights was one of many issues of the war. The constitution allowed slavery. The Bill of Rights gave states free will. He admits after the war states had less autonomy. He admits Lincoln's constitutionally debatable reason for war was to save the union from being broken. 

Author2 questions how Author1's stance on the war applies to the rights he says only the states have. He says Author1 has the same position as the South. He questions whether the states had free will. He says the South wanted to leave the union and keep their slaves and autonomy. He said the North had slaves too. He ask what was Lincoln's real reason for the war.


----------
D4
----------
Author1 doesn't think republics are required to form governments. The fact that the government's power is limited by a constitution makes it constitutional and the fact that the head of state isn't a monarch and the people play a role makes it a republic. He doesn't think every State in the union needs to have the same laws. Doc may be referring to the Full Faith and Credit Clause, but this Clause does not have the meaning he's providing. Author2 says he can reply to any post he wants and asks about the Civil War. Author1 says he apologized for the misquote and asks what about the Civil War. Author2 wants him to put this in terms of the Civil War. Author1 asks does he mean slavery was allowed and says it was. Author2 says the states had free will. Author1 agrees. Author2 says the south wanted to break away. Author1 isn't talking about motivation, but the Constitution applied. Author2 says this is fair but not correct. Author1 says there wasn't Constitutional justification against slavery.

