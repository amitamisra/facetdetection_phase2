
----------
D0
----------
Author1 believes the ATF is abusing the system as well as their acts are unconstitutional.  There needs to be a trace on gun smugglers working for the ATF as well as the ATF's records they are withholding illegally from Congress. Congress makes the laws and ATF is being charged with contempt of Congress.  The law states  approvals through the NICS check be destroyed in 24 hours and no permanent records  will exist anywhere except in gun shops. Author1 sees Author2 is not up with current events.

Author2 contests protests against the ATF is a conspiracy and the ATF abides by the law.  The ATF was asked for copies, but not asked for the books themselves.  There is a statute that states a federal agency (ATF) cannot ask for copies.  Sellers are bound by the law to produce any records when needed.  Author1 keeps refusing to refute the proof that they have shown for the argument. Author1 claims that Author1 keeps diverting away from Author2's argument.


----------
D1
----------
Author1 mentions ATF unconstitutionally seizing records from gun sales and withholding information from Congress during their Fast & Furious initiative over the past several years. They illegally removed all sales records from gun shops, which has been illegal since 1968. They've operated independently under the radar of Congress for years and disregarded constitutional rights and limitations. The ATF should be prosecuted and acknowledged as criminals. Author2 is submitting misguided arguments, without resources or links, based on no research or education of subject matter. 

Author2 feels Author1 is inflating the magnitude of the ATF actions intentionally. The ATF was only requesting legal sales information from gun shops in order to trace the sale and activity of firearms. Author1's own cited references clearly state they requested "copies" not all records as Author1 is claiming. The congressman (Rheberg) Author1 is quoting is a staunch pro-gun supporter. He's not a valid source of  factual unbiased information as Author1 claims. Author1 has provided no solid resources or proof to substantiate his/her ability to weigh in so heavily on this.


----------
D2
----------
Author1 reports a headline about a representative questioning the ATF concerning AK records checks, and contains a byline about Holder being charged with contempt. He says they aren't really tracing guns, they're trying to get a hold of the dealers records. He says the only reason they could be doing that is to start a registration, and that's illegal. He says they should be trying to trace the guns used in the gun smuggling done by the ATF.  He says taking records is a crime. He says he's posted statutes of the laws. He states the law says all approval through the NCIS check must be destroyed by 24 hours later, and there aren't supposed to be permanent records kept except in gun shops.

Author2 post a link to ATF rules about having to respond to a request to trace a firearm. Author2 argues they only want copies, not the actual records. He says that is not confiscation. He says the law says requested copies are mandatory. He denies Author1 posted statutes proving anything.


----------
D3
----------
Author1 argues that the Alcohol Tobacco and Firearms Bureau (ATF) is breaking the constitution and the law. The ATF is requesting records of who owns what guns, which is used to create a registry of gun owners, which is illegal since 1968. This information gathering is not supposed to be carried out by any party, and has been ruled unconstitutional. Gun sale records are not supposed to leave the gun shop owner's possession. It is a federal crime to request records. The Congress has made it illegal to do so, and is requesting the ATF's records. 
Author2 argues that it is not illegal for the ATF to request copies of records. It is the law that the shop owners must comply with requests to trace firearms. The ATF only wants copies of the records not the original ledgers. It is not a crime, federal or otherwise, for the ATF to request copies of records. The Constitution does not apply to private companies, only the federal government.


----------
D4
----------
Author1 quotes a headline about about AK records checks and thinks illegal, unlawful and unconstitutional acts are still ongoing. Author2 thinks this is right-wing conspiracy and asks what is his responsibility when asked to trace a gun. Author1 says they aren't tracing guns, they are trying to seize the entire dealers ' records. Bound volumes always stay with the dealer and never leave the shop. They can be examined on premises but cannot be taken from the shop under any circumstance and is a violation of law. Author2 disagrees with Author1's assertions. Author1 posts a link about atf's records they are illegally withholding from congress. Author2 thinks this is more nonsense. Author1 thinks that Author2 is defending unconstitutional acts. Author2 asks how asking for copies turns into an illegal confiscation of bound ledgers. Author1 says it's in contempt of congress. Author2 says no such statues have been quoted. Author1 says he quoted congress. Author2 says the ATF asked for copies, not the volumes, and the sellers are bound by law to comply. Author1 doesn't agree.

