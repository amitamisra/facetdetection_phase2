
----------
D0
----------
Author1 says injuries are reduced by regulation. He says no one said anything about if guns didn't exist except Author2. He accuses Author2 of misstating what other people say.

Author2 says saying if something didn't exist isn't realistic. Guns are well made and last a long time. He ask what Author1 means by "sufficient regulation." He says regulations haven't stopped school shootings, the only thing that has is when the shooter is killed or injured. He was simply pointing out how untrue it would be to imagine if guns didn't exist. He says Author1 DID make a reference to guns not existing when he said if it was like in Australia and the UK where the killer couldn't have bought a gun, the 32 victims would be safe and alive. He says this proves Author2 thinks there should be a total ban on handguns like in the UK. He says that would only be possible if all handguns were destroyed. He says the system failed because the person wasn't entered in the system.


----------
D1
----------
1 believes that the point is not that some people are dangerously mentally ill, but that guns should be regulated sufficiently enough to minimize gun fatalities. He does not think guns will someday be made totally unavailable or that better guns will someday be made that have been designed to be less deadly. He does not think guns will someday cease to exist. He simply believes that better regulation is most likely to lead to less gun violence and premature death.  Author2 believes that guns will always be available, just as they are now. Sufficient regulations are all very well, but when someone is actually shooting innocent people, the only thing that will solve the problem is another shooter to bring the first one down. Regulations have not stopped active school shooters. On occasion though, shooting them has. That has sometimes minimized shooting fatalities. People of all sorts will always be able to get guns, unless every single gun is somehow destroyed or taken out of circulation somehow. That is extremely unlikely to happen.


----------
D2
----------
Author1 states that lethal lead patterns are minimized by sufficient regulation, but Author2 tells Author1 that modern guns are of superior quality. He believes that guns will always exist, and Author2 wants Author1 to define sufficient regulation. Author1 refutes by stating that the quality of manufacture does not lessen the lethality of a gun. Author2 stresses his weariness of the argument that people would be safer if guns did not exist, and he gives various examples of dangerous things that exist. Author2 wonders how Author1 came to the conclusion that 32 people would have been saved if their killer did not have gun access. Author1 mentioned the UK where nobody can own a handgun, so Author2 wonders if Author1 wants guns to not exist at all. The killer got access because he was not in the system, meaning that he could pass any background check like most Americans. He believes that unless Author1 thinks the system should be reformed, then he must want to deny people their right to own guns in the first place.


----------
D3
----------
Author1 says the point isn't sickness patterns, but lethal lead patterns, which occurrence is minimized by regulation. Author2 thinks Author1's party keeps diverting off the side and confusing the issue. He thinks the point is the claim that if this didn't exist it doesn't work in the real world and should be abandoned. They shouldn't whine about guns because they are going to continue to exist and may work for hundreds of years. Furthermore, no school shooting has been stopped by regulation, only when the shooter has been killed or incapacitated. Author1 thinks Author2's version of reality must be different and asks where the if guns didn't exist claim was made. Author2 says he's pointing out the flaws of the argument and compares guns to money. If money didn't exist then no one would be poor. Author1 asks why he keeps repeating it and unlike Author2, he understands the meanings of the words he uses. Author2 quotes Author1 if killers didn't have guns people would be safe. That means destroying guns unless he's talking reform.


----------
D4
----------
Author1 argues that deaths can be reduced by regulation of guns. Guns do not become less lethal when they are not made as well as other guns. Author1 argues that regulations of guns will decrease violence and that more gun regulation is necessary. School shootings would happen less often if there were sufficient regulation of guns. 

Author2 argues that guns will always exist, because old guns are still around and work just fine. Modern guns are made of higher quality materials and will still be around many more years from now. It is impossible to remove all guns from society. Gun regulation should not be increased. Gun violence has not decreased due to regulations, but rather guns have helped to end school shootings while they are in progress. Since guns do exist, we should not be worried about hypothetical situations which will not occur, and should instead provide guns to as many people as possible to protect ourselves. A total ban on guns is impossible in the US.

