
----------
D0
----------
Author1 asks what part of the Brady II authorizes unannounced searches. Author2 cites section 203. Author1 says the "at any time" section only applies to guns used in a criminal investigation, so this scenario is nothing but a scare tactic. Author2 asks if Author1 missed the part that says "or". He says there are no scare tactics involved. Author1 replies that he didn't miss anything and quotes the section following the "or". He then asks Author2 when most inspections take place as he thinks it is during business hours. Author2 replies Author1 keeps skipping over the part that says or any time they investigate. He thinks this means at any hours and not just during normal business hours. Author1 says that this still applies to criminal investigations, so it is still a scare tactic. Author2 thinks Author1 is being obtuse on purpose and he says they may investigate up to three times per year. Furthermore, it doesn't state only during business hours, so it could be done at any time. He doesn't trust the ATF.


----------
D1
----------
Author1 argues that firearms inspections are only allowed to be investigated at any time, only when the gun is involved in a criminal investigation. Guns are not allowed to be requested to be searched at any time when they are not related to a criminal investigation. When not related to a criminal investigation, searches are only legal not more than once in any 12 month period. The only exception is that records can be searched three times each year. If they are investigating a firearm in a crime that belongs to a person, they can still investigate someone, but there is a likelihood that it would not be in the middle of the night. 
Author2 argues that they added two additional chances for legal unannounced searches in each 12 month period, and they also are now allowed to search at any time in regard to a firearm in a criminal investigation. They will not announce their searches. It is fair game for any searches because it is not stated otherwise.


----------
D2
----------
Author1 asks where the authorization of "unannounced searches" of the holders of arsenal license comes from. Upon learning that the "at anytime" pertains to the records relating to a firearm involved in a criminal investigation and is traced back to the owner of the  arsenal license. He claims that Author2 is just trying to scare people.  
Author2 disagrees and says that he is not trying to scare anyone and that Author1 must think that the Bureau of Alcohol, Tobacco, and Firearms send out written invitations when the want to come by and inspect. Author1 says he didn't think that - but on the other hand - the BATF doesn't just show up in the middle of the night, right? Author2 tries to set Author1 straight and explains that when they are investigating a firearm the BATF is not limited to the stores hours, but that they can show up anytime they want up to 3 times in a year.  He goes on to say that not all the agents are "nice guys".


----------
D3
----------
Author1 wants Author2 to cite the portion of the Brady II that would authorize these unannounced searches, and Author2 obliges him by quoting a section. It deals with the arsenal license that requires its holders to adhere to all obligations and requirements. Author1 thanks Author2 for the quote, and then proceeds to quote another section. Author1 emphasizes the portion that mentions a firearm involved in a criminal investigation, then he states his belief that it is mentioned to cause fear. Author2 argues that the BATFE's track record proves that there is no scare mongering, then Author1 tells Author2 that most gun dealers do not have their licenses inspected at midnight. Author2 believes that a gun dealer can be investigated outside of business hours. Author1 agrees that happens when they are investigating the owner of a firearm used in a crime, but Author2 states that gun dealers can be inspected three times a year. It does not state only during business hours, and then Author2 asserts that it is well over three visits in criminal investigations.


----------
D4
----------
Author1 ask what part of the Brady law covers unannounced searches. He points out the section of the code saying "at any time" only pertains to when the gun is involved in a criminal investigation. He says gun dealers records are rarely searched in the middle of the night. He says after hours inspections would only be during an investigation. 


Author2 cites US code Section 923 of title 18 covering Arsenal License. It says the licensee is subject to all obligations and requirements pertaining to dealers. It was changed from once every 12 months to 3 times every 12 months, or any time related to a firearm involved in a criminal investigation. He notes the "or at any time" part of the code, and mocks they make convenient appointments. He implies the "or any time during an investigation" means they could just say they were doing an investigation if they wanted to search more than 3 times a year. He says Author1 should know police aren't restricted to investigating only during business hours.

