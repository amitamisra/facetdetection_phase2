
----------
D0
----------
Author1 says we've never banned ownership of weapons. It is being suggested that we consider this in the article. He thinks Author2 has reading comprehension difficulties. If a law doesn't seem to produce the intended effects then no law will. Author2 thinks Author1 is showing ignorance. California has its own AWB. The state has forced licensed owners of specific firearms to turn them in for destruction even though the specific weapons weren't illegal under the law. He's showing how his argument can be interpreted in another way. If current laws aren't being enforced then new ones won't be either. Author1 says at the time the Attorney General assured gun owners that rifles were legal and not subject to the confiscation order. A man owning one was arrested for possessing an illegal firearm. The Supreme Court said the rifles are illegal. Author2 thinks California law is more reason to prevent such an occurrence from happening. Author1 asks how authorities would find out the weapons were turned over. Author2 says it could be traced back to the buyers.


----------
D1
----------
Author1 tells Author2 that the ownership of certain weapons has never been banned. Author2 replies that California has forced licensed owners of specific firearms to turn them in for destruction. Author1 talks about converted SKS rifles. The owners were told they were not subject to the gun confiscation order. A converted rifle owner was arrested in Santa Clara for possessing an illegal firearm which led to his prosecution. Author1 then tells Author2 about the case reaching the Supreme Court. They ruled that the rifle was illegal. Laws that attempt to address straw purchases are unenforceable. Author2 tells Author1 that it is impossible to prove that an SKS was converted. He gives him examples of how difficult it is to catch the straw purchasers. Author1 ponders the sense of confiscating an SKS since it easily converts back to stock format. Author1 conveys to Author2 his belief that filling out a form does not combat the problem. Author2 then tells Author1 that the way to combat the problem is to aggressively prosecute people who buy illegal guns.


----------
D2
----------
*Author1 states that the US has never banned automatic weapons despite what individual states have decided. This is no threat to the Constitutional rights to RKBA. State-level decisions are specifically protected by the Constitution. There'd be no logical reason to confiscate SKS rifles if they could be converted back to stock settings. 

Author2 states that California has its own automatic weapon ban in place forcing licensed owners to turn over any such firearms. It's impossible to prove an SKS was converted to accept detachable magazines if it's easily converted back to stock settings. If a banning can happen in California it would be possible for it to take hold in other states or even at Federal levels. It's impossible to pin illegal firearm possession on someone unless all straw purchases are monitored. 

*This discussion is referring to a 1996 case where a man was told his converted rifle was legal by the then Attorney General. The man was later arrested for the same firearm, stating it was indeed illegal. SCOTUS then officially determined its illegality.


----------
D3
----------
Author1 argues that certain laws are slow to produce effects. Some law will produce the desired effect however. California bans specific types of weapons. California bans weapons which can be converted into banned weapons. All guns have never been banned. Straw purchase laws are impossible to enforce. Straw purchases are when one person buys a gun for another. The California Supreme Court ruled convertible guns are illegal weapons too. Straw purchase laws should not exist. There is always an excuse to avoid straw purchase laws. 
Author2 argues that California broke its promise. It previously said certain guns were legal and now illegal. Current laws are not being enforced correctly. New laws will not be enforced correctly. Bulk purchases of weapons are monitored. Liberal courts do not combat crime. Strict sentencing would reduce crime. Punishing people who buy from straw purchasers is good. Guns can be traced back to the buyer. The buyer might snitch on the later purchaser. Harsh sentencing would stop illegal straw sales of guns.


----------
D4
----------
Author1 says the article is just suggesting banning these weapons. Author2 argues that California has an automatic weapon ban. He says owners were forced to turn them in. These weren't illegal weapons to buy in the country. He says current laws are not obeyed. New ones wouldn't be either. Author1 refers to a case involving a gun that was banned. It was able to be converted. The attorney General said at the time they were not illegal. A man was later arrested for owning one. The Supreme Court ruled the gun was illegal. Author2 says that is what he was referring to. He says it is necessary to prevent this happening to others.Guns were banned and confiscated in California. Thus it could happen anywhere. He says it is difficult to catch criminals breaking the laws. People buy guns for others. This makes it difficult to catch law-breakers. He says the courts should be stricter with punishment when they caught people breaking laws. Stricter punishment might stop people from breaking laws.

