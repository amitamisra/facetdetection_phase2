
----------
D0
----------
Author1 says Author2 is incorrect. The government is limited by the Constitution and Bill of Rights. It has been given concessions. He says the founding fathers were warning about people like Author2. They think they can limit people's rights. He states 44 states have the right to bear arms in their state Bill of Rights. The state and federal rights were the same. He thinks the bill was necessary. That's  why the founders wouldn't sign the constitution without it. 

Author2 says he has shown proof that the Bill of Rights is only to restrict the government. He says Author1 doesn't understand his own quote. He quoted they would contain various exceptions to powers that were not granted. He says the constitution granted power to the federal government. The other powers were left to the states. He says the quote is supposed to show the feds did not have the power to do the things in the Bill of Rights. He says is dangerous and unnecessary for them to have included the bill.


----------
D1
----------
Author1 thinks government is restrained by the Constitution and it's Bill of Rights. He thinks Author2 acts like the 2nd Amendment gives him the right to all of the gun control he wants. Author2 says he's cited evidence the Bill of Rights is only a restriction on the Federal government. The Constitution only grants power to the Federal government. The other powers are left to the States or the people. Author1 replies states have their own Bill of Rights and there are currently 27 amendments limiting government's power. Author1 says states have their own Bill of Rights. 44 of them currently have the Right to Keep and Bear Arms. Also, there are currently 27 Amendments limiting government's power. Author2 says before the 14th Amendment it's untrue that rights listed in the Bill of Rights applied to the state. Author1 thinks that both the State and the Federal protected rights were the same. Author2 questions all states having the same constitutions that match the Federal Constitution. Author1 says state and federal protected rights are the same.


----------
D2
----------
Author1 believes the Bill of Rights is the basis of all laws.  Each state has its own bill of rights.  In the United States, 44 states support the right to keep and bear arms.  Each state protects the same rights the federal government does.  The 2nd Amendment is used as a blanket for gun control.  The Bill of Rights is set forth for the people.  The Bill of Rights limits the government's control.  Without the Constitution and the Bill of Rights, there would be no rights for the people.  

Author2 believes the Bill of Rights is unnecessary and dangerous to include in gun control discussions.  The Bill of  Rights was not created for the states, it was for the federal government.  State and federal protected rights are not one in the same.  State constitutions do not run hand in hand with the Federal Constitution.  Author2 Everything prior to the Fourteenth Amendment is not needed.  Author2 wants evidence to back up claims.


----------
D3
----------
Author1 argues the Bill of Rights restrains the Government. People are protected from the government by the Constitution. The Second Amendment should still be true. People's rights are being infringed upon. The right to own guns should not be regulated. Many States have their own Bills of Rights. Many states also guarantee gun ownership. The federal government is breaking its restrictions. The government's power should be limited. Gun regulation should not be increased. There are almost 30 amendments meant to limit the government. The people's rights should be protected more.
 
Author2 argues the Constitution didn't always apply to the states. The 14th amendment made the Constitution apply to the states. The Federal government has exceptions in the Bill of Rights. Each state's constitution is different from the federal constitution. Regulation of guns is not a bad thing. Each state does not protect the same rights. The commerce clause gave the government a lot of power. The Bill of Rights limited our rights, not the governments.


----------
D4
----------
Author1 states that all of government is restrained by the Constitution and Bill of Rights. Despite government infringement on Constitutional rights, the 2nd Amendment isn't completely open for restriction and limitations. 44 states have established the Right to Keep and Bear Arms. This is a result of the 2nd Amendment and the Bill of Rights in general. There are 27 Amendments limiting government's power. The Bill of Rights is essential. 

Author2 states that the 14th Amendment provides for 2nd Amendment limits to be established. Sources have stated the purpose of the Bill of Rights is as a restriction on the Federal government. Author1's own reference to the Federalist No. 84 document dismisses the Bill of Rights as being too vague. It's left vulnerable to many additions and alterations. The Constitution granted powers to the Federal government. All other rights were to be determined by the states and by the people. The Bill of Rights is unnecessary and dangerous to include. Author1 is citing unrelated articles and information not backed up by facts or resources.

