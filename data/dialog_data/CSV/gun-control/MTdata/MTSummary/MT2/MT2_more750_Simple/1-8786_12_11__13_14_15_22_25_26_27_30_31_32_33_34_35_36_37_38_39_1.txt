
----------
D0
----------
Author1 is discussing firearms registration in other countries. He doesn't think registration has led to confiscation. He says the Constitution should be interpreted in a contemporary way. He says the authors wrote it a long time ago. They couldn't know how things would be in the future. He says he doesn't know Gore's position. He ask why Author2 wants to control others behavior. The behavior is not criminal. He thinks morals are relative. He says some killings are allowed by society. This shows morals are relative. Not all killing is considered murder. He says killing during self defense or war is okay.

Author2 mentions NY and CA state constitutions. There is also the right to a state militia. He mentions Al Gore and the "living constitution." He says the authors of the constitution did make a way it could be changed. They must have known the future could be different. He says some morals like murder are absolute. He says killing someone in your own group is murder.


----------
D1
----------
Author1 believes the Constitution must be interpreted in light of contemporary realities.  The forefathers could not have foreseen contemporary circumstances.  There is no reason to think that registration would lead to confiscation of guns in the US, where the people's right to bear arms is in the constitution.  Author1 asks why others feel the need to control others, when no crime has been committed?  Morals are relative.  Murder is immoral by definition, but whether a death is murder is culturally relative.

Author2 says that the forefathers did not foresee moral relativism.  The forefathers would not agree that the constitution is a "living document," open to reinterpretation.  The forefathers were wise enough to include a mechanism for changing the constitution.  Author2 doesn't seek to control anyone but himself, unless a crime is committed.  Killing someone within one's group is always murder, and is an example of a moral absolute.  Registration of weapons leads to their confiscation.  This has happened in California and New York, despite RKBA.


----------
D2
----------
Author1 asks which countries that confiscated firearms post registration had the people's RKBA in their constitutions. Author2 replies to look at the NY and CA constitutions. Author1 sees no reason to assume registration leads to confiscation. Author2 thinks he is ignoring the facts. Author1 supports interpretation of the Constitution and thinks that morals are relative. Author2 has no need to control anyone beyond himself. He says murder crosses all cultures. Author1 says murder is killing without society's sanction. It exists in all cultures. When society sanctions killing varies. 
Author2 says he just demonstrated a moral absolute. Author1 thinks killing isn't always murder. Murder is the killing of one's own in group. Author1 asks if he kills someone who is about to kill someone else, has he murdered. Author2 replies the killing of one's family or clan is murder and crosses all cultures. The killing of the out group is not necessarily murder. Author1 says he never claimed otherwise. He simply thinks we aren't all intellectual equals. Author1 and Author2 trade insults but later apologize for it.


----------
D3
----------
Author1 argues that certain countries restrict firearm rights. Those countries never protect the citizens rights to keep and bear arms. Gun registrations will not lead to gun confiscation. There is no precedent for that transition. Gun registration cannot be compared to Hitler's Germany. There is no reason to control other people's behavior. When no crimes are committed people should not be controlled. Morals are relative ideas. There is no definite set moral idea. Murder is illegal. There is some killing that is not murder. Each society has different definitions of murder. That shows that morals are relative. Individual morals and religion do not matter. Law is all that matters when debating. Not all people are intellectually equal. People should not hold grudges.
Author2 argues that states are moving towards registration. That registration has moved towards restriction. The Constitution was made to change. The founding father did not predict our modern problems. Regulations should be put in place after crimes. Murder is the same as killing. Killing is always morally wrong.


----------
D4
----------
Author1 begins by talking about required registration of firearms. He says that it doesn't mean the firearms will be confiscated by the government. He says that confiscation has only occurred in countries that do not have the right to keep and bear arms in their constitution. He challenges Author2 to prove him wrong. Author2 cites the New York and California state constitutions. Author1 does not accept these as valid examples. Author2 then changes the topic and claims that murder is always wrong. Author1 says that is not true. He gives an example of killing someone who is about to kill someone else. He says that society would sanction the murder in that situation. Author2 agrees that some murders might be acceptable, but not if the person is part of your ingroup. The discussion then turns into personal attacks. Author2 claims to be intellectually superior and more educated than Author1. Author1 responds by pointing out spelling errors in Author2's comments. Author1 says that name calling does not help their debate. Author2 apologizes. Author1 accepts the apology.

