
----------
D0
----------
Author1 states that if the Obama administration is altering the Constitution to restrict firearms, they should back it with viable proof of its effectiveness in curtailing crime statistics. These rights were hard-won. Studies in England and Australia have already proven this is not the case. If gun control doesn't actually reduce crime rates, what is its purpose? Lack of gun availability or increased cost will not deter criminals. Criminal behavior is not so predictable. (Author1 cites two examples of kids being caught with/in the presence of military-grade firearms).

Author2 states that the goal of firearm purchase and import restrictions is "to reduce the supply of firearms available to criminals". Also to identify negligence and criminal behavior in gun owners that is threatening "the general welfare of their fellow citizens". It is a focus on supply and demand. Illegal weapon prices will skyrocket making it much more difficult to procure. Author1 is saying "if a law is broken, it's ineffective and pointless"? No laws go unbroken. This fact doesn't undermine the necessity or effectiveness of laws.


----------
D1
----------
Author1 doesn't want to control guns. Gun control hurts citizens. Criminals will still get guns no matter what. Criminals can pay higher prices for guns. They will be able to hurt citizens easier. Citizens should be able to defend themselves. English and Australian gun control is bad. There are still guns in those places. Guns have been stolen in England. Gangs still kill people with illegal guns. We should not give up our rights to guns. The laws in the UK do not work. Higher prices does not stop crime. 

Author2 argues gun control is good. The UK laws have reduced the number of handguns. Guns are more expensive in the UK. The number of guns on the street is down. Fewer guns is good for the people. The law can be broken. It can still be a good law. Gun control is worth investigating. The UK may take time to reduce violence further. Gun control can help protect the citizens. We should consider removing the second amendment.


----------
D2
----------
Author1 tells Author2 that we should not give up our constitutional rights to suit anti-gun hopes. We need proof that these infringements work. England and Australia both implemented gun control schemes that did not work. Author1 wonders why gun control is needed if controlling guns does not seem to control crime. Author2 tells Author1 that the purpose is to reduce the supply of firearms that is available to criminals. They need to identify negligence on the part of gun owners.  Author1 reads from an article that talks about a schoolgirl that got arrested for having a sub-machine gun in her bedroom. Author2 tells Author1 that the police are testing the weapon to see if it is a replica. Author2 reads articles to Author1 about schoolchildren getting arrested for possessing weapons. Author1 then reciprocates by reading to Author2. Author2 asks Author1 if his point is that broken laws are ineffective and pointless. Author1 states that Author2 needs to prove that it is right to ask Americans to give up their constitutional right to own a gun.


----------
D3
----------
Author1 thinks most Americans would agree before we continue to burden our Constitution with infringements there should be evidence that the infringements would actually reduce violent outcomes. Before demanding that we give up Constitutional Rights they should show us that the schemes would actually work. All that Author2 and Obama want has been tried in the two countries most like ours. The effects were low. In England weapons sell for 50. The victims are younger. A study suggests a weapons culture. Author2 cites reducing the supply of guns to criminals. He also cites identifying negligence in gun owners. He says a 9 mm handgun sells for 1,000 to 1,400 on the black market in Britain. Author1 says that's way out of a criminal's price range. He talks about a 13 year old arrested with a sub-machine gun. Author1 talks about other cases in response. Author2 asks if Author1 thinks because a law is broken it isn't effective. Author1 says that you should prove the worth of Constitutional infringements before asking Americans to give up rights.


----------
D4
----------
Author1 says it should be determined if gun controls are effective before infringing on the Constitution. He cites two other countries who have gun control laws. He says guns are still cheap there for criminals. He argues gun control does not reduce crime. Author2 says the purpose is to reduce the supply. He cites higher prices for guns. Author1 mocks that money would be an issue for criminals. He cites a case where a 13 year old had a sub machine gun. Author2 says the gun was a replica. Police are still examining if it fired bullets. Author1 says another case said weapons preferred by gang members were stolen from the military. He cites a report of a 15 year old killed by a machine gun. Author2 says no laws are capable of preventing people from breaking them. Author1 argues this is  taking away people's rights and property. It infringes on the Constitution. There should be proof of good benefits. He says Americans have fought and given their lives to protect those rights.

