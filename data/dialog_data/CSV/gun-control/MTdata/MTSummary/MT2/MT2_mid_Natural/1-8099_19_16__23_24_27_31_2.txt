
----------
D0
----------
Author1 asks if allowing guns on a school campus is a good idea. Author2 Think it is, more or less, he says to think about shooting rage and asks if there has ever been a mass killing due to shooting rage on armed patrons. Author1 questions if he should reply but says there is a difference between schools and gun clubs because not everyone in a school will be responsible, but gun club members would be. Author2 says Author1 isn't the one with the states. Author1 thinks Author2 isn't grasping the point even though it has been laid out in front of him. Author2 thinks that challenging someone's evidence is like spitting in a river unless you provide evidence of your own to support it.


----------
D1
----------
Author1 and Author2 are arguing about whether or not it would be a good idea for people to be allowed to carry weapons on school grounds. Author1 believes it might be dangerous if people are armed because there are untrained people there, including children, who could get access to the guns. Not everyone at the school would necessarily be trained in the use of firearms. Author2 disagrees, comparing it to a shooting range where there are many armed people. Author2 challenges Author1 to name an instance where there was a mass shooting at a gun range. Author1 counters by saying the two situations aren't comparable because unlike a school, a shooting range has only people trained in how to safely use guns.


----------
D2
----------
Author1 questions whether or not allowing guns on school campuses is a good idea. Author2's referencing the unlikelihood of a shooting range being the site of gun violence in response to guns on school campuses preventing gun violence in the same way is completely unrelated. Gun club members are responsible around firearms. Every person in a school will not necessarily be as experienced. Author2 is challenging Author1's statements just to be argumentative and is providing no legitimate input to the conversation. 

Author2 mentions the lack of mass shootings or killings at shooting ranges. A room full of fully-armed citizens is not likely to be the site of gun violence. Allowing guns on school campuses would have the same effect. Author1 provides no statistics validating position.


----------
D3
----------
Author1 asks Author2 if allowing guns on campus is a good idea. Author2 says yes. Author2 says there hasn't been a mass shooting in places people are armed. Author1 says a school is not a gun club, and that responsibility plans an issue. Author1 suggests Author2 doesn't understand the difference. Author2 says Author1 has no stats. Author1 says Author2 is missing the point of the conversation. Author2 says that Author1 has no evidence, and suggests their argument is futile as a result.


----------
D4
----------
Author1 asks Author2 whether allowing guns on a school campus is in any way shape or form a good idea or not. Author2 answers that it is more or less, if you think of a shooting range. He asks Author1 to name a situation where there was a mass killing on a shooting range full of armed patrons. Author1 believes Author2 deserves no response, but he clarifies that talking about a school is much different than talking about a gun club. Everyone in a gun club will be responsible around firearms, but not every member of a school will be. Author2 tells Author1 that he is the one with statistics, but Author1 insults him by saying that Author2 cannot grasp the point of a conversation.

