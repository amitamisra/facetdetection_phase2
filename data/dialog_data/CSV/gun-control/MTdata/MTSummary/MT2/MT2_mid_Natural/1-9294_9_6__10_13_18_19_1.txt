
----------
D0
----------
Author1 states criminals don't obey laws. He says a record of private party sales should be kept with a FFl doing a background check. He says the manufacturer could tell law enforcement where to look for an owner. Author2 debunks this idea. He says manufacturers do not know who guns are sold to. They sell to a distributor, who sells then to licensed sellers. He says this does not show responsible gun owners are the problem. Author1 is confused and ask if law enforcement finds the owner through the distributor assisted by the manufacturer. Author2 says no. The manufacturer can only tell them the distributor, who can tell them which FFL holder or retail store it went to.


----------
D1
----------
Author1 asks Author2 to let them speak for themselves. They say that of course criminals don't obey laws and that if private gun sales were recorded there could be data available to law enforcement. Author1 talks about the manufacturer being involved with law enforcement. Author2 says Author1 is losing sight of something with their ideas. They say the manufacturer wouldn't have to get involved with the law. They ask if manufacturers should keep track of cars. They say manufacturers don't sell to dealers, they sell to distributors. They say law abiders are not the problem. Author1 asks if the law finds the original owner through the dealer. Author2 says the manufacturer only knows where it was shipped. Author1 thanks Author2. Author2 says you're welcome.


----------
D2
----------
Author1 states that trace data in the sale of firearms to a person operating with a Federal Firearm License could be available for law enforcement if needed. This would be legal in and around the confines of the 2nd Amendment and private seller freedoms. Law enforcement would have to rely on information provided them by the last legal source of information, the manufacturer. This could be helpful in solving gun crimes by significantly narrowing the suspect pool. 

Author2 feels Author1's statement is unethically involving the manufacturers of firearms in law enforcement. It'd be similarly ludicrous to investigate major car manufacturers if a vehicle was involved in a vehicular crime. Manufacturers could only link to distributors, not to dealers. This falsely casts doubt on legal firearm owners.


----------
D3
----------
Author1 states that if the record of private party sales is kept with a FFL doing the background check, then trace data is available to law enforcement. The manufacturer has to know where to point law enforcement, since the FFL that corresponds to the serial number would not have a record of the current owner. Author2 tells Author1 that he wants the manufacturer to get involved with the police, but firearm manufacturers do not sell to dealers. The distributors sell to the dealers, then Author1 asks Author2 if law enforcement finds the original owner through the distributor with the manufacturer's help. Author2 answers that the manufacturer leads them to the distributor, then the distributor tells law enforcement which FFL holder has it. Author1 thanks Author2.


----------
D4
----------
Author1 thinks it's a given criminals don't obey laws, but if data of private property sales is kept with a FFL doing the background check trace data could be available to law enforcement.The manufacturer would have to know where to point LE because the FFL corresponding to the serial number would no longer have a record of the owner. Author2 points out because the manufacturer would have to know where to point LE the manufacturer would then be involved in law enforcement. He questions if manufacturers of products involved in drive-bys should work with LE. He says manufacturers don't sell to dealers, distributors do. Author1 asks if LE finds the first owner via the dealer. Author2 replies manufacturers say which distributor they sold it to.

