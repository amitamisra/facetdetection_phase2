
----------
D0
----------
Author1 doesn't think the right to carry a gun is a natural right. It's a granted right. Author2 says that Author1 is forgetting that nobody gave the United States citizens rights, they took the rights. The Bill of Rights is a statement of what those rights are. Author1 thinks that Author2 has a very silly notion of the law. He thinks that Author2 believes in natural laws that aren't natural. One example is laws that allow people to defend the state being used to defend themselves from a mugger. Author2 says the Bill of Rights isn't a contract. The rights are natural rights. Author1 asks how the rights are natural rights if they have been taken. Author2 thinks that Author1 is anti-American with a grudge.


----------
D1
----------
Author1 says guns are a natural right. Guns were meant to protect against the government. Guns were not protected for hunting reasons. The Bill of Rights is not enforceable. The Bill of Rights is ignored when profit is involved. The US beat the British. The US was not taking their rights from the British. The US had already won. Natural rights were not taken from any man. Author2 argued that the Bill of Rights was not a contract. The Bill of Rights is a guarantee that isn't enforceable. The Bill of Rights can never be infringed on. Author2 claims Author1 is anti-American. The Bill of Rights was a good idea. The Bill of Rights is better than Parliament.


----------
D2
----------
Author1 argues that that right to carry weapons specifically guns is not a natural right of American individuals. Author1 states the Constitution grants the right to guns for the states to protect themselves from the attack of tyranny. Author1asserts the language of "shall not be infringed upon," is not literally translated by laws in America in certain situations. Author2 asserts the rights for Americans to own and carry guns is a natural right. Author2 believes the Constitution protects Americans' natural rights to arms rather than grants the rights. Author2 believes the Bill of Rights is not a contract between men. Author2 states America's Constitution uses very specific language meant to endure through any political climate to ensure the individual's rights.


----------
D3
----------
Author1 states one's gun ownership rights is ensured "by the Constitution in defense of....tyranny." A contract cannot be formed based on merely legalizing antecedent self-assured rights. There cannot exist a contract ensuring anything from no specific person/group. Statute laws are unenforceable. Author1 is merely using bits of the Constitution to uphold personal opinions.

Author2 states the Bill of Rights is the legal writeup of rights already claimed by U.S. citizens. Our predetermined "natural rights" are for gun ownership for protection, hunting or personal use. Other countries have established similar "bills of rights". These are only granted deals with authority. Author1 hails from Australia which makes him/her ill equipped to infer true meaning of U.S. legislation. Americans demand freedoms while other countries beg them from authority.


----------
D4
----------
Author1 states the right to carry a gun is not a natural right but a right granted by the constitution. Author1 feels that Author2 lacks understanding of law. Author1 implies that a contract must exist in order that a law can be followed and enforced.  Author1 argues natural rights aren't binding when you try to apply them to both state and private citizens. Author1 feels that Author2 doesn't understand the history as well as the application of 2A law and the Bill of Rights.

Author2 reminds Author1 that the US made their own system of rights. Author2 believes the 2A and the US bill of rights serves every citizen of the US which is the exact opposite of the Bill of Rights in other countries. Author2 states the Bill of Rights in other countries is a contract with authority.    Author2 further argues that the US constitution and Bill of Rights isn't a contract with anyone but a document of protection spelling out the natural rights for all US citizens

