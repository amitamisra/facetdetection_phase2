
----------
D0
----------
Author1 (an American) mentions multiple cases where police/military officers abused their RKBA. They recklessly fired weapons without regard for bystanders. Many innocent victims have been killed by corrupt officers. Australians like Author2 are unprepared/have no solution for preventing police/military officers from going rogue. The government is purchasing the weapons and ammo for these officers, knowing they're being used to kill. Citizens need to equally be able to defend themselves in any situation. Regulations don't keep people from becoming violent. Author2 is resorting to sarcasm instead of offering up a valid response to the topic.  

Author2 is from Australia and claims to not have had major issues with rogue citizens discharging firearms or abusing their RKBA. In the event of such an incident, this would be even more reason for law enforcement to be armed. Self-defense is our ultimate right. This does not necessitate lax firearm ownership. Author1 is assuming American issues are prevalent globally. America's biggest issue is gun-crazy radicals. If this situation arose in Australia, it'd be handled/regulated more efficiently than in America.


----------
D1
----------
Author1 argues that police officers endanger lives. Police shootouts endanger citizens lives. Reckless firing can hurt bystanders. Cops ignore people around them in shootouts. Cops can easily hurt other people. There is no easy solution when dealing with rogue cops. Rogue police and military shooting civilians is bad. Police brutality is too common. Guns are too much of a problem in the government's hands. Control of weapons should be in the people's hands. Police officers should practice better gun control. Police officers should have better gun training. Police officers should not be the only people armed. Rogue police officers cause too much damage. Australia is unprepared for rogue police agents. Government agencies should not kill their citizens.
Author2 argues that citizens shoot and kill each other often. Rogue police officers are rare. If there is a threat people will demand guns. If there is a threat the government will provide guns. The government could provide training, ammo, and guns to all. Most incidents of brutality are started by citizens anyway.


----------
D2
----------
Author1 thinks an officer was threatening the lives of everyone when he was engaged in a shootout. Anybody walking by was at risk of being hit. He asks the solution when members of the military police start killing people. Author2 asks if Author1 has examples of civilians killing each other. Author1 wants Author2 to stay on topic. He wants an answer for what to to about police killing people. Author2 says if it were a problem he would expect to be given guns by the government and training to use it. Australia is socialist. Author1 thinks this means Author2 doesn't have a solution. Author2 jokes that we have a lot to learn from Australia. Author1 says they are talking about cops. Author2 thinks cops should be the only ones with guns because only they can be trusted. He says the argument is good until the cops start shooting people. When they do you realize it isn't a good idea after all. Author2 says they are totally unprepared. All that saves them is the power of prayer.


----------
D3
----------
Author1 is discussing a police shooting. An officer went crazy and was shooting at other cops. Citizens were in danger from stray bullets. He ask how Australians would protect themselves. They can't own guns. What would they do if a cop went crazy? There are lots of examples of cops going crazy. He scolds Author2 for his country being so unprepared. He says only arming people you think are trustworthy is foolish. If they go crazy, you can't defend yourself. You have no gun and they do. He says people who want to murder will murder. No laws would stop them from that. 

Author2 points out citizens go crazy too. He says they don't have that problem over there with police. He makes jokes about what they would do. He says the chances of of that happening are very low. They have only had two incidents. One was a citizen. He says they believe in God and the power of prayer.


----------
D4
----------
Author1 does not feel gun laws should be stricter.  Rogue police officers and military service men and women sometimes cause injury to the general public.  Police brutality incidences have increased.  Sometimes officers shoot family pets, even if they post no danger.  SWAT teams have been found to have left their automatic weapons at crime scenes.  Author1 feels there could be another Port Arthur incident in Australia.  Australia is not prepared for private citizens to be able to defend themselves.  Private citizens have a right to protect themselves and their families.  

Author2 is from Australia.  Australian citizens do not own personal firearms.  He is in favor of stricter gun control.  Port Arthur was started by a "law abiding citizen," until he began shooting.  The chance of another Port Arthur incident is very low.  If there were an incident of rogue officers in Australia, citizens may demand weapons for defense.  God will protect those who believe in Australia and guns are not needed.

