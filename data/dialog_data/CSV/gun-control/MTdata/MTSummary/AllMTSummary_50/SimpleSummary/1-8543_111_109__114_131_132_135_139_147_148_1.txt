
----------
D0
----------
Author1 is against stricter gun laws.  The NRA survey is incorrect in saying gun ownership has declined.  More than one in four households own guns.  Why are criminals not being targeted if studies show who is buying illegal weapons?  Why are they not being caught and brought to justice?  Tightening gun laws will lessen the number of criminals which could be caught via dealers.  If all studies are true, why is this information not being used to catch and prosecute the criminals?  Putting the squeeze on dealers would thwart the efforts of the Alcohol, Tobacco, and Firearms department.  

Author2 is in favor of stricter gun laws.  75% of United States homes do not have firearms.  The black market is the cause for criminals getting guns.  The NRA reports gun ownership is at its lowest in 35 years.  Criminals have others such as sisters or girlfriends purchase guns for them.  Stricter gun laws will cripple the black market dealers.  Criminals buy guns.


----------
D1
----------
Author2 says 75% of households don't own guns. He says there is a black market that supplies criminals with guns. The black market gets them from assembly lines, stolen guns, and gun dealers. Author1 questions the number of people who own guns. He says he gets his from licensed dealers. He questions if it is legal to buy directly from a factory. He ask Author2 if he can prove dealers sell to criminals. Author2 cites a report that less people own guns. He says only a few dealers are prosecuted every year. He says a large number of guns are trafficked. Author1 says this survey is wrong. He says more people own guns now. He questions whether criminals get guns from dealers. Police would be able to track illegal purchases with dealer's records. Author2 says it is because other people buy guns for criminals. They also get them on the black market. He cites a report of a dealer and gang members who were charged for trafficking guns.


----------
D2
----------
Author1 argues that one third of houses in the US have guns. No guns are bought directly from the factory. Gun store owners buy guns from factories. Gun ownership has been increasing for 35 years. More people own guns since Obama became president. People buy guns from licensed dealers. The few who don't are arrested. The police are not incompetent. There must not be a large black weapon market. The system is working how it should. People get arrested for breaking the law. 
Author2 argues that only one quarter of houses in the US have guns. There is a black market for weapons. The black market is mostly domestically made handguns. Gun ownership has been declining for 35 years. Only a few people are arrested for illegal weapon sales. There is a large number of weapons moved. These weapons are not always found. There are many more who don't get caught. People proxy buy guns for those who can't. This is illegal. Proxy buying is not heavily prosecuted.


----------
D3
----------
Author1 says it's fine for Author2 but what he feels isn't right for the rest of society. Author2 says only 75% of households in the US don't keep firearms. On the black market legally registered weapons are stolen. Licensed dealers more interested in profit than where guns end up also sell guns. Author1 asks where Author2 got his figures. A conservative estimate of possession is 85 million. A liberal estimate suggests over 100 million. Both are higher than only 1 in 4 households having a gun. He is unaware that it is legal to get your guns directly from the factory that made them without going through a middle man. Author2 says they go through middle men. They come from the same assembly line. He cites a survey. Author1 says the survey is wrong. If there are no records then there is no evidence of dealers doing anything wrong. Author2 says there is the stolen market. Author1 says the system is working. People will break laws. They have to break the law to be caught.


----------
D4
----------
Author1 believes Author2 is not the norm of society.He states that gun owners make up 25% to 33% of the population. He only buys guns from licensed dealers. He has never bought guns directly from the factory.  Author1 wants names and businesses as proof that dealers are breaking such laws. Author1 calls Author2 press release proof "wrong".  He says more people own guns now especially since Obama took office. He says the crooked  dealers only make up 1% of all the dealers that have licenses. 
Author2 claims that only 25% own guns. He believes the black market is where most guns end up. He quotes a press release  "gun ownership declines". Author2 believes that the criminals are getting guns by way of their wives. Criminals are getting guns off "the stolen weapons market". He claims the crooked dealers are not being prosecuted.

