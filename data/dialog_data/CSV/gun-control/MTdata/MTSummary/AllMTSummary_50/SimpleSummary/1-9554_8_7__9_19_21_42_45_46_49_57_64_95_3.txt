
----------
D0
----------
Author1 quotes an article. It says groups target illegal sales and gun shows as the problem. Loopholes and poor enforcement provides criminals with guns. Author1 ask how can they prevent illegal sales? Author2 says they should arm citizens. He says gang members don't go to gun shows. Author1 says citizens can sell their guns. It is legal in many states. Criminals buy them from private citizens. They also have their girlfriends buy them at shows. Author2 says he never sees women at gun shows. Author1 says people do buy guns for others. So it is logical girlfriends would buy for criminals. He cites a report. A father bought guns for his two underage sons. Author2 ask what loophole problem are they trying to solve then? Author1 says the problem is a lack of background checks. He says this happens at both shows and private sales. Citizens who sell their guns don't know if the person is legally allowed to buy them.


----------
D1
----------
Author1 feels gun show loopholes are the main issue.  If gun shows are closed down, the loop holes go away.  Guns can be bought at gun shows without background checks.  In most states, people can sell guns to anyone without questions.  If someone can not purchase a firearm for whatever reason, they will having someone else get it for them.  Sometimes guns purchased for someone else are involved in heinous crimes, such as drive by shootings.  A father purchased two handguns for his underage sons which were later used in a drive by.  

Author2 feels closing gun shows would be a mistake.  Gang members do not buy their guns at gun shows.  Not many women attend gun shows.  Law abiding citizens should be able to arm themselves for protection.  Gun shows are not the cause of gun related crimes.  Not all guns are purchased by criminals.  Gun shows are not the only place to buys guns if unable to legally.


----------
D2
----------
Author1 says groups like the Brady Campaign to Prevent Violence have mostly targeted illegal sales and gun-show loopholes as the primary problem in America. They believe it allows criminals to get guns. He asks about the pro-gun response to combat illegal sales. Author2 says to arm law-abiding citizens. Author1 says in most states law-abiding citizens can sell their guns with no questions asked. Author2 asks if Author1 has ever been to a gun show. He says he can count the number of women buying guns on one finger. Author1 says he's never been to a gun show. He thinks straw purchasers buy for others to get around legal requirements. In one case two guns used in a drive-by were purchased by the father. Author2 asks what problem is Author1 trying to solve. Author1 says the lack of background checks. Author2 thinks Author1 is trying to backpeddle about gang bangers getting guns. Gun shows aren't the problem. Author1 asks when he claimed gang bangers were trying to buy guns. Author2 says they refers to gang bangers.


----------
D3
----------
Author1 argues for more gun regulation. Private sales of guns should be regulated. Illegal sales of guns should be stopped. Background checks should be required for private sales. Some people should not own guns. They can get guns by buying from their friends. This is a large loophole in the law. Background checks for buying guns are constitutional. Background checks should be in place for all gun sales. Prohibited persons could be stopped with background checks. Buying guns by proxy should be illegal. "Pro gun" owners do not combat illegal sales of guns. Subsequent gun purchasers should be checked. Fewer guns should be on the streets.

Author2 thinks private gun sales are fine. No background checks are needed. People do not buy guns at gun shows. Gun regulation should be reduced. Fewer background checks are better than more. Private sales are not regulated by law. More people should have guns. Fewer laws get broken when more people have guns. There is no gun show loophole.


----------
D4
----------
Author1 quotes a CSM article: "groups like the Brady Campaign to Prevent Gun Violence have mostly targeted illegal sales and gun-show loopholes as the primary problem in American gun culture. They say loopholes and lax law enforcement allow violent criminals (access) to used, stolen, and inexpensive guns." Criminals send their friends/family to purchase weapons for them (info from "Path of a Bullet", article posted by Dart Center for Journalism and Trauma). This is how they skirt age- or criminal record- limitations. The lack of restrictions on private gun sales in/out of gun sales provides a gaping loophole for unregulated firearm access. Background searches should be mandatory each time a gun is sold. 

Author2 replies to a question Author1 submitted by saying that pro-gun proposals to combat illegal firearm sales in armed law-abiding citizens. Criminals don't send their girlfriends to purchase guns from gun shows as Author1 suggests. Author1 keeps changing his/her statements when challenged. The issues raised by Author1 in the quoted articles aren't factually-based. It's liberal discourse aimed at garnering support for gun-control.

