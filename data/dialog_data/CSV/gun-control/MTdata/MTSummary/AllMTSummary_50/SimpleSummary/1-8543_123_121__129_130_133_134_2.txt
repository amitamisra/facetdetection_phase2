
----------
D0
----------
Author1 says there would be less injuries with regulation. He states Author2 is the only one who said something about guns not existing He accuses him of misstating what other people say.

Author2 argues saying if something didn't exist is unrealistic. Guns are well made and last a long time. He ask Author2 what he means by "sufficient regulation." He says regulations haven't stopped school shootings. The only thing that stops them is when the shooter is killed or injured. He was showing imagining no guns is improbable. He says Author1 made a reference to guns not existing. He said if it was like in Australia and the UK. That means he thinks there should be no guns like in those countries. That would only be possible if all guns were destroyed. He says the system failed because the killer wasn't in the system.


----------
D1
----------
Author1 thinks guns should be regulated well enough to minimize fatalities. Minimizing fatalities is the whole point. The dangerously deranged will not go away. They are not the important factor, though. Some guns will probably remain easy to find. They will probably not become less dangerous. They will certainly not cease to exist. Regulation is the answer. It can reduce gun violence and human violent use of guns. It will not save every life. But sufficient regulation can save lives. Author2 believes people will always be able to get guns. They will stay available forever. Just as available as they are now. Sufficient regulations seem like a fine idea. Yet they will not do the job. When someone is actually shooting, he must be stopped immediately. Someone must bring him down. Regulations cannot stop an active school shooter. Yet a practiced marksman could do it. Shooting the shooter minimizes casualties. All kinds of people will always get guns. Unless every single gun somehow disappears or is made unavailable. That will never happen.


----------
D2
----------
Author1 states that lethal lead patterns are minimized by sufficient regulation. Author2 tells Author1 that modern guns are of superior quality. He believes that guns will always exist. Author2 wants Author1 to define sufficient regulation. Author1 refutes by stating that the quality of manufacture does not lessen the lethality of a gun. Author2 stresses his weariness of the argument that people would be safer if guns did not exist. He gives various examples of dangerous things that exist. Author2 wonders how Author1 reached the conclusion that 32 people would have been saved if their killer did not have gun access. Author1 mentioned the UK where nobody can own a handgun. Author2 wonders if Author1 wants guns to not exist at all. The killer got access because he was never entered into the system. He could pass any background check like most Americans. Author2 hopes that Author1 is talking about reforming the system. He mentions the NICS Improvement Act to illustrate his point. Author2 fears that Author1 really does not want guns to exist at all.


----------
D3
----------
Author1 says the point isn't sickness patterns. It's lethal lead patterns where occurrence is minimized by regulation. Author2 thinks Author1's party keeps diverting off the side and confusing the issue. He thinks the point is the claim that if this didn't exist it doesn't work in the real world and should be abandoned. They shouldn't whine about guns. Guns are going to continue to exist. They may work for hundreds of years. No school shooting has been stopped by regulation. It's stopped when the shooter has been killed or incapacitated. Author1 thinks Author2's version of reality must be different. He asks where the if guns didn't exist claim was made. Author2 says he's pointing out the flaws of the argument. He compares guns to money. If money didn't exist then no one would be poor. Author1 asks why he keeps repeating it. He says unlike Author2 he understands the meanings of the words he uses. Author2 quotes Author1 if killers didn't have guns people would be safe. That means destroying guns unless he's talking reform.


----------
D4
----------
Author1 argues that guns should be regulated. More regulation could save lives. Old guns are still as lethal as new guns. Gun regulation will decrease gun violence. More gun regulation is necessary. Gun regulation would reduce school shootings. More gun regulation is needed. Enough regulation would end gun violence and gun deaths. It is possible to remove guns from society. The US can still be saved with regulation.

Author2 argues that guns will always exist. Old guns from many years ago still work. New guns will last longer. New guns are made of better materials. Guns cannot be removed from society. Gun regulation should not be increased. Gun violence does not decrease due to regulation. Guns stop school shootings while they are in progress. Removing guns would not remove violence. Removing all guns is impossible. Giving more people guns will protect more citizens. A total ban on guns is impossible. All guns would have to be destroyed. It is impossible to find all the guns in the US.

