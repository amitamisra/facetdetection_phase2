
----------
D0
----------
Author1 tells Author2 that the Second Amendment protects a preexisting right. The RKBA used to be connected with the militia service. Author2 wants to know when a physically incapable person that could not serve in the militia was ever barred from owning a firearm. Author2 believes that Author1 thinks regular people should not own firearms if they cannot serve the state. Author1 asks why ownership of weapons by those outside the militia would prove that such ownership is protected by the RKBA. Author2 answers that Author1 has argued that the right to keep and bear arms only applies to the militia. Author2 states that Author1 believes that certain females were barred from owning firearms because it was not in the interest of the government. Author2 asserts that Author1 needs to prove that women and those over 46 were not allowed to be armed. He wants proof that it was because they could not serve in the militia. Author1 clarifies that he never argued that there was a complete ban on firearms used for non-militia purposes.


----------
D1
----------
Author1 believes the RKBA was not for a preexisting right.  The 2nd Amendment was to protect people who were members of a state's militia.  People owning weapons who were not in a militia group were subject to different but reasonable regulations.  The fact that people who are not in a militia are allowed to keep arms does not support the RKBA's being constructed for that purpose.  A militia is for state protection should the need arise. Laws regarding gun control had to be aimed at a legitimate purpose to serve the public.  The second amendment has nothing to do with personal defense.  There's no purpose for regular people to own a gun.  

Author2 believes the RKBA was established to protect the common man and not just militia members.  If a member reached retirement age, were his guns taken?  Could women not bear arms?Show irrefutable proof the RKBA was intended to only protect the militia.  Prove the second amendment was not created for personal defense.


----------
D2
----------
Author1 says Heller found the English Bill of Rights allowed people to keep guns for self defense. A court brief argued he was wrong. Author1 says the right to bear arms was connected with militia. The second amendment wasn't protecting ordinary citizen rights. He says because people owned guns doesn't prove they were protected to do so. He says he wasn't saying there was a ban on people from owning guns.


Author2 says to prove people who were not able to serve in militias were not allowed to own weapons. He says Author1 is arguing that only the militia were protected in owning guns. That means anyone unable to serve in a militia would then be excluded from owning guns. He says people were allowed to own guns. That shows that they were protected to do so. Otherwise people outside of militia members wouldn't have been allowed to. He says this shows Author1's statements that the second amendment doesn't protect citizen rights are false.


----------
D3
----------
Author1 arguees the Magna Carta was approved in 1215. By 1765, people typically held the right to bear arms to be typical. The second amendment is claimed to protect a preexisting right. The right to keep and bear arms is only connected with militia service. The right to keep arms is not a preexisting condition. People misinterpret the second amendment often. There is no ban against owning weapons outside of a militia. There is no guarantee to own guns outside of a militia. The states have the right to regulate gun use. The right to keep and use guns is only protected in militias. 
Author2 argues there is no proof weapons are only guaranteed for milita members. There is no proof typical citizens cannot own guns. The right to keep and bear arms was guaranteed to all people in the second amendment. You do not need to be in a militia. All citizens can own guns because of the second amendment. There is no mass ban on guns outside of militias.


----------
D4
----------
Author1 says Heller explored the rights origins. The 1689 English Bill of Rights protected a right to keep arms for self-defense. Blackstone was able to assert the right to keep and bear arms. The English Historians Brief says that Heller was wrong. The RKBA was connected with militia service. He thinks pointing out that the RKBA is a preexisting right doesn't support Author2's argument.
Author2 asks where it has been established that any person who was incapable of taking part in the militia was barred from owning a firearm. Author1 asks how people owning guns outside the militia would prove it's protected by the RKBA. Author2 says Author1 argues that the right to keep and bear arms exists to guarantee that only the militia may be armed. He should prove his arguement. Author1 replies that a lack of ban on firearms by those outside of the militia does not mean that ownership was considered to be protected. Author2 thinks it does. Author1 says he never said there was a weapons ban for non-military purposes.

