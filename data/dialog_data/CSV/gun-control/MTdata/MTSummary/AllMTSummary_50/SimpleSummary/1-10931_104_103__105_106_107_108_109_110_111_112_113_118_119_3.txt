
----------
D0
----------
Author1 feels the ATF is abusing the system.  Their acts are unconstitutional.  Gun smugglers working for the ATF need to be traced.  Some of the ATF's records are being illegally withheld from Congress.  Congress makes the laws.  The ATF is being charged with contempt of Congress.  Approvals through the NICS check must be destroyed within 24 hours.  No permanent records or registrations will exist anywhere.  Those records exist in gun shops only. Author1 says no links means no proof.  Author1 feels Author2 is not up with current events.  

Author2 protests the ATF's involvement in a conspiracy.  The ATF abides by the laws.  The ATF produced the copies they were asked for.  But not for the original books they were kept in.  Gun sellers are bound by law to provide copies.  The ATF cannot ask for copies.  Author2 feels Author1 is not refuting their proof in this argument. Author2 claims that Author1 keeps diverting from the argument.


----------
D1
----------
Author1 states the ATF has been Unconstitutionally seizing and withholding gun sale records during the course of their Fast & Furious initiative. This seizure of gun sale records has been illegal since 1968. ATF has deliberately acted outside the scope of Congressional law. They are now rightfully being held accountable for having committed unconstitutional breaches. Author2 is submitting arguments based only on vague understanding of this subject matter. Author2 has not provided links, quotes or resources to adequately argue this matter. Author2 has had a history of not doing research into these discussions. 

Author2 feels that Author1 is inflating the magnitude of this matter because of personal bias. Author1's own resources acknowledge that the ATF only collected "copies" of gun sale records. They didn't seize all information as Author1 is claiming. Congressman Rheberg, whom Author1 is quoting, is a staunch pro-gun advocate. He is not a legitimate source of factual information as Author1 states. He/she has provided no solid resources or proof that substantiates his/her ability to weigh in so heavily here as an expert.


----------
D2
----------
Author1 post a headline. A representative is questioning why the ATF is doing record checks. It also says Holder will be charged with contempt. Author1 says they aren't really tracing guns. They want to get the dealers records. He says they're trying to create a registration. That is illegal. He says they should trace the guns the ATF smuggled. He states taking records is a crime. He says he posted statutes of the laws. The law says all approvals through the NCIS check must be destroyed within 24 hours. There are not supposed to be any permanent records. Only gun shops can keep records. 

Author2 post a link to ATF rules. It is about responding to requests to trace a gun. The law says requests are mandatory. He says they only asked for copies. They didn't ask for the actual records. That is not confiscation. He denies Author1 posted statues proving anything.


----------
D3
----------
Author1 argues the ATF is going against the second amendment. The ATF is breaking the constitution. Requesting records of gun owners is illegal. Creating a registry of gun owners is illegal since 1968. No one is supposed to know where all the guns are. The ATF should stop collecting information. The ATF has gone past its limits. The ATF should show congress its records. Gun sale records are not supposed to leave the gun shop. It is a federal crime to request these records. Congress has made it illegal to request records. 

Author2 argues the ATF is not doing anything illegal. The ATF was granted permission to ask for copies of records. The ATF is within its boundaries. Gun shop owners must comply with trace requests. The ATF wants copies of records not the originals. If the ATF wanted the orignals it would be illegal. The ATF has not broken any law. The ATF is not going against the constitution.


----------
D4
----------
Author1 quotes a headline about about AK records checks. He thinks illegal, unlawful and unconstitutional acts are still ongoing. Author2 thinks this is right-wing conspiracy. He asks what is his responsibility when asked to trace a gun. Author1 says they aren't tracing guns. They are trying to seize the entire dealers ' records. Bound volumes always stay with the dealer and never leave the shop. They can be examined on premises but cannot be taken from the shop under any circumstance. This is a violation of law. Author2 disagrees with Author1's assertions. Author1 posts a link about atf's records they are illegally withholding from congress. Author2 thinks this is more nonsense. Author1 thinks that Author2 is defending unconstitutional acts. Author2 asks how asking for copies turns into an illegal confiscation of bound ledgers. Author1 says it's in contempt of congress. Author2 says no such statues have been quoted. Author1 says he quoted congress. Author2 says the ATF asked for copies not the volumes. The sellers are bound by law to comply. Author1 does not agree.

