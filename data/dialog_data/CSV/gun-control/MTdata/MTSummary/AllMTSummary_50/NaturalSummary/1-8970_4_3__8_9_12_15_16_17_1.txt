
----------
D0
----------
Author1 makes the assertion that in Miami an AK-47 can be bought on the street illegally for less than the price of a PlayStation game console. Author2 demands that Author1 provide evidence of this by stating the price of each, and accuses Author1 of trying to derail the discussion of gun regulations with a sensational statement. Author1 counters that Author2 will accuse anyone who disagrees with their position on the issue of gun regulation of lying. Author2 quotes prices of different models of PlayStation and a higher price for the least expensive AK-47 manufactured. Author1 counters by accusing Author2 of making the prices up. Author2 says the gun wouldn't be sold for less than it would cost to buy the guns from the manufacturer.


----------
D1
----------
Author1 makes the claim that in Miami, an AK-47 can be bought on the street for less than the cost of a Sony Playstation.  He  objects to being called a liar by Author2, and defines a lie as a deliberately false statement.  He says his refusal to comply with Author2's demands for proof doesn't make him a liar, and he challenges Author2 to prove that he's wrong.

Author2 calls on Author1 to back up his assertion with evidence, and asks what the exact street price for an AK-47 in Miami is.  He accuses Author1 of being a lying troll, and gives prices for different Playstation models, and for the cheapest AK-47.  He says he doesn't have to prove Author1 is wrong.


----------
D2
----------
Author1 says in Miami a Playstation costs more than an AK 47. Author2 asks exactly what the street price for an AK 47 is in Miami. Author1 replies it's less than a Playstation. Author2 says Author1 has made a claim but isn't backing it up. If Author1 can't give the exact price for an AK 47 then the statement is false and a lie. Author1 says a lie is a deliberate false statement, and his statement isn't false. Author2 provides Playstation prices and the price of the cheapest AK 47 which is higher than a Playstation. Author1 says Author2 is a liar and asks the current prices available on Miami streets. Author2 says Author1 has to prove his statement and he has provided proof already.


----------
D3
----------
Author1 claims that in Miami a PlayStation costs more than an AK-47. Author2 asks Author1 what the street price of an AK-47 is, and that if they don't know they're not contributing. Author1 responds less than a PlayStation. Author2 insists Author1 backs up their claim and calls them a liar. They ask again for the price of an AK-47. Author1 says they're not lying with their assertion and that their claim also isn't false. Author2 lists the prices of various models of PlayStations and says an AK-47 is more expensive. Author1 says they can't back up that claim. Author2 places the burden of proof on Author1 and says their initial statement had no proof and their claims are thus invalid.


----------
D4
----------
Author1 says that an AK-47 costs less than a playstation in Miami. Author2 asks what the cost of an AK-47 is, and says Author1 is making it up. Author1 says he does know the price is less. Author2 says to name the price to prove it. Author1 says accusing him of lying is illogical. He says lying is being deliberately untruthful. His statement has not been shown to be untruthful. Author2 quotes prices of playstations and an AK-47, and says he is wrong. Author1 accuses him of lying about the prices. Author2 says Author1 needs to prove he is right, and does not present any proof of figures. He says Author1 is denying he is wrong even though he is.

