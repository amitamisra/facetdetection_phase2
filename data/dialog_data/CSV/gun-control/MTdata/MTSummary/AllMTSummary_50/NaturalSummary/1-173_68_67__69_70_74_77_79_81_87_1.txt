
----------
D0
----------
Author1 argues that the only reason that second amendment rights can  be removed is by a felony conviction. Other misdemeanors are not enough to remove one's right to own weapons. The states individually can deny permits when there are felony charges or warrants for arrest. When there are open charges against people it is difficult to determine if someone should be allowed to acquire a new weapon. Transactions and ownership are protected by the Constitution, but the Constitution does not guarantee the right to drive, that is reserved for the states. Gun ownership could be considered in a similar way. Driving is licensed and regulated, but there is no right to drive.
Author2 argues that having a gun means that you must have the right to have a gun. The courts have decided, unconstitutionally, that there is no individual federal right to keep a handgun. The second amendment does not protect individual rights. Vehicles are registered, while rights are supposed to be determined to the Constitution. The second amendment isn't what most people think.


----------
D1
----------
Author1 argues only a felony conviction can take away someone's second amendment right, and this was a misdemeanor charge. Author2 states he said it was a misdemeanor, and was pointing out the court didn't recognize an individual right. Author1 argues Love did get to keep her gun. The state is allowed to deny permits if someone has an open arrest record. She had 3 charges, and one was a misdemeanor, but the other two were still open cases. The sheriff stated the other two charges were okay, but the permit was still denied. The court ruled it was justified to deny the permit because of the issues, but they were cleared up and she still had her gun. Author2 argues there is no individual constitutional right to own a gun. She did win the first case, but the appeals court dismissed the case when she filed in district court it had been a violation of the second amendment. She appealed to the federal court, who also disagreed her constitutional right had been violated.


----------
D2
----------
Author1 says only felony convictions take away your 2nd Amendment rights. Author2 replies he tried to make it clear that it was just a misdemeanor so that you couldn't say that the court is just saying that felons can't have guns, and the court did not recognize an individual right to own a gun in this case. Author1 disagrees. He says Love got to keep her gun. The state kept the right to deny permits to felons and when there are open warrants for arrest. Author2 says he never disagreed, but having a gun does not mean she had an individual and constitutional right to have it. The federal appeals court rejected her claim that she had an individual federal constitutional right to a handgun. He compares it to a car. Author1 replies transactions and property ownership is protected under the Constitution. Author2 says the court ruled it wasn't a right. Author1 thinks it can only go one way. Author2 says courts have rejected that the second amendment protects an individual right several times.


----------
D3
----------
Author1 argues with Author2 about the difference between a felony and a misdemeanor, and he explains that felony convictions take away your Second Amendment rights. Author2 and Author1 argue about a case that involved a woman named Love, the state did not give her a gun permit because she had a misdemeanor and two open charges. Author2 explains that Love appealed and argued that she has an individual federal constitutional right to keep and bear a handgun, and Maryland should not infringe upon that right. Author2 states that the federal courts rejected her arguments because she was wrong, and Author1 asserts that rights cannot be denied, regulated, or licensed. Author2 argues that they are talking about what the Second Amendment protects, and he agrees that the court ruled it was not an individual right.  Author1 thinks that he knows how the Supreme Court would decide this case, but Author2 states that the Supreme Court usually lets the rulings of the lower courts stand. They could have reversed rulings involving individual rights, but they have not.


----------
D4
----------
(Author1 & Author2 are talking about a recent court case and its effects on firearm rights)

Author1 states that only felonies affect 2nd Amendment rights. In the court case, the defendant had three charges that amounted to two open charges which led to her being denied a firearm permit. The charges were later cleared but she was still denied. An appeal was filed dictating once charges are cleared, firearm permits cannot be denied. You have the right to purchase a car, not drive it, because driving is not a protected right like the RKBA. The RKBA cannot be regulated because it's a Constitutional right. A jury would agree.

Author2 states having a gun does not mean that right cannot be taken away due to criminal behavior. Firearm use, like driving a car, is a privilege not an untouchable right. Juries have voted against RKBA rights occasionally. If Constitutional rights were violated, why hasn't the Supreme Court stepped in to reverse these verdicts? All rights are given but can be subject to revision or reversal with criminal activity.

