
----------
D0
----------
Author1 states that since the airports are able to carry out security measures such as searches, metal detectors, x-rays and limits on what can be carried on he doesn't see why DC can do the same with its commuters on a daily basis. He goes on asking for proof that gun owners want to repeal the federal age limit that covers the purchase of firearms and that people on the prohibited list to be allowed.  He then asks what evidence there is the no gun law in Great Britain has worked?
Author2 states that because airports are private property they can have that security, for a city the size of DC it would be expensive and impossible. Also would have to convince people of the constitutionality of it. Airports can because of the unfounded fear of death by terrorist.   He says that no one believes that gun control will stop all gun crime but it will slow it down. He does concede that both sides have no actual data for proof.


----------
D1
----------
Author1 believes that security procedures like the ones that work at airports could protect whole cities from guns. These kinds of procedures should be legal for cities too, just as they are for airports. It is essential to take away the guns. The way it is now, criminals, kids, drug addicts, the insane and terrorists go armed. That is wrong. By using the methods that work at the US/Mexican border, it would be possible to keep guns out of D.C. Certainly Gangsters would evade the checkpoints and there would be other problems of course. But there would be many fewer guns, as seems to be the case in Britain.Author2 believes that the logistics would make these checkpoints impossible. Also people might rebel, because they only put up with airport security procedures out of fear of terrorism. Lawyers would have a field day. This massive undertaking wouldn't prevent all gun crime. For example, there would be a gun black market. Alcohol Prohibition kept alcohol out. Yet it made people's lives worse, not better.


----------
D2
----------
Author1 details how airports search people before flights.  He says if they are constitutional for airports to perform, they should be for cities. Author2 says airports are private property. Searching 400,000 commuters in a city would be logistically impossible. Author1 says that saying everyone should be able to have guns is untrue, because there are obvious groups who people do not want to have guns such as felons, terrorists, insane people, etc. Since people are stopped along the Mexican border and searched, he doesn't understand why it can't be done for a city. Author2 says it would have to come from a city budget instead of a federal budget, which is unfeasible. The number of commuters would mean 4.6 people would need to be checked every second. Author1 says the federal government is located in the city. Author2 points out other cities have government located in them without federal funds. He says the benefit of gun control is to reduce the supply, and uses the prohibition of alcohol as an example.


----------
D3
----------
Author1 says nobody can fly without being searched, deprived of all liquid items over three ounces, having their shoes checked and their luggage x-rayed. If we can do that at airports then why can't we implement a similar procedure for an entire city? If such security measures wouldn't pass constitutional muster when it comes to the security of a city, then how can they be constitutional at airports? Author2 says it is because airports are private property, and logistics of scanning over 400,000 commuters every day, plus tourists, etc, is near-impossible. Author1 would like to see proof gun owners have called for the repealing of the age limit that covers firearms, and for the prohibited persons list. Author2 cites the budget and flow. Author1 says local prohibitions reply on honor systems and we know how well that works. Author2 says no-one is claiming gun control will prevent all crime but it will reduce the supply. Author1 asks how it is reduced. Author2 says that prohibition of an item will result in a reduced supply.


----------
D4
----------
Author1 argues that more people should have guns, but not that people who would be a risk or dangerous to others. There should still be some restrictions on who can own a gun, including age, previous crimes, and addictions, but access to guns for the majority of the population should be allowed. Since it is typical for people to be searched at airports when entering or leaving an area, it should also be legal for people to be searched entering or leaving a major city, such as DC. This could protect the safety of many people.
Author2 argues that comparing an airport to a major city is unreasonable. An airport is private property, and each airport deals with many fewer people than all those who commute each and every day. The Federal Government is able to keep up a border patrol because there are fewer people attempting to cross the border, and they have more money. Gun control is a good idea, and limiting guns will reduce, not remove, gun crimes.

