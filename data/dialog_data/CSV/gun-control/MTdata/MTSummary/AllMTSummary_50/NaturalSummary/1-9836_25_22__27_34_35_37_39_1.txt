
----------
D0
----------
Author1 claims that people who kill police officers are commonly using drugs. He demands proof that militias are responsible for any police deaths. Author2 says a militia group in the Midwest planned to kill police, and ask if Author1 is claiming they were on drugs. Author1 argues they didn't commit the crime, but were accused by someone else who had an ulterior motive. He mentions other conspiracies, and claims this is another conspiracy. He says Author2 has not read how accused criminals who killed police blamed drugs for their behavior. Author2 says he never said militia were responsible for police deaths. Author1 says Author2 judges a group guilty before they are convicted. Author2 said he was posting based on what federal prosecutors had said. He accuses Author1 of accusing prosecutors of false reporting of facts. Author1 points out how federal prosecutors have tried to prove false cases. He accuses them of trying to influence opinion. He accuses Author2 of making comments about militias in the same way to influence opinions of them.


----------
D1
----------
Author1 believes that illegal drugs are almost always the root cause when a police officer gets killed by violence. Those who murder policemen do not tend to be the members of militias or adherents of religious cults. Presidents Clinton and Obama have lied about such things in the past to cover their illicit involvement or hide their shady connections. In other cases, people have lied to get funding for their groups. But in reality, there is a strong correlation between drug use and police deaths. It is wrong to blame the militias. Juries are likely to proclaim such victims of the system to be innocent and it is best to wait for their verdict. Author2 believes that a particular militia group did plan to do deadly violence. He does not say they actually killed anyone, but that they planned to do so. He believes he is not the one who sees conspiracies everywhere. He also believes that Federal prosecutors are reliable sources, and that quoting them provides reliable information on the topic under discussion.


----------
D2
----------
Author1 and Author2 are discussing death by perpetrator fatalities of police officers.  Author1 feels most of these incidents stem from drug use, while Author2 believes many of these acts are planned attacks.  Author2 cites an investigation into a Midwest militia group which was alleged to have planned to kill officers.   Author1 feels blaming militia groups is the attempt at a cover up and to draw attention away from Islamic connections.  Author2 advises he did not blame a militia group, but did cite the investigation currently taking place.  Author1 compares this to the Branch Davidian case in which a prosecutor wouldn't allow armed federal investigators in the proceedings.  He advises a person is to be presumed innocent until proven guilty beyond doubt and it is up to a jury to make the decision as to whether or not there is a shadow of a doubt related to guilt.  He advises that shadow must be found by a jury of the person's peers not by a judge or prosecutor.


----------
D3
----------
Author1 argues that the most common reason that police officers die is due to drug use, not anyone who is part of a militia. Terrorism is associated with Islamic backgrounds, even though they are trying to distance themselves. Drugs are almost always mentioned in the police report when officers are killed, across a wide range of cases. Members of militia's are not guilty just because they are part of a militia group. Federal prosecutors and federal agents often lie in court and attempt to get others convicted on false charges. The justice system in the US is based on the concept of innocent until proven guilty. Juries must be used to find the truth in this country.
Author2 argues that there are members of militia groups who have planned to kill police officers that were not on drugs. Conspiracy theories have no place in a discussion of law. Federal prosecutors can provide interesting information, but it is up to an informed jury to make a final decision in court cases.


----------
D4
----------
Author1 says he got one link showing the common thing with those that kill police officers, drugs. He says if Author2 can show an officer killed by militia to do it or get off the point. Author2 asks if members of the militia in the mid-west that planned to kill officers were on drugs and asks if Author1 is defending them. He thinks that Author1 is into conspiracy theories. Author1 says they were accused by an insider and he will wait to see what a jury has to say. He thinks Author2 is making false claims and failed to read the officer death synopsis where drugs are mentioned in almost every death. Author2 says he never claimed police officers were killed by militia group members. Author1 asks why mention militias then. He says Author2 made the claim some were saved from maoist slaughter in India but can't back it up. Author2 thinks Author1 is being hypocritical and his post was based on what federal prosecutors had to say. Author1 says you're innocent until proven guilty.

