
----------
D0
----------
Author1 says that Author2 does not have an idea where a figure they used came from. Author2 calls Obama a liar, and says that we need to seal our borders. Author1 believes Obama was intentionally misleading but not lying and wants to open the border. Author2 says that Author1 would not make the same argument for Bush. Author1 says there is a difference between misspeaking and misleading. Author2 tells Author1 to heed their own advice and asks Author1 to agree that Obama and his administration are liars. Author1 says that a 90 percent figure came from the DEA was for guns that could be traced. Author1 also says that firearms are being transported from the US into Mexico and that 90% of weapons were American.


----------
D1
----------
Author1 argues that the Obama administration has made unintentionally misleading statements that should be analyzed before arguing about them. Author1 wants to remove border controls entirely, instead of sealing off the US-Mexico border. Author1 respects the Obama administration, even if he dislikes the current president, because they are the Commander in Chief of the military forces. Firearms are routinely taken over the border into Mexico, and 90 percent of those firearms were made in the US according to the Obama administration. Author2 wants the border to be closed in order to stop illegal trade, traffic, and immigration. Author2 does not trust the Obama administration, claiming that they have continuously lied in order to protect themselves. Author2 does not trust the government.


----------
D2
----------
Author1 asks if Author2 is acknowledging that we have no idea how many of the 29,000 are of non-US manufacture, and asks Author2 to read slowly. Author2 thinks Author1 should take his own advice and they should agree that the Obama administrations lied, and continues to lie. He thinks we need to seal the border for various reasons. Author1 thinks Obama's use of the word recovered was misleading bit not intentional, and therefore, not a lie. He would end border control. Author2 doesn't think Author1 would make that distinction if it were Bush. Author1 says someone can make a mistake without intending to mislead. He says the ATF tracing center has reported that 90% of guns traced came from various sources within the United States.


----------
D3
----------
Author2 says that the border between the United States and Mexico should be closed to prevent crime such as drug and gun trafficking. He says that the Obama administration has lied and continues to lie about the facts. Author1 defends the President, claiming that his words may have been misleading but were not direct lies. He argues that we have no idea what the actual percentage of recovered weapons originated outside of the United States. He cites the ATFs National Tracing Center which claimed that 90% of the weapons found crossing the border were actually manufactured inside the United States. Author1 also accuses Author2 of disrespecting the Presdient, which he says is inappropriate, especially for a soldier, because the President is the Commander in Chief.


----------
D4
----------
Author1 states no one has any idea what percentage of the 29,000 guns not manufactured by the US come from.  He claims that the President mislead with by using the word "recovered," but it was not the President's intention.  Author1 would not take down all of the border controls. A statement from ATF Assistant Director clarified that 90 percent could be trace, and that firearms are routinely being transported from the U.S into Mexico, in violation of laws from the U.S. and Mexico.  Author2 counters that the Obama administration has lied.  That the United States should seal the borders.  Not only to keep guns and money from going out, but also to stop drugs and law breakers from coming in.

