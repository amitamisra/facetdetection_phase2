
----------
D0
----------
Author1 mocks a training course Author2 took, in which students used fake weapons to simulate real fights.  He disparages Author2 for thinking such training is the same as having real life experience in dangerous situations.  Author1 thinks Author2 is in awe of the course's instructor, who Author1 views as a charlatan. He says he would hope that people training for the military or police force understand the difference between simulated and real life danger.

Author2 is frustrated by Author1's continued mischaracterization of what he (Author2) said about the course and its instructor. He says he never claimed to have trained in real life situations, and explains the context in which the topic had originally come up, which was Doc calling him (Author2) untrained.


----------
D1
----------
Author1 raises doubt about Author2's training authenticity. Training with non-dangerous weapons or in simulated situations is not real-life experience. The fear and risk associated with these situations is absent in training situations. It is inaccurate for Author2 to claim to be trained in "real-life situations" when he/she has never experienced actual dangerous situations. Author2 is inflating his/her experience.

Author2 feels Author1 is simply being argumentative by qualifying Author2's fighting experience. Author2 learned under the expertise of an ex-police officer. The whole of the military and police force train in simulated dangerous situations. By all accounts, this provides them adequate experience in dealing with real-life situations. Author2 never declared to train in "real life situations". Author1 has no personal information enough to comment on Author2's abilities..


----------
D2
----------
Author1 tells Doc to be careful because this guy trained under the bullet proof monk for two. Author2 says he isn't bullet proof or a monk, he's a white ex-cop. Author1 says if he says that fighting with rubber knives is like the real thing then he's a charlatan. He asks what he says to do if you get stabbed. Author2 asks if Author1 is saying military and police training doesn't work. Author1 says he hopes they wouldn't say they train in real situations when they don't. Author2 asks if he should say he has never been trained. He thinks Author1 is bashing someone he has never met and falsely accusing him of saying he trained in real life situations when he didn't say that.


----------
D3
----------
Author1 jokes about a guy training under a bulletproof monk. Author2 says the guy is an ex-cop, not a bulletproof monk. Author1 calls the guy a charlatan for saying that rubber knives are like real ones because the situation is not comparable. Author2 says that of course training isn't the same, but it's the way military and police train too. Author1 doesn't think police and military training is comparable because a grandmaster isn't involved and that they know that they don't need to train in real life situations. Author2 insults Author1's reading comprehension. Author2 says that Doc Jones called them untrained and that it's aggravating because they do train but people always have negative opinions about their instructor. Author2 does not like said absurd assumptions.


----------
D4
----------
Author1 jokes about a guy who trained under the bullet proof monk, but Author2 states that he is not bullet proof or a monk because he is a white ex-cop. Author1 asserts that rubber knives are different from real ones which is why he believes the trainer is a charlatan. Author2 asks Author1 if he thinks that the training methods of both the military and the police are bogus. Author1 answers him by stating that people trained by the military and police do not claim to be trained in real life situations. Author2 states that he was accused of being untrained which is why he stated that he was trained. The accusation that he claimed to have been trained in real life situations is false.

