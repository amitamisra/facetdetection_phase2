
----------
D0
----------
Author1 tells Author2 that the preamble is not the law, and that it is an introduction to laws that follow. Author2 agrees that is the definition for their existence, and that the Constitution is the source of those rights that cannot be removed by government. Author1 shares his reason for pointing out that the preamble is not law, and that is because liberals enact social engineering legislation that is outside the scope of the Constitution. Promoting the general welfare does not mean enriching individuals with monetary rewards for doing nothing, and then Author1 asks Author2 for links to Walter William and Thomas Sowell. Author2 gives Author1 a link to Walter William's website, and he describes books that were written by scholars of the Constitution. Author2 continues to list links to sites that have historical information about the Constitution, and Author1 thanks Author2 for the links. Author2 explains why he quoted the preamble to the Bill of Rights. The Bill of Rights enumerates in detail all of the power that Congress is ever allowed to have.


----------
D1
----------
Author1 says the Preamble is not a law, it's an introduction to them, and could be considered a precedent for the laws. He says the authors listed rights people should have that were considered God given rights. He says he states this because liberal people tend to misinterpret the part that says "promote the general welfare" as an excuse to create social welfare legislation that isn't covered by the Constitution. He says he would like the links to the author. 

Author2 says the Preamble is a definition of laws, and since it's part of the Constitution it protects those rights from being taken away. He ask if Author1 would like links to an author. He says some people are confusing the Declaration of Independence with the Constitution. He says there are many good websites on the Constitution, and list them for Author1. He says the reason he quoted the Preamble is because article 1, section 8, details what powers Congress has, and if it isn't in there, then they don't really have the power.


----------
D2
----------
Author1 says the preamble is not law. It's an introduction of the laws that follow. Author2 agrees, it's a definition of why they exist and as part of the Constitution, and the source of those rights, they can't be removed by government. Author2 thinks this is statutory precedent. Author1 says he is careful making the point that the preamble isn't law because liberals like to say general welfare gives them authority to enact legislation outside of the scope of the Constitution. General welfare doesn't mean giving people money for nothing. Author2 says that's in the Declaration of Independence, not the Constitution. He provides a link to a website on JFPO. He cites many sites that he believes are good and apologizes for the delay in responding - he has been busy with dough. Author1 thanks him for the links. Author2 says this is why he quoted the preamble to the BOR. Congress has its powers enumerated in article 1, section 8 and any claim to the contrary is not withstanding. He then talks about his hobbies.


----------
D3
----------
Author1 states that the "Preamble" is NOT law, only an introduction to the Constitution.  He believes that liberals use the "promote the general welfare" part of the Preamble to "enact social engineering legislation" not covered by the Constitution.  
Author2 agrees with Author1 that the Preamble is not law and goes on to explain it as being a definition of why the Constitution was written. He goes on to say the laws in the Constitution are rights that are given by a higher power (i.e. God) and would exist written or not. Also, he states that these rights can not be diminished or removed by any government branch. He lists some useful links related to the Constitution and history.  He mentions that Congress has its powers enumerated in Article 1 Section 8.  "If it isn't enumerated, they don't have it."


----------
D4
----------
Author1 argues that the Preamble of the Constitution is not a law itself, and the purposes stated in the Preamble are not enough for people to enact social changes based upon. Liberals like to use the Preamble to defend their ideas relating to social engineering and giving monetary rewards out to people for little to no work. Because the Preamble to the Constitution only introduces the Constitution, it is not enough to cite the Preamble as justification for a new law or idea. 
Author2 argues that the Preamble defines why the document exists, and that the definitions for why the Constitution exists are just as important as the text within the Constitution. The Preamble does provide a statutory precedent because those ideas cannot be removed by any other branch of government. Also, the Preamble is a part of the Declaration of Independence, not the Constitution. Congress's powers are specifically defined in article 1 of the Constitution, not anywhere else. If the power is not listed there they technically do not have it.

