
----------
D0
----------
Author1 states that since the US is now banning guns, the next natural step, as seen in Canada, England and Australia, may be confiscation or a government buy-back. These are the only two ways to get guns off of the street. A confiscations is essentially creating a police state. A buy-back will significantly damage the economy. If they melt down the guns down it's burning money and resources. With the US economy as it is, we need to be creating jobs and not wasting/throwing away money. Author1 acknowledges the Canadian government has a buy-back program and does not seize them, but they trade-in rates compared to the FMV of the guns is abysmal. 

Author2 is from Canada and argues that Canada has not simply confiscated its guns, as Author1 suggested, it merely put a restriction on importing and/or buying new ones. You are allowed to keep the weapons you own. This all only applies to handguns anyway, hunting weapons aren't a part of the restriction. Illegal weapons have no FMV. Compensation is sometimes generous.


----------
D1
----------
Author1 argues that gun banning only leaves two options, either that the government will buy back all the guns or the government will simply take all of the guns. Other countries around the world have taken their citizens guns in these manners and it will happen in the US if they want to get rid of guns. If the government buys back guns it is basically just wasting money. The money they spend will not provide the government with any advantage and the guns will go to waste. Gun buy back programs offer too little money for the market value of the weapons which they sell. People accept the lower prices because they are afraid of the government. Buy backs are government stealing from its people.
Author2 argues that there is another way to reduce gun numbers on the streets. Instead of taking or buying back all guns, the government bans the sale of guns, new or old. Hunting weapons do not apply in this situation.


----------
D2
----------
Author1 asks what happens after guns are banned, a confiscation or buy back. Author2 questions why either of those. Author1 thinks that's the way the UK and Canada gets rid of guns and thinks that is what would happen here. These are the only two ways to get them off the streets. He thinks the first is one step closer to a police state and the second will ruin the economy. Author2 is says Canada hasn't gotten rid of guns - you just can't buy or import new ones, and you can't resell old ones, but you can keep ones you own. This applies to handguns, hunting guns are legal. Author1 says he was mistaken. Canada doesn't take guns, they buy them back. He doesn't think many would volunteer because the government is paying a low price. Author2 says the amount paid is that much over market value because they can't be sold. Author1 thinks this implies the government should take them. Author2 disagrees and says the market value is zero, so it's fair.


----------
D3
----------
Author1 says there are only two ways for the government to get guns of the street, through confiscation or buyback, and refers to this having happened in the UK, Australia, and Canada.  Confiscation is a step toward a police state, and is equivalent to the government stealing from the people.  But buybacks are the equivalent of the government burning money, and would cost at least $230 billion, if fair market value (>$800 per gun) was paid.  If the government instead offers $50 per gun, very few people will turn the guns in voluntarily.  A forced buy back is like the government stealing.

Author2 says that Author1 is wrong about Canada.  Canada has not confiscated guns, and is not trying to get rid of existing guns; they've simply limited sales of new handguns.  Existing guns are legal, as are all hunting guns.  If a gun is illegal to sell, then its fair market value is $0, making $50 a reasonable amount for the government to offer in a buyback.


----------
D4
----------
Author1 states since guns are banned, confiscation or buy backs will happen. Author2 ask why, and Author1 says that is how the UK, Australia, and Canada did it. He says this is the only way for police to get rid of guns on the streets. One is creating a police state and one is burning money, since the guns are melted down. Author2 lives in Canada, and disagrees with this. He says you can keep your old guns and just can't buy new handguns. Author1 agrees Canada is buying guns, but it is not voluntary. They only pay $50 regardless of value. He states a low number of owners have participated. He gives an example of a boy who sold his $900 gun and ask if the boy felt forced to sell. Author2 says $50 is above the value of the gun now. Since the gun can not be legally sold to someone else, it's value is zero. Author1 insists the boy was cheated out of property value. He calls this government theft.

