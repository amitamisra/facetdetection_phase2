
----------
D0
----------
Author1 says is is armed and has years of weapons training. He says he will give Author2 a sermon. Author2 says he is trying to help out a mental midget since they asked for help. Author1 says he didn't ask for help - just his opinion on a scripture that mentions weapons and didn't give the impression he's against Author2 because he isn't. Author2 is treating him as an enemy because he paraphrased a scripture to agree with him. Author2 thinks because Author1 can't produce an argument to refute him that he has won. Author1 thinks Author2 is blinded by arrogance and says he doesn't want to refute his stance. He thinks if Author2 was rational he would have pointed out he said get rid of and simply explained that wasn't the correct interpretation. Author2 says he can see how the fault is on him, but since he supports some of the issues they still don't agree. Author1 says Author2 is in no position to tell him what he meant. Author2 thinks that he is.


----------
D1
----------
Author1 argues that guns and tactical training are good skills, and that Christians do not need to be against weapons and guns. Scripture often mentions weapons, and it does allow for a positive view of weapons while still being Christians. People should not need a license to purchase a weapon, as it is a constitutional right to own weapons. Registering guns with the federal authorities should not be enforced either. Waiting periods are not as big of an issue as most people make them out to be. Carry and Conceal laws that restrict concealed carry infringe upon the second amendment right granted in the Constitution. Gun control methods are not a good idea. The Constitution guarantees the people's rights to own weapons. 
Author2 argues that cops spend their time annoying teenagers and tazering innocent citizens. Licenses to purchase guns, registration, waiting periods, carry and conceal laws, are all unlawful and should not be implemented. Author2 agrees with Author1 that gun control laws are not a good idea.


----------
D2
----------
Author1 asserts that he has years of tactical weapons training, and that he has been a Christian for 29 years. Author2 responds to Author1 with a litany of insults, which causes Author1 to state that he will not waste his time refuting the insults. Author1 tells Author2 that he asked for his opinion on another scripture that mentions weapons, and that he is not against Author2. Author2 continues to berate Author1, then he tells Author1 that he cannot produce a single argument to refute his stance. Author1 calls Author2 arrogant, and tells him that he is not trying to refute his stance. Author1 insults Author2, and tells him that he has no idea how to debate. Author2 expresses disbelief over the statement that Author1 made where he claimed to agree with Author2, and he tells Author1 that they probably will not agree on other issues. Author2 mentions those issues, and they all involve some form of gun regulation. Author1 reassures Author2 that they will agree, because he dislikes gun regulations that infringe on his rights.


----------
D3
----------
Author1 says he thinks people should be allowed to be armed. He owns guns and has weapons training, and is a Christian. He asked what was Author2's opinion on a scripture that talks about weapons that agrees with what Author2 thinks. He wasn't saying he was against Author2. He says Author2 has anger issues and an inferiority complex, and doesn't know how to debate. He disagrees he favors things Author2 doesn't. He doesn't think people should have to have a license, and gun registration should not be mandatory. He says they should not have waiting periods as much as they do now, and the carry and conceal laws are violating people's rights. 


Author2 says police misuse weapons and are unintelligent. He says Author1 is unable to successfully argue against his statements. He doubts Author1 agrees with him, since he probably supports some things Author2 doesn't; like having to have a license to buy a gun, gun registration, waiting periods, or carry and conceal laws. He says cops should defuse situations, not escalate them.


----------
D4
----------
Author1 believes that Author2 is taking a sermon discussed earlier out of context as well as not understanding Author1's argument. Author1 states he is armed and has had years of training. Author1 has been a Christian for 26 years. Author1 reiterated the request of Author2's opinion on a scripture mentioning weapons and states that both speaker agree on the appropriateness of weapons but disagree on copying and pasting quotes in forums. Author1 believes Author2 is arrogant and irrational and new to forums. Author1 believes licenses to buy guns are wrong, waiting periods to purchase guns should not be as prevalent as they are and that conceal carry laws infringe on American's rights.

Author2 defines "drubbing" then goes on to refute Author1's suggestion that they are in agreement and states that Author1 desires weapons to be removed from society. Author2 is opposed to licenses to buy weapons, gun registrations, waiting periods to purchase guns and conceal carry laws. S 2 believes police officers should publically claim to defuse interactions with suspects even if not happening in every interaction with the public. Both speakers disagree often with word choice, who agrees with whom and the ground rules for debates.

