
----------
D0
----------
Author1 feels the US's problem isn't guns but the violent overzealous people who wield them. In the UK (where Author1 is from) cities of comparable size and population have far lower murder and gun violence rates than cities in the US. Guns are not the problem. A country is safer protected by a police force than vigilantes. Zealots like Author2 happily surrender their rights to the Patriot Act but only feel personal rights are infringed on if guns are regulated? Guns regulation does not create a police state. Author2 is ignorant of the history of firearms and/or all of its surrounding issues. 

Author2 feels criminals and a weak justice system are the problems in America. The real threat is those who easily relinquish their rights. Author1 is incorrectly labeling those who claim their 2A right to self-defense as vigilantes or "gun nuts". How can Author2 feel safe in a place where only the police and military control firearms like in the book '1984'? The loss of American rights would result in a tyrannical police state.


----------
D1
----------
Author1 points out that British Canada won the war of 1812. Guns are not the major issue in the US, it is the people who don't know how to use guns safely. In Britain people can walk safely through major cities without fear of crime or murder. Guns are rarely used to commit murder in Britain, and the majority of murders are drunken altercations. Vigilantism is not a better alternative than a trained police force. Taking the law into your own hands is the definition of vigilantism, and the claimed defense of US rights is being eroded daily by the Patriot Act. Since there is no popular uprising, it is clear that guns do not help protect anyone's rights.
Author2 argues that the US didn't lose the War of 1812, and that the issues with gun ownership lie in criminals and a weak justice system. Americans do not blindly surrender their rights. Britain is a police state with their excessive regulations and surveillance. Violent criminals should not be left on the streets.


----------
D2
----------
Author1 believes the problem in America is not guns, it is gun lovers. In his country he can walk though his whole city at night without a qualm. Gun murders are unusual, and so is gun crime. Occasionally someone might get stabbed, but even that is fairly rare. He does not think vigilantism is better than policing. He thinks policing is better. It is sensible and reasonable. Vigilantism is not. As a Canadian, he can see that the root cause of gun violence is unregulated guns. Author2 believes he simply does not want the government to take his rights. He believes that he must not knuckle under to government control, and he especially must not surrender his right to carry a gun. Exercising his rights is not vigilantism. Look at Britain. The authorities there cosset the citizens. They all live under surveillance. They are forbidden to defend themselves with guns. The authorities trample their rights constantly. Freed criminals cause violence, guns do not. When we give up our rights, we create a police state.


----------
D3
----------
Author1 claims guns aren't so much the problem in the US as the people who think they have to have them. Author2 says it is not the pro-gun people who are the problem, but the criminals who misuse guns and the court system who does not properly punish criminals. Author1 says he lives in a large city in Canada, and says they have 6-7 murders per year. Author2 says the problem is not the pro-gun people, but the ones who don't understand why people don't want to surrender their rights and be controlled by the government and police. Author1 ask why is an armed group of citizens better than a police force. Author2 says being able to defend oneself is not the same as being a vigilante. Author1 argues it does because you are taking the law in your own hands. He mentions how personal rights are being violated by the Patriot Act. Author2 argues being unarmed creates a police state. Author1 says Author2 is arguing citizens should be able to act as police.


----------
D4
----------
Author1 talks about Canada burning down the White house and thinks the problem isn't guns, the problem is gun nuts. Author2 says just because we didn't finish the war the first day doesn't mean we lost. It took time to get the British out. Author2 doesn't think we're the problem, the weak justice system is. Author1 thinks Author2 is ignorant of his own history and the War of 1812. He says he can't remember the last time a gun was used in a murder where he lives and the guns aren't the problem, but the US gun culture is. Author2 says if the US has a gun culture because they have shootings then Canada must have a knife culture with all the stabbings. Author1 asks why vigilantism is better than police force. Author2 says defending the 2nd amendment isn't vigilantism. Author1 says it's supporting vigilantism to say it's a surrender of rights to be policed. Author2 doesn't agree and doesn't support a police state. Author1 thinks citizens are acting as police and that's vigilantism.

