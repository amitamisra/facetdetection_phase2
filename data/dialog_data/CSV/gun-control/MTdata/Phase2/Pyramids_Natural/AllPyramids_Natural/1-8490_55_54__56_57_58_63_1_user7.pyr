<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>Author1 is talking about someone who left a knife on a counter, and how obvious the intent to do harm is. Author2 posts the relevant headline of an injured woman stabbed in a store shooting. Author1 says that if it had been a gun, the headline would have been about seven dead people. Author2 posts a headline about seven people stabbed to death in Japan. Author1 brings up the Columbine School shooting. Author2 argues it is not relevant. The two people were crazy, and got their guns illegally. They took them to a no guns area, and killed without any regard for people. This is different than someone carrying a gun for protection, he says.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>Author1 says you can't apologize a mistake of someone's intent if they're dead. Author2 names a headline about a supermarket stabbing. Author1 says if the attacker had a gun the headline would be that more people died. Author2 lists a headline where seven people died in a stabbing spree. Author1 makes fun of Author2 for using anecdotes and mentions Columbine. Author2 asks how Columbine is relevant and denounces it as an edge case. They say it's different from somebody wanting a gun for self defense.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>Author1 thinks is isn't easy to apologize for mistaking someone's intent when they are dead. Author2 cites an article from Times Online about a women that's in critical condition after a random supermarket stabbing. Author1 says that if the attacker had a gun it would read seven dead in a supermarket shooting spree. Author2 says he can top that, and that seven were killed in a Tokyo stabbing spree. Author1 notes that it seems to be anecdote city and asks if Author2 remembers Columbine. Author2 asks how Columbine is relevant. It was a massacre by two individuals that got guns through illegal methods, took them to an area where they were off limits, and killed people. He thinks Columbine made people want guns for defense.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>Author1 tells Author2 that it is not easy to apologize for mistaking the intent of a person once they are dead. Author2 reads from a headline that states that a woman was in critical condition after a random supermarket stabbing. Author1 believes that the headline would have read differently if the attacker had a gun, and that the headline would state that seven people died in a supermarket shooting spree. Author2 reads to Author2 a headline that states that seven people were killed in a Tokyo stabbing spree. Author1 brings up Columbine, but Author2 asks Author1 what the relevancy was. Two crazed individuals got guns through illegal channels, and they massacred people in a gun-free zone. Columbine is different than carrying a gun for self-defense.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>Author2 quotes headlines in which knives rather than guns were used to kill, including one case in which seven were killed in a Tokyo supermarket.  He objects to Author1 bringing up Columbine, saying that is hugely different than someone wanting to carry a gun for personal defense.  He points out that those guns were obtained illegally, and used in an area identified as off limits to guns.</line>
  <line></line>
  <line>Author1 raises the point that if a gun-owner incorrectly judges someone else's intent, an innocent person might be dead before the shooter realizes his mistake.  In response to Author2's headline about a random knife attack in a store, Author1 writes that if the attacker had had a gun, the outcome might have been multiple deaths.</line>
 </text>
 <scu uid="7" label="Columbine is not relevant">
  <contributor label="He objects to Author1 bringing up Columbine">
   <part label="He objects to Author1 bringing up Columbine" start="2919" end="2962"/>
  </contributor>
  <contributor label="Author2 asks Author1 what the relevancy was">
   <part label="Author2 asks Author1 what the relevancy was" start="2534" end="2577"/>
  </contributor>
  <contributor label="Author2 asks how Columbine is relevant">
   <part label="Author2 asks how Columbine is relevant" start="1720" end="1758"/>
  </contributor>
  <contributor label="Author2 asks how Columbine is relevant...denounces it as an edge case">
   <part label="Author2 asks how Columbine is relevant" start="1059" end="1097"/>
   <part label="denounces it as an edge case" start="1102" end="1130"/>
  </contributor>
  <contributor label="Author2 argues it is not relevant">
   <part label="Author2 argues it is not relevant" start="431" end="464"/>
  </contributor>
 </scu>
 <scu uid="3" label="If attacker had gun, the headline would be about seven dead">
  <contributor label="Author1 says that if it had been a gun, the headline would have been about seven dead people">
   <part label="Author1 says that if it had been a gun, the headline would have been about seven dead people" start="217" end="309"/>
  </contributor>
  <contributor label="Author1 says if the attacker had a gun the headline would be that more people died">
   <part label="Author1 says if the attacker had a gun the headline would be that more people died" start="832" end="914"/>
  </contributor>
  <contributor label="Author1 says that if the attacker had a gun it would read seven dead in a supermarket shooting spree">
   <part label="Author1 says that if the attacker had a gun it would read seven dead in a supermarket shooting spree" start="1445" end="1545"/>
  </contributor>
  <contributor label="Author1 believes that the headline would have read differently if the attacker had a gun, and that the headline would state that seven people died in a supermarket shooting spree">
   <part label="Author1 believes that the headline would have read differently if the attacker had a gun, and that the headline would state that seven people died in a supermarket shooting spree" start="2216" end="2394"/>
  </contributor>
  <contributor label="Author1 writes that if the attacker had had a gun, the outcome might have been multiple deaths">
   <part label="Author1 writes that if the attacker had had a gun, the outcome might have been multiple deaths" start="3401" end="3495"/>
  </contributor>
 </scu>
 <scu uid="2" label="Posting headline of injured woman stabbed in a store shooting">
  <contributor label="Author2 posts the relevant headline of an injured woman stabbed in a store shooting">
   <part label="Author2 posts the relevant headline of an injured woman stabbed in a store shooting" start="132" end="215"/>
  </contributor>
  <contributor label="Author2 names a headline about a supermarket stabbing">
   <part label="Author2 names a headline about a supermarket stabbing" start="777" end="830"/>
  </contributor>
  <contributor label="Author2 cites an article from Times Online about a women that's in critical condition after a random supermarket stabbing">
   <part label="Author2 cites an article from Times Online about a women that's in critical condition after a random supermarket stabbing" start="1322" end="1443"/>
  </contributor>
  <contributor label="Author2 reads from a headline that states that a woman was in critical condition after a random supermarket stabbing">
   <part label="Author2 reads from a headline that states that a woman was in critical condition after a random supermarket stabbing" start="2098" end="2214"/>
  </contributor>
  <contributor label="Author2 quotes headlines in which knives rather than guns were used to kill, including one case in which seven were killed in a Tokyo supermarket...In response to Author2's headline about a random knife attack in a store">
   <part label="Author2 quotes headlines in which knives rather than guns were used to kill, including one case in which seven were killed in a Tokyo supermarket" start="2771" end="2916"/>
   <part label="In response to Author2's headline about a random knife attack in a store" start="3327" end="3399"/>
  </contributor>
 </scu>
 <scu uid="6" label="Bringing up Columbine">
  <contributor label="Author1 brings up Columbine">
   <part label="Author1 brings up Columbine" start="2501" end="2528"/>
  </contributor>
  <contributor label="asks if Author2 remembers Columbine">
   <part label="asks if Author2 remembers Columbine" start="1683" end="1718"/>
  </contributor>
  <contributor label="mentions Columbine">
   <part label="mentions Columbine" start="1039" end="1057"/>
  </contributor>
  <contributor label="Author1 brings up the Columbine School shooting">
   <part label="Author1 brings up the Columbine School shooting" start="382" end="429"/>
  </contributor>
 </scu>
 <scu uid="9" label="Columbine is different from wanting gun for self defense">
  <contributor label="They say it's different from somebody wanting a gun for self defense">
   <part label="They say it's different from somebody wanting a gun for self defense" start="1132" end="1200"/>
  </contributor>
  <contributor label="This is different than someone carrying a gun for protection, he says">
   <part label="This is different than someone carrying a gun for protection, he says" start="599" end="668"/>
  </contributor>
  <contributor label="Columbine is different than carrying a gun for self-defense">
   <part label="Columbine is different than carrying a gun for self-defense" start="2683" end="2742"/>
  </contributor>
  <contributor label="that is hugely different than someone wanting to carry a gun for personal defense">
   <part label="that is hugely different than someone wanting to carry a gun for personal defense" start="2971" end="3052"/>
  </contributor>
 </scu>
 <scu uid="13" label="Guns were obtained illegally">
  <contributor label="got their guns illegally">
   <part label="got their guns illegally" start="497" end="521"/>
  </contributor>
  <contributor label="got guns through illegal channels">
   <part label="got guns through illegal channels" start="2602" end="2635"/>
  </contributor>
  <contributor label="got guns through illegal methods">
   <part label="got guns through illegal methods" start="1802" end="1834"/>
  </contributor>
  <contributor label="He points out that those guns were obtained illegally">
   <part label="He points out that those guns were obtained illegally" start="3055" end="3108"/>
  </contributor>
 </scu>
 <scu uid="4" label="Posting headline about seven people stabbed to death in Japan">
  <contributor label="Author2 posts a headline about seven people stabbed to death in Japan">
   <part label="Author2 posts a headline about seven people stabbed to death in Japan" start="311" end="380"/>
  </contributor>
  <contributor label="Author2 says he can top that, and that seven were killed in a Tokyo stabbing spree">
   <part label="Author2 says he can top that, and that seven were killed in a Tokyo stabbing spree" start="1547" end="1629"/>
  </contributor>
  <contributor label="Author2 reads to Author2 a headline that states that seven people were killed in a Tokyo stabbing spree">
   <part label="Author2 reads to Author2 a headline that states that seven people were killed in a Tokyo stabbing spree" start="2396" end="2499"/>
  </contributor>
  <contributor label="Author2 lists a headline where seven people died in a stabbing spree">
   <part label="Author2 lists a headline where seven people died in a stabbing spree" start="916" end="984"/>
  </contributor>
 </scu>
 <scu uid="14" label="Two crazy people took guns to a gun-free zone">
  <contributor label="took them to an area where they were off limits">
   <part label="took them to an area where they were off limits" start="1836" end="1883"/>
  </contributor>
  <contributor label="used in an area identified as off limits to guns">
   <part label="used in an area identified as off limits to guns" start="3114" end="3162"/>
  </contributor>
  <contributor label="They took them to a no guns area">
   <part label="They took them to a no guns area" start="523" end="555"/>
  </contributor>
  <contributor label="in a gun-free zone">
   <part label="in a gun-free zone" start="2663" end="2681"/>
  </contributor>
 </scu>
 <scu uid="10" label="It is not easy to apologize for mistaking the intent of a person once they are dead">
  <contributor label="Author1 tells Author2 that it is not easy to apologize for mistaking the intent of a person once they are dead">
   <part label="Author1 tells Author2 that it is not easy to apologize for mistaking the intent of a person once they are dead" start="1986" end="2096"/>
  </contributor>
  <contributor label="Author1 thinks is isn't easy to apologize for mistaking someone's intent when they are dead">
   <part label="Author1 thinks is isn't easy to apologize for mistaking someone's intent when they are dead" start="1229" end="1320"/>
  </contributor>
  <contributor label="Author1 says you can't apologize a mistake of someone's intent if they're dead">
   <part label="Author1 says you can't apologize a mistake of someone's intent if they're dead" start="697" end="775"/>
  </contributor>
 </scu>
 <scu uid="11" label="The two people were crazy">
  <contributor label="The two people were crazy...killed without any regard for people">
   <part label="The two people were crazy" start="466" end="491"/>
   <part label="killed without any regard for people" start="561" end="597"/>
  </contributor>
  <contributor label="It was a massacre by two individuals...killed people">
   <part label="It was a massacre by two individuals" start="1760" end="1796"/>
   <part label="killed people" start="1889" end="1902"/>
  </contributor>
  <contributor label="Two crazed individuals...they massacred people">
   <part label="Two crazed individuals" start="2579" end="2601"/>
   <part label="they massacred people" start="2641" end="2662"/>
  </contributor>
 </scu>
 <scu uid="5" label="Makeing fun for using anecdotes">
  <contributor label="Author1 makes fun of Author2 for using anecdotes">
   <part label="Author1 makes fun of Author2 for using anecdotes" start="986" end="1034"/>
  </contributor>
  <contributor label="Author1 notes that it seems to be anecdote city">
   <part label="Author1 notes that it seems to be anecdote city" start="1631" end="1678"/>
  </contributor>
 </scu>
 <scu uid="17" label="Columbine made people want guns for defense">
  <contributor label="He thinks Columbine made people want guns for defense">
   <part label="He thinks Columbine made people want guns for defense" start="1904" end="1957"/>
  </contributor>
 </scu>
 <scu uid="64" label="If gun-owner incorrectly judges someone's intent, innocent person might be dead before owner realizes ">
  <contributor label="Author1 raises the point that if a gun-owner incorrectly judges someone else's intent, an innocent person might be dead before the shooter realizes his mistake">
   <part label="Author1 raises the point that if a gun-owner incorrectly judges someone else's intent, an innocent person might be dead before the shooter realizes his mistake" start="3165" end="3324"/>
  </contributor>
 </scu>
 <scu uid="66" label="Intent to do harm is obvious">
  <contributor label="how obvious the intent to do harm is">
   <part label="how obvious the intent to do harm is" start="94" end="130"/>
  </contributor>
 </scu>
 <scu uid="65" label="Someone left a knife on a counter">
  <contributor label="Author1 is talking about someone who left a knife on a counter">
   <part label="Author1 is talking about someone who left a knife on a counter" start="26" end="88"/>
  </contributor>
 </scu>
</pyramid>
