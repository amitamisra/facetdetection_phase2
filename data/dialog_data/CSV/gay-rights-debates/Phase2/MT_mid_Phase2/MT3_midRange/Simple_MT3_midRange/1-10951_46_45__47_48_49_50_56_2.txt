
----------
D0
----------
S1 and S2 are discussing gay rights.  
S1 is in support of gay marriage.
S2 supports it.  
S2 feels gay rights would impose gay families onto straight members of society.
S1 believes S2's opinions are comparable to those of Taliban terrorists.  
S2 is in support of comments made by a previous participant.
The comments contend he did not make remarks based on mandatory therapy for gays or anything of the like.  
S1 believes the previous comments made center on religion.
S1 believes that civil law and religion are not synonymous.  
S2 feels his religious beliefs are violated by just the consideration of allowing gay marriage.  
S1 believes marriage is a contract between a woman and a man, not same sex.


----------
D1
----------
S1 labeled someone Mr Taliban.
S1 said that this person who is against sharia law is himself imposing that on gays through his religious belief.
The belief is that a civil marriage contract is only for a man and a woman. 
He said Mr.Taliban's religious beliefs should not have anything to do with civil law. 
S2 challenged S1's comparison of the man to the Taliban.
He has not advocated the execution, jailing, etc. of homosexuals. 
S2 then stated that is indeed the gay lobby that is attempting to impose its view on the man. 
S2 concluded by asking S1 to stop complaining.
There have been no real violations of S1's rights. 
S1 said he can have a contract.
He can have as much sex as he likes.


----------
D2
----------
S1 claims that religious beliefs are preventing the recognition of homosexual marriages. 
He compares basing law on Christian beliefs to be the same thing as Islamists advocating Sharia law. 
He argues that the religious belief that marriage is between a man and a woman should have no influence on a civil marriage contract.
S2 claims that homosexuals are not being persecuted through things like execution, imprisonment, or mandatory therapy.
It is actually the homosexual lobby who are pressing their beliefs about marriage onto everyone else. 
He cites the fact that homosexual marriage has not been banned in any state.
Homosexual marriage is simply not recognized.
People are free to sign whatever contracts they want.
People are free to engage in intercourse with whomever they want.


----------
D3
----------
Two people are discussing legal law and gay rights.  
Gays are not allowed to have a civil government contract because of religious beliefs. 
S1 states that by not allowing gays to have a civil government contract is equivalent to sharia law in the Mideastern countries. 
He states that it is a religious belief that a civil marriage contract is between a man and a women.
A civil marriage is between a man and a woman is the current law.
Religious beliefs should have nothing to do with civil law.  
S2 contends that the execution, imprisonment or even mandatory therapy for homosexuals.  
He states that it is the homosexual lobby who are trying to force people to recognize and support homosexuality in the public sphere.  
He also states homosexual marriages haven't been banned by law.
Homosexual marriages are just not recognized.


----------
D4
----------
S1 compares being against gay marriage to the Taliban and Sharia law. 
He sees no difference in those that oppose gay marriage for religious reasons and have laws against it and Muslims that pass laws based on religion. 
He finds it hypocritical for people to be against Sharia law.
In his opinion people are doing the same thing.
He believes marriage is a legal contract between two people and the state.
He supports gay marriage. 
S2 does not agree with the comparison of Sharia law and being against gay marriage. 
He has not advocated for jail time or the death of homosexuals. 
They are free to do as they wish. 
He believes gay marriage pushes that relationship onto others.
He opposes it.

