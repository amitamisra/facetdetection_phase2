
----------
D0
----------
S1 does not believe that what Jefferson recommended to do to homosexuals is okay. 
S1 points out that according to the Christian faith homosexual is sinful.
To modern law homosexual is not sinful. 
S1 argues that the country is not a theocracy.
The country allows everyone equal rights to practice their religious beliefs as they see fit.
The country does not allow for one to exert control over another.

S2 believes Jefferson was correct in stating homosexuality as being a crime. 
S2 personally thinks homosexuality is a sin.
There should be a penalty against homosexuals.
There should be a penalty against those advocate homosexuals. 
S2 argues that liberal tolerance assumes a mindset that advocates its own views on conduct.
Liberal tolerance excludes and dismisses those that do not advocate its views. 


----------
D1
----------
S1 claims that Thomas Jefferson did not recommend the negative behaviors.
Negative behaviors are targeted toward homosexuals.
Homosexuality is a sin according to Christianity.
Homosexuality may have been illegal in the past.
Homosexuality is not a sin anymore. 
S1 points out that America is not a theocracy.
Christians are not persecuted.
S1 cites the fact that they are free to practice religion as they choose.  

S2 claims that Thomas Jefferson was correct in labeling homosexuality as a sin and a crime.
S2 believes that there should be heavy monetary fines for engaging in or advocating homosexual behavior.
S2 argues that liberal tolerance is just as dogmatic as Christianity.
It is no more right to base laws on this perspective over any another.




----------
D2
----------
S2 thinks homosexuality is a sin.
Homo should be a crime along with heavy fines. 
S2 believes liberals are hypocritical in talking about tolerance and justice. 
S2 thinks liberals think only they are right.
Liberals push the liberal way as social norm.
Liberals ridicule anyone that doesn't agree. 
S2 points out this isn't a theocracy.
If someone wants to live in a society based on religion there are other places that have that. 
S2 thinks Christians are hypocrites.
Christians claim to be persecuted.
Christians can't force their religion on others. 
S2 says Christians may practice their religion as they wish.
Christians can't force others to practice their religion.


----------
D3
----------
S1 believes homosexuality was considered a crime in earlier times.
Homosexuality is no longer a crime today.  
S1 feels S2's argument of liberal persecution can't hold water.
Liberal tolerance is what allows for S2 to worship as he or she chooses.
Liberal tolerance is what allows for S2 to state openly his or her opinions.



S2 believes homosexuality should still be considered a crime.
Those who participate in homosexuality should be punished by law.  
Those who support participators in homosexuality should be punished by law.  
S2 believes liberal tolerance is, as a whole, not actually tolerant.  
S2 feels people with different views from liberalism are persecuted for non-acceptance publicly.  

S1 advises gay marriage was voted on and lost there they live and feels it is mostly based upon religious values. 


----------
D4
----------
Two people are discussing laws and politics.  
S2 states that homosexual sex is a sin and a crime.
There should be a huge fine on the homosexual behavior.  
S1 retorts that Christianity states homosexual sex is a sin.
Homosexual sex is not a crime anymore.  
S1 states that the US is not a theocracy.
The freedom available in the US means that the responsibility of appeasing a Christian God cannot be forced on the population.  
S2 retorts that liberal tolerance is contradictory, dogmatic, intolerant and coercive as it only allows for one correct view.  
To contradict, S1 states that the "liberal" tolerance still allows for the free practice of religion.

