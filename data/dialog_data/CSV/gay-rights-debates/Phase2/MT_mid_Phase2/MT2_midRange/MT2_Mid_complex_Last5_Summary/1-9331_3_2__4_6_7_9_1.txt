
----------
D0
----------
S1 argues that the proponents of Prop 8 need to verify that the California Amendment banning gay marriage was enacted legally. This person believes that if DOMA isn't declared unconstitutional than those arguing against Prop 8 don't have a valid platform. He or she argue that they would have to first strike down the federal law that gives states the right to ignore other states' laws before they could do the same to the California Amendment.
S2 argues that the California Amendment is being tried to see if it violates the 14th Amendment of the Constitution. This person believes that DOMA violates the federal constitution. This person argues that there is nothing in DOMA that prevents California's court from ruling the 14th amendment requires gay marriage. 


----------
D1
----------
S1 claims that proponents of gay marriage legislation need only to verify that the state constitutional amendment banning gay marriage was enacted illegally, and that if the defense of marriage act is constitutional, then it is legal to ban gay marriage. He also argues that gay marriage has a financial impact on all tax payers. 
S2 argues that the state constitution does not supersede the federal constitution, so the question is whether the state constitutional amendment violates the 14th amendment. He also suggests that the defense of marriage act will likely be ruled unconstitutional which leads to the question of the legality of the state amendments. Finally, he points out that the defense of marriage act does not prohibit a state from recognizing gay marriage.



----------
D2
----------
S1 thinks Prop 8 needs to be verified as being enacted legally. S1 believes that if DOMA isn't unconstitutional there is not point in fighting over it. He says it has a financial impact on all tax payers and that federal law (about DOMA) would have to be struck down before the California law could be considered unconstitutional. S2 says that the federal DOMA is unconstitutional and that the discussion of the California law could lead to this recognition. S2 points out that gay people pay taxes and pay for straight marriages while being denied the right to marry themselves. He believes the 14th amendment requires gay marriage.


----------
D3
----------
S1 believes DOMA on a state level is unconstitutional.  S1 argues gay marriage has an impact on all tax payers in a negative way.  He/she believes the state ban on gay marriage in California was enacted legally additionally citing the federal ruling that each state must acknowledge the marriage laws of another state.

S2 provides the argument that DOMA on a federal level could actually be found unconstitutional.  S2 also argues the impact on taxpayers advising gay couples pay taxes on heterosexual marriages and activities so they are impacted based on fundamental law thus making S1's observance incorrect, in S2's theory.  S2 also advises a state constitution cannot supersede the federal constitution. S2 argues that S2 is incorrect concerning each state's recognition. 


----------
D4
----------
Two people are discussing the Defense of Marriage Act (DOMA). S1 believes that the State Constitutional Amendment banning gay marriage is legal.  He states that the outcome of the courts ruling impacts all taxpayers, and he argues that the court cannot rule a California Constitutional Amendment unconstitutional because Federal Law gives states the right to ignore other state's gay marriage law.  S2 asserts that the state constitution cannot violate the federal constitution, and based on what DOMA does it  violates the 14th amendment.  He also states that gay people pay taxes too and he wonders why gays must pay for straight marriages while not being allowed to marry themselves and that DOMA doesn't prohibit states from recognizing gay marriage.

