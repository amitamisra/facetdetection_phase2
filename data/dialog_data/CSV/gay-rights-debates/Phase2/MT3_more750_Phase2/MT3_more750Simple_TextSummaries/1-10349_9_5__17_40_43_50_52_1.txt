
----------
D0
----------
S1 States that the current definition of marriage specifically excludes homosexuals
The current definition is "two people that are not underage, or opposite genders, same species, not closely related and not already married".
Gays want to be married under current definition. 
S1 believes that ability to reproduce is implied in definition. 
S2 claims S1's definition isn't only possible.
S2 refers to lack of reproductive abilities of an elderly person and a younger person marrying. 
S1 refers to differences between cultural definitions of marriage and mating pairs or legal definitions of marriage. 
S2 ask who gave S1 rights to define cultural meanings. 
S1 Claims that cultural meaning comes from observation.
Legal definition can be decided any way. 
S2 asks who gets to make that legal definition. 
S2 claims that those oppose gay marriage are opposing something that harms no one.  They are trying to limit other's actions. 
S1 States that in America people vote on laws or those that make them.
Those that make laws is who gets to determine legality of different marriage concepts.


----------
D1
----------
S1 argues that marriage has a specific and deeply embedded definition within culture. 
Marriage is a formalized mating pair. 
Marriage's legal definition closely mirrors that definition. 
Homosexual marriage has never been an explicit or implicit part of those definitions. 
Including homosexual marriage would make the definition of marriage inherently self-contradictory. 
S2 argues that S1 is exercising a subjective definition. 
S2 notes that different members of society have different definitions of marriage. 
There are instances of accepted marriage which violate S1's provided definitions. 
Love, the emotional connection, is what is most important. 
S1 argues that legal and cultural definitions are not the same. 
The cultural definition is what is important. 
Most associate marriage with producing offspring.
Producing offspring is deemed important in the cultural definition. 
S2 questions S1's authority to determine what is culturally important.
S2 questions whether S1's cultural definition is appropriate for a legal definition. 
S1 says appropriateness is irrelevant. 
Cultural definitions have formed the root of all other definitions. 
The cultural definition of marriage has always been one male and female in monogamy.


----------
D2
----------
S1 is focused on the definition of marriage and the cultural definition.
Both definition and the cultural definition limiting marriage to heterosexual couples.
The cultural definition includes reproduction. He uses the term mating mating pairs.  
S1 argues that this definition automatically excludes homosexuals.
Homosexuals cannot create a family without outside help. 
S1 discusses other marriage outside of the norm-a young woman and older man- as being less meaningful than those that fulfill the cultural definition.  
S1 sees it as the job of elected officials and the citizens to change the legal definition of marriage if that is what the majority want.

S2 disagrees with and questions the cultural definition given by S1.
S2 feels that the main factor in marriage is not reproduction. 
The main factor in marriage is love and commitment between two individuals.
S2 challenges the cultural definition with the example of a 65 and 68 year old getting married.  
S2 finds it unfair that a group of people are being excluded from an institution.
They pose no threat to those who are enjoying the status quo on the institution.


----------
D3
----------
S1 argues that the cultural meaning of marriage is a formalization of a mating pair.
A mating pair create and support a family unit. 
S1 believe that in order for homosexuals to be included, the definition and cultural meaning of marriage would have to change to accommodate it. 
S1 argue that the most dominant cultural view on marriage is based upon evolutionary sound logic.
Monogamous pair of individuals will reproduce. 
S1 does not believe that the argument stands on equality.
S1 do not believe that equality factors into the evolutionary view of marriage.

S2 argues against the idea that there is a set, evolutionary definition of marriage.
S2 uses the example that people well over the age of reproduction are not only legally capable of getting married.
People well over the age of reproduction suffer no special stigma for doing so. 
S2 believe what defines marriage is love between two people who desire a commitment between themselves. 
S2 argues that many do not share S1's definition of marriage. It’s an issue of personal belief rather than culture.


----------
D4
----------
S1 states that the legal definition of marriage is a contract between a man and a woman, who are not related, with no regard to love, or the intention of reproduction. 
S1 maintains, from the basis of observation, that the cultural definition of marriage is a formalization of a mating pair.
Love and the intension to reproduce being implied.
S1 saying that homosexual couples are not a part of either definition.
Homosexual couples being included in the current definition is a catch-22. 
Homosexuality is not in sync with evolution. 
S1 further states that the only connection between mating couples and marriage are cultural.

S2 defines marriage as a commitment between two people who love one another. Using a hypothetical marriage between a 65 year old woman and a 68 year old man as an example, argues that reproduction has no bearing on the definition of marriage, and that without a harm involved, equality of rights is what is important. S2 believes that personal opinions are subjective and should not be used as the basis to limit the rights of the minority.

