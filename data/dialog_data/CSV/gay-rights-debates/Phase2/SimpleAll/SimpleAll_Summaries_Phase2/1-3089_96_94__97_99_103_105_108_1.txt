
----------
D0
----------
S1 feels that there is a silent majority of people who appose gay marriage. 
The silent majority have spoken in the polls by electing Bush and not Kerry for president. 
He uses the argument that Bush had a wider margin of victory over Kerry than Clinton had in either of his elections. 
He notes that Ohio passed a Gay Marriage amendment. 
Elected Kerry was smashed in the Ohio presidential election.
Kerry being smashed is credited to the silent majority. 
He states that it is the silent majority that make the polls used by the left wing media indicating that the presidents approval rating is down.
This indicates the economy is bad and the war in Iraq is going badly. 
He feels that morals and the gay marriage issue played a large role in the presidents victory.
   S2 states that the people of Ohio were equally as vocal about their views on gay marriage.
    The people of Ohio choice for president as any one else.
    The people of Ohio are therefore not a silent majority. 
   He notes that Clinton won by fewer total votes.
   Clinton carried a larger percentage of votes.
    Clinton had a greater amount of support than Bush. 
   He feels that due to the polls being accurate going into the election the term silent majority does not apply. 
   He notes that the majority's desire to suppress the minority likely played a  role in Bush's victory.
   The polls suggest that it was a very small role.


----------
D1
----------
S1 feels that the silent majority in Ohio spoke out about the issue of the state's passing of a gay rights amendment. 
S1 compares it to the large percentage of voters in past presidential elections. 
Morals and beliefs in the state were the reasons for the Republican presidential candidate, George Bush, winning by a larger percentage than either democratic candidate, John Kerry and Bill Clinton, in the past elections. 

S2 rebuts that the majority of people in Ohio were not silent about their views on gay marriage or on their choice for president.
The people in that state were very vocal about their views. 
He states that Bush did win by a larger percentage than Clinton.
The increase in population was the determining factor for those numbers. 
He feels that majority's desire to oppress the minority, in Ohio, did play some factor in Bush's win. 
He uses percentages from various candidates, like Clinton, Bush, Dole, Kerry and Gore to back up his argument. 
He disagrees that Bush was more supported than Clinton.


----------
D2
----------
S1 notes that John Kerry was expected to win the election in Ohio.
 John Kerry lost. 
He attributes this loss to the disapproval of homosexuality by Ohioans.
Disapproval of homosexuality is indicated by the passing of a gay marriage amendment and their support for President Bush.
He claims Bush won by a larger margin than President Clinton. 
S2 rejects the claim that the people of Ohio were silent about their views of same-sex marriage.
S2 claims that President Bush won with a larger margin.
 President Clinton still won by a larger percentage.
Bush won due to the increased population and larger voter turnout in President Bush's election. 
S1 claims that population is irrelevant.
S1 cites several statistics showing a low level of support for President Clinton.
S1 claims that the same-sex marriage issue definitely had an effect on the outcome of the Ohio election. 
S2 again responds with statistics showing that President Clinton won by larger percentages than President Bush. 
He does not deny that the same-sex marriage issue was relevant.
He denies that there was a silent majority.


----------
D3
----------
S1 believes that since Ohio passed a Gay Marriage amendment, it was the silent majority speaking.
It was the same as when President Bush was re-elected by a large margin. 
This person believes that morals and homosexual marriage had a large bearing on the election's eventual outcome. 
S1 believes that because of the silent majority Kerry did not win in that state.
Kerry was confident in his victory in the polls in Ohio.

S2 does not believe that the voters of Ohio were silent in any manner regarding the issue of homosexual marriage or their choice for president. 
S2 believes that Bush won by a larger margin than Bill Clinton.
It doesn't mean that Clinton did not win by a larger percentage. 
This person believes that population increases and higher voter turnout is what won President Bush the election. 
They explain that percentages were relatively low for the two primary candidates during President Clinton's election periods.
The difference in the elections was that there was a competent third party running in both.


----------
D4
----------
S1 believes in a conservative silent majority that negates liberal or left polling.
The silent majority votes in a politically correct and like minded conservative fashion. They want to defeat liberal agendas such as gay marriage.  
S2 doesn't think the conservatives are silent. 
They both cite various election results from the past to debate the premise that there is a silent conservative majority.  
S2 doesn't believe there is a silent conservative majority. 
S2 offers statistics to suggest that President Clinton was better supported by the electorate than President George W. Bush.  
S1 is clearly a republican.
S1 repeatedly refers to President Clinton as "Slick Willie".
S2 is somewhat more guarded about his party affiliation in an apparent attempt to argue the matter from a point of neutrality.
S2 both asserts Clinton's superior level of support over Bush.
S2 simultaneously implies that he, S2, is not a Clinton supporter.  
S1 argues that Kerry expected to win Ohio. 
The silent majority surprised him.  
S2 replies that Kerry wasn't so sure he was going to win Ohio.

