
----------
D0
----------
S1 argues that main stream media is biased for Obama on the gay marriage issue. 
S2 states that every Republican presidential candidate is anti-gay marriage and pro-sodomy bans. 
According to S2, unlike the Republicans, Obama has refrained from attempting to make his personal opinion law. 
Further, Obama has done more for gay rights than any other president.
S1 accuses S2 of fear mongering. 
S1 accuses S2 of misrepresenting the Republican stance on gay marriage. 
S2 accuses the Republican party of fascism and a zero-tolerance position regarding gay marriage. 
S1 accuses S2 of misrepresenting the nature of Republican opposition. 
S2 notes that the United States has a history of oppressing minorities, such as Native Americans and African Americans.
History of oppressing minorities could be extended to include gay citizens. 
S1 notes that these are past not present US policies and unlikely to happen again. 
S2 is not so sure. 
S1 asks if S2's solution is to put a protection for gays in the US constitution. 
S2 proposes limiting religious influence upon society as a suitable means of providing protection for homosexuals in America.


----------
D1
----------
S1 does not believe that new laws will call for the imprisonment or execution of homosexual citizens in the United States. 
S1 believes that there is more danger of economic collapse under Obama than those laws concerning the gay issues. 
S1 doesn't think that S2's fears are warranted. 
S1 thinks extremes cannot happen again.

S2 believes that every Republican candidate has stood on a platform that opposes gay rights, such as marriage and civil unions.
S2 believes that every Republican candidate wishes to reinstate sodomy laws across the country. 
S2 argues that Obama personally believes that marriage is between one man and one woman
Obama does not wish to push his personal beliefs into law.
Instead Obama has supported marriage equality as much as he can. 
S2 argues that no president has done more for the homosexual community as Obama. 
S2 argues that there is a legitimate danger of laws going too far.
S2 argues that imprisonment of homosexuals can happen. 
Imprisonment of different races such as Native Americans and Japanese was federally accepted in the past.


----------
D2
----------
S1 believes that the gay-rights movement and S2 in particular, are paranoid.  
There is not much being said to support S1's argument
Most of the conversation is dominated by S2.
S2 argues that economic issues are more likely to hurt those in the gay rights movement than any possible laws.
S2 has concerns about the possibility of creating a true second class citizens of the GLBTQ community. 
S2 refers to Obama being the only politician to put his beliefs aside.
Obama supports gay marriage.
S2 references past injustices to support his claims
It is possible that gays will be lawfully discriminated against.  
S2 mentions Native Americans and the genocide they experienced.
S2 mentions Japanese Americans and the internment camps.
S2 mentions African Americans and past slavery.
S2 worries that history will be allowed to repeat itself in the form of discrimination of homosexuals.
S2 points out recent attempts at renewing sodomy laws by some politicians.  
At the end S2 seems to be in favor of an amendment to the Constitution to protect gay rights.


----------
D3
----------
S1 references a Yahoo News article titled "On Gay Marriage, Media Gives Obama a Pass." 
S1 accuses S2 of being a fear-monger
S1 claims that it is absurd to claim that people are attempting to imprison or execute homosexuals in the United States.
The police are not currently attempting to prevent people from engaging in homosexual behaviors. 

S2 claims that every republican presidential candidate in the previous election wanted to ban same-sex marriage. 
Every republican presidential candidate in the previous election wanted to persecute homosexuals by reinstating anti-sodomy laws. 
He mentions that Obama believes that marriage is between a man and a woman.
Obama is not willing to legislate based on his personal beliefs.
Obama has been a strong supporter of marriage equality.
Obama is more a supporter than any other president in US history. 
S2 accuses republicans of basing their legislation of personal beliefs. 
S2 claims that the rights of homosexuals should be protected through a constitutional amendment.
A constitutional amendment should restricts religion's role in society. 
A constitutional amendment should prevent future human rights atrocities like those that occurred to Native Americans and Japanese Americans.


----------
D4
----------
S1 liked an editorial about Obama. 
S2 claims every Republican candidate wants gays to be in jail.
Every Republican candidate reinstated anti-sodomy laws and banned gay marriage. 
S2 claims Obama has done more for gays than any other president. 
Obama will come out for full marriage equality.
Obama is not trying to impose his views on others.
Republicans are trying to impose his views on others. 
S1 references S2 as fear-mongering and engaging in gay-pride politics. 
S2 references the Republicans as being like the brown shirts. 
S2 claims that Rick Perry supports a Ugandan law that would execute gays. 
S1 accuses S2 of paranoia. 
S2 references Japanese internment 
S2 references killings of Native Americans by the military. 
S1 states that these events haven't happened since then. 
S2 states that they could happen again. 
S1 makes rude comments about the police not closing meat-markets or hook-up spots. 
S2 references that internment and killings has happened under this constitution and can again. 
S1 asks if S2 wants a change to the constitution. 
S2 says a limit to religions role in society.

