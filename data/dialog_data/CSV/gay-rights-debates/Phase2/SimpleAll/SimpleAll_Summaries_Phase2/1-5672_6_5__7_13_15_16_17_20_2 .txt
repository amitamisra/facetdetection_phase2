
----------
D0
----------
S1 links same-sex marriage to negative effects on society and traditional families.
He is still against the constitutional amendment to ban same-sex marriages on the grounds that it is an inappropriate topic for a constitutional amendment. 
He references a person named Colson who believes that the terms "marriage" and "family" need to meet specific criteria.
Legalizing same-sex marriage would change those criterias.
This would lead to changes in the meaning of traditional marriage. 
He also claims that Colson believes that traditional families have the best outcomes in regards to criminal behavior of offspring. 

S2 rejects the claim that same-sex marriage has a negative effect on society.
S2 suggests that traditional marriage was on the decline long before same-sex marriage was ever legalized. 
He also questions what negative effect same-sex marriage could have on society.
He claims that there are no studies suggesting that two heterosexual parents fare any better than two homosexual parents.
There is no difference in reality.
There is no reason to predict that same-sex marriages would affect crime.


----------
D1
----------
S1 feels that Colson has the most compelling argument for the Marriage Protection Amendment yet.
S1 still does not support it personally.
Changing the constitution isn't right. 
He feels that the legalization of gay marriage would redefine what family is in a negative manner. 
He feels that the broadening of what constitutes the definition of marriage would somehow weaken traditional marriage. 
This, he says, would lead to more crime.
  S2 feels that the problems with traditional marriage in this country began long before gay marriage began to be legalized. 
  He makes an analogy of a man jumping out of a plane before gay marriage, and gay marriage being blamed. 
  He does not feel that gay marriage has any effect on traditional marriage at all. 
  Society is not harmed by the inclusion of gay marriage. 
  He believes that any statement that says that gay marriage harms society should be proven before being widely talked about. 
  He feels that S1 is misrepresenting himself.
  S1 is twisting his argument to make statements that are different from what Colson had written.


----------
D2
----------
S1 doesn't agree that amending the Constitution is the right thing to do.
They find the argument connecting society and traditional family compelling regardless. 
They believe that the definition of the words family and marriage are being broadened and redefined. 
They argue that those who oppose this change believe that marriage and family fit a certain criteria to truly be a marriage.
That criteria is a traditional marriage. 
They agree on some points with these people.
They believe that adopting homosexual marriage will have an effect on that definition. They do not agree on other points. S2 doesn't believe that there will be an effect on traditional marriage because of the adoption of homosexual marriages. 
They believe that the traditional family has its own problems.
The traditional family has been in decline long before the issue was brought up. 
They don't agree that connecting society and traditional family to homosexual marriage is logical or compelling. 
There is reasonable evidence for the contrary. 
They don't believe there is substantial proof to argue that homosexual marriage will affect society.


----------
D3
----------
S1 states that the author is not drawing a link between gay marriage and crime.
The author is linking gay marriage to the overall effect on family and community. 
S1 is against the proposed amendment.
S1 does not believe that the constitution should be changed for this. 
S1 feels the author has a compelling argument.
Colson is not saying gay marriage is responsible for increased crime.   
S1 is saying that Colson is right when he says that broadening the defintion of marriage will impact "traditonal" marriage.

S2 feels this argument has no merit. 
That traditional marriage is not effected just because gay marriage is legal. 
One has no impact on the other. 
Straight marriage has been in decline for years long before gay marriage was legalized.  
For Colson to state that traditional marriage will deteriorate is preposterous. 
S2 states that a household with two parents will obviously do better in raising their children.
With two parents there is twice the capability to care for the child.
The sex of those two parents is irrelevant. 
S2 also states that Colson is saying that gay marriage yields more criminals.
Colson links gay marriage to a decrease in traditional families.
Decreases in traditional marriages result in more criminals.


----------
D4
----------
S1 considers Colson's article on gay marriage to be compelling.  
He reads it as a comment on society generally. 
He defends the author's views on traditional families.
He does not see it as a direct attack on same sex marriage. 
He refers to the statistics on children of traditional families being at a lower risk of ending up in prison.  
He later mentions that he does not necessarily agree with this conclusion that was drawn by Colson.
S2 argues against any protection of the traditional family pointing out that it has been having its own issues.  
He sees Colson's article as a direct attack on any family other than what has been labeled traditional- two heterosexual parents raising children.  
He believes that the article is inferring that homosexual parents would end up raising children who commit crimes.  
He also argues with the concept that homosexual marriages will have a direct negative impact on heterosexual marriages.
Any claims need to be backed up to be reliable.
There is nothing to back up any claims on this issue.

