
----------
D0
----------
S1 and S2 discuss the United States Constitutional legal system.
S1 and S2 discuss whether and how same sex marriage can be banned.  
S1 believes the concept of marriage has evolved dramatically over time.
S1 believes Supreme Court can now declare marriage to be a fundamental right not to be denied to same sex couples.   
S2 believes it is a legislative matter which should be left to voting.  
S1 argues that fundamental rights are protected by the Constitution.
S1 argues that fundamental rights are not subject to a plebescite.  
S2 accuses S1 of believing in an activist judiciary.
S2 accuses S1 of evolving Constitutional standards which usurp the role of the legislature.  
S2 also criticizes Loving v. Virginia as incorrect in reasoning.
Loving v. Virginia is an example of judicial activism.


----------
D1
----------
S1 thinks the definition of marriage is subject to change. 
As an example S1 points to an old definition of marriage.
In an old definition of marriage a woman lost her identity into that of a man. 
S1 believes fundamental rights are not subject to vote.
S1 cites Loving v. Virginia as an example. 
He believes the courts are an intermediate body between the people and legislature.
To ban gay marriage one must prove it isn't a right.
To ban gay marriage one must give compelling evidence against it. 
S2 wonders what marriage being a fundamental right has to do with defining marriage. 
If marriage is subject to change then he asks how to change it if not by vote. 
He takes issue with the judiciary becoming the legislative branch. 


----------
D2
----------
The issue here is the definition of marriage.
The issue is what government body or level has the right to change marriage definition.



S1 believes that courts and federal government should be able to rule on the definition of marriage.
S1 cites several previous cases in the past. 
In the past the federal government stepped up to introduce legislature. 
S1 believes that "fundamental rights" are not subject to state votes.
"Fundamental rights" can be decided by government.



S2 believes that each state should decide the definition of marriage.
S2 questions what "fundamental rights" have to do with the "definition" of marriage. 
S2 believes that it should left up to a vote to decide the definition of marriage.
S2 believes that legislature should not be confused with judiciary.




----------
D3
----------
Two people are discussing gay marriage laws.  
S1 states that the traditional definition of marriage is human made.
Definition of marriage can be changed to meet changing times.  
S2 contends that the legal definition is human made and can change.
We're in a democracy.
We must change legal definition by vote.  
He also states that so far votes favor a "one man one woman" definition of marriage.  
S1 states that the courts were designed to be an intermediate between people and legislature.
If same gender marriage can be banned by state action it must be proven that marriage is not a fundamental right.  
S2 states that by allowing this the judiciary branch becomes the legislative branch.
Judiciary branch is not legislative branch.


----------
D4
----------
Two people are debating gay marriage rights.  
S1 is pro gay marriage while S2 appears against.  
S1 feels the constitution can evolve with changing times.
Constitution is a man-made construct.
S2 cites the fact 26 states still consider the sanctity of marriage to be one man and one woman.  
S2 also advises a key point to the nation is the basis of laws are voted upon.  
S2 does not feel S1's statements are correct regarding whether or not marriage is a right.  
S1 cites Loving v. Virginia in argument marriage is not a human construct. 
S1 also advises marriage is a fundamental right.
Marriage should be a fundamental right for all.
Marriage should not be determined by each state individually.

