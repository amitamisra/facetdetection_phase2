PREPROCESSING :STEMMING, cos Distance, num_cluster=70Nouns_Verb_AdJ

 NEW CLUSTER  0



Terrorists would also use strategy
key  1-546_20_19__24_27_28_29_33_34_1_user7.pyr

Giving up rights means the terrorists win
key  1-546_20_19__24_27_28_29_33_34_1_user7.pyr

Trained and determined terrorists would overwhelm armed citizens
key  1-546_20_19__24_27_28_29_33_34_1_user7.pyr

Danger is in confusing citizens and terrorists
key  1-546_230_229__231_232_233_234_235_3_user7.pyr

There is no reported injuries in citizens carrying guns onto planes
key  1-546_230_229__231_232_233_234_235_3_user7.pyr

A citizen can't gain the skills to defend planes
key  1-546_230_229__231_232_233_234_235_3_user7.pyr


 NEW CLUSTER  1



The cop murder proves that gun control is a good idea
key  1-10756_37_35__39_42_43_44_1_user9.pyr

No one can prove that gun owners are safer than non gun owners.
key  1-1543_41_40__44_47_49_51_1_user8.pyr

Gun ownership is comparable to car ownership
key  1-173_68_67__69_70_74_77_79_81_87_1_user9.pyr

Gun control is wrong
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr

Own guns
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr

Labeled as a gun hater
key  1-546_140_139__141_142_143_144_145_146_2_user9.pyr

Gun ownership is about controlling destiny
key  1-546_20_19__24_27_28_29_33_34_1_user7.pyr

Gun control would be a good idea but not gun ban
key  1-546_230_229__231_232_233_234_235_3_user7.pyr

Government can oppress with no gun control
key  1-671_3_2__4_5_6_7_8_9_10_11_13_14_1_user7.pyr

whether or not allowing guns on school campus is a good idea.
key  1-8099_19_16__23_24_27_31_2_user8.pyr

It is more or less a good idea to have guns on school campus.
key  1-8099_19_16__23_24_27_31_2_user8.pyr

Gun owners will not advocate for gun control.
key  1-8143_42_37__45_50_55_56_1_user8.pyr

Results prove the hypothesis was simply wrong.
key  1-8143_42_37__45_50_55_56_1_user8.pyr

Gun ownership has declined
key  1-8543_111_109__114_131_132_135_139_147_148_1_user9.pyr

Gun ownership is higher now
key  1-8543_111_109__114_131_132_135_139_147_148_1_user9.pyr

Gun control will slow gun crime
key  1-8543_150_143__151_153_156_160_162_164_3_user9.pyr


 NEW CLUSTER  2



Well armed Iraqi public didn't prevent Saddam Hussein's genocide
key  1-342_9_8__12_13_15_16_1_user7.pyr

Shiites revolted against Hussein
key  1-342_9_8__12_13_15_16_1_user7.pyr

No surveys show favorable outcomes when one has a gun in public.
key  1-9874_30_3__32_40_41_55_59_82_86_91_92_2_userA.pyr


 NEW CLUSTER  3



No permanent records will exist anywhere except in gun shops.
key  1-10931_104_103__105_106_107_108_109_110_111_112_113_118_119_3_userA.pyr

Since 1968, it is illegal to take sales records from gun shops.
key  1-10931_104_103__105_106_107_108_109_110_111_112_113_118_119_3_userA.pyr

A forced sale of guns
key  1-671_3_2__4_5_6_7_8_9_10_11_13_14_1_user7.pyr

Guns will always be available
key  1-8543_123_121__129_130_133_134_2_user9.pyr

It is untrue to imagine if guns didn't exist
key  1-8543_123_121__129_130_133_134_2_user9.pyr

Lack of gun availability or increased cost will not deter criminals
key  1-9059_22_15__23_28_43_70_76_3_user9.pyr

A record of private party gun sales should be kept
key  1-9294_9_6__10_13_18_19_1_user9.pyr

Recorded private gun sales could be available to law enforcement
key  1-9294_9_6__10_13_18_19_1_user9.pyr


 NEW CLUSTER  4



2nd Amendment divides military power between states and the federal government
key  1-342_9_8__12_13_15_16_1_user7.pyr

2nd Amendment prevents one taking all the military power
key  1-342_9_8__12_13_15_16_1_user7.pyr

Dictatorship is defined as one retaining all military power
key  1-342_9_8__12_13_15_16_1_user7.pyr

Buybacks are the government burning money
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

27 amendments currently limit government's power.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr

The constitution granted power to the federal government.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr

The other powers were left to the states.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr

The government would contain various exceptions to powers that were not granted.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr


 NEW CLUSTER  5



Other states cannot be controlled by the rulings of a different state
key  1-10756_37_35__39_42_43_44_1_user9.pyr

States can not ignore home rule issues
key  1-10756_37_35__39_42_43_44_1_user9.pyr

A confiscations is essentially creating a police state
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

United States citizens had to claim natural rights as they were not granted.
key  1-764_66_63__67_70_74_75_2_user8.pyr

Britain is a police state
key  1-8003_38_37__39_40_41_42_43_45_47_49_1_user9.pyr

The loss of rights would result in a tyrannical police state
key  1-8003_38_37__39_40_41_42_43_45_47_49_1_user9.pyr

75% of United States households do not have firearms
key  1-8543_111_109__114_131_132_135_139_147_148_1_user9.pyr

1/3 of households owns guns
key  1-8543_111_109__114_131_132_135_139_147_148_1_user9.pyr

After the Civil War, the 14th Amendment took away some of state's autonomy and free will.
key  1-9841_196_194__199_202_204_205_206_207_208_209_210_4_userA.pyr

Lincoln's justification for war was questionable.
key  1-9841_196_194__199_202_204_205_206_207_208_209_210_4_userA.pyr

The Civil War regarded issues including states rights.
key  1-9841_196_194__199_202_204_205_206_207_208_209_210_4_userA.pyr


 NEW CLUSTER  6



California has high crime rates
key  1-10756_37_35__39_42_43_44_1_user9.pyr

More gun control is good because of the high crime rate in Californian cities
key  1-10756_37_35__39_42_43_44_1_user9.pyr

Police endanger others lives when engage in shootouts.
key  1-10996_14_10__17_19_22_25_27_28_1_userA.pyr

Canada, England and Australia are examples for gun buyback
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

Carry a gun because of crime
key  1-737_78_77__80_81_82_83_84_1_user9.pyr

Australia has less crime
key  1-737_78_77__80_81_82_83_84_1_user9.pyr

Cops do not respond well to crime in Australia
key  1-737_78_77__80_81_82_83_84_1_user9.pyr

Crime in Australia because they didn't have guns
key  1-737_78_77__80_81_82_83_84_1_user9.pyr

People commit crimes even without guns
key  1-737_78_77__80_81_82_83_84_1_user9.pyr

There are gang rapes in Australia
key  1-737_78_77__80_81_82_83_84_1_user9.pyr

There is crime in the U.S.
key  1-737_78_77__80_81_82_83_84_1_user9.pyr

Britain has lower rates of crime or murder
key  1-8003_38_37__39_40_41_42_43_45_47_49_1_user9.pyr

Keep guns out of DC by using US/Mexican border methods
key  1-8543_150_143__151_153_156_160_162_164_3_user9.pyr

In England and Australia the gun control did not work
key  1-9059_22_15__23_28_43_70_76_3_user9.pyr

Australia is dealing with gun crimes.
key  1-9836_92_88__94_101_103_108_110_115_2_userA.pyr

Guns are being brought into Australia.
key  1-9836_92_88__94_101_103_108_110_115_2_userA.pyr

He lives in Australia.
key  1-9836_92_88__94_101_103_108_110_115_2_userA.pyr

About 63% of guns recovered in Toronto came from the US.
key  1-9884_5_4__7_9_11_13_18_1_userA.pyr

Most guns used in crimes aren't recovered.
key  1-9884_5_4__7_9_11_13_18_1_userA.pyr


 NEW CLUSTER  7



States can deny permits when there are felony charges or warrants for arrest
key  1-173_68_67__69_70_74_77_79_81_87_1_user9.pyr

The gun permit was denied
key  1-173_68_67__69_70_74_77_79_81_87_1_user9.pyr

Denies his poll is biased.
key  1-8143_42_37__45_50_55_56_1_user8.pyr

Poll results do not match his preconception.
key  1-8143_42_37__45_50_55_56_1_user8.pyr

Poll's accuracy is questionable.
key  1-8143_42_37__45_50_55_56_1_user8.pyr


 NEW CLUSTER  8



Recent rulings will not repeal existing gun regulations
key  1-10145_4_3__5_9_21_29_1_user9.pyr

Support recent supreme court rulings
key  1-10145_4_3__5_9_21_29_1_user9.pyr

Illegal weapons have no FMV
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

The Supreme Court ruled that the rifles were illegal.
key  1-8635_36_33__38_40_41_46_49_53_56_1_user8.pyr

A man owning a rifle was arrested for possessing an illegal firearm.
key  1-8635_36_33__38_40_41_46_49_53_56_1_user8.pyr


 NEW CLUSTER  9



No political party is perfect.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

There are flawed individulas in every political party.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

No validating statistics are provided.
key  1-8099_19_16__23_24_27_31_2_user8.pyr

The statistics are often flawed.
key  1-9874_30_3__32_40_41_55_59_82_86_91_92_2_userA.pyr


 NEW CLUSTER  10



People owned guns doesn't prove they were protected to do so.
key  1-10121_22_21__23_28_30_34_37_1_userA.pyr

people owned guns shows that they were protected to do so.
key  1-10121_22_21__23_28_30_34_37_1_userA.pyr

Gun laws steal rights from people
key  1-10145_4_3__5_9_21_29_1_user9.pyr

God will protect the people, not guns.
key  1-10996_14_10__17_19_22_25_27_28_1_userA.pyr

Guns are more a security risk than protection
key  1-546_20_19__24_27_28_29_33_34_1_user7.pyr

Only criminals and those declared insane should lose the right to defend themselves
key  1-546_20_19__24_27_28_29_33_34_1_user7.pyr

The right to own guns was granted in order to protect against tyranny rather than hunting.
key  1-764_66_63__67_70_74_75_2_user8.pyr

If the killers didn't have guns, people would not be hurt
key  1-8543_123_121__129_130_133_134_2_user9.pyr

The state and federal do not protect the same rights.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr

The state and federal protect the same rights.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr

Gun possession increases the odds of being hurt by four to five times.
key  1-9874_30_3__32_40_41_55_59_82_86_91_92_2_userA.pyr

Gun didn't protect people from being shot.
key  1-9874_30_3__32_40_41_55_59_82_86_91_92_2_userA.pyr

People successfully defended themselves with a gun.
key  1-9874_30_3__32_40_41_55_59_82_86_91_92_2_userA.pyr


 NEW CLUSTER  11



The RKBA was connected with militia service.
key  1-10121_22_21__23_28_30_34_37_1_userA.pyr

Australia is unprepared for rouge police agents.
key  1-10996_14_10__17_19_22_25_27_28_1_userA.pyr

Rogue police officers are rare in Australia, and anywhere.
key  1-10996_14_10__17_19_22_25_27_28_1_userA.pyr

solutions if rouge officers and military start killing people
key  1-10996_14_10__17_19_22_25_27_28_1_userA.pyr

Learn under an ex-police officer
key  1-9057_129_126__131_134_138_142_3_user9.pyr

A militia group in the Midwest planned to kill police.
key  1-9836_25_22__27_34_35_37_39_1_userA.pyr

People who kill police officers commonly use drugs.
key  1-9836_25_22__27_34_35_37_39_1_userA.pyr

The militia were not responsible for police deaths.
key  1-9836_25_22__27_34_35_37_39_1_userA.pyr


 NEW CLUSTER  12



The ATF only requested copies of the records not the original ledgers.
key  1-10931_104_103__105_106_107_108_109_110_111_112_113_118_119_3_userA.pyr

The ATF 's acts are unconstitutional.
key  1-10931_104_103__105_106_107_108_109_110_111_112_113_118_119_3_userA.pyr

It is legal for the ATF to request copies of records.
key  1-10931_104_103__105_106_107_108_109_110_111_112_113_118_119_3_userA.pyr

Protests against the ATF is a conspiracy.
key  1-10931_104_103__105_106_107_108_109_110_111_112_113_118_119_3_userA.pyr

The ATF are withholding the records illegally from Congress.
key  1-10931_104_103__105_106_107_108_109_110_111_112_113_118_119_3_userA.pyr

The ATF is being charged with contempt of Congress.
key  1-10931_104_103__105_106_107_108_109_110_111_112_113_118_119_3_userA.pyr

The claimed defense of rights is undermined by the Patriot Act
key  1-8003_38_37__39_40_41_42_43_45_47_49_1_user9.pyr

Conspiracies are everywhere.
key  1-9836_25_22__27_34_35_37_39_1_userA.pyr


 NEW CLUSTER  13



gun restrictions laws viloate rights
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr

Restrict buying new guns
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

Casualties are reduced by gun regulation
key  1-8543_123_121__129_130_133_134_2_user9.pyr

Regulations have not reduced gun violence
key  1-8543_123_121__129_130_133_134_2_user9.pyr

The goal of firearm restriction is to reduce supply to criminals
key  1-9059_22_15__23_28_43_70_76_3_user9.pyr

Have proof that restricting firearms would reduce crime before infringing on constitutional righs
key  1-9059_22_15__23_28_43_70_76_3_user9.pyr

The goal of firearm restriction is to identify negligent and criminal gun owners
key  1-9059_22_15__23_28_43_70_76_3_user9.pyr

Guns are restricted in Australia.
key  1-9836_92_88__94_101_103_108_110_115_2_userA.pyr


 NEW CLUSTER  14



Firearm regulation is constitutional
key  1-10145_4_3__5_9_21_29_1_user9.pyr

Death is the same no matter which way it happens
key  1-546_230_229__231_232_233_234_235_3_user7.pyr

The Constitution is not enforceable.
key  1-764_66_63__67_70_74_75_2_user8.pyr

California and New York have begun confiscating weapons despite people's RKBA in the constitution.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr

The Constitution should be interpreted in a contemporary way.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr

The US government is a constitutional republic.
key  1-9841_196_194__199_202_204_205_206_207_208_209_210_4_userA.pyr


 NEW CLUSTER  15



Vigilantism is not a better alternative to a trained police force
key  1-8003_38_37__39_40_41_42_43_45_47_49_1_user9.pyr

Life experience support his conclusion.
key  1-8143_42_37__45_50_55_56_1_user8.pyr

Did not claim to have trained in real life situations
key  1-9057_129_126__131_134_138_142_3_user9.pyr

People training for the military or police force understand the difference between simulated and real danger
key  1-9057_129_126__131_134_138_142_3_user9.pyr

Train under the bullet proof monk 
key  1-9057_129_126__131_134_138_142_3_user9.pyr


 NEW CLUSTER  16



Criminals acquire firearms illegally through the black market
key  1-8543_111_109__114_131_132_135_139_147_148_1_user9.pyr

Guns must be bought from licensed dealers
key  1-8543_111_109__114_131_132_135_139_147_148_1_user9.pyr

The black market gets guns from assembly lines, stolen guns, and unscrupulous dealers
key  1-8543_111_109__114_131_132_135_139_147_148_1_user9.pyr


 NEW CLUSTER  17



You may not be able to leave the scene when your safety is being threatened
key  1-3320_45_44__46_47_48_49_1_user9.pyr

Airlines are good at safety
key  1-546_20_19__24_27_28_29_33_34_1_user7.pyr


 NEW CLUSTER  18



The mayor was actually busted on child sex charges.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

A mayor was arrested in a sex sting.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

Ted Nugent should be punished by the NRA.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

The mayor is a convicted felon.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

What mayor's arrest has to do with Ted Nugent?
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr


 NEW CLUSTER  19



Gun registration should not be mandatory
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr

Canada has not confiscated guns
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

Confiscating guns equals tyranny 
key  1-671_3_2__4_5_6_7_8_9_10_11_13_14_1_user7.pyr

The Attorney General assured gun owners that rifles were legal and not subject to the confiscation order.
key  1-8635_36_33__38_40_41_46_49_53_56_1_user8.pyr

Firearms registration has not led to confiscation.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr

Not all people are intellectually equal.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr


 NEW CLUSTER  20



 people who were not able to serve in militias were not allowed to own weapons needs to be proofed.
key  1-10121_22_21__23_28_30_34_37_1_userA.pyr

People in the UK are allowed to keep weapons with the laws.
key  1-10121_22_21__23_28_30_34_37_1_userA.pyr

An example of a prohibited weapon being used is not a good reason for the ban
key  1-10756_37_35__39_42_43_44_1_user9.pyr

Scripture mentions weapons
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr

People should not need a license to purchase a weapon
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr

You are allowed to keep the weapons you own
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

Not all guns are being banned
key  1-671_3_2__4_5_6_7_8_9_10_11_13_14_1_user7.pyr

is it okay to have a nuclear weapon 
key  1-690_2_1__3_4_5_7_1_user8.pyr

if it's okay to carry guns
key  1-690_2_1__3_4_5_7_1_user8.pyr

 ban to own nuclear weapons.
key  1-690_2_1__3_4_5_7_1_user8.pyr

Citizens should have the same access to nuclear weapons that the US does.
key  1-690_2_1__3_4_5_7_1_user8.pyr

The question is irrelevant since nuclear weapons are not for sale.
key  1-690_2_1__3_4_5_7_1_user8.pyr

California has its own automatic weapon ban in place.
key  1-8635_36_33__38_40_41_46_49_53_56_1_user8.pyr

Weapons have never been banned.
key  1-8635_36_33__38_40_41_46_49_53_56_1_user8.pyr

 banning the convertable weapons.
key  1-8635_36_33__38_40_41_46_49_53_56_1_user8.pyr

A case involving a gun that was banned but was converted.
key  1-8635_36_33__38_40_41_46_49_53_56_1_user8.pyr

Should be allowed to have a gun
key  1-9036_154_152__155_157_158_159_160_161_162_163_4_user9.pyr


 NEW CLUSTER  21



Inspections can only be done during an investigation.
key  1-8860_7_6__8_9_12_15_16_17_1_user8.pyr

Investigation can be done at anytime.
key  1-8860_7_6__8_9_12_15_16_17_1_user8.pyr

After hour inspections are a scare tactic.
key  1-8860_7_6__8_9_12_15_16_17_1_user8.pyr

After hour inspections are not a scare tactic.
key  1-8860_7_6__8_9_12_15_16_17_1_user8.pyr

The survey out of Philadelphia investigated the relationship between being shot and possession of a gun.
key  1-9874_30_3__32_40_41_55_59_82_86_91_92_2_userA.pyr


 NEW CLUSTER  22



Ballistics traces are not possible.
key  1-9884_5_4__7_9_11_13_18_1_userA.pyr

Ballistics databases only compare rounds linked to a crime.
key  1-9884_5_4__7_9_11_13_18_1_userA.pyr

Many guns aren't traceable except through ballistics.
key  1-9884_5_4__7_9_11_13_18_1_userA.pyr

Referring to forensics instead of ballistics.
key  1-9884_5_4__7_9_11_13_18_1_userA.pyr


 NEW CLUSTER  23



Do not want repeals
key  1-10145_4_3__5_9_21_29_1_user9.pyr

Liberals want to reduce moral standards.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

Liberals want free money for doing nothing.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

Liberals are praised as being perfect.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

Liberals get rid of religion.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

Liberals let the government have more and more power.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

Liberals worship the government.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

Obama is a liberal.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

is Obama  a liberal?
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

People don't want some groups to have guns
key  1-8543_150_143__151_153_156_160_162_164_3_user9.pyr


 NEW CLUSTER  24



Other states could adopt gun controls like California
key  1-10756_37_35__39_42_43_44_1_user9.pyr

Propose that the U.S. adopt California's standards
key  1-10756_37_35__39_42_43_44_1_user9.pyr

States adopting controls would depend on the voters
key  1-10756_37_35__39_42_43_44_1_user9.pyr

Support is lacking for adopting California's gun laws as US standards
key  1-10756_37_35__39_42_43_44_1_user9.pyr

He is judging others by his own standards.
key  1-8143_42_37__45_50_55_56_1_user8.pyr

It's best to judge everything by the same set of standards.
key  1-8143_42_37__45_50_55_56_1_user8.pyr


 NEW CLUSTER  25



make jokes instead of offering up a valid response to the topic.
key  1-10996_14_10__17_19_22_25_27_28_1_userA.pyr

how the post relates to gun control
key  1-546_140_139__141_142_143_144_145_146_2_user9.pyr

Make identical posts
key  1-546_140_139__141_142_143_144_145_146_2_user9.pyr

Not making any judgements about gun laws
key  1-9036_154_152__155_157_158_159_160_161_162_163_4_user9.pyr

Broken laws does not make them ineffective
key  1-9059_22_15__23_28_43_70_76_3_user9.pyr

Juries make the final decision in court cases.
key  1-9836_25_22__27_34_35_37_39_1_userA.pyr


 NEW CLUSTER  26



Citizens do not support stricter laws
key  1-10145_4_3__5_9_21_29_1_user9.pyr

A liberal mayor wants stricter gun laws.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

Law enforcement should be armed to deal with shooting incident.
key  1-10996_14_10__17_19_22_25_27_28_1_userA.pyr

Gun laws don't effect criminals
key  1-1103_28_27__29_30_31_32_33_34_1_user9.pyr

Laws provide punishment for criminals
key  1-1103_28_27__29_30_31_32_33_34_1_user9.pyr

Criminals disregard laws
key  1-1103_28_27__29_30_31_32_33_34_1_user9.pyr

Criminals break laws
key  1-1103_28_27__29_30_31_32_33_34_1_user9.pyr

Gun control laws affect law-abiding citizens
key  1-1103_28_27__29_30_31_32_33_34_1_user9.pyr

Gun laws do effect criminals
key  1-1103_28_27__29_30_31_32_33_34_1_user9.pyr

vilify law-abiding citizens defending themselves
key  1-546_230_229__231_232_233_234_235_3_user7.pyr

Support new law gun buyback
key  1-671_3_2__4_5_6_7_8_9_10_11_13_14_1_user7.pyr

Taking the law into your own hands is the definition of vigilantism
key  1-8003_38_37__39_40_41_42_43_45_47_49_1_user9.pyr

If current laws aren't being enforced, new ones won't be either.
key  1-8635_36_33__38_40_41_46_49_53_56_1_user8.pyr

can law enforcement find the owner through the manufacturer and/or dealer
key  1-9294_9_6__10_13_18_19_1_user9.pyr

The manufacturer could tell law enforcement where to look for an owner
key  1-9294_9_6__10_13_18_19_1_user9.pyr

The manufacturer would get involved with law enforcement
key  1-9294_9_6__10_13_18_19_1_user9.pyr

Criminals don't obey laws
key  1-9294_9_6__10_13_18_19_1_user9.pyr


 NEW CLUSTER  27



Nuke sellers would sell if they were allowed.
key  1-690_2_1__3_4_5_7_1_user8.pyr

Nobody would sell nukes.
key  1-690_2_1__3_4_5_7_1_user8.pyr

Firearm manufacturers sell to distributors not dealers
key  1-9294_9_6__10_13_18_19_1_user9.pyr

Distributors sell to dealers
key  1-9294_9_6__10_13_18_19_1_user9.pyr

The manufacturer can tell them the distributor
key  1-9294_9_6__10_13_18_19_1_user9.pyr


 NEW CLUSTER  28



Rights are created by society
key  1-1103_28_27__29_30_31_32_33_34_1_user9.pyr

Numbers can be deceiving as to what creates a problem.
key  1-1543_41_40__44_47_49_51_1_user8.pyr

 how the guns were traced without their serial numbers.
key  1-9884_5_4__7_9_11_13_18_1_userA.pyr

A gun can be traced wiouth the serial numbers in some cases.
key  1-9884_5_4__7_9_11_13_18_1_userA.pyr

A large number of guns have their serial numbers removed.
key  1-9884_5_4__7_9_11_13_18_1_userA.pyr


 NEW CLUSTER  29



Cite kids possessing military-grade firearms
key  1-9059_22_15__23_28_43_70_76_3_user9.pyr

Children should be taught how to handle a gun early.
key  1-9836_92_88__94_101_103_108_110_115_2_userA.pyr

Kids are prepared for a killing spree at school.
key  1-9836_92_88__94_101_103_108_110_115_2_userA.pyr

Learning to use guns teaches kids to respect firearms.
key  1-9836_92_88__94_101_103_108_110_115_2_userA.pyr

Mocking  about how kids handle guns early.
key  1-9836_92_88__94_101_103_108_110_115_2_userA.pyr


 NEW CLUSTER  30



The right to keep and bear arms only applies to the militia.
key  1-10121_22_21__23_28_30_34_37_1_userA.pyr

The RKBA is not a preexisting condition by the 2nd Amendment.
key  1-10121_22_21__23_28_30_34_37_1_userA.pyr

The Second Amendment protects a preexisting right.
key  1-10121_22_21__23_28_30_34_37_1_userA.pyr

Owning a gun is a violation of others' right to life
key  1-1103_28_27__29_30_31_32_33_34_1_user9.pyr

The 2nd amendment preserves rights and not grant them
key  1-1103_28_27__29_30_31_32_33_34_1_user9.pyr

Rights are being violated.
key  1-1543_41_40__44_47_49_51_1_user8.pyr

Illogical arguments aren't a violation of rights.
key  1-1543_41_40__44_47_49_51_1_user8.pyr

Second amendment rights can be removed by a felony conviction
key  1-173_68_67__69_70_74_77_79_81_87_1_user9.pyr

The federal court disagreed her constitutional right had been violated
key  1-173_68_67__69_70_74_77_79_81_87_1_user9.pyr

It is a right to own weapons
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr

You cannot be pro gun rights if you're willing to limit those rights
key  1-546_140_139__141_142_143_144_145_146_2_user9.pyr

Giving up Second Amendment rights
key  1-546_20_19__24_27_28_29_33_34_1_user7.pyr

Ban right to bear arms using undocumented statements
key  1-546_230_229__231_232_233_234_235_3_user7.pyr

No one says that gun carrying endangers anyone, increases crime or violates rights.
key  1-690_2_1__3_4_5_7_1_user8.pyr

The right to carry a gun isn't a natural right.
key  1-764_66_63__67_70_74_75_2_user8.pyr

The Bill of rights isn't a contract.
key  1-764_66_63__67_70_74_75_2_user8.pyr

The rights in the Bill of Rights are natural rights.
key  1-764_66_63__67_70_74_75_2_user8.pyr

Must not relinquish rights
key  1-8003_38_37__39_40_41_42_43_45_47_49_1_user9.pyr

Those who claim 2nd Amendment rights are not vigilantes
key  1-8003_38_37__39_40_41_42_43_45_47_49_1_user9.pyr

Confiscation has only occurred in countries that did not have the right to keep and bear arms in their constitution.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr

44 states have the right to bear arms in their state Bill of Rights.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr

The government is limited by the Constitution and Bill of Rights.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr

The 2nd Amendment gives the right to own guns.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr

The Bill of Rights only restricts the government.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr

Before the 14th Amendment, the Constitution and the Bill of Rights do not apply to the state.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr

States have their own Bill of Rights.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr

The Bill of Rights is unnecessary and dangerous to include.
key  1-9841_142_140__147_149_151_153_157_158_6_userA.pyr

The Bill of Rights gave states free will.
key  1-9841_196_194__199_202_204_205_206_207_208_209_210_4_userA.pyr


 NEW CLUSTER  31



 confiscation or a government buy-back may happen after banning guns
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

Confiscation and buy-backs are the only two ways to get guns off of the street
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

Gun buy-back prices are low
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

Buy-backs are the government stealing from its people
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

Not many would volunteer to turn in guns if the price is low
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

Criminals have others buy their guns
key  1-8543_111_109__114_131_132_135_139_147_148_1_user9.pyr


 NEW CLUSTER  32



Respondents could have lied.
key  1-8143_42_37__45_50_55_56_1_user8.pyr

Accuse him of lying
key  1-8970_4_3__8_9_12_15_16_17_1_user9.pyr

A lie is a deliberately false statement
key  1-8970_4_3__8_9_12_15_16_17_1_user9.pyr

His statement isn't false
key  1-8970_4_3__8_9_12_15_16_17_1_user9.pyr


 NEW CLUSTER  33



Retired militia members were never barred from owning a firearm.
key  1-10121_22_21__23_28_30_34_37_1_userA.pyr

Is a Mensa member
key  1-9036_154_152__155_157_158_159_160_161_162_163_4_user9.pyr


 NEW CLUSTER  34



$50 is a reasonable amount for the government to offer
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

No reason to control other people's behavior that isn't criminal.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr

The reason for war was to preserve the Union.
key  1-9841_196_194__199_202_204_205_206_207_208_209_210_4_userA.pyr


 NEW CLUSTER  35



Be blinded by arrogance
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr


 NEW CLUSTER  36



Doesn't like comparing the elimination of Jews to the elimination of guns
key  1-671_3_2__4_5_6_7_8_9_10_11_13_14_1_user7.pyr

They did not take away all the Jews
key  1-671_3_2__4_5_6_7_8_9_10_11_13_14_1_user7.pyr

Jews were eliminated because guns were taken away
key  1-671_3_2__4_5_6_7_8_9_10_11_13_14_1_user7.pyr


 NEW CLUSTER  37



Waiting periods to purchase guns should not be prevalent
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr

It is difficult to catch straw purchasers.
key  1-8635_36_33__38_40_41_46_49_53_56_1_user8.pyr


 NEW CLUSTER  38



Argument can be proven.
key  1-1543_41_40__44_47_49_51_1_user8.pyr

Win the argument
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr


 NEW CLUSTER  39



Be a Christian
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr


 NEW CLUSTER  40



If society's opinion changes "rights" can change
key  1-1103_28_27__29_30_31_32_33_34_1_user9.pyr

Family encounters with gun violence changes significance.
key  1-1543_41_40__44_47_49_51_1_user8.pyr


 NEW CLUSTER  41



People who cannot serve the state should not own firearms.
key  1-10121_22_21__23_28_30_34_37_1_userA.pyr

There has never been a complete ban on firearms for people not in a militia.
key  1-10121_22_21__23_28_30_34_37_1_userA.pyr

Firearms should be unregulated
key  1-10145_4_3__5_9_21_29_1_user9.pyr

Schools and gun clubs are different.
key  1-8099_19_16__23_24_27_31_2_user8.pyr

Everyone in a gun club will be responsible around firearms.
key  1-8099_19_16__23_24_27_31_2_user8.pyr

Not everyone in a school will be trained in the use of firearms.
key  1-8099_19_16__23_24_27_31_2_user8.pyr

California forced licensed owners of specific firearms to turn firearms in for destruction.
key  1-8635_36_33__38_40_41_46_49_53_56_1_user8.pyr

People in the U.S. understand firearms.
key  1-9836_92_88__94_101_103_108_110_115_2_userA.pyr


 NEW CLUSTER  42



Morals are absolute like murder.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr

Morals are relative.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr

Murder is the killing of one's own in group.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr

Killing isn't always considered murder.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr

Murder is always immoral.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr


 NEW CLUSTER  43



They agree
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr

They do not agree
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr


 NEW CLUSTER  44



Sellers are bound by the law to produce any records when needed.
key  1-10931_104_103__105_106_107_108_109_110_111_112_113_118_119_3_userA.pyr

All that is needed is to establish the victim was in danger
key  1-3320_45_44__46_47_48_49_1_user9.pyr


 NEW CLUSTER  45



You shouldn't infringe on rights because you can't prove something.
key  1-1543_41_40__44_47_49_51_1_user8.pyr

Define 'infringe'
key  1-546_140_139__141_142_143_144_145_146_2_user9.pyr


 NEW CLUSTER  46



Comparisons widely vary depending on perspective.
key  1-1543_41_40__44_47_49_51_1_user8.pyr


 NEW CLUSTER  47



It is not required to flee from an attacker
key  1-3320_45_44__46_47_48_49_1_user9.pyr


 NEW CLUSTER  48



Love had a misdemeanor charge
key  1-173_68_67__69_70_74_77_79_81_87_1_user9.pyr

Love had two open charges
key  1-173_68_67__69_70_74_77_79_81_87_1_user9.pyr


 NEW CLUSTER  49



Have years of weapons training
key  1-3449_16_6__18_19_20_21_23_24_1_user9.pyr

Australians will not be able to have any guns next year
key  1-671_3_2__4_5_6_7_8_9_10_11_13_14_1_user7.pyr

Criminal investigators can search three times each year.
key  1-8860_7_6__8_9_12_15_16_17_1_user8.pyr

Most Australians are not as fearful as Americans are.
key  1-9836_92_88__94_101_103_108_110_115_2_userA.pyr


 NEW CLUSTER  50



Not talking about guns.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

Talk about free speech
key  1-737_78_77__80_81_82_83_84_1_user9.pyr


 NEW CLUSTER  51



The context was Doc Jones called him untrained
key  1-9057_129_126__131_134_138_142_3_user9.pyr


 NEW CLUSTER  52



Four cops were killed by a criminal with an AK-47
key  1-10756_37_35__39_42_43_44_1_user9.pyr

Demand the exact price of an AK-47
key  1-8970_4_3__8_9_12_15_16_17_1_user9.pyr

In Miami, an AK-47 is cheaper than a PlayStation
key  1-8970_4_3__8_9_12_15_16_17_1_user9.pyr

Prices of different models of PlayStation are cheaper than the least expensive AK-47
key  1-8970_4_3__8_9_12_15_16_17_1_user9.pyr

Demand evidence that AK-47s are cheaper than PlayStations
key  1-8970_4_3__8_9_12_15_16_17_1_user9.pyr


 NEW CLUSTER  53



You have to leave if you can do so safely to claim self defense
key  1-3320_45_44__46_47_48_49_1_user9.pyr

It is self defense if you did not want to be in the fight
key  1-3320_45_44__46_47_48_49_1_user9.pyr

It is self defense only if there's immediate threat
key  1-3320_45_44__46_47_48_49_1_user9.pyr

Defending yourself is legal as self defense
key  1-3320_45_44__46_47_48_49_1_user9.pyr

law-abiding citizens should be able to carry guns, and use them in self-defense
key  1-546_230_229__231_232_233_234_235_3_user7.pyr

Some killings are sanctioned by society.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr

Self defense is a sanctioned killing.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr

Guns are important and useful for self defense.
key  1-9874_30_3__32_40_41_55_59_82_86_91_92_2_userA.pyr


 NEW CLUSTER  54



Terrorists having guns and shooting is not simple
key  1-546_20_19__24_27_28_29_33_34_1_user7.pyr

There is lack of mass shootings at shooting ranges.
key  1-8099_19_16__23_24_27_31_2_user8.pyr

Shooting the shooter has stopped school shootings
key  1-8543_123_121__129_130_133_134_2_user9.pyr

Teaching young children to shoot guns is darkly comic.
key  1-9836_92_88__94_101_103_108_110_115_2_userA.pyr

A criminal will shoot the person even without gun possession.
key  1-9874_30_3__32_40_41_55_59_82_86_91_92_2_userA.pyr


 NEW CLUSTER  55



The instructor is a charlatan
key  1-9057_129_126__131_134_138_142_3_user9.pyr


 NEW CLUSTER  56



Liberals provide for every person's needs like they are a child.
key  1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr

His message was not addressed to the other person
key  1-546_140_139__141_142_143_144_145_146_2_user9.pyr

Consider the other person a friend
key  1-9036_154_152__155_157_158_159_160_161_162_163_4_user9.pyr

A person is innocent until proven guilty.
key  1-9836_25_22__27_34_35_37_39_1_userA.pyr


 NEW CLUSTER  57



Does not understand the point of the converstation.
key  1-8099_19_16__23_24_27_31_2_user8.pyr


 NEW CLUSTER  58



The court didn't recognize an individual right to own guns
key  1-173_68_67__69_70_74_77_79_81_87_1_user9.pyr

Every individual state should not have the same laws and liberties.
key  1-9841_196_194__199_202_204_205_206_207_208_209_210_4_userA.pyr

The study includes individuals in nonthreatening events that self-reported the incident without proof of a threat.
key  1-9874_30_3__32_40_41_55_59_82_86_91_92_2_userA.pyr


 NEW CLUSTER  59



The South broke away to keep the free will and the choice to have slaves.
key  1-9841_196_194__199_202_204_205_206_207_208_209_210_4_userA.pyr

The North also had slaves.
key  1-9841_196_194__199_202_204_205_206_207_208_209_210_4_userA.pyr


 NEW CLUSTER  60



Guns last a long time
key  1-8543_123_121__129_130_133_134_2_user9.pyr

should manufacturers keep track of cars 
key  1-9294_9_6__10_13_18_19_1_user9.pyr

Long gun registries would not help track the guns.
key  1-9884_5_4__7_9_11_13_18_1_userA.pyr


 NEW CLUSTER  61



Guns are not the problem
key  1-737_78_77__80_81_82_83_84_1_user9.pyr

US's problem is people who wield guns
key  1-8003_38_37__39_40_41_42_43_45_47_49_1_user9.pyr

US's problem isn't guns
key  1-8003_38_37__39_40_41_42_43_45_47_49_1_user9.pyr

US's problem is criminals and a weak justice system
key  1-8003_38_37__39_40_41_42_43_45_47_49_1_user9.pyr

The 18th century forefathers could not have foreseen the problems of the future.
key  1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr

Guns have made the US a great country
key  1-9036_154_152__155_157_158_159_160_161_162_163_4_user9.pyr

Responsible gun owners are not the problem
key  1-9294_9_6__10_13_18_19_1_user9.pyr


 NEW CLUSTER  62



Gun dealers are rarely searched in the middle of the night.
key  1-8860_7_6__8_9_12_15_16_17_1_user8.pyr

what part of the Brady law covers unannounced searches.
key  1-8860_7_6__8_9_12_15_16_17_1_user8.pyr

Firearm searches are not announced.
key  1-8860_7_6__8_9_12_15_16_17_1_user8.pyr


 NEW CLUSTER  63



Be in the radical minority
key  1-10145_4_3__5_9_21_29_1_user9.pyr


 NEW CLUSTER  64



Seek legal advice
key  1-3320_45_44__46_47_48_49_1_user9.pyr

Hunting weapons are legal
key  1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr

California declared that certain weapons are legal.
key  1-8635_36_33__38_40_41_46_49_53_56_1_user8.pyr

Slavery was legal before the Fourteenth Amendment.
key  1-9841_196_194__199_202_204_205_206_207_208_209_210_4_userA.pyr


 NEW CLUSTER  65



DC should be able to carry out the same security measures with its commuters as airports
key  1-8543_150_143__151_153_156_160_162_164_3_user9.pyr

Security measures are expensive and impossible for a city like DC
key  1-8543_150_143__151_153_156_160_162_164_3_user9.pyr

Security measures are possible in airports because they are private property
key  1-8543_150_143__151_153_156_160_162_164_3_user9.pyr


 NEW CLUSTER  66



Prices are high for handguns in the UK
key  1-9059_22_15__23_28_43_70_76_3_user9.pyr


 NEW CLUSTER  67



He cites code section.
key  1-8860_7_6__8_9_12_15_16_17_1_user8.pyr


 NEW CLUSTER  68



Federal prosecutors are reliable sources.
key  1-9836_25_22__27_34_35_37_39_1_userA.pyr

Federal prosecutors have tried to prove false cases.
key  1-9836_25_22__27_34_35_37_39_1_userA.pyr


 NEW CLUSTER  69



Does not compare the US and Britain
key  1-9036_154_152__155_157_158_159_160_161_162_163_4_user9.pyr

Rubber knives are not comparable to real ones
key  1-9057_129_126__131_134_138_142_3_user9.pyr
