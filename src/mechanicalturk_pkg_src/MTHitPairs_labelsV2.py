'''
Created on Sep 1, 2014
create MT hits for similarity task. combine instances to form a hit file
@author: amita
'''
from data_pkg import FileHandling
import os
from random import shuffle


class MTPairs:
    def __init__(self,Input,Output,No_ofitem,No_ofhits,NotPairEntry):
        self.Input=Input
        self.Output=Output
        self.No_ofitem=No_ofitem
        self.No_ofhits=No_ofhits
        self.fieldnames=[]
        self.NotPairEntry=NotPairEntry
        
    def ReadInput(self):
        Rowdicts=FileHandling.read_csv(self.Input)
        return Rowdicts
    
    def createhitinput(self,rowdicts):
        MTrow=dict()
        MTRows=list()
        intc=1
        extc=0
        fields=rowdicts[0].keys()
        for field in fields:
            if field in self.NotPairEntry:
                self.fieldnames.append(field)
            else:
                self.fieldnames.append(field[:-1])
        self.fieldnames=set(self.fieldnames)        
        for row in rowdicts:
            if extc < self.No_ofhits:  
                if intc<=self.No_ofitem:
                    for field in self.fieldnames:  
                        if field in  self.NotPairEntry:
                            if str(self.No_ofitem) == str(1):
                                MTrow[field + str("a")]=row[field]
                            else:
                                MTrow[field +str("a")+"_"+str(intc)]=row[field]
                        else: 
                            if str(self.No_ofitem) == str(1):
                                MTrow[field +str("a")]=row[field+str(1)]
                                MTrow[field +str("b")]=row[field+str(2)] 
                            else:    
                                MTrow[field +str("a")+"_"+str(intc)]=row[field+str(1)]
                                MTrow[field +str("b")+"_"+str(intc)]=row[field+str(2)] 
                    intc=intc+1       
                                         
                if intc-1==self.No_ofitem:
                    MTRows.append(MTrow)   
                    extc=extc+1
                    intc=1
                    MTrow=dict()
                    
            else:
                break
        Fieldnames=MTRows[0].keys()  
        FileHandling.write_csv(self.Output, MTRows, Fieldnames)            


def Execute(MTPairsobj):
    RowDicts=MTPairsobj.ReadInput()
    MTPairsobj.createhitinput(RowDicts)
    
def MTRowFilter(MTAllPairs,MT_task1,rowstart,rowend):
    AllRows=FileHandling.read_csv(MTAllPairs)
    fieldnames=sorted(AllRows[0].keys())
    FileHandling.write_csv(MT_task1, AllRows[rowstart:rowend], fieldnames)
    
if __name__ == '__main__':
    topic="gun-control"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"/data/paraphrase_data/"+ topic+"/cluster/TFIdf/Phase1"
    Input= Directory + "/Red_Pairs_Cos_Cluster_70_AVG_Noun_Verb_Ad_SortUMBC"
    Output=basedir +"/data/mechanicalturk_data/" + topic + "/MTdata_cluster/Phase1/MT_PairsLabels"
    No_ofitem=5 
    No_ofhits=316
    NotPairEntry=["_UMBC","_uniqueRowNo"]
    MTPairsobj=MTPairs(Input,Output,No_ofitem, No_ofhits,NotPairEntry)
    Execute(MTPairsobj)
    
    No_ofitem=1 
    No_ofhits=1580
    Output=basedir +"/data/mechanicalturk_data/" + topic + "/MTdata_cluster/Phase1/MT_PairsLabels_Oneitem"
    MTPairsobj=MTPairs(Input,Output,No_ofitem, No_ofhits,NotPairEntry)
    Execute(MTPairsobj)
     
