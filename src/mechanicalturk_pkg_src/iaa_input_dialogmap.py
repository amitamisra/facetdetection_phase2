'''
Created on Jun 30, 2015
Take the MT results file "MT1_dialog_labelmap_split_worker"  as input.
 and update each worker annotated text for iaa using DKPRO. 
 Find the position in the dialog where the worker text maps using substring
@author: amita
'''
from nltk import word_tokenize
import ConfigParser, codecs, copy, csv, sys, re
from bs4 import BeautifulSoup
from file_formatting import csv_wrapper


def read_csv(inputcsv):   
        inputfile = codecs.open(inputcsv,'r',encoding='utf-8') 
        result = list(csv.DictReader(inputfile))
        return result 


def runGC():
    args= ReadCongigFile()
    MT1_dialog_labelmap_split_worker= args[0]
    MT1dialog_file=args[1]
    output_file=args[2]
    Execute(MT1_dialog_labelmap_split_worker ,MT1dialog_file,output_file)

def ReadCongigFile():
    config = ConfigParser.ConfigParser()
    config.read('iaa_input_dialogmap.ini')
    MT1_dialog_labelmap_split_worker = config.get('GC', 'MT1_dialog_labelmap_split_worker')
    MT1dialog_labelmap=config.get('GC', 'MT1dialog_labelmap')
    output_file= config.get('GC', 'output-file')
    arguments=(MT1_dialog_labelmap_split_worker ,MT1dialog_labelmap,output_file)
    return  (arguments)



def Execute(MT1_dialog_labelmap_split_worker ,MT1dialog_file,outputfile):
    
    split_worker_rows=read_csv(MT1_dialog_labelmap_split_worker)
    dialog_rows=read_csv(MT1dialog_file)
    fieldlist=split_worker_rows[0].keys()
    AllRows=[]
    for row in split_worker_rows:
        newrow=copy.deepcopy(row)
        key_splitworker=row["key_user"]
        row_dialogfile=[rowdialog for rowdialog in dialog_rows if rowdialog["key"]==key_splitworker][0]
        dialog_html=row_dialogfile["Dialogtext_"]
        soup=BeautifulSoup(dialog_html)
        dialogplain=soup.get_text()
        for field in fieldlist:
            if field.startswith("Id"):
                workeridext=row[field]
                tokens=word_tokenize(workeridext)
                num_tokens=len(tokens)
                if num_tokens > 0:
                    
                    dialogplainNoPunc = re.sub('["]', '', dialogplain)
                    workeridextNoPunc=re.sub('["]', '',workeridext )
                    
                    dialogplainNoAuthor= re.sub(r'S[1-2]:[0-9]-', r'', dialogplainNoPunc)
                    workeridextNoAuthor=re.sub(r'S[1-2]:[0-9]-', r'', workeridextNoPunc)
                    
                    
                    dialogplain=" ".join(dialogplainNoAuthor.split())
                    workeridext=" ".join(workeridextNoAuthor.split())
                    
                    workertextpos=str(dialogplain).find(workeridext)
                    if row["key_user"]=="1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr" and row["id"] == "19" : 
                        newrow[field+"_startpos"]=workertextpos
                        newrow[field+"_num_words"]=num_tokens
                        newrow["consider"]="0"
                        continue
                    if row["key_user"]=="1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1_user8.pyr" and row["id"] == "19" :
                        newrow[field+"_startpos"]=workertextpos
                        newrow[field+"_num_words"]=num_tokens
                        newrow["consider"]="0"
                        continue
                    if row["key_user"]=="1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr" and row["id"] == "103" :
                        newrow[field+"_startpos"]=workertextpos
                        newrow[field+"_num_words"]=num_tokens
                        newrow["consider"]="0"
                        continue
                    if row["key_user"]=="1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1_user9.pyr" and row["id"] == "101" :
                        newrow[field+"_startpos"]=workertextpos
                        newrow[field+"_num_words"]=num_tokens
                        newrow["consider"]="0"
                        continue
                    if row["key_user"]=="1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr" and row["id"] == "21" :
                        newrow[field+"_startpos"]=workertextpos
                        newrow[field+"_num_words"]=num_tokens
                        newrow["consider"]="0"
                        continue
                    if row["key_user"]=="1-10931_30_25__35_38_46_48_51_52_54_2_userA.pyr" and row["id"] == "17" :
                        newrow[field+"_startpos"]=workertextpos
                        newrow[field+"_num_words"]=num_tokens
                        newrow["consider"]="0"
                        continue
                    if row["key_user"]=="1-671_3_2__4_5_6_7_8_9_10_11_13_14_1_user7.pyr" and row["id"] == "4" :
                        newrow[field+"_startpos"]=workertextpos
                        newrow[field+"_num_words"]=num_tokens
                        newrow["consider"]="0"
                        continue
                    if row["key_user"]=="1-671_3_2__4_5_6_7_8_9_10_11_13_14_1_user7.pyr" and row["id"] == "1" :
                        newrow[field+"_startpos"]=workertextpos
                        newrow[field+"_num_words"]=num_tokens
                        newrow["consider"]="0"
                        continue
                    if row["key_user"]=="1-10121_22_21__23_28_30_34_37_1_userA.pyr" and row["id"] == "3" :
                        newrow[field+"_startpos"]=workertextpos
                        newrow[field+"_num_words"]=num_tokens
                        newrow["consider"]="0"
                        continue
                    
                    if workertextpos== -1:
                        print dialogplain
                        print workeridext
                        print """ Error in finding substring in dialog,\n 
                        FunctionName:Execute\n
                        Script:iaa_input_dialogmap.py\n
                        WorkerId: """ + field + "key: " + row["key_user"]+ "label_id: " +row["id"]
                        sys.exit(1)
                    else:        
                        newrow[field+"_startpos"]=workertextpos
                        newrow[field+"_num_words"]=num_tokens
                        newrow["consider"]="1"
                    
        AllRows.append(newrow) 
    writecsv(AllRows,outputfile)   
               
def writecsv(AllRows,outputfile):
    fieldnames=AllRows[0].keys()  
    csv_wrapper.write_csv(outputfile, AllRows, fieldnames)             
  
if __name__ == '__main__':
    runGC()