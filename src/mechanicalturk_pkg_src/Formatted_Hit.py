'''
Created on Jun 8, 2014
@author: amita
Not used any further.

Take input as MT Results file containing summaries and output a formatted summary html file with word count, was used only for MT1_more 750
simple summaries. Not used any further.
'''
from data_pkg import FileHandling
import os
from data_pkg import NewFormat_text
from collections import OrderedDict
#from nlp.text_obj import TextObj

class IndividualHits:
    def __init__(self,InHitMT,outputtextfile,Noofdialog_hit):
        self.input=InHitMT
        self.dialognos_hit=Noofdialog_hit
        self.outputtextfile=outputtextfile
        
    def CreateHitOutput(self):
        rowdicts=FileHandling.read_csv(self.input)
        Textline=""
        Textline= Textline + """<html><head></head><body>"""
        for row in rowdicts:
            num_dialog=0
            Textline=Textline +"<p><b>" +"Worker_Id:  " +"</b>"
            Textline=Textline+row["WorkerId"]
            Textline=Textline+"<br><b>" +"HIT_Id:   " +"</b>"
            Textline=Textline+row["HITId"]
            Textline=Textline +"<br><b>"+"AssignmentStatus:  " + row["AssignmentStatus"] +"</b>"
            while num_dialog < self.dialognos_hit: 
                    Dialog=row["Input.Dialogtext"+ str(num_dialog+1)]
                    Textline=Textline +"<br><b>"+"Dialog </b></br>" + Dialog +"<br>"
                    Text_Summary=row["Answer.Dialog"+ str(num_dialog+1)+"_Summary"]
                    Word_count= str(NewFormat_text.word_count(Text_Summary, removelist))
                    Textline=Textline +"<br><b>"+"Word_Count:  " + Word_count +"</b>"
                    Textline= Textline+"<br><b>"+"Summary" + str(num_dialog+1) +"<br></b>"
                    Textline= Textline + Text_Summary + "</p>"
                    num_dialog=num_dialog+1
                    
        Textline=Textline + """</body> </html>""" 
        FileHandling.writeHtml(self.outputtextfile, Textline)            


if __name__ == '__main__':
    topic="gun-control"
    InputCsv= os.getcwd()+ "/"+ topic +"/MTdata/MT1/Summary_results/MT1Results_more750"
    OutHTML=os.getcwd()+ "/"+ topic +"/MTdata/MT1/Summary_results/MT1Results_more750"
    removelist=[".","?","!"]
    noofitems=3
    MThitobj= IndividualHits(InputCsv,OutHTML,noofitems)
    MThitobj.CreateHitOutput()
    