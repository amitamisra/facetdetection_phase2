'''
Created on Jan 25, 2015
Take input as MT Results file containing simple and complex summaries and output a formatted summary html file with word count
'''
from data_pkg  import FileHandling 
import os
from data_pkg import NewFormat_text 
from collections import OrderedDict 
#from nlp.text_obj import TextObj
import re
from dialog_pkg_src import createpyramidInputGunControl as createpyramidInput
class IndividualHits:
    def __init__(self,InHitMT,outputtextfile,Noofdialog_hit):
        self.input=InHitMT
        self.dialognos_hit=Noofdialog_hit
        self.outputtextfile=outputtextfile
        
        
    # change s1 to author1 s2 to author2
    def formattextfield(self,summary,Textline,row): 
        Summary_Text=row[summary]
        p1 = re.compile( '(s1 | S1)')
        p1.sub("Author1",Summary_Text )
        p2 = re.compile( '(s2 | S2)')
        p2.sub("Author2",Summary_Text )
        Word_count= str(NewFormat_text.word_count(Summary_Text, removelist))
        Textline=Textline +"<br><b>"+"Word_Count:  " + Word_count +"</b>"
        Textline= Textline+"<br><b>"+ summary  +"<br></b>"
        Textline= Textline + Summary_Text + "</p>" 
        return Textline  
    
    
    
     # take the results hit file and create a formatted html output   
    def CreateHitOutput(self):
        rowdicts=FileHandling.read_csv(self.input)
        #=======================================================================
        # if str(self.input).endswith("gun-control/MTdata/MT2/Summary_results/MT2_more750Results"):
        #     rowdictsA14=FileHandling.read_csv(A14_MT2_More750guncontrol)
        #     lensA14 =len(rowdictsA14)
        #=======================================================================
        Textline=""
        Textline= Textline + """<html><head></head><body>"""
        for row in rowdicts:
            if str(self.input).endswith("midResults") and row["Input.key"+str(1)].startswith(tuple(createpyramidInput.ignorelist)):
                    continue
            if str(row["AssignmentStatus"]).startswith("Approved") :
                if str(self.input).endswith("MT2_midResults") and str(row["WorkerId"]).startswith("A3JHXEH79DJ83L") :
                        #print "stop"
                        continue
            
            Textline=Textline +"<p><b>" +"Worker_Id:  " +"</b>"
            Textline=Textline+row["WorkerId"]
            Textline=Textline+"<br><b>" +"HIT_Id:   " +"</b>"
            Textline=Textline+row["HITId"]
            Textline=Textline +"<br><b>"+"AssignmentStatus:  " + row["AssignmentStatus"] +"</b>"
            Dialog=row["Input.Dialogtext1"]
            key=row["Input.key1"]
            Textline=Textline +"<br><b>"+"Key </b></br>" + key +"<br>"
            Textline=Textline +"<br><b>"+"Dialog </b></br>" + Dialog +"<br>"
            #===================================================================
            # if A14:
            #     rowname= rowA14[0]
            # else:
            #     rowname=row    
            #===================================================================
            NewText=self.formattextfield("Answer.Dialog_Natural",Textline,row)
            Textline=self.formattextfield("Answer.Dialog_Simple",NewText,row)
                
        Textline=Textline + """</body> </html>""" 
        FileHandling.writeHtml(self.outputtextfile, Textline)            


if __name__ == '__main__':
    
    topic= "abortion"
    removelist=[".","?","!"]
    noofitems=1
    
    #===========================================================================done for gun -control
    #topic="gun-control" 
    #InputCsv1= os.path.dirname(os.getcwd())+ "/MechanicalTurk_Pkg_Data/"+ topic +"/MTdata/MT1/Summary_results/MT1_midResults"
    # OutHTML1=os.path.dirname(os.getcwd())+ "/MechanicalTurk_Pkg_Data/"+ topic +"/MTdata/MT1/Summary_results/MT1_midResults"
    # revisedworker= os.path.dirname(os.getcwd())+ "/MechanicalTurk_Pkg_Data/"+ topic +"/MTdata/MT2/Summary_results/A14H_MT2_More750"
    # removelist=[".","?","!"]
    # noofitems=1
    # 
    # MThitobj= IndividualHits(InputCsv1,OutHTML1,noofitems)
    # MThitobj.CreateHitOutput()
    # 
    # InputCsv2= os.path.dirname(os.getcwd())+ "/MechanicalTurk_Pkg_Data/"+ topic +"/MTdata/MT2/Summary_results/MT2_midResults"
    # OutHTML2=os.path.dirname(os.getcwd())+ "/MechanicalTurk_Pkg_Data/"+ topic +"/MTdata/MT2/Summary_results/MT2_midResults"
    # 
    # MThitobj= IndividualHits(InputCsv2,OutHTML2,noofitems)
    # MThitobj.CreateHitOutput()
    # 
    # InputCsv3= os.path.dirname(os.getcwd())+ "/MechanicalTurk_Pkg_Data/"+ topic +"/MTdata/MT2/Summary_results/MT2_more750Results"
    # OutHTML3=os.path.dirname(os.getcwd())+ "/MechanicalTurk_Pkg_Data/"+ topic +"/MTdata/MT2/Summary_results/MT2_more750Results"
    # createpyramidInput.replaceA14(InputCsv3,revisedworker)
    # MThitobj= IndividualHits(InputCsv3,OutHTML3,noofitems)
    # MThitobj.CreateHitOutput()
    #===========================================================================
    
    
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"/data/mechanicalturk_data/" 
    InputCsv3= Directory + topic +"/MTdata/Phase1/MT1/Summary_results/MT1_more750Results"
    OutHTML3= Directory +   topic +"/MTdata/Phase1/MT1/Summary_results/MT1_more750Results"
    MThitobj= IndividualHits(InputCsv3,OutHTML3,noofitems)
    MThitobj.CreateHitOutput()
