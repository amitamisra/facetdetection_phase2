'''

Created on Dec 30, 2014
create MT input file for summary task for gun control, was used only for MT2_more 750, MT1mid and MT2mid with 
simple summaries and natural summaries.
create MT input file for summary task for abortion and evolution,use more750
simple summaries and natural summaries.

@author: amita
'''
ignorelist=["1-7005_33_31__35_40_41_43_45_49_1","1-8549_122_121__123_125_128_129_130_132_133_134_135_136_1", \
            "1-7587_140_139__142_146_148_149_150_151_152_154_157_161_2" , "1-10660_56_55__58_59_60_67_1", \
            "1-8828_33_32__35_36_37_39_1", "1-8828_90_87__92_96_99_103_3", "1-2504_90_80__92_93_95_98_100_1",  \
            "1-3154_210_203__211_212_215_217_218_222_274_277_281_297_2", "1-903_198_194__202_213_218_224_2",\
            "1-3791_291_290_1_295_298_315_321_339_348_10","1-5289_51_50__56_59_60_61_1","1-3483_22_21__23_25_26_27_3",\
            "1-8863_47_18__48_49_50_51_52_2","1-4481_23_22__32_34_35_36_5","1-4481_81_80__82_83_84_85_1",\
            "1-6459_139_135__144_147_148_149_151_4","1-6013_30_29__31_33_34_36_1",\
            "1-3580_88_87__91_93_95_110_6","1-3580_284_283__285_286_287_288_3","1-9549_37_32__39_41_42_43_1"]

import os
from data_pkg import FileHandling
def createHitTask1(MTAll,MT1,StartRow,EndRow):
    AllRows=FileHandling.read_csv(MTAll)
    fieldnames=AllRows[0].keys()
    FileHandling.write_csv(MT1, AllRows[StartRow:EndRow], fieldnames)
    
    
def changeHITto1dialog(MT1_more750,AllRows, len_to_put,AllRowsitemcount): 
    
    NewListRows=[]
    count=1  
    AllRowcount=0
    while count < len_to_put :
        for i in range(1,AllRowsitemcount+1):
            if AllRows[AllRowcount]["key"+ str(i)]  in ignorelist:
                continue
            else:  
                if count > len_to_put:
                    continue
                else:  
                    newdict=dict()
                    newdict["key1"]=AllRows[AllRowcount]["key"+ str(i)]       
                    newdict["Dialogtext1"]=AllRows[AllRowcount]["Dialogtext"+str(i)] 
                    NewListRows.append(newdict)
                    count=count+1    
        AllRowcount=AllRowcount+1
    fieldnames= NewListRows[0].keys()   
    FileHandling.write_csv(MT1_more750, NewListRows, fieldnames)

if __name__ == '__main__':
    # MTAll_more750 : contains all more 750 dialogs created in data pkg, 1 row contains 3 dialogs
    # MT3_more750_3dialogs: select a subset for a task, say 3  from MTAll_more750
    # MT3_more750: change to one dialog per row
    
    
    #Start with MT3_task3  #=========================================================================== done for gun control
    #-------------------------------------------------------------------- task=3
    #------------------------------------------------------- topic="gun-control"
    # MTAll_more750=os.path.dirname(os.getcwd()) + "/Data_Pkg_Data/CSV/"+ topic+"/MTdata/MTAll/MTAll_more750"
    # MT3_more750_3dialogs=os.path.dirname(os.getcwd()) + "/MechanicalTurk_Pkg_Data/"+ topic+"/MTdata/" + "MT" + str(task)+"/MT" + str(task)+"More750/MT"+ str(task)+"_more750_3dia"
    # MT3_more750=os.path.dirname(os.getcwd()) + "/MechanicalTurk_Pkg_Data/"+ topic+"/MTdata/" + "MT" + str(task)+"/MT"+ str(task)+"More750/MT"+ str(task) +"_more750"
    #---------------------------------------------------------------- StartRow=0
    #----------------------------------------------------------------- EndRow=16
    #-------- createHitTask1(MTAll_more750,MT3_more750_3dialogs,StartRow,EndRow)
    #---------------------- changeHITto1dialog(MT3_more750,MT3_more750_3dialogs)
    
    
    
    #=========================================================================== done for gun control
    # task=2
    # topic="gun-control"
    # basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    # MTAll_more750=basedir + "/data/dialog_data/CSV/"+ topic+"/MTdata/MTAll/MTAll_more750"
    # MT2_more750_3dialogs=basedir + "/data/mechanicalturk_data/"+ topic+"/MTdata/Phase1/" + "MT" + str(task)+"/MT" + str(task)+"More750/MT"+ str(task)+"_more750_3dia"
    # MT2_more750=basedir + "/data/mechanicalturk_data/"+ topic+ "/MTdata/Phase1/" + "MT" + str(task)+"/MT"+ str(task)+"More750/MT"+ str(task) +"_more750"
    # StartRow=16
    # EndRow=26
    # #-------- createHitTask1(MTAll_more750,MT2_more750_3dialogs,StartRow,EndRow)
    # changeHITto1dialog(MT2_more750,MT2_more750_3dialogs)
    # 
    #===========================================================================
    
    
    
    
    
    
    
    #Already done for MT1_750more and MT1_mid
    #===========================================================================
    # task=1
    # topic="gun-control"
    # 
    # MTAll_more750=os.path.dirname(os.getcwd()) + "/Data_Pkg/CSV/"+ topic+"/MTdata/MTAll/MTAll_more750"
    # MT1_more750=os.getcwd() + "/"+topic+"/MTdata/" + "MT" + str(task)+"/MT1_more750/MT1_more750"
    # MTAll_midrange=os.path.dirname(os.getcwd()) + "/Data_Pkg/CSV/"+ topic+"/MTdata/MTAll/MTAll_midrange"
    # MT1_midrange_3dialogs=os.getcwd() + "/"+ topic+"/MTdata/" + "MT" + str(task)+"/MT1Mid/MT1_midrange_3dia"
    # MT1_midrange=os.getcwd() + "/"+ topic+"/MTdata/" + "MT" + str(task)+"/MT1Mid/MT1_midrange"
    # 
    # StartRow=0
    # EndRow=16
    # #createHitTask1(MTAll_more750,MT1_more750,StartRow,EndRow)
    # 
    # StartRow=0
    # EndRow=3
    # createHitTask1(MTAll_midrange,MT1_midrange_3dialogs,StartRow,EndRow)
    # changeHITto1dialog(MT1_midrange,MT1_midrange_3dialogs)
    #===========================================================================
    
    
    # START WITHTASK 2
    
    


     
# Already done for midRange
#      task=2
#      topic="gun-control"
#     MTAll_midrange=os.path.dirname(os.getcwd()) + "/Data_Pkg/CSV/"+ topic+"/MTdata/MTAll/MTAll_midrange"
#     MT2_midrange_3dialogs=os.getcwd() + "/"+ topic+"/MTdata/" + "MT" + str(task)+"/MT2Mid/MT2_midrange_3dia"
#     MT2_midrange=os.getcwd() + "/"+ topic+"/MTdata/" + "MT" + str(task)+"/MT"+ str(task)+"Mid/MT"+ str(task) +"_midrange"
#      
#     StartRow=3
#     EndRow=8
#     createHitTask1(MTAll_midrange,MT2_midrange_3dialogs,StartRow,EndRow)
#     changeHITto1dialog(MT2_midrange,MT2_midrange_3dialogs)



    #-------------------------------------------------------------------- task=1 done for abortion
    #---------------------------------------------------------- topic="abortion"
    # MTAll_more750=os.path.dirname(os.getcwd()) + "/Data_Pkg_Data/CSV/"+ topic+"/MTdata/MTAll/MTAll_more750"
    # MT1_more750_3dialogs= os.path.dirname(os.getcwd()) + "/MechanicalTurk_Pkg_Data/"+ topic+"/MTdata/" + "MT" + str(task)+"/MT" + str(task)+"More750/MT"+ str(task)+"_more750_3dia"
    # MT1_more750=os.path.dirname(os.getcwd()) + "/MechanicalTurk_Pkg_Data/"+topic+"/MTdata/" + "MT" + str(task)+"/MT"+ str(task)+"More750/MT"+ str(task) +"_more750"
    #---------------------------------------------------------------- StartRow=1
    #----------------------------------------------------------------- EndRow=11
    #-------- createHitTask1(MTAll_more750,MT1_more750_3dialogs,StartRow,EndRow)
    #---------------------- changeHITto1dialog(MT1_more750,MT1_more750_3dialogs)

    task=1
    topic="abortion"
    AllRowsitemcount=3
    len_to_put=22
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    dialogdir=basedir+"/data/dialog_data/CSV/"
    MTAll_midrange=dialogdir + topic +"/MTdata/MTAll/MTAll_midrange"
    MT2_midrange_3dialogs= basedir + "/data/mechanicalturk_data/"+ topic+"/MTdata/Phase1/" + "MT" + str(task)+"/MT" + str(task)+"_midrange/MT"+ str(task)+"_midrange_3dia"
    MT2_midrange=basedir + "/data/mechanicalturk_data/"+topic+"/MTdata/Phase1/" + "MT" + str(task)+"/MT"+ str(task)+"_midrange/MT"+ str(task) +"_midrange"
    StartRow=0
    EndRow=11
    createHitTask1(MTAll_midrange,MT2_midrange_3dialogs,StartRow,EndRow)
    AllRows=FileHandling.read_csv(MT2_midrange_3dialogs)
    changeHITto1dialog(MT2_midrange,AllRows,len_to_put,AllRowsitemcount)





#     task=1    done for evolution
#     topic="evolution"
#     basedir=os.path.dirname(os.path.dirname(os.getcwd()))
#     dialogdir=basedir+"/data/dialog_data/CSV/"
#     MTAll_more750=dialogdir + topic+"/MTdata/MTAll/MTAll_more750"
#     MT1_more750_3dialogs= basedir + "/data/mechanicalturk_data/"+ topic+"/MTdata/Phase1/" + "MT" + str(task)+"/MT" + str(task)+"_more750/MT"+ str(task)+"_more750_3dia"
#     MT1_more750= basedir + "/data/mechanicalturk_data/"+topic+"/MTdata/Phase1/" + "MT" + str(task)+"/MT"+ str(task)+"_more750/MT"+ str(task) +"_more750"
#     StartRow=0
#     EndRow=21
#     createHitTask1(MTAll_more750,MT1_more750_3dialogs,StartRow,EndRow)
#     AllRows=FileHandling.read_csv(MT1_more750_3dialogs)
#     AllRowsitemcount=3
#     len_to_put=49 # put only 50 dialogs, leave one dialog
#     changeHITto1dialog(MT1_more750,AllRows,len_to_put,AllRowsitemcount)
