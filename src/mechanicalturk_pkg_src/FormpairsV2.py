'''
Created on Sep 1, 2014
This file takes all clusters from cluster file created in paraphrase  program Input_clustering, forms pairs of labels in same cluster
 and creates a pair file for Mechanical similarity  task
@author: amita
'''
import operator
import itertools
from data_pkg import FileHandling
import os
from paraphrase import similarity
from copy import deepcopy

def CombinationStrings(RowStrings,ErrorLines,Label,fields,umbcfieldname,umbckeyname):
    
    type_sim='relation'
    corpus='webbase'
    PairListCluster=list()
    Combinations=list(itertools.combinations(RowStrings,2))
    for tuple_comb in Combinations:
                rowdict=dict()
                for field in fields:
                    rowdict[str(field)+ "_"+str(1)]= (tuple_comb[0])[field]
                    rowdict[str(field)+ "_"+str(2)]= (tuple_comb[1])[field]
                    if umbcfieldname ==  str(field):
                        S1=(tuple_comb[0])[field]
                        S2=(tuple_comb[1])[field]
                        sim=similarity.sss(S1, S2, type_sim, corpus)
                        if sim==-1:
                            ErrorLines.append("\n\n Error in these sim pairs\n")
                            ErrorLines.append("1)"+S1 +"\n")
                            ErrorLines.append("2)"+S2+"\n")
                        rowdict["_"+umbckeyname]=sim
                PairListCluster.append(rowdict)
    return PairListCluster            

def createpairs(RowDicts,ErrorLines,PairLabel,fields,umbcfieldname,umbckeyname):
    AllPairs=list()
    RowDicts.sort(key=operator.itemgetter(PairLabel))
    for key, items in itertools.groupby(RowDicts, operator.itemgetter(PairLabel)):
        ListRows=list(items)
        PairListCluster=CombinationStrings(ListRows,ErrorLines,key,fields,umbcfieldname,umbckeyname)
        AllPairs.extend(PairListCluster)
    
    return AllPairs

def ReadInput(FilenameCsv):
    
    RowDicts=FileHandling.read_csv(FilenameCsv)
    return RowDicts


    
def Execution(InputCsv,OutputCsv, Error_UMBC,PairLabel,umbcfieldname,umbckeyname):
    ErrorLines=list()
    RowDicts=ReadInput(InputCsv)
    fields=RowDicts[0].keys()
    AllPairs=createpairs(RowDicts,ErrorLines,PairLabel,fields,umbcfieldname,umbckeyname)
    fieldnames=AllPairs[0].keys()
    FileHandling.write_csv(OutputCsv, AllPairs, fieldnames)
    FileHandling.WriteTextFile(Error_UMBC, ErrorLines)
    
  
if __name__ == '__main__':
   
    topic="gun-control"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"/data/paraphrase_data/"+ topic+"/cluster/TFIdf/Phase1"
    ClusterLabelInp=Directory + "/Cos_Cluster_70_AVG_Noun_Verb_Ad"
    ClusterLabelPairs=Directory + "/Pairs_Cos_Cluster_70_AVG_Noun_Verb_Ad"
    Error_UMBC=Directory+"/ErrorPairsUMBC_Cos_Cluster_70_AVG_Noun_Verb_Ad"
    PairLabel="label_cluster" # field for pairs
    #fields=["string","key","doccount"]
    umbcfieldname="label"
    umbckeyname="UMBC"
    Execution(ClusterLabelInp,ClusterLabelPairs, Error_UMBC,PairLabel,umbcfieldname,umbckeyname)
    