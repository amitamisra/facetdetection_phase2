'''
Created on May 12, 2015
remove workers based on number of pairs
@author: amita
'''
import os
from data_pkg import FileHandling
def ReadRows(inputcsv):
    rows=FileHandling.read_csv(inputcsv)
    return rows


# keep only workers with minimum count of rows done
def filterworkercount( paircountworker, count):
    WorkerList=[]
    for workercount  in paircountworker:
        if len(workercount["No"].strip())==0:
            continue
            
        if int(workercount["No"]) > int(count):
                WorkerList.append(workercount["Worker_Id"])
                
    return WorkerList  

#keep only those workers that are in WorkerList  
def filtercorrelation(correlationrows, WorkerList ):  
    AllRows=[]
    for row in  correlationrows:
        NewRow={}
        if row["Worker"]  in WorkerList :
            for key, value in row.iteritems():
                if key in WorkerList:
                    NewRow[key]= value
                    if str(value) == str(1):
                        NewRow["worker"]=key
        AllRows.append(NewRow)                
    return AllRows 
def Execute(workermincount,workercountfile,corrfile,count): 
    paircountworker=ReadRows(workercountfile)
    correlationrows=ReadRows(corrfile)
    WorkerList =filterworkercount(paircountworker, count)
    rowdicts=filtercorrelation(correlationrows, WorkerList )
    fieldnames=sorted(rowdicts[0].keys(), reverse = True)
    FileHandling.write_csv(workermincount, rowdicts, fieldnames)
     
         
if __name__ == '__main__':
    topic= "gun-control"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"/data/mechanicalturk_data/"+ topic+"/MTdata_cluster/Phase1/"
    corrfile= Directory +"MT1/MT1Results/MT_Task1_split_worker_corr"
    workercountfile= Directory +"MT1/MT1Results/WorkerPairCount"
    workermincount=Directory +"MT1/MT1Results/MT_Task1_split_worker_corr_mincount"
    count=20
    Execute(workermincount,workercountfile,corrfile,count)
    
    