'''
Created on Apr 23, 2015
Remove irrelevant gun control pairs
@author: amita
'''
from data_pkg import FileHandling
import SortUMBC
import os
def ReadRows(InputFile):
    Rows=FileHandling.read_csv(InputFile)
    return Rows

def createremoverowslist(Rows,Removelist):
    NewRows=[]
    for row in Rows:
        if row["label_1"] in Removelist  or  row["label_2"] in Removelist:
            continue
        else:
            NewRows.append(row)
            
             
    return NewRows         
                

# remove the rows with uniquerowlist
def Execute(sortedpairsfile,reducedpairsfile,redpairlimitcol,Removelist,fieldnameslist):
    AllRows=ReadRows(sortedpairsfile)
    NewRows  = createremoverowslist(AllRows,Removelist)
    fieldnames=NewRows[0].keys()
    FileHandling.write_csv(reducedpairsfile, NewRows, fieldnames)
    SortUMBC.filterfieldnames(reducedpairsfile, redpairlimitcol, fieldnameslist)
    
    
if __name__ == '__main__':
    Removelist=[]
    topic="gun-control"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"/data/paraphrase_data/"+ topic+"/cluster/TFIdf/Phase1/"
    
    Removelist.append("He lives in Australia.")
    Removelist.append("Consider the other person a friend")
    Removelist.append("Life experience support his conclusion.")
    
    fieldnameslist=["_UMBC","label_1","label_2","_uniqueRowNo"]
    
    sortedpairsfile=Directory + "Pairs_Cos_Cluster_70_AVG_Noun_Verb_AdSortUMBC"
    reducedpairsfile= Directory + "Red_Pairs_Cos_Cluster_70_AVG_Noun_Verb_Ad_SortUMBC"
    redpairlimitcol=Directory + "Redpair_limitecol"
    Execute(sortedpairsfile,reducedpairsfile,redpairlimitcol,Removelist,fieldnameslist)
    
