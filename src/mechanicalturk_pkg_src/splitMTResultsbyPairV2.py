'''
Created on Apr 30, 2015

@author: amita
'''
import os 
import operator
import itertools
import sys
from data_pkg import FileHandling
from collections import defaultdict
from file_formatting import csv_wrapper


# fieldnames in original pairs file were 1 and 2. For MT changed them to a and b.
#since we are getting fieldnames from original pairs file, cgnage 1 to a and 2 to b in fieldnames
def restorefieldnames( fields,NotPairEntry):
    Updatedfields=[]
    for field in fields:
        if field[-1]== "1" :
            newfield=field[:-1] +"a_"
        else:
            if field[-1]== "2" :
                newfield=field[:-1] +"b_"
            else:
                if field in NotPairEntry:
                    newfield=field +"a_"
                else:    
                    print" error in function restorefieldnames"    
                    sys.exit(0)
        Updatedfields.append(newfield)  
    return Updatedfields      
    
def ReadRows(InputCsv):
    rows=FileHandling.read_csv(InputCsv)
    return rows
    
def splitRowPairwise(rowdicts,noofitemsHit,fields):
    AllRows=list()
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for key, items in itertools.groupby(rowdicts, operator.itemgetter('HITId')): # get rows with same HITID
        Hitids=list(items)   # All rows in HItids list have same hitid 
        for count in range(1,noofitemsHit+1):
            NewDict=defaultdict()
            for row in Hitids:
                    NewDict["Id_"+ row["WorkerId"]]=row["Answer.response"+ str(count)]
            for field in fields:
                NewDict[field ]=row["Input."+field+ str(count)]        
            NewDict["HITId"]=row["HITId"]
            NewDict["HITTypeId"]=row["HITTypeId"] 
            AllRows.append(NewDict) 
            
    return AllRows        
                
                    
                    
def Execute(MTinputFile,MTResults,MTsplitResults,noofitemsHit,NotPairEntry): 
    oneitemrows=ReadRows(MTinputFile)
    fields=oneitemrows[0].keys()
    updatedfields=restorefieldnames(fields,NotPairEntry)
    rowdicts= ReadRows(MTResults)
    NewRows=splitRowPairwise(rowdicts,noofitemsHit,updatedfields)
    csv_wrapper.write_csv(MTsplitResults, NewRows)
    
    
if __name__ == '__main__':
    topic="gun-control"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"/data/mechanicalturk_data/"+ topic+"/MTdata_cluster/Phase1/"
    
    MTinputFile=basedir +"/data/paraphrase_data/"+ topic+"/cluster/TFIdf/Phase1/Red_Pairs_Cos_Cluster_70_AVG_Noun_Verb_Ad_SortUMBC" # file used to create MT Task
    MTResults=Directory + "MT1/MT1Results/Results_MT_Task1"
    MTsplitResults=Directory +"MT1/MT1Results/MT_Task1_split_worker.csv"
    NotPairEntry=["_UMBC","_uniqueRowNo"]
    noofitemsHit=5
    Execute( MTinputFile,MTResults,MTsplitResults,noofitemsHit,NotPairEntry)
    
