'''
Created on Apr 18, 2015

@author: amita
'''
from copy import deepcopy
from data_pkg import FileHandling
import os

def filterfieldnames(inputcsv,outputcsv,fieldnameslist):
    rowdicts=FileHandling.read_csv(inputcsv)
    FileHandling.write_csv(outputcsv, rowdicts, fieldnameslist)
    
def AddUniqueRowFile(outputcsvsort):
    RowDicts=FileHandling.read_csv(outputcsvsort)
    NewRows=FileHandling.AddUniqueRowNo(RowDicts)
    fieldnames=NewRows[0].keys()
    FileHandling.write_csv(outputcsvsort, NewRows, fieldnames)
                
def sortumbc(inputcsv,outputcsv,fieldname):
    Rows=FileHandling.read_csv(inputcsv)
    NewRows=FileHandling.SortRows(Rows, fieldname)
    fieldnames=NewRows[0].keys()
    FileHandling.write_csv(outputcsv, NewRows, fieldnames)
if __name__ == '__main__':
    topic="gun-control"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"/data/paraphrase_data/"+ topic+"/cluster/TFIdf/Phase1"
    inputcsv=Directory + "/Pairs_Cos_Cluster_70_AVG_Noun_Verb_Ad"
    outputcsvsort= Directory + "/Pairs_Cos_Cluster_70_AVG_Noun_Verb_AdSortUMBC"
    fieldname="_UMBC" # fieldname to sort
    fieldnameslist=["_UMBC","label_1","label_2","_uniqueRowNo"]
    outputcsvlimited= Directory + "/PairslimitedCol"
    
    sortumbc(inputcsv,outputcsvsort,fieldname)
    AddUniqueRowFile(outputcsvsort)
    filterfieldnames(outputcsvsort,outputcsvlimited,fieldnameslist)