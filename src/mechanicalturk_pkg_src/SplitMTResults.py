'''
Created on May 17, 2015

@author: amita
'''

import operator
import itertools
import os
from data_pkg import FileHandling
from collections import defaultdict
from file_formatting import csv_wrapper


def ReadRows(InputCsv):
    rows=FileHandling.read_csv(InputCsv)
    return rows
    
def splitRowPairwise(rowdicts,noofitemsHit,fields):
    AllRows=list()
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for key, items in itertools.groupby(rowdicts, operator.itemgetter('HITId')): # get rows with same HITID
        Hitids=list(items)   # All rows in HItids list have same hitid 
        for count in range(1,noofitemsHit+1):
            NewDict=defaultdict()
            for row in Hitids:
                    NewDict["Id_"+ row["WorkerId"]]=row["Answer.SimilarLabel"+ str(count)]
            for field in fields:
                if field =="key_user": 
                    NewDict[field ]=row["Input.key"]  
                    continue
                if field =="_uniqueRowNo":
                    field=field[1:]
                NewDict[field ]=row["Input."+field+ "_"+ str(count)]        
            NewDict["HITId"]=row["HITId"]
            NewDict["HITTypeId"]=row["HITTypeId"] 
            AllRows.append(NewDict) 
            
    return AllRows        
                
                    
                    
def Execute(MTinputFile,MTResults,MTsplitResults,noofitemsHit): 
    oneitemrows=ReadRows(MTinputFile)
    fields=oneitemrows[0].keys()
    rowdicts= ReadRows(MTResults)
    NewRows=splitRowPairwise(rowdicts,noofitemsHit,fields)
    csv_wrapper.write_csv(MTsplitResults, NewRows)
    
       
        
        
if __name__ == '__main__':
    topic="gun-control"    #done for naacl begin end
    basedir=os.path.dirname(os.path.dirname(os.getcwd())) 
    MTinputFile= basedir +"/data/dialog_data/CSV/gun-control/MTdata/Phase1/Phase1_1SCUCounts/Keys_SCU_9"  # file used to create MT Task
    MTdir=  basedir+ "/data/mechanicalturk_data/gun-control/MTdata/Phase1/Phase1_1/"
    MTResults=MTdir + "MTResults/MT1Results/MT1_dialog_labelmapResults"
    MTsplitResults=MTdir +"MTResults/MT1Results/MT1_dialog_labelmap_split_worker.csv"
    noofitemsHit=9
    Execute( MTinputFile,MTResults,MTsplitResults,noofitemsHit)
