'''
NOT USED ANYMORE
Created on Nov 13, 2014
Format Summary from MT ouptput CSV to a HtML document , not used any further, for only simple summaries
@author: amita
'''
import os
from dialog_pkg_src import NewFormat_text
from dialog_pkg_src import FileHandling
def ReadRows(inputcsv):
    rows=FileHandling.read_csv(inputcsv)
    return rows
def Format(rows,noofitems):
    Lines=list()
    for row in rows:
        for count in range (1,noofitems+1):
            Lines.append("<br>")
            Lines.append("<br><b>Key</b>")
            Lines.append(row["key"+ str(count)])
            Lines.append("<br><b>Dialogtext</b>")
            Lines.append(row["Dialogtext" + str(count)])
           
            
            
    return Lines

def WriteHtml(outputfile,lines):
    f = open(outputfile +".html",'w')
    all_lines=" ".join(lines)
    all_lines= """<html><head></head><body>""" + all_lines +"""</body> </html>"""  
    f.write(all_lines)    
    f.close()
def Execute(InputCsv,OutHTML,noofitems):
    rows=ReadRows(InputCsv)
    FormattedLines=Format(rows,noofitems)
    WriteHtml(OutHTML, FormattedLines)
    
    

        
        
if __name__ == '__main__':
    topic="gun-control"
    InputCsv= os.getcwd()+ "/"+ topic +"/MTdata/MT1/Summary/MT1Results_1_more750"
    OutHTML=os.getcwd()+ "/"+ topic +"/MTdata/MT1/Summary/MT1Results_1_more750"
    noofitems=3
    Execute(InputCsv,OutHTML,noofitems)
    