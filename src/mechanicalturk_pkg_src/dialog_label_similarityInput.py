'''
Created on May 8, 2015

@author: amita
'''

from data_pkg import FileHandling
from itertools import groupby
from operator import itemgetter
import os,sys


def ReadRows(AllScusjson):
    rows=FileHandling.jsontorowdicts(AllScusjson)
    return rows

def createMTrow(RowswithLabels,DialogSummRows):
    AllMTrows=[]
    sortedlabelskeys=sorted(RowswithLabels, key = itemgetter("key_user"))
    for key, items in groupby(sortedlabelskeys, key = itemgetter("key_user")):
        key_records = list(items)
        MTrow={}
        MTrow["key"]=key
        lastindexkey=key.find("user")
        dialogtext=[row for row in DialogSummRows if row["Key"]== key[:lastindexkey-1]]
        if len(dialogtext) != 1:
            print " error in function onerowMT for key - " + key
            sys.exit(0)
        MTrow["Dialogtext_"]=dialogtext[0]["Dialog"]
        intc=1
        for keyid_rec in key_records:
            MTrow["label_"+ str(intc)]=keyid_rec["label"]
            MTrow["weight_" + str(intc)]=keyid_rec["weight"]
            MTrow["id_" + str(intc)]=keyid_rec["id"]
            MTrow["uniqueRowNo_"+ str(intc)]=keyid_rec["_uniqueRowNo"]
            MTrow["contrib_"+ str(intc)]=keyid_rec["contrib"]
            intc=intc + 1
            
        AllMTrows.append(MTrow)
    return AllMTrows    
          
def Execute(AllScusJson,scucountperkey,DialogSummjson,MTfile): 
    
    
    
    
    
    RowswithLabels=ReadRows(scucountperkey) 
    DialogSummRows=ReadRows(DialogSummjson) 
    AllMTrows=createMTrow(RowswithLabels,DialogSummRows)
    fieldnames=AllMTrows[0].keys() 
    FileHandling.write_csv(MTfile, AllMTrows, fieldnames)
    FileHandling.writejson(AllMTrows,MTfile)
    
if __name__ == '__main__':
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    topic="gun-control"
    directory=basedir + "/data/dialog_data/CSV/"+ topic+ "/MTdata/Phase1"
    AllScusJson=directory + "/AllScusJson.json"
    scucountperkey=directory + "/Phase1_1SCUCounts/Keys_SCU_9.json"
    DialogSummjson=basedir + "/data/dialog_data/CSV/"+ topic+ "/MTdata/MTSummary/AllMTSummary_50/NaturalSummaryJson.json"
    MTfile=basedir + "/data/mechanicalturk_data/" + topic+"/MTdata/Phase1/Phase1_1/MT1dialog_labelmap"
    Execute(AllScusJson,scucountperkey,DialogSummjson,MTfile)
    