'''
Created on Jan 25, 2015
Take input as MT Results file containingnatural summaries and output a formatted summary html file with word count
redo 1-3206_44_29__64_66_69_77_82_107_1  A1MYLQQL8BBOYT
'''
from data_pkg  import FileHandling 
import os
from data_pkg import NewFormat_text 
from collections import OrderedDict 
import re

ignorelist=["1-15_71_69__72_74_77_82_83_87_89_92_2", "1-10677_206_205_1_207_208_210_211_214_219_221_222_223_225_226_227_228_229_230_12"]
class IndividualHits:
    def __init__(self,InHitMT,outputtextfile,Noofdialog_hit):
        self.input=InHitMT
        self.dialognos_hit=Noofdialog_hit
        self.outputtextfile=outputtextfile
        
        
    # change s1 to author1 s2 to author2
    def formattextfield(self,summary,Textline,row): 
        Summary_Text=row[summary]
        p1 = re.compile( '(s1 | S1)')
        p1.sub("Author1",Summary_Text )
        p2 = re.compile( '(s2 | S2)')
        p2.sub("Author2",Summary_Text )
        Word_count= str(NewFormat_text.word_count(Summary_Text, removelist))
        Textline=Textline +"<br><b>"+"Word_Count:  " + Word_count +"</b>"
        Textline= Textline+"<br><b>"+ summary  +"<br></b>"
        Textline= Textline + Summary_Text + "</p>" 
        return Textline  
    
    
    
     # take the results hit file and create a formatted html output   
    def CreateHitOutput(self):
        rowdicts=FileHandling.read_csv(self.input)
        Textline=""
        Textline= Textline + """<html><head></head><body>"""
        for row in rowdicts:
            if row["Input.key1"]  in  ignorelist:
                continue
            Textline=Textline +"<p><b>" +"Worker_Id:  " +"</b>"
            Textline=Textline+row["WorkerId"]
            Textline=Textline+"<br><b>" +"HIT_Id:   " +"</b>"
            Textline=Textline+row["HITId"]
            Textline=Textline +"<br><b>"+"AssignmentStatus:  " + row["AssignmentStatus"] +"</b>"
            Dialog=row["Input.Dialogtext1"]
            key=row["Input.key1"]
            Textline=Textline +"<br><b>"+"Key </b></br>" + key +"<br>"
            Textline=Textline +"<br><b>"+"Dialog </b></br>" + Dialog +"<br>"
            Textline=self.formattextfield("Answer.Dialog_Natural",Textline,row)
                
        Textline=Textline + """</body> </html>""" 
        FileHandling.writeHtml(self.outputtextfile, Textline)            


if __name__ == '__main__':
    
    topic= "abortion"
    removelist=[".","?","!"]
    noofitems=1
    
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"/data/mechanicalturk_data/" 
    InputCsv= Directory + topic +"/MTdata/Phase1/MT1/Summary_results/MT1_midrangeResults"
    OutHTML= Directory +  topic +"/MTdata/Phase1/MT1/Summary_results/MT1_midrangeResults"
    MThitobj= IndividualHits(InputCsv,OutHTML,noofitems)
    MThitobj.CreateHitOutput()
