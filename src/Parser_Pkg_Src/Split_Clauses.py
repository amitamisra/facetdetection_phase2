'''
Created on Mar 30, 2015
Take input summaries from json with no regular expression, create input directory for split clauses, split into clauses
Input comes from  executing AllMTSummaryCSV.py
@author: amita
'''
import subprocess
from dialog_pkg_src import FileHandling
import os
import sys
# Read Input File containing summaries
def ReadRows(InputJson):
    Rows=FileHandling.jsontorowdicts(InputJson)
    return Rows

# Not working from here, instead run the command on shell
#python run_all.py /Users/amita/git/FacetIdentification/src/Parser_Pkg_Data/gay-rights-debates/SL-seg/Input /Users/amita/git/FacetIdentification/src/Parser_Pkg_Data/gay-rights-debates/SL-seg/Output  ./parser05Aug16
#python run_all.py /Users/amita/git/FacetIdentification/src/Parser_Pkg_Data/gun-control/SL-seg/Input /Users/amita/git/FacetIdentification/src/Parser_Pkg_Data/gun-control/SL-seg/Output ./parser05Aug16
# download slseg, download charnaik parser as 
def Split(SummaryInpDir, SummaryOutDir, ParserPath, CharnaikParser,currentdir):
    os.chdir(ParserPath)
    cmd=str("python run_all.py" +" "+ SummaryInpDir +" "+  SummaryOutDir +" " + CharnaikParser)  
    proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    output, errors=proc.communicate()
    print errors
   

# create input directory for splitting summaries into clauses as required by slseg
def createdirectory(SummaryList,summarykeys,SlsegInput,SlsegOutput,keyfile):
    if not os.path.exists(SlsegInput):
        print " dir not exist" + SlsegInput
        os.makedirs(SlsegInput) 
    if not os.path.exists(SlsegOutput):    
        os.makedirs(SlsegOutput) 
    for summno in summarykeys:
            Filepath=os.path.join(SlsegInput,keyfile+"Num" +"NatText_"+ summno[-1])
            summary= SummaryList[summno]
            FileHandling.WriteTextFile(Filepath, summary)
            
             
         
         
def Execute(InputJson,SlsegInput,SlsegOutput,summarykeys,ParserPath,CharnaikParser,currentdir):    
    Rows=ReadRows(InputJson)
    for row in Rows:
        SummaryList=row["Natural_Summary_Text"] 
        keyfile=row["Key"]
        Indir= os.path.join(SlsegInput,keyfile)
        OutDir=os.path.join(SlsegOutput,keyfile)
        createdirectory(SummaryList,summarykeys,SlsegInput,SlsegOutput,keyfile)
        #Split(Indir, OutDir,ParserPath, CharnaikParser,currentdir)
        
        
if __name__ == '__main__':
    #done for gun control
    #------------------------------------------------------- topic="gun-control"
    # summarykeys=["Natural_Summary_Text_5","Natural_Summary_Text_4","Natural_Summary_Text_3","Natural_Summary_Text_2","Natural_Summary_Text_1"]
    # InputFile=os.path.join(os.path.dirname(os.getcwd()),"Data_Pkg_Data","CSV",topic,"MTdata","MTSummary","AllMTSummary_50", "NaturalSummaryJson.json")
    #-------------------------- ParserPath="/Users/amita/software/SLSeg_ver_0.2"
    #-------------------------------------------- CharnaikParser="parser05Aug16"
    #------------------------------------------------------ currentdir=os.getcwd
    # SlsegInput=os.path.join(os.path.dirname(os.getcwd()), "Parser_Pkg_Data",topic,"SL-seg","Input")
    # SlsegOutput=os.path.join(os.path.dirname(os.getcwd()), "Parser_Pkg_Data",topic,"SL-seg","Output")
    # Execute(InputFile,SlsegInput,SlsegOutput,summarykeys,ParserPath, CharnaikParser,currentdir)
#------------------------------------------------------------------------------ 
    
    
    
    topic="gay-rights-debates"
    ParserPath="/Users/amita/software/SLSeg_ver_0.2"
    CharnaikParser="parser05Aug16"
    summarykeys=["Natural_Summary_Text_5","Natural_Summary_Text_4","Natural_Summary_Text_3","Natural_Summary_Text_2","Natural_Summary_Text_1"]
    
    #InputFile=os.path.join(os.path.dirname(os.getcwd()),"Data_Pkg_Data","CSV",topic,"MTdata","MTPhaseFormatted1-2", "SummaryNoRegPhase1_2.json")
    #SlsegInput=os.path.join(os.path.dirname(os.getcwd()), "Parser_Pkg_Data",topic,"MTPhaseFormatted1-2","SL-seg","Input")
    #SlsegOutput=os.path.join(os.path.dirname(os.getcwd()), "Parser_Pkg_Data",topic,"MTPhaseFormatted1-2","SL-seg","Output")
    #Execute(InputFile,SlsegInput,SlsegOutput,summarykeys,ParserPath, CharnaikParser,os.getcwd())
    
    InputFile=os.path.join(os.path.dirname(os.getcwd()),"Data_Pkg_Data","CSV",topic,"MTdata","MTPhaseFormatted1", "SummaryNoRegPhase1.json")
    SlsegInput=os.path.join(os.path.dirname(os.getcwd()), "Parser_Pkg_Data",topic,"MTPhaseFormatted1","SL-seg","Input")
    SlsegOutput=os.path.join(os.path.dirname(os.getcwd()), "Parser_Pkg_Data",topic,"MTPhaseFormatted1","SL-seg","Output")
    Execute(InputFile,SlsegInput,SlsegOutput,summarykeys,ParserPath, CharnaikParser,os.getcwd())
    
    
    
    InputFile=os.path.join(os.path.dirname(os.getcwd()),"Data_Pkg_Data","CSV",topic,"MTdata","MTPhaseFormatted2", "SummaryNoRegPhase2.json")
    SlsegInput=os.path.join(os.path.dirname(os.getcwd()), "Parser_Pkg_Data",topic,"MTPhaseFormatted2","SL-seg","Input")
    SlsegOutput=os.path.join(os.path.dirname(os.getcwd()), "Parser_Pkg_Data",topic,"MTPhaseFormatted2","SL-seg","Output")
    Execute(InputFile,SlsegInput,SlsegOutput,summarykeys,ParserPath, CharnaikParser,os.getcwd())