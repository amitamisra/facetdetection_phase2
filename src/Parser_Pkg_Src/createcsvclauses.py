'''
Created on Mar 30, 2015
Take the html file created by split_clauses, the final segmentation was done at command line 
by running the script
python run_all.py /Users/amita/git/FacetIdentification/src/Parser_Pkg_Data/SL-seg/Input /Users/amita/git/FacetIdentification/src/Parser_Pkg_Data/SL-seg/Output ./parser05Aug16 -T120
 from /Users/amita/software/SLSeg_ver_0.2 directory.
Use the output from the command to and create a csv with clauses as individual fields.

@author: amita
'''
import os 
from dialog_pkg_src import FileHandling
from dialog_pkg_src import NewFormat_text
from bs4 import BeautifulSoup
def ReadClauseDir(ClauseDir):
    print

    
def Execute(ClauseInputdir,ClauseOutDir,ClauseFile):
    AllRows=list()
    FileList=os.listdir(ClauseInputdir)
    for filename in FileList:
        filepath=ClauseOutDir +"/"+ "out5"+ filename
        Lines=FileHandling.ReadTextFile(filepath)
        LinesStr=" ".join(Lines)
        soup=BeautifulSoup(LinesStr)
        sents=soup.find_all("s")
        sent_no=0
        for sent in sents:
            sent_no=sent_no+1
            clauses=sent.find_all("c")
            clause_no=0
            for clause in clauses:
                clause_no=clause_no+1
                clausetext=clause.get_text()
                row=dict()
                row["Clause_text_POS"]=clausetext
                text_NoPos=NewFormat_text.removePOSSSeg(clausetext)
                row["Clause_text"]= text_NoPos
                row["Clause_No"]=clause_no
                row["Key"]=filename[: str(filename).find("NumNatText")] 
                row["Summary_No"]=filename[-5]
                row["Sent_No"]=sent_no
                row["Sent_text_POS"]=sent.text
                text_NoPos=NewFormat_text.removePOSSSeg(sent.text)
                row["Sent_text"]=  text_NoPos
                row["Summary_text"]=FileHandling.ReadTextFile(ClauseInputdir + "/" + filename)
                AllRows.append(row)
    fieldnames=sorted(AllRows[0].keys())            
    FileHandling.write_csv(ClauseFile, AllRows,fieldnames)
    FileHandling.writejson(AllRows, ClauseFile)

if __name__ == '__main__':
    topic="gun-control"
    SlSegDir=os.path.dirname(os.getcwd()) +"/Parser_Pkg_Data/"+ topic +"/Phase1/SL-seg/"
    ClauseInputdir=SlSegDir+"Input"
    ClauseOutDir= SlSegDir +"Output/step5_segmented"
    ClauseFile=os.path.dirname(os.getcwd()) +"/Parser_Pkg_Data/"+ topic +"/Phase1/SL-seg/Phase1Clauses"
    Execute(ClauseInputdir,ClauseOutDir,ClauseFile)
    
    
    
    topic="gay-rights-debates"
    SlSegDir=os.path.dirname(os.getcwd()) +"/Parser_Pkg_Data/"+ topic +"/MTPhaseFormatted1/SL-seg/"
    ClauseInputdir=SlSegDir+"Input"
    ClauseOutDir= SlSegDir +"Output/step5_segmented"
    ClauseFile=SlSegDir+"/Phase1Clauses"
    Execute(ClauseInputdir,ClauseOutDir,ClauseFile)
    
    
    
    
    