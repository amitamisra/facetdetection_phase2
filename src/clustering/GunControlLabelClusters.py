'''
Created on May 1, 2015

@author: amita
'''
import os
import InputScikit
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from sklearn.cluster import DBSCAN, AgglomerativeClustering 
import operator
import itertools
from data_pkg import FileHandling
from sklearn.preprocessing import StandardScaler
from sklearn.feature_extraction.text  import TfidfVectorizer
import ClusterTypes


def createclusterFile(inputjson,field,disfield,clusterfile, featurelist,stem,stemmername,stop, stoplist, take_set_string):
    AllNewRows=InputScikit.featureRowsclustering(inputjson,field,disfield, featurelist,stem,stemmername,stop, stoplist, take_set_string)
    fieldnames= AllNewRows[0].keys()
    FileHandling.write_csv(clusterfile,AllNewRows,fieldnames)


def AddLIWCStringFile(inputjson, OutputLIWC,LIWCfield):
    rows=FileHandling.jsontorowdicts(inputjson) 
    NewRows=InputScikit.AddLIWCStringRows(rows,LIWCfield) 
    fieldnames=NewRows[0].keys()
    FileHandling.write_csv( OutputLIWC, NewRows, fieldnames)
    FileHandling.writejson(NewRows,  OutputLIWC)
     
       
def Execute(numcluster,disfield, inputjson,OutputLIWC,LIWCfield,clusterInpfile,clusterout, featurelist,stem,stemmername,stop,stoplist, take_set_string,field):
    AddLIWCStringFile(inputjson, OutputLIWC, LIWCfield)
    createclusterFile(OutputLIWC+".json",LIWCfield,disfield, clusterInpfile, featurelist,stem,stemmername,stop, stoplist, take_set_string)
    AllRows=FileHandling.read_csv(clusterInpfile)
    lines=[]
    clustertexts=[row[field] for row in AllRows]
    Res_Labels_metric=ClusterTypes.createAgglomerativecluster(clustertexts,numcluster)
    InputScikit.cluster_output_labels(Res_Labels_metric[0],AllRows,lines)
    metric=Res_Labels_metric[1]

    ClusterFeatures="\n Features used are \n" +  str(featurelist)  + "\n" + "Agglomerativecluster, numcluster: "+ str(numcluster)+" clusters, metric: " + str(metric)
    lines.append( ClusterFeatures)
    FileHandling.WriteTextFile(clusterout, lines)     
if __name__ == '__main__':
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    dialogdir=basedir +"/data/dialog_data/CSV/"
    topic="gun-control"
    clusterdir=basedir + "/data/similarity_data/"
    inputjson= dialogdir + topic + "/MTdata/Phase1/Pyramids_Natural/Disammore2_Scus.json"
    OutputLIWC=dialogdir + topic + "/MTdata/Phase1/Pyramids_Natural/Disammore2_Scus_LIWC"
    
    featurelist=["DISAM"]
    filename="clusterTextInput_"+str(featurelist)
    clusterInpfile=clusterdir + topic + "/MTdata_cluster/Phase1/Pyramids_Natural/ClusterTypes/Agg/"+ filename
    clusterout=clusterdir + topic + "/MTdata_cluster/Phase1/Pyramids_Natural/ClusterTypes/Agg/" + filename
    stem=True
    stop=True
    take_set_string=True
    stemmername=SnowballStemmer("english")
    stoplist=stopwords.words('english')
    
    InputAttr="/Users/amita/git/FacetIdentification/data/cluster_data/verb_Attribution"
    ownstoplist=["Author1", "Author2", "author1", "author1"]
    stoplist= stoplist+ ownstoplist
    field="clustertext"
    LIWCfield="label"
    numcluster=50
    disfield="Words_WSD"
    Execute(numcluster,disfield,inputjson, OutputLIWC,LIWCfield,clusterInpfile,clusterout,featurelist,stem,stemmername,stop, stoplist, take_set_string, field)
    