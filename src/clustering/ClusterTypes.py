'''
Created on May 1, 2015

@author: amita
'''
import os
import InputScikit
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from sklearn.cluster import DBSCAN, AgglomerativeClustering 
import operator
import itertools
from data_pkg import FileHandling
from sklearn.preprocessing import StandardScaler
from sklearn.feature_extraction.text  import TfidfVectorizer
from sklearn import metrics

def createAgglomerativecluster(AllStrings, Numcluster):
    Agg_cluster=AgglomerativeClustering(Numcluster)
    termdoc=createtermdocTFIDFVect(AllStrings)
    Res_Labels=Agg_cluster.fit_predict(termdoc.toarray())
    metric=metrics.silhouette_score(termdoc.toarray(), Res_Labels)
    return  (Res_Labels,metric)

def createclusterDBSCAN(AllStrings,epsvar,minsample,metricvar):
    DBSCanCluster=DBSCAN(eps=epsvar,min_samples=minsample,metric=metricvar)
    termdoc=createtermdocCountVector(AllStrings)
    termdoc=termdoc.todense()
    Res_Labels=DBSCanCluster.fit_predict(termdoc)
    return  Res_Labels
    

def createtermdocTFIDFVect(AllStrings):
    vectorizer_termTuple= InputScikit.TermDoc_TFIDFCountVector(AllStrings)
    termdoc=vectorizer_termTuple[1]
    return termdoc

def createtermdocCountVector(AllStrings):
    vectorizer_termTuple= InputScikit.TermDoc_CountVector(AllStrings)
    termdoc=vectorizer_termTuple[1]
    return termdoc

def getVerbAttr(InputAttr):
    Rows=FileHandling.read_csv(InputAttr)
    verbattr=[stemmername.stem(row["VerbAttr"]) for row in Rows]
    return verbattr
def createclusterFile(inputjson,clusterfile, featurelist,stem,stemmername,stop, stoplist, take_set_string):
    AllNewRows=InputScikit.featureRowsclustering(inputjson, featurelist,stem,stemmername,stop, stoplist, take_set_string)
    fieldnames= AllNewRows[0].keys()
    FileHandling.write_csv(clusterfile,AllNewRows,fieldnames)
    
       
def Execute(inputjson,clusterInpfile,clusterout, featurelist,stem,stemmername,stop,stoplist, take_set_string,field):
    createclusterFile(inputjson,clusterInpfile, featurelist,stem,stemmername,stop, stoplist, take_set_string)
    AllRows=FileHandling.read_csv(clusterInpfile)
    SortedRows=sorted(AllRows, key= operator.itemgetter('Key'))
    lines=[]
    eps=2.0
    minsample=2
    metric='euclidean'
    for key, items in itertools.groupby(SortedRows, operator.itemgetter('Key')):
        Summaries_dialog=list(items)
        clustertexts=[row[field] for row in Summaries_dialog]
       # Res_Labels=createcluster(clustertexts, eps,minsample,metric)
        Res_Labels=createAgglomerativecluster(clustertexts)
        InputScikit.cluster_output_labels(Res_Labels,Summaries_dialog,lines)
   # ClusterFeatures= "\n Features used are \n" +  str(featurelist)  + "\n"+ "eps value is     " + str(eps) + " \nminsample value is "  +  str(minsample) + "\nmetric used is " + str(metric)
    
    ClusterFeatures="\n Features used are \n" +  str(featurelist)  + "\n" + "Agglomerativecluster,  7 clusters"
    lines.append( ClusterFeatures)
    FileHandling.WriteTextFile(clusterout, lines)     
if __name__ == '__main__':
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    dialogdir=basedir+"/data/dialog_data/CSV/"
    topic="gun-control"
    clusterdir=basedir + "/data/similarity_data/"
    
    
    
    
    #inputjson=dialogdir+topic + "/MTdata/Phase1/MTSummary/CorefReplacedSummary/AllMTSummary_50Replaced/SimpleSummary/CoreNLPRepCorefSimpSumm.json"
    #featurelist=["Adj_New_SummarySent","Noun_New_SummarySent","Verb_New_SummarySent"]
    #clusterInpfile=clusterdir + topic + "/MTdata_cluster/Phase1/SimpleSummary/AllMTSummary_50Replaced/ClusterTypes/Agg/clusterTextInput1"
    #clusterout=clusterdir + topic + "/MTdata_cluster/Phase1/SimpleSummary/AllMTSummary_50Replaced/ClusterTypes/Agg/clusterOutput1"
    
    
    inputjson=dialogdir+topic + "/MTdata/Phase1/MTSummary/CorefReplacedSummary/AllMTSummary_50Replaced/NaturalSummary/CoreNLPRepCorefNaturalSumm.json"
    clusterInpfile=clusterdir + topic + "/MTdata_cluster/Phase1/NaturalSummary/AllMTSummary_50Replaced/ClusterTypes/Agg/clusterTextInput1"
    clusterout=clusterdir + topic + "/MTdata_cluster/Phase1/NaturalSummary/AllMTSummary_50Replaced/ClusterTypes/Agg/clusterOutput1"
    featurelist=["Adj_New_SummarySent","Noun_New_SummarySent","Verb_New_SummarySent"]
   
    
    
    inputjson= dialogdir + topic + "/MTdata/Phase1/MTSummary/AllMTSummary_50/Ext_DistCoreNLPNaturalSummaryOneSumm.json"
    clusterInpfile=clusterdir + topic + "/MTdata_cluster/Phase1/NaturalSummary/AllMTSummary_50/ClusterTypes/Agg/clusterTextInput1"
    clusterout=clusterdir + topic + "/MTdata_cluster/Phase1/NaturalSummary/AllMTSummary_50/ClusterTypes/Agg/clusterOutput1"
    featurelist=["Adj_Sent","Noun_Sent","Verb_Sent"]
    


    #inputjson= dialogdir + topic + "/MTdata/Phase1/MTSummary/AllMTSummary_50/CoreNLPSimpSumm.json"
    #clusterInpfile=clusterdir + topic + "/MTdata_cluster/Phase1/SimpleSummary/AllMTSummary_50/ClusterTypes/Agg/clusterTextInput1"
    #clusterout=clusterdir + topic + "/MTdata_cluster/Phase1/SimpleSummary/AllMTSummary_50/ClusterTypes/Agg/clusterOutput1"
    #"5ExtendedAdVerb_Sent","5ExtendedVerb_Sent","5ExtendedAdj_Sent","5ExtendedNoun_Sent"]
    #featurelist=["Noun_SummarySent","Adj_SummarySent","Verb_SummarySent"]
    
    
    stem=True
    stop=True
    take_set_string=True
    stemmername=SnowballStemmer("english")
    stoplist=stopwords.words('english')
    
    InputAttr="/Users/amita/git/FacetIdentification/data/cluster_data/verb_Attribution"
    verbattr= getVerbAttr(InputAttr)
    ownstoplist=["Author1", "Author2", "author1", "author1"]
    stoplist= stoplist+ ownstoplist + verbattr
    field="clustertext"
    Execute(inputjson, clusterInpfile,clusterout,featurelist,stem,stemmername,stop, stoplist, take_set_string, field)
    