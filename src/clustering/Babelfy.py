'''
Created on Jun 23, 2015

@author: amita
'''

import urllib
import json
import gzip
import urllib2
from io import StringIO
import ConfigParser, codecs, copy, csv

def runGC():
    args= ReadCongigFile()
    inputcsv= args[0]
    outputcsv=args[1]
    babelfy_field=args[2]
    key=args[3]
    lang=args[4]
    Execute(inputcsv,outputcsv,babelfy_field,key,lang)

def ReadCongigFile():
    config = ConfigParser.ConfigParser()
    config.read('Babelfy_Config.ini')
    input_file = config.get('GC', 'input-file')
    output_file= config.get('GC', 'output-file')
    babelfy_field= config.get('GC', 'babelfy_field')
    key= config.get('GC', 'key')
    language= config.get('GC', 'language')
    arguments=(input_file,output_file,babelfy_field,key,language)
    return  (arguments)

def Execute(inputcsv,outputcsv,babelfy_field,key,lang):
    text="In study after study the evidence shows that states with the least"
    synset=getsynset(text, key, lang)
    rows=read_csv(inputcsv)
    AllRows=[]
    for row in rows:
        text=row[babelfy_field]
        synset=getsynset(text, key, lang)
        NewRow=copy.deepcopy(row)
        NewRow["BabSynset"]=synset
        AllRows.append(NewRow)
        
    
    
def read_csv(inputcsv):   
        inputfile = codecs.open(inputcsv,'r',encoding='utf-8') 
        result = list(csv.DictReader(inputfile))
        return result 

def getsynset(text, key, lang):
        service_url = 'https://babelfy.io/v1/disambiguate'
        params = {
            'text' : text,
            'lang' : lang,
            'key'  : key,
            'match':  'PARTIAL_MATCHING',
            'cands':'TOP',
            #'MCS':'ON'
            #'MCS':'OFF'
        }
        
        url = service_url + '?' + urllib.urlencode(params)
        request = urllib2.Request(url)
        request.add_header('Accept-encoding', 'gzip')
        response = urllib2.urlopen(request)

        
        if response.info().get('Content-Encoding') == 'gzip':
            buf = StringIO( response.read())
            f = gzip.GzipFile(fileobj=buf)
            data = json.loads(f.read())
        
            # retrieving data
            for result in data:
                        # retrieving token fragment
                        tokenFragment = result.get('tokenFragment')
                        print tokenFragment
                        tfStart = tokenFragment.get('start')
                        tfEnd = tokenFragment.get('end')
                        print result.get('BabelNetURL')
                        print str(tfStart) + "\t" + str(tfEnd)
              
                # retrieving char fragment
                        charFragment = result.get('charFragment')
                        cfStart = charFragment.get('start')
                        cfEnd = charFragment.get('end')
                        #print str(cfStart) + "\t" + str(cfEnd)
                        #print text[cfStart:cfEnd+1]
                        # retrieving BabelSynset ID
                        synsetId = result.get('babelSynsetID')
                        return synsetId  
                    
                    
                    
                      

                       
if __name__ == '__main__':
    runGC()