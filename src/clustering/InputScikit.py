'''
Created on May 3, 2015

@author: amita
'''
from sklearn.feature_extraction.text  import CountVectorizer
from sklearn.feature_extraction.text  import TfidfVectorizer
from data_pkg import FileHandling
import os
from sklearn.feature_selection import  SelectKBest, chi2 
from nltk.tokenize import word_tokenize
from data_pkg import NewFormat_text
from copy import deepcopy
from operator import itemgetter
import itertools
from similarity_pkg import word_category_counter
NotToUseCategory=["Unique Words","Words Per Sentence","Word Count","Dictionary Words","Common Verbs", "Six Letter Words", "Present Tense",\
                   "Total Function Words", "Word Count", "Total Pronouns","Present Tense","Adverbs", "Impersonal Pronouns", "Sentences","Space","Past Tense", "Auxiliary Verbs",\
                   "Quantifiers","Dash","Number","All Punctuation","Impersonal Pronouns","Personal Pronouns", "First Person Singular", "First Person Plural",\
                   "Second Person", "Third Person Singular", "Third Person Plural", "Articles", "Future Tense", "Total third person", "Total first person", \
                     "Other Punctuation", "Prepositions"]
def AddLIWCCategory(String):
        NewString=" "
        LIWC=word_category_counter.score_text(String)
        for key, value in LIWC.iteritems():
            if key not in NotToUseCategory:
                key=str(key).replace(" ", "")
                for count in range(0,value):
                    NewString = NewString + " "+ key
        return NewString      
def cluster_output_labels(Res_Labels,Allrow_dicts,lines):
        
        doccount=0
        cluster_list=list() 
        for label in Res_Labels:
            row_cluster=dict()
            row_cluster["label_cluster"]=str(label)
            row_cluster["Sentence"]=Allrow_dicts[doccount]["label"]
            row_cluster["key"]=Allrow_dicts[doccount]["key_user"]
            doccount=doccount+1
            cluster_list.append(row_cluster)
        sortedcluster=sorted(cluster_list, key = itemgetter("label_cluster")) 
        lines.append("key " )
        lines.append(row_cluster["key"] )
        for key, items in itertools.groupby(sortedcluster, itemgetter('label_cluster')):
            cluster_list =list(items)
            lines.append("\n\nNew Cluster")
            lines.append(key)
            for cluster in cluster_list:
                for keyp,value in cluster.iteritems():
                    if keyp =="Sentence":
                        lines.append("\n")
                        lines.append(value)
        lines.append("\n\n New Key\n")             

def ReadRows(InputFile):
    AllRows=FileHandling.jsontorowdicts(InputFile)
    return AllRows

def TextField(AllRows,Textfieldname,labelname):
    Texts=[row[Textfieldname] for row in AllRows]
    if labelname:
        labels=[row[labelname] for row in AllRows]
        return (Texts,labels)
    else:
        return (Texts,labelname)


def AddLIWCStringRows(rows, stringfield):
    NewRows=[]
    for row in rows:
        newdict=deepcopy(row)
        String=row[stringfield]
        LIWC=AddLIWCCategory(String)
        newdict["LIWC"]=LIWC
        NewRows.append(newdict)
    return NewRows
    
# stemming, lowercase, removestopwords, decide which features( noun, verb, adj, adverb, dist sim to keep        


def getfeaturelist(featurelist):
    featurelistnames=[]
    if "baseline" in featurelist:
        featurelistnames.append("label")
        
    else:    
        featurelistnames.extend(["AdVerb_label", "Verb_label","Adj_label","Noun_label"])
        if "LIWC" in featurelist:
            featurelistnames.append("LIWC")
        if "DIST" in featurelist:
            extlist=["5ExtendedAdVerb_label","5ExtendedVerb_label","5ExtendedAdj_label","5ExtendedNoun_label"]
            featurelistnames.extend(extlist)
    return     featurelistnames


def disamtext(AllRows,field,disfield, stop,stoplist,stem,stemmername):
    AllNewRows=[]
    for row in AllRows:
        Newrow=deepcopy(row)
        newstring =" "
        text=row[field]
        wors_arr=list(row[disfield])
        wordlist=word_tokenize(text) 
        wordlist=NewFormat_text.stemstoptokens(stop,stoplist,stem,stemmername,wordlist)
        for token in wordlist:
            word_details=[worddict for worddict in wors_arr if worddict["WordText"]==token]
            if word_details:
                synset=word_details[0]["BabeId"]
                newstring=newstring + " " + synset
            else:
                newstring=newstring + " " + token
        Newrow["clustertext"] = newstring
        AllNewRows.append(Newrow)     
    return AllNewRows                
            

def featureRowsclustering(inputjson,field,disfield, featurelist,stem,stemmername,stop,stoplist, take_set_string):
    extlist=["5ExtendedAdVerb_label","5ExtendedVerb_label","5ExtendedAdj_label","5ExtendedNoun_label"]
    featurelistnames=getfeaturelist(featurelist)
    AllRows=ReadRows(inputjson)
    AllNewRows=[]
    if "DISAM" in featurelist:
        AllNewRows=  disamtext(AllRows,field,disfield, stop,stoplist,stem,stemmername)
        return AllNewRows
    for row in AllRows:
        Newrow=deepcopy(row)
        NewString=""
        for featurename in featurelistnames:
            String=row[featurename].lower()
            wordlist=word_tokenize(String) 
            newwordlist=NewFormat_text.stemstoptokens(stop,stoplist,stem,stemmername,wordlist)
            if  featurename in  extlist :
                setwordlist= set(newwordlist)
                NewString= NewString + " " + " ".join(setwordlist)
            else:
                NewString= NewString + " ".join(newwordlist)
                
        Newrow["clustertext"] = NewString
        AllNewRows.append(Newrow)
    return AllNewRows
    
def ckeckvectorizer(Texts):
    vectorizer = CountVectorizer()
    term_doc=vectorizer.fit_transform(Texts)
    names= vectorizer.vocabulary_ 
    print names
    feature_names = vectorizer.get_feature_names()
    for col in term_doc.nonzero()[1]:
        print feature_names[col], ' - ', term_doc[0, col]
        print feature_names[col], ' - ', term_doc[1, col]

# get term document matrix with count vectorizer, fit and transform both
def TermDoc_CountVector(Texts):
    vectorizer = CountVectorizer()
    term_doc=vectorizer.fit_transform(Texts)
    return (vectorizer,term_doc)



def TermDoc_TFIDFCountVector(AllStrings):
    vectorizer = TfidfVectorizer()
    term_doc=vectorizer.fit_transform(AllStrings)
    return (vectorizer,term_doc)
    

def Execute(Alltopicscv,Textfieldname,labelfield,outputcsv):
    AllRows=ReadRows(Alltopicscv)
    Texts=TextField(AllRows,Textfieldname,labelfield)
    vec_termdoc=TermDoc_TFIDFCountVector(Texts[0])
    labels=Texts[1]
    chisquare(vec_termdoc[0],vec_termdoc[1],labels,outputcsv)
    
def chisquare(vectorizer,term_doc,labels,outputcsv): 
    feature_names = vectorizer.get_feature_names()
    ch2 = SelectKBest(chi2, "all")
    X_train = ch2.fit_transform(term_doc, labels)
  #  Anova=SelectKBest(f_classif, "all")
   # X_trainAnova=Anova.fit_transform(term_doc, labels)
    #feature_names = [feature_names[i] for i in ch2.get_support(indices=True)]
    #print get_feature_names()
    Allrows=[]
    count=0
    length=len(feature_names)
    while count < length:
        
        row={}
        row["name"]=feature_names[count]
        row["score"]=ch2.scores_[count]
        count=count+1
        Allrows.append(row)
    fieldnames=Allrows[0].keys()    
    FileHandling.write_csv(outputcsv, Allrows, fieldnames)
    
if __name__ == '__main__':
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Alltopicscv=basedir + "/data/SQL_data/Alltopics.json"
    fieldname="text"
    labelfield="label"
    test="/Users/amita/Documents/workspace/EmailWorker/src/TestScikit/VectorizerTest"
    outputcsv="/Users/amita/Documents/workspace/EmailWorker/src/TestScikit/Vectorizerchi"
    term_doc=Execute(Alltopicscv, fieldname,labelfield,outputcsv)
   # print term_doc