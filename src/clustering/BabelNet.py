'''
Created on Jun 23, 2015

@author: amita
'''

import json
import gzip
import urllib
import urllib2
from StringIO import StringIO
import ConfigParser


def ReadCongigFile():
    config = ConfigParser.ConfigParser()
    config.read('BabelNet_Config.ini')
    input_file = config.get('GC', 'input-file')
    output_file= config.get('GC', 'output-file')
    arguments=(input_file,output_file)
    return  (arguments)


#retrieve all hypernyms,antonyms, holonym 
def GetEdges(Synid,Key):
    hypernymlist=[]
    holonymlist=[]
    antonymlist=[]
    service_url = 'https://babelnet.io/v1/getEdges'
    params = {
        'id' : Synid,
        'key'  : Key
    }
    
    url = service_url + '?' + urllib.urlencode(params)
    request = urllib2.Request(url)
    request.add_header('Accept-encoding', 'gzip')
    response = urllib2.urlopen(request)
    
    if response.info().get('Content-Encoding') == 'gzip':
        buf = StringIO( response.read())
        f = gzip.GzipFile(fileobj=buf)
        data = json.loads(f.read())
    
        # retrieving Edges data
        for result in data:
            target = result.get('target')
            language = result.get('language')
            if language =="EN" :
            
                # retrieving BabelPointer data
                pointer = result['pointer']
                relation = pointer.get('fName')
                if 'hypernym' in relation.lower():
                    hypernymlist.append(target.encode('utf-8'))
                if 'antonym' in relation.lower():
                    antonymlist.append(target.encode('utf-8'))
                if  'holonym' in relation.lower():
                    holonymlist.append(target.encode('utf-8'))
                    
        return(hypernymlist,holonymlist,antonymlist)                


synid="bn:00032047n"
key="579a0637-bc40-4860-83f9-34e66151a737"
GetEdges(synid,key)