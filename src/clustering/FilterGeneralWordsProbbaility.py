#!/usr/bin/env python
#coding: utf8 
'''
Created on May 2, 2015
NOt USED
@author: amita
'''
import os
from data_pkg import FileHandling
from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize
import nltk.probability
from data_pkg import NewFormat_text

import InputScikit

# read a file and return all words in the filw
def ReadText(topictextfile):
    Allwords=[]
    AllLines=FileHandling.ReadTextFile(topictextfile + ".txt")
    for line in AllLines[1:100]:
        asciitext=NewFormat_text.ascii_only(line)
        sent_list = sent_tokenize(asciitext)
        for sent in sent_list:
            words=word_tokenize(sent)
            Allwords.extend(words)
    return  Allwords

def probability(AllTopicWordList,AllwordsList):
    freqDistTopic=[]
    for topiclist in AllTopicWordList:
        top=nltk.probability.FreqDist(topiclist[0])
        print top.most_common(10)
        freqDistTopic.append(top)
    freqwallword= nltk.probability.FreqDist(AllwordsList[0]) 
    Topall=freqwallword.most_common(10)
    print  Topall
    print
        
        
        

def commonwords(AllTopicWordList):
    setword=set(AllTopicWordList[0][0])
    for topicwordlist in  AllTopicWordList:
        setwordtopic=set(topicwordlist[0])
        setword=set.intersection(setword,setwordtopic)
    return  setword   

#create two types of word list, one list for each topic and other as set of all wordsfor all topics    
def TopicTextWordList(topic_list,NodialogdataDir, AllwordsList):
    AllTopicWordList=[]
    for topic in topic_list:
        topictextfile=NodialogdataDir + "/"+ topic
        words=ReadText(topictextfile)
        countwords=len(words)
        wordtuple=(words,countwords)
        AllTopicWordList.append(wordtuple)
        AllwordsList.extend(wordtuple)
    return AllTopicWordList   
        
def Execute(NodialogdataDir,topic_list):
    AllwordsList=[]
    AllTopicWordList=TopicTextWordList(topic_list,NodialogdataDir, AllwordsList) 
    probability(AllTopicWordList,AllwordsList)
    common = commonwords(AllTopicWordList)
    print common
    
    
    
       
if __name__ == '__main__':
    topic_list=["gay-rights-debates","evolution","abortion","gun-control","deathpenalty"]
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    dialogdir=basedir+"/data/dialog_data/CSV/"
    SQLResultsDir=basedir +"/data/SQL_data/"
    NodialogdataDir=SQLResultsDir + "/Nodialogdiscussions"
    Execute(NodialogdataDir,topic_list)
    