'''
Created on May 2, 2015

@author: amita
'''
import MySQLdb as mdb
import sys
import os
from data_pkg import FileHandling
from data_pkg import NewFormat_text

# get all posts from nldsiac
def createtopicPostCSV(dataset_id,topic_id,topicname, topiccsv):
    try:
            db1 = mdb.connect(host="localhost",user="root",passwd="",db="nldsiac042015")
            cursor = db1.cursor(mdb.cursors.DictCursor) 
            Query= """ select p.dataset_id,p.discussion_id,d.topic_id, p.post_id, d.discussion_url, t.text
                    from  discussions as d 
                    join 
                    posts as p
                    on p.dataset_id=d.dataset_id and
                       p.discussion_id = d.discussion_id
                    join texts as t
                    on t.dataset_id=p.dataset_id and
                        t.text_id=p.text_id
                    where d.dataset_id= (%s)
                    and d.topic_id= (%s)"""
            args=(dataset_id,topic_id)  
            cursor.execute(Query,args)
            rowdicts_tuple= cursor.fetchall()
            Listrowdicts=list(rowdicts_tuple)
            Fieldnames=Listrowdicts[0].keys()
            FileHandling.write_csv(topiccsv,Listrowdicts,Fieldnames)          
    
    
    except db1.Error, e:
        print e
        if db1:
            db1.rollback()
            sys.exit(1)
            print "Error %d: %s" % (e.args[0],e.args[1])
    finally:
        if db1:
            db1.close()   
   
def createtopicPost_Nodialog(topiccsv,topicname,SQLResultsDir,discavoid,topicNodialogcsv,AlltopicRows, topiclabel):
    AllText=[]
    topicPost_Nodialog=[]
    AllRows=FileHandling.read_csv(topiccsv)
    for row in AllRows:
        if row["discussion_id"] not in discavoid:
            topicPost_Nodialog.append(row) 
            AllText.append(row["text"])
            sent_list=NewFormat_text.getsents(row["text"])
            for sent in sent_list:
                Alltopicrow=dict()
                Alltopicrow["text"]=str(sent).lower()
                Alltopicrow["label"]=topiclabel
                AlltopicRows.append(Alltopicrow)
                
    fieldnames=topicPost_Nodialog[0].keys()
    FileHandling.write_csv(topicNodialogcsv,topicPost_Nodialog, fieldnames)
    FileHandling.WriteTextFile(topicNodialogcsv, AllText)

def gettopicname(topic_id, dataset_id,topic_id_mapping):
    Alltopics=FileHandling.read_csv(topic_id_mapping)
    topicname=[row["topic"] for row in Alltopics if row["dataset_id"] == dataset_id  and row["topic_id"]== topic_id]
    return topicname[0]


def creatediscussionAvoid(dialogdir):  
    dialogtopicdir=os.listdir(dialogdir)
    discavoid=[]
    for discussion_id in dialogtopicdir:
        discussionid=discussion_id[:-10]
        discavoid.append(discussionid)
    return discavoid     

def getdirectoryname(topicname):
    if topicname =="death penalty"  :
        return "deathpenalty" 
    if topicname =="gun control"  :
        return "gun-control" 
    else:
        if topicname =="gay marriage":
            return "gay-rights-debates"
        else:
            return topicname
    
def gettopicdialogdir(topicname ,topicnamedir, dialogdir, phase2, gayrightsdialogdir):
    if topicname in phase2:
        topicdialogdir= dialogdir + topicnamedir + "/discussion_dialogs" 
        return  topicdialogdir
    else:
        if topicname=="gay marriage" :
            return gayrightsdialogdir
              
def  Execute(Alltopicscv,dataset_id,topic_list,SQLResultsDir,dialogdir,topic_id_mapping,phase2,gayrightsdialogdir):
    AlltopicRows=[]
    for topic_id in topic_list:
        topicname=gettopicname(topic_id, dataset_id,topic_id_mapping)
        topicnamedir=getdirectoryname(topicname)
        topiccsv=SQLResultsDir+"AllTopicPostsCSV/" + topicnamedir
        topicdialogdir= gettopicdialogdir(topicname,topicnamedir,dialogdir,phase2,gayrightsdialogdir)
        createtopicPostCSV(dataset_id,topic_id,topicname, topiccsv)
        discavoid=creatediscussionAvoid(topicdialogdir)
        topicNodialogcsv=SQLResultsDir+ "Nodialogdiscussions/"+ topicnamedir
        createtopicPost_Nodialog(topiccsv,topicname,SQLResultsDir,discavoid,topicNodialogcsv,AlltopicRows, topic_id)
        fieldnames=AlltopicRows[0].keys()
        FileHandling.write_csv(Alltopicscv, AlltopicRows, fieldnames)
        FileHandling.writejson(AlltopicRows, Alltopicscv)
        
if __name__ == '__main__':
    topic_list=["gay-marriage-rights","Evolution","abortion","gun_control","death_penalty"]
    topic_list=["3","7","9","6","8"]
    topic_id_mapping="/Users/amita/git/Argument_Extraction_Rep/Argument_Extraction/src/Data_pkg/Data/SQL_Information/Topic_Id_Mappings"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    dialogdir=basedir+"/data/dialog_data/CSV/"
    SQLResultsDir=basedir +"/data/SQL_data/"
    gayrightsdialogdir="/Users/amita/git/summary_repo/Summary/src/data_pkg/CSV/gay-rights-debates/discussion_dialogs"
    Alltopicscv=basedir +"/data/SQL_data/Alltopics"
    dataset_id="1"
    phase2=["evolution","abortion","gun control","death penalty"]
    Execute(Alltopicscv,dataset_id,topic_list,SQLResultsDir,dialogdir,topic_id_mapping,phase2,gayrightsdialogdir)