'''
Created on Jun 18, 2015
Take input from natural summaries HIT. For the approved hits, create a pyramid file by taking natural summaries.
Additionally remove files that do not relate to the topic at all 
create MT1mid_Natural,MT1mid_Simple, similarly for MT2mid
@author: amita
'''
import os 
from collections import defaultdict
from data_pkg import FileHandling
import operator
import itertools
from data_pkg import NewFormat_text
from bs4 import BeautifulSoup
import re
import shutil,os;
from copy import deepcopy
if __name__ == '__main__':
    pass

ignorelist=["1-15_71_69__72_74_77_82_83_87_89_92_2", "1-10677_206_205_1_207_208_210_211_214_219_221_222_223_225_226_227_228_229_230_12"]

def writedir(dict_key,outdir):
    if os.path.exists(outdir):
        shutil.rmtree(outdir)
    os.mkdir(outdir)
    for key in dict_key.keys():
        #if not os.path.exists(outdir):
    #   os.makedirs(outdir)
        Outfile=outdir + "/"+key
        Lines=list()
        Lines.append(dict_key[key])
        FileHandling.WriteTextFile(Outfile, Lines)
#replace occurrences of s1,s2 by author1 author2

# manually replaced A1M answers and created a new file with revised answers
def replaceA14(HitFile, revisedworker):
    
    rowdicts=FileHandling.read_csv(HitFile)
    fieldnames=rowdicts[0].keys()
    if str(HitFile).endswith("evolution/MTdata/Phase1/MT1/Summary_results/MT1_more750Results"):
                rowdictsA1M=FileHandling.read_csv(revisedworker)
                lensA1M =len(rowdictsA1M)
    NewRows=list()   
    for row in rowdicts:
        Newdict=deepcopy(row)
        if str(row["WorkerId"]).startswith("A1MYLQQL8BBOYT"):
            rowA1M=[ rowdictsA1M[i] for i in range(0,lensA1M) if rowdictsA1M[i]["HITId"] ==row["HITId"]  ]  
            if  rowA1M  :    
                Newdict["Answer.Dialog_Natural"]=rowA1M[0]["Answer.Dialog_Natural"]
        NewRows.append(Newdict)  
    FileHandling.write_csv(HitFile, NewRows, fieldnames)      

def replace_s1s2(Text):
    Text1=re.sub("s1|s1:","Author1",Text,flags=re.IGNORECASE ) 
    Text2=re.sub("s2|s2:","Author2",Text1,flags=re.IGNORECASE ) 
    return Text2   

    
def allSummary(HitFile,Nat_Dir): 
    dict_key_natural=defaultdict(str)
    
    counter_key=defaultdict(int)
    rowdicts=FileHandling.read_csv(HitFile)
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for key, items in itertools.groupby(rowdicts, operator.itemgetter('HITId')):
        Hitids=list(items)
        for row in Hitids:
            if row["Input.key"+str(1)].startswith(tuple(ignorelist)):
                    continue
            for count in range(1,noofitems+1):
                    key=row["Input.key"+str(count)]
                    
                    Answer_Natural=row["Answer.Dialog_Natural"]
                    Answer_Natural=replace_s1s2(Answer_Natural)
                    
                    dialog_html=row["Input.Dialogtext"+str(count)]
                    soup=BeautifulSoup(dialog_html)
                    dialogplain=soup.get_text()
                    dialogplain=replace_s1s2(dialogplain)
                    
                    Natural_text=(NewFormat_text.ascii_only(Answer_Natural)) 
                    
                    dict_key_natural[key]=dict_key_natural[key]+"\n"+ str(RegularExp[counter_key[key]]) + "\n" + Natural_text +"\n\n"
                    counter_key[key]=counter_key[key]+1    
    writedir(dict_key_natural,Nat_Dir)
 

if __name__ == '__main__':
    Noofsummaries=5 
    topic="evolution"  
    noofitems=1 
    RegularExp=["----------\nD"+ str(i) +"\n"+ "----------" for i in range(Noofsummaries)]  
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir+"/data/dialog_data/CSV/" + topic + "/MTdata/Phase1/MTSummary/MT"
    
    TaskNo=1
    revisedworker=basedir+ "/data/mechanicalturk_data/"+ topic +"/MTdata/Phase1/MT"+ str(TaskNo) +"/Summary_results/A1MY_evolution"
    
    Directory1=Directory+str(TaskNo)
    HitFile=basedir + "/data/mechanicalturk_data/"+topic+ "/MTdata/Phase1/MT"+str(TaskNo)+"/Summary_results/"+ "MT"+ str(TaskNo)+ "_more750Results"
    Nat_Dir=Directory1 + "/MT" + str(TaskNo) +"_more750_Natural"
    replaceA14(HitFile,revisedworker)
    allSummary(HitFile,Nat_Dir)
    