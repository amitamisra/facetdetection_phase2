'''
Created on May 5, 2015
Replace the text with coref resolution
@author: amita
'''
from data_pkg import FileHandling
import os
from data_pkg import CoreNLPCSV
from operator import itemgetter
from data_pkg.NewFormat_text import  convertUnicode_to_dictlist as convertUnicode_to_dictlist
import itertools
from copy import deepcopy
def ReadRows(inputjson):
    Rows=FileHandling.jsontorowdicts(inputjson)
    return Rows

def getcorefhead(corefsent,SentArray):
    index=corefsent["repmentionheadindex"]
    sent=corefsent["repmentionsentno"]
    value=[sentdict["value"] for sentdict in SentArray if sentdict["sentno"]== sent and  sentdict["index"]== index]
    return value[0]
    
def replacerow(Coreferchain,SentArray):
    NewSummary=""
    sentsortedArray=sorted(SentArray,  key=lambda x: int( itemgetter("sentno")(x)))
    for key, items in itertools.groupby(sentsortedArray, key=lambda x: int(itemgetter('sentno')(x))):
        SentNo=list(items)
        SortedSentNobyindex= sorted(SentNo,key=lambda x: int(itemgetter("index")(x)))
        corefsents=[coref for coref in Coreferchain if coref["mentionsentno"]== str(key)]
        sortedcorefsents=sorted(corefsents, key=lambda x: int(itemgetter("mentionstartIndex")(x)))
        corefint=0
        counttokendict=0
        sortedcorefsentslen=len(sortedcorefsents)
        while counttokendict < len(SortedSentNobyindex):
                tokendict=SortedSentNobyindex[counttokendict]
                if corefint < sortedcorefsentslen:
                    if int(tokendict["index"]) < int(sortedcorefsents[corefint]["mentionstartIndex"]):
                        NewSummary=NewSummary + tokendict["value"] + " "
                        counttokendict = counttokendict+1
                    else:
                        corefvalue=getcorefhead(sortedcorefsents[corefint],SentArray)
                        NewSummary=NewSummary + corefvalue + " " 
                        counttokendict=int(sortedcorefsents[corefint]["mentionendIndex"]) -1
                        corefint=corefint+1
                else:
                    NewSummary=NewSummary + tokendict["value"]+ " "
                    counttokendict = counttokendict+1        
    return NewSummary
def ReplaceAllRows(AllRows, SentsArray,CorefMention):
    NewSummRows=[]
    for row in AllRows:
        newrow=deepcopy(row)
        Coreferchain = row[CorefMention]
        SentArray=row[SentsArray]
        NewSummary=replacerow(Coreferchain,SentArray)
        newrow["New_Summary"] =NewSummary
        NewSummRows.append(newrow)
    return NewSummRows     
        
        
        
def Execute(InputJson,OutputFile,SentsArray,CorefMention):
    AllRows=ReadRows(InputJson)
    Newrows=ReplaceAllRows(AllRows, SentsArray,CorefMention)
    fieldnames=Newrows[0].keys()
    FileHandling.write_csv(OutputFile, Newrows, fieldnames) 
    FileHandling.writejson(Newrows, OutputFile)
    
    
if __name__ == '__main__':
    topic="gun-control"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"/data/dialog_data/CSV/" + topic + "/MTdata/Phase1/MTSummary/"
    InputJson=Directory+ "AllMTSummary_50/CorefSimpSumm.json"
    InputCSV=Directory+ "AllMTSummary_50/CorefSimpSumm"
    OutputFile=Directory+ "/CorefReplacedSummary/AllMTSummary_50Replaced/SimpleSummary/ReplacedCorefSimpSumm"
    SentsArray="Summary -SentArray"
    CorefMention="Summary -CoreferChain"
    CoreNLPCSV.json_to_csv(InputJson,InputCSV)
    Execute(InputJson,OutputFile,SentsArray,CorefMention)
    
    InputJson=Directory+ "AllMTSummary_50/CorefNaturalSumm.json"
    InputCSV=Directory+ "AllMTSummary_50/CorefNaturalSumm"
    OutputFile=Directory+"/CorefReplacedSummary/AllMTSummary_50Replaced/NaturalSummary/ReplacedCorefNaturalSumm"
    CoreNLPCSV.json_to_csv(InputJson,InputCSV)
    Execute(InputJson,OutputFile,SentsArray,CorefMention)
    