'''
Created on Feb 1,2015
Take input from natural and simple summaries HIT. For the approved hits, create a pyramid file by taking natural summaries.
Additionally remove files that do not relate to the topic at all 
create MT1mid_Natural,MT1mid_Simple, similarly for MT2mid

If you rerun this, then rerun AllMTSummary too.
@author: amita


'''
import os 
from collections import defaultdict
from data_pkg import FileHandling
import operator
import itertools
from data_pkg import NewFormat_text
from bs4 import BeautifulSoup
import re
import shutil,os;
from copy import deepcopy
if __name__ == '__main__':
    pass

ignorelist=["1-8863_21_20__24_25_26_27_1","1-9057_49_47__50_51_53_59_4","1-8977_23_22__24_27_28_29_32_33_34_1","1-9057_54_47__56_57_62_66_1","1-9057_107_106__108_109_110_112_5"]

def writedir(dict_key,outdir):
    if os.path.exists(outdir):
        shutil.rmtree(outdir)
    os.mkdir(outdir)
    for key in dict_key.keys():
        #if not os.path.exists(outdir):
         #   os.makedirs(outdir)
        Outfile=outdir + "/"+key
        Lines=list()
        Lines.append(dict_key[key])
        FileHandling.WriteTextFile(Outfile, Lines)
#replace occurrences of s1,s2 by author1 author2

# manually replaced A14 answers and created a new file with revised answers
def replaceA14(HitFile, revisedworker):
    
    rowdicts=FileHandling.read_csv(HitFile)
    fieldnames=rowdicts[0].keys()
    if str(HitFile).endswith("gun-control/MTdata/Phase1/MT2/Summary_results/MT2_more750Results"):
                rowdictsA14=FileHandling.read_csv(revisedworker)
                lensA14 =len(rowdictsA14)
    NewRows=list()   
    for row in rowdicts:
        Newdict=deepcopy(row)
        if str(row["WorkerId"]).startswith("A14HQNH7COGDK2"):
            rowA14=[ rowdictsA14[i] for i in range(0,lensA14) if rowdictsA14[i]["HITId"] ==row["HITId"]  ]         
            Newdict["Answer.Dialog_Simple"]=rowA14[0]["Answer.Dialog_Simple"]
            Newdict["Answer.Dialog_Natural"]=rowA14[0]["Answer.Dialog_Natural"]
        NewRows.append(Newdict)  
    FileHandling.write_csv(HitFile, NewRows, fieldnames)      

def replace_s1s2(Text):
    Text1=re.sub("s1|s1:","Author1",Text,flags=re.IGNORECASE ) 
    Text2=re.sub("s2|s2:","Author2",Text1,flags=re.IGNORECASE ) 
    return Text2   

    
def allSummary(HitFile,Simp_Dir,Nat_Dir): 
    dict_key_simple=defaultdict(str)
    dict_key_natural=defaultdict(str)
    
    
    counter_key=defaultdict(int)
    rowdicts=FileHandling.read_csv(HitFile)
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for key, items in itertools.groupby(rowdicts, operator.itemgetter('HITId')):
        Hitids=list(items)
        for row in Hitids:
            if row["Input.key"+str(1)].startswith(tuple(ignorelist)):
                    continue
            if str(row["AssignmentStatus"]).startswith("Approved") :
                if str(HitFile).endswith("MT2_midResults") and str(row["WorkerId"]).startswith("A3JHXEH79DJ83L") :
                        #print "stop"
                        continue
                if str(HitFile).endswith("MT1_midResults") and str(row["WorkerId"]).startswith("A5V3ZMQI0PU3F") and str(row["HITId"]).startswith("3YOAVL4CA0HPFFJY6UJSWN3UULC4Z3"):    
                        continue
                for count in range(1,noofitems+1):
                    key=row["Input.key"+str(count)]
                    #print key
                    Answer_Simple=row["Answer.Dialog_Simple"]
                    Answer_Simple=replace_s1s2(Answer_Simple)
                    
                    Answer_Natural=row["Answer.Dialog_Natural"]
                    Answer_Natural=replace_s1s2(Answer_Natural)
                    
                    dialog_html=row["Input.Dialogtext"+str(count)]
                    soup=BeautifulSoup(dialog_html)
                    dialogplain=soup.get_text()
                    dialogplain=replace_s1s2(dialogplain)
                    
                    Simple_text=(NewFormat_text.ascii_only(Answer_Simple)) 
                    Natural_text=(NewFormat_text.ascii_only(Answer_Natural)) 
                    
                    dict_key_simple[key]=dict_key_simple[key]+"\n"+ str(RegularExp[counter_key[key]]) + "\n" + Simple_text +"\n\n"
                    dict_key_natural[key]=dict_key_natural[key]+"\n"+ str(RegularExp[counter_key[key]]) + "\n" + Natural_text +"\n\n"
                    counter_key[key]=counter_key[key]+1    
                    
    
    writedir(dict_key_simple,Simp_Dir)
    writedir(dict_key_natural,Nat_Dir)
 

if __name__ == '__main__':
    Noofsummaries=5 
    topic="gun-control"  
    noofitems=1 
    RegularExp=["----------\nD"+ str(i) +"\n"+ "----------" for i in range(Noofsummaries)]  
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir+"/data/dialog_data/CSV/" + topic + "/MTdata/Phase1/MTSummary/MT"
    
    
    #Already done for MT1mid and MT2mid
     # Do for mid range task1
    TaskNo=1
    Directory1=Directory+str(TaskNo)
    HitFile=basedir + "/data/mechanicalturk_data/"+topic+ "/MTdata/Phase1/MT"+str(TaskNo)+"/Summary_results/"+ "MT"+ str(TaskNo)+ "_midResults"
    Nat_Dir=Directory1 + "/MT" + str(TaskNo) +"_mid_Natural"
    Simp_Dir=Directory1 + "/MT" + str(TaskNo) +"_mid_Simple"
    
    allSummary(HitFile,Simp_Dir,Nat_Dir)
    
    # Do for mid range task2
    TaskNo=2
    Directory2=Directory + str(TaskNo)
    HitFile=basedir + "/data/mechanicalturk_data/"+topic+ "/MTdata/Phase1/MT"+str(TaskNo)+"/Summary_results/"+ "MT"+ str(TaskNo)+ "_midResults"
    key=""
    Nat_Dir=Directory2 + "/MT" + str(TaskNo) +"_mid_Natural"
    Simp_Dir=Directory2 + "/MT" + str(TaskNo) +"_mid_Simple"
    allSummary(HitFile,Simp_Dir,Nat_Dir)

    
    
    # Do for more750 task2
    TaskNo=2
    revisedworker=basedir+ "/data/mechanicalturk_data/"+ topic +"/MTdata/Phase1/MT2/Summary_results/A14H_MT2_More750"
    Directory2=Directory + str(TaskNo)
    HitFile=basedir + "/data/mechanicalturk_data/"+topic+ "/MTdata/Phase1/MT"+str(TaskNo)+"/Summary_results/"+ "MT"+ str(TaskNo)+ "_more750Results"
    Nat_Dir=Directory2 + "/MT" + str(TaskNo) +"_more750_Natural"
    Simp_Dir=Directory2 + "/MT" + str(TaskNo) +"_more750_Simple"
    replaceA14(HitFile,revisedworker)
    allSummary(HitFile,Simp_Dir,Nat_Dir)
    
    
    
    
    
    
    