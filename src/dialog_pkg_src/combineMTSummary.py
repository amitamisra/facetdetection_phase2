'''
Created on Feb 2, 2015
Take all MT hits(MT1,MT2) and make a single directory for summaries, Also use for pyramid input allocation
@author: amita

NOT USED
'''
import os
import shutil
class DirManip:
    
        
    def __init__(self, input1,input2,input3,input4,input5,input6,input7,input8,outdir1,outdir3,outdir5,outdir7):
        self.inputdir1=input1
        self.inputdir2=input2
        self.inputdir3=input3
        self.inputdir4=input4
        self.inputdir5=input5
        self.inputdir6=input6
        self.inputdir7=input7
        self.inputdir8=input8
        self.outdir1=outdir1
        self.outdir3=outdir3
        self.outdir5=outdir5
        self.outdir7=outdir7
        
        
        
    
    def Emptydir(self,dirlist):
        for dirname in dirlist:
            if os.path.exists(dirname):
                shutil.rmtree(dirname)
            os.mkdir(dirname)
        
    def combine(self,inputDir, outputDir):
        filelist=os.listdir(inputDir)
        for filename in filelist:
            if filename.startswith("."):
                continue
            src=inputDir+filename
            dst=outputDir+filename
            shutil.copy(src, dst)
            
    def CombineMore750Input(self,inputdir2,outdir1,inputdir4, outdir3,inputdir6,outdir5,inputdir8,outdir7):
        self.combine(inputdir2,outdir1)
        self.combine(inputdir4,outdir3)
        self.combine(inputdir6,outdir5) 
        self.combine(inputdir8,outdir7) 
    
    def CombineMidInput(self,inputdir1,inputdir2,outdir1,inputdir3,inputdir4,outdir3, inputdir5,inputdir6,outdir5,inputdir7,inputdir8,outdir7):
        
        self.combine(inputdir1,outdir1)
        self.combine(inputdir2,outdir1)
        self.combine(inputdir3,outdir3)
        self.combine(inputdir4,outdir3)
        self.combine(inputdir5,outdir5)
        self.combine(inputdir6,outdir5)
        self.combine(inputdir7,outdir7)
        self.combine(inputdir8,outdir7)
    
    def createInputOutDir(self,wordrange,outwordrange):
        self.inputdir1=Directory+"/MT1/MT1"+ wordrange+"_Natural/"
        self.inputdir2=Directory+"/MT2/MT2"+ wordrange +"_Natural/"
        
        self.inputdir3=Directory+"/MT1/MT1"+ wordrange + "_Simple/"
        self.inputdir4=Directory+"/MT2/MT2" + wordrange + "_Simple/"
        
        self.outdir1=Directory+"/AllMT"+ outwordrange + "_NaturalSummary/"
        self.outdir3=Directory+"/AllMT"+ outwordrange + "_SimpleSummary/"
       
       
if __name__ == '__main__':
    topic="gun-control"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"data/dialog_data/CSV/" + topic + "/MTdata/Phase1/MTSummary/MT1/MT1_mid_Natural"
    wordrange='_mid'
    outwordrange=wordrange[1].upper() + wordrange[2:]  
    
    DirManipMid=DirManip("","","","","","","","","","","","")
    DirManipMid.createInputOutDir(wordrange,outwordrange)
    dirlistMid=[DirManipMid.outdir1, DirManipMid.outdir3, DirManipMid.outdir5, DirManipMid.outdir7]
    DirManipMid.Emptydir(dirlistMid)
    DirManipMid.CombineMidInput(DirManipMid.inputdir1,DirManipMid.inputdir2,DirManipMid.outdir1,DirManipMid.inputdir3,DirManipMid.inputdir4,DirManipMid.outdir3, DirManipMid.inputdir5,DirManipMid.inputdir6,DirManipMid.outdir5,DirManipMid.inputdir7,DirManipMid.inputdir8,DirManipMid.outdir7)
    
    
    wordrange="_more750"
    outwordrange=wordrange[1].upper() + wordrange[2:]  
    DirManipMore=DirManip("","","","","","","","","","","","")
    DirManipMore.createInputOutDir(wordrange,outwordrange)
    dirlistMore=[DirManipMore.outdir1, DirManipMore.outdir3, DirManipMore.outdir5, DirManipMore.outdir7]
    DirManipMore.Emptydir(dirlistMore)
    DirManipMore.CombineMore750Input(DirManipMore.inputdir2,DirManipMore.outdir1,DirManipMore.inputdir4,DirManipMore.outdir3,DirManipMore.inputdir6,DirManipMore.outdir5,DirManipMore.inputdir8,DirManipMore.outdir7)
    
    
    
    
    
    
    
    
    