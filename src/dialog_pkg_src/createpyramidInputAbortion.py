'''
Created on Feb 1,2015
Take input from natural and simple summaries HIT. For the approved hits, create a pyramid file by taking natural summaries.
Additionally remove files that do not relate to the topic at all, these are in ignorelist
create MT1mid_Natural,MT1mid_Simple, similarly for MT2mid
@author: amita


'''
import os 
from collections import defaultdict
from data_pkg import FileHandling
import operator
import itertools
from data_pkg import NewFormat_text
from bs4 import BeautifulSoup
import re
import shutil,os;
from copy import deepcopy

ignorelist=["1-731_72_70__74_75_77_78_79_84_85_3","1-8863_21_20__24_25_26_27_1"]

def writedir(dict_key,outdir):
    if os.path.exists(outdir):
        shutil.rmtree(outdir)
    os.mkdir(outdir)
    for key in dict_key.keys():
        Outfile=outdir + "/"+key
        Lines=list()
        Lines.append(dict_key[key])
        FileHandling.WriteTextFile(Outfile, Lines)
#replace occurrences of s1,s2 by author1 author2




    
def allSummary(HitFile,Simp_Dir,Nat_Dir): 
    dict_key_simple=defaultdict(str)
    dict_key_natural=defaultdict(str)
    
    counter_key=defaultdict(int)
    rowdicts=FileHandling.read_csv(HitFile)
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for key, items in itertools.groupby(rowdicts, operator.itemgetter('HITId')):
        Hitids=list(items)
        for row in Hitids:
            if row["Input.key"+str(1)].startswith(tuple(ignorelist)):
                    continue
            if str(row["AssignmentStatus"]).startswith("Approved") :
                if  str(row["HITId"]).startswith("3CKVGCS3PG5BFFFJS12IORGESF40SU") and row["WorkerId"].startswith("A2JORQCETWUR0R"):
                    continue
                for count in range(1,noofitems+1):
                    key=row["Input.key"+str(count)]
                    Answer_Simple=row["Answer.Dialog_Simple"]
                    Answer_Simple=NewFormat_text.replace_s1s2(Answer_Simple)
                    
                    Answer_Natural=row["Answer.Dialog_Natural"]
                    Answer_Natural=NewFormat_text.replace_s1s2(Answer_Natural)
                    
                    dialog_html=row["Input.Dialogtext"+str(count)]
                    soup=BeautifulSoup(dialog_html)
                    dialogplain=soup.get_text()
                    dialogplain=NewFormat_text.replace_s1s2(dialogplain)
                    
                    Simple_text=(NewFormat_text.ascii_only(Answer_Simple)) 
                    Natural_text=(NewFormat_text.ascii_only(Answer_Natural)) 
                    
                    dict_key_simple[key]=dict_key_simple[key]+"\n"+ str(RegularExp[counter_key[key]]) + "\n" + Simple_text +"\n\n"
                    dict_key_natural[key]=dict_key_natural[key]+"\n"+ str(RegularExp[counter_key[key]]) + "\n" + Natural_text +"\n\n"
                    counter_key[key]=counter_key[key]+1    
                    
    
    writedir(dict_key_simple,Simp_Dir)
    writedir(dict_key_natural,Nat_Dir)
 

if __name__ == '__main__':
    Noofsummaries=5 
    topic="abortion"  
    noofitems=1 
    RegularExp=["----------\nD"+ str(i) +"\n"+ "----------" for i in range(Noofsummaries)]  
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir+"/data/dialog_data/CSV/" + topic + "/MTdata/Phase1/MTSummary/MT"
    
    # Do for more750 task1
    TaskNo=1
    Directory2=Directory + str(TaskNo)
    HitFile=basedir + "/data/mechanicalturk_data/"+topic+ "/MTdata/Phase1/MT"+str(TaskNo)+"/Summary_results/"+ "MT"+ str(TaskNo)+ "_more750Results"
    Nat_Dir=Directory2 + "/MT" + str(TaskNo) +"_more750_Natural"
    Simp_Dir=Directory2 + "/MT" + str(TaskNo) +"_more750_Simple"
    allSummary(HitFile,Simp_Dir,Nat_Dir)
    
    
    
    
    
    
    