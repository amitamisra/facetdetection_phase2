'''
Created on May 8, 2015

@author: amita
'''
from data_pkg import FileHandling
from itertools import groupby
from operator import itemgetter
import os

def ReadRows(AllScusjson):
    rows=FileHandling.jsontorowdicts(AllScusjson)
    return rows


#requiredcount: number of SCUS items desired per dialog for mapping
#min_mandatoryweight, example weight3,4,5
# Add min_mandatoryweight SCUS, but count of SCUS may be less than requiredcount,
# then add a few of the lower weights to get the required  number of SCUs.
def countrows(AllSCusKey, requiredscucount, min_mandatoryweight):
    rows=[row for row in AllSCusKey if int(row["weight"]) >= int(min_mandatoryweight)]
    count_weights=len(rows)
    if count_weights >= requiredscucount :
        return rows
    else:
        nextweight= int(min_mandatoryweight) 
        while count_weights < requiredscucount:
            nextweight= nextweight -1
            nextrows=[row for row in AllSCusKey if int(row["weight"]) == int(nextweight)]
            lennextrows=len(nextrows)
            count=0
            while count < lennextrows and count_weights < requiredscucount:
                rows.append(nextrows[count])
                count=count+1
                count_weights=count_weights+1
    return rows

def selectkey_scu(countscus,begin,end): 
    Allrows= ReadRows(countscus)
    selectedrows=Allrows[begin:end]
    keys=[row['key'] for row in selectedrows ]
    return keys

def Execute(AllScusjson,countscus,begin ,end, requiredscucount, min_mandatoryweight,requiredcountcsv):
    Keys= selectkey_scu(countscus,begin,end)
    AllSCus= ReadRows(AllScusjson)
    rows =[row for row in AllSCus if row["key_user"] in Keys]
    sortedrows=sorted(rows, key = itemgetter("key_user"))
    
    countfilerows=[]
    for key, items in groupby(sortedrows, key = itemgetter("key_user")):
        AllScusKey=list(items)
        NewRows=countrows(AllScusKey, requiredscucount, min_mandatoryweight)
        countfilerows.extend(NewRows)
    
    NewAddedRows=FileHandling.AddUniqueRowNo(countfilerows)
    fieldnames= NewAddedRows[0].keys()         
    FileHandling.write_csv(requiredcountcsv, NewAddedRows, fieldnames)
    FileHandling.writejson(NewAddedRows, requiredcountcsv)
    

if __name__ == '__main__':
    
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    topic="gun-control"
    directory=basedir + "/data/dialog_data/CSV/"+ topic+ "/MTdata/Phase1/"
    AllScusJson=directory + "Pyramids_Natural/AllScusJson.json"
    scucountperkey=directory + "Pyramids_Natural/scucountkey.json"
    outputdir=directory + "Phase1_1SCUCounts/"
    begin=0
    end=20
    requiredscucount=9
    min_mandatoryweight=4 
    requiredcountcsv=outputdir+"Keys_SCU_"+str(requiredscucount)
    Execute(AllScusJson,scucountperkey,begin ,end, requiredscucount, min_mandatoryweight, requiredcountcsv)
    
    
    
    