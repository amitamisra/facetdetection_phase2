'''
Created on May 8, 2015

@author: amita
'''
from operator import itemgetter, attrgetter
from data_pkg import FileHandling
from itertools import groupby
import os

def ReadRows(json):
    rows=FileHandling.jsontorowdicts(json)
    return rows

def getcountrowsscu(Allscuskey, contr_no):
    scuscontrib=[rowscu for rowscu in Allscuskey if str(rowscu["weight"]) ==  contr_no ]
    count_contrb=len(scuscontrib)
    return count_contrb
    
def scu_count_key(more2_scuRows,contrbs):
    AllNewRows=[]
    sorted_scus=sorted(more2_scuRows, key = itemgetter("key_user"))
    for key, items in groupby(sorted_scus, key = itemgetter("key_user")):
        newrow={}
        list_scus=list(items)
        total=0
        for contr in contrbs:
            countcontrb = getcountrowsscu(list_scus, contr)
            newrow["weight_"+str(contr)]=countcontrb 
            total=total+countcontrb
        newrow["key"]= key
        contrib4_5=total- newrow["weight_"+str(3)] - newrow["weight_"+str(2)]
        newrow["weight_4_5"]=contrib4_5
        AllNewRows.append(newrow) 
            
    return   AllNewRows  

def Execute(more2_scujson,contrbslist, outputcsv):
    more2_scuRows=ReadRows(more2_scujson)
    NewRows= scu_count_key(more2_scuRows,contrbslist)
    fieldnames=NewRows[0].keys()
    sorted_rows=sorted(NewRows, key = itemgetter("weight_4_5", "weight_3"), reverse= True)
    FileHandling.write_csv(outputcsv, sorted_rows, fieldnames)
    FileHandling.writejson(sorted_rows, outputcsv)         
    
    
if __name__ == '__main__':
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    topic="gun-control"
    directory=basedir + "/data/dialog_data/CSV/"+ topic+ "/MTdata/Phase1/Pyramids_Natural/"
    more2_scujson=directory + "AllScusJson.json"
    outputcsv=directory + "scucountkey"
    contrbslist=["2","3","4","5"]
    
    Execute(more2_scujson,contrbslist, outputcsv)
    