
'''
Created on Feb 14, 2015
create a summmary csv file for all the summaries, remove regular expression, create a csv , json with 2 fields
a key and Natural_Summary_Text

@author: amita
'''
import os
from data_pkg import FileHandling
import re
from copy import deepcopy
import sys


def AddAlldialogs(DialogFile, AllNewRows):
    Allrows=FileHandling.read_csv(DialogFile)
    for row in Allrows:
        newrow=dict()
        newrow["key"]=row["key1"]
        newrow["dialog"]=row["Dialogtext1"]
        AllNewRows.append(newrow)
        
    return AllNewRows    
        

def Addalldialogs(DialogFile1,DialogFile2,DialogFile3):
    AllNewRows=[]
    AllNewRows = AddAlldialogs(DialogFile1, AllNewRows)
    AllNewRows = AddAlldialogs(DialogFile2, AllNewRows)
    AllNewRows = AddAlldialogs(DialogFile3, AllNewRows)
    return AllNewRows
    
    
        
def getfieldname(dirname):
    if "NaturalSummary" in dirname:
        fieldname="Natural_Summary_Text"
    if  "SimpleSummary" in dirname:
        fieldname="Simple_Summary_Text"
        
    return fieldname      
    
def combinesummaryTocsvGayRights(InputFile,OutFile):
    rowdicts=FileHandling.jsontorowdicts(InputFile+".json")
    AllRows=[]
    count=0
    for row in rowdicts:
        newrow=dict()
        newrow["Key"]=row["key"]
        fieldname="Natural_Summary_Text"
        NoRegExpDict=FileHandling.removeregularexp(row["Summary"],fieldname)
        newrow[fieldname]=NoRegExpDict
        AllRows.append(newrow)
    CsvSummaryFile=OutFile
    Jsonsummary=OutFile
    AllFields=["Key",fieldname]
    FileHandling.write_csv(CsvSummaryFile, AllRows, AllFields)  
    FileHandling.writejson(AllRows,Jsonsummary)      

    
    
# create a csv and json file with summaries as dict and regular rxpression removed
def combinesummaryTocsv(dirlist,Alldialogs):
    for dirname in dirlist:
            AllRows=list()
            OneSummaryRowS=[]
            for filename in os.listdir(dirname):
                row=dict()
                filepath=dirname+"/"+filename
                row["Key"]=filename[:-4]
                dialog=[dgrow["dialog"] for dgrow in Alldialogs if dgrow["key"] == filename[:-4]]
                if len(dialog)!= 1:
                    print "error in dialog" + row["Key"]
                    sys.exit(0)
                row["Dialog"]= dialog[0]   
                text=FileHandling.ReadTextFile(filepath)
                textstring=" ".join(text)
                fieldname=getfieldname(dirname)
                NoRegExpDict=FileHandling.removeregularexp(textstring,fieldname)
                for key, value in NoRegExpDict.iteritems() :
                    OneSummaryRow=deepcopy(row)
                    OneSummaryRow["SummaryNo"]= key
                    OneSummaryRow["Summary"]=value
                    OneSummaryRowS.append(OneSummaryRow)
                
                row[fieldname]=NoRegExpDict
                AllRows.append(row)
            CsvSummaryFile= dirname+"CSV"
            Jsonsummary=dirname+"Json"
            CsvSummaryOneRow= dirname +"OneSummCSV"
            JsonsummaryOneRow=dirname +"OneSummJson"
            
            AllFields=AllRows[0].keys()
            FileHandling.write_csv(CsvSummaryFile, AllRows, AllFields)  
            FileHandling.writejson(AllRows,Jsonsummary) 
            
            FieldsOneSumm=OneSummaryRowS[0].keys()
            FileHandling.write_csv(CsvSummaryOneRow, OneSummaryRowS, FieldsOneSumm)  
            FileHandling.writejson(OneSummaryRowS,JsonsummaryOneRow)  
          
    

if __name__ == '__main__':
    
    #Done for gun control
    topic="gun-control"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"/data/dialog_data/CSV/" + topic + "/MTdata/Phase1/MTSummary/AllMTSummary_50/"
    DialogFileDir=basedir+"/data/mechanicalturk_data/gun-control/MTdata/Phase1/"
    
    DialogFile1=DialogFileDir+"MT1/MT1Mid/MT1_midrange"
    DialogFile2=DialogFileDir+"MT2/MT2Mid/MT2_midrange"
    DialogFile3=DialogFileDir+"MT2/MT2More750/MT2_more750"
    
    Alldialogs=Addalldialogs(DialogFile1,DialogFile2,DialogFile3)
    
    inputdir_nat= Directory+"NaturalSummary"
    inputdir_sim=Directory+ "SimpleSummary"
    dirlist=[inputdir_nat,inputdir_sim]
    combinesummaryTocsv(dirlist,Alldialogs)
    
    #===========================================================================
    # topic="gay-rights-debates"
    # Directory="/Users/amita/git/summary_repo/Summary/src/data_pkg/CSV/gay-rights-debates/MTdata/"
    # #input_file=Directory+"AllDialogs_phase_1_2_Formatted"
    # #summaryNoReg=os.path.dirname(os.getcwd())+"/Data_Pkg_Data/CSV/" + topic + "/MTdata/MTPhaseFormatted1-2/SummaryNoRegPhase1_2"
    # 
    # input_file=Directory+"MTPhase1/LabelUpdated/AllDialogs_phase1_Formatted"
    # summaryNoReg=os.path.dirname(os.getcwd())+"/Data_Pkg_Data/CSV/" + topic + "/MTdata/MTPhase1/SummaryNoRegPhase1"
    # combinesummaryTocsvGayRights(input_file,summaryNoReg)
    # 
    # input_file=Directory+"MTPhase2/AllDialogs_phase2_Formatted"
    # summaryNoReg=os.path.dirname(os.getcwd())+"/Data_Pkg_Data/CSV/" + topic + "/MTdata/MTPhase2/SummaryNoRegPhase2"
    # combinesummaryTocsvGayRights(input_file,summaryNoReg)
    #===========================================================================
    