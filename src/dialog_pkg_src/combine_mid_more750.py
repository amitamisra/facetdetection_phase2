import os
import shutil
'''
Created on Feb 14, 2015
 used to create one combined file AllMT50 for gun control
@author: amita
'''
def combine(inputDir, outputDir):
        filelist=os.listdir(inputDir)
        for filename in filelist:
            if filename.startswith("."):
                continue
            src=inputDir+filename
            dst=outputDir
            shutil.copy(src, dst)
            
            
if __name__ == '__main__':
    topic="gun-control"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"/data/dialog_data/CSV/" + topic + "/MTdata/Phase1/MTSummary/"
    
    InDirMid_nat1=  Directory +"MT1/MT1_mid_Natural/"
    InDirMid_nat2=  Directory +"MT2/MT2_mid_Natural/"
    InDirMore_nat2= Directory + "MT2/MT2_more750_Natural/"

    InDirMid_sim1= Directory +"MT1/MT1_mid_Simple/"
    InDirMid_sim2=  Directory +"MT2/MT2_mid_Simple/"
    InDirMore_sim2= Directory + "MT2/MT2_more750_Simple/"
    combineddirSim=basedir +"/data/dialog_data/CSV/" + topic + "/MTdata/Phase1/MTSummary/AllMTSummary_50/SimpleSummary/"
    combineddirNat=basedir +"/data/dialog_data/CSV/" + topic + "/MTdata/Phase1/MTSummary/AllMTSummary_50/NaturalSummary/"

    
    combine(InDirMid_nat1,combineddirNat)
    combine(InDirMid_nat2,combineddirNat)
    combine(InDirMore_nat2,combineddirNat)
    
    combine(InDirMid_sim1,combineddirSim)
    combine(InDirMid_sim2,combineddirSim)
    combine(InDirMore_sim2,combineddirSim)
    
    