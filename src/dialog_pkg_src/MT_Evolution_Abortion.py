#------------------------------------------------------------------------------ ABortion Not used dialogs
# word count > 1500  1-6408_293_292__296_298_299_306_308_310_311_312_313_314_315_316_317_320_321_322_324_333_335_341_342_343_344_345_346_350_355_357_358_5
# word count > 1500  1-6122_103_98__105_106_110_113_116_117_118_119_120_121_122_123_124_125_126_4
# word count > 1500  1-6122_134_133__135_136_138_140_142_143_144_145_146_147_150_5
# word count > 1500  1-7081_38_35__41_42_45_46_48_49_50_53_54_56_58_59_60_61_62_2
# word count > 1500  1-6925_148_147__151_155_156_158_159_161_162_163_164_165_166_167_168_1
#-------------- word count > 1500  1-8656_6_5__9_10_12_32_37_39_41_42_44_45_47_5
# word count > 1500  1-4948_12_11__13_14_15_16_17_18_23_24_33_34_35_37_43_44_45_46_1
#--- word count > 1500  1-8582_76_74__77_79_83_84_88_93_94_96_97_98_99_100_104_6
#-------- word count > 1500  1-9062_39_38__40_41_42_43_44_46_48_50_53_61_66_68_3
# word count > 1500  1-4375_367_364__371_376_377_378_379_380_381_382_384_386_388_390_391_392_393_394_395_396_397_398_2
#---------- word count > 1500  1-8920_428_426__430_431_432_433_436_441_444_445_7
#----------------- word count > 1500  1-6950_22_17__26_27_29_32_37_38_40_57_58_1
#-------------- word count > 1500  1-2902_20_19__21_22_24_25_27_30_48_50_56_59_2
#-- word count > 1500  1-6496_4_3__6_11_14_18_21_30_39_45_52_54_56_57_58_60_63_1
#------ word count > 1500  1-754_39_38__40_42_43_55_61_62_64_66_70_80_81_82_83_3
#------------------ word count > 1500  1-3315_185_184__186_192_194_195_202_203_6
# word count > 1500  1-7580_161_142__165_167_169_181_184_186_187_204_207_210_211_5
# word count > 1500  1-7748_143_139__148_156_169_173_179_182_183_184_186_187_188_189_190_191_1
#------------------------------------------------------------------------------ 
#===============================================================================Evolution ignored
# word count > 1500  1-8549_176_175__177_178_183_191_197_198_201_3
# word count > 1500  1-11013_98_96__100_101_105_107_108_109_110_111_112_113_114_115_116_3
# word count > 1500  1-8435_584_83__586_592_596_597_603_604_607_608_610_611_613_614_6
# word count > 1500  1-5417_65_58__71_77_84_91_101_103_117_124_128_3
# word count > 1500  1-8409_761_758__771_773_775_778_780_784_786_787_788_789_790_791_792_794_795_796_4
# word count > 1500  1-819_31_28__32_34_42_51_54_55_56_66_75_79_80_82_84_93_98_100_3

#create MT input file for summary task for gun control, was used only for MT2_more 750, MT1mid and MT2mid with 
#simple summaries and natural summaries.
#create MT input file for summary task for abortion and evolution,use more750
#simple summaries and natural summaries.
#===============================================================================
# encoding=utf8

'''
Created on May 1, 2014

@author: amita
'''
from data_pkg import FileHandling
from unidecode import unidecode
from data_pkg import NewFormat_text
from nlp.text_obj import TextObj
import MySQLdb as mdb
import sys
import os
import re
from html import HTML
import itertools
import operator
from collections import defaultdict
import logging

def Addrowdict(dialog_MaxWord,rowdict,Filepath):
    keyrowdict=os.path.basename(Filepath)
    dialogturns=[dictturn["Dialog_Turn"] for dictturn in dialog_MaxWord]
    rowdict[keyrowdict]=",".join(dialogturns)
    
    
def ExtractPairs(Listdialogs):
    d = defaultdict(lambda: [])

    for x in Listdialogs:
        s = frozenset((x["quote_source_author_id"], x["posts_author_id"]))
        d[s].append(x)
    return d 
def DialogMaxWord(sameauthorsdialogs): 
    List_maxword=[]
    for key, values in  sameauthorsdialogs.iteritems():
        dict_maxword=max(values, key=lambda x:x['TotalWordCount'])
        List_maxword.append(dict_maxword)
    return List_maxword    
     
def createrowdict(rowdict):
    
    dialogdir=os.path.dirname(os.getcwd()) + "/Data_Pkg_Data/CSV/"+ topic+"/discussion_dialogs"
    Filelist=os.listdir(dialogdir)
    for filename in Filelist:
        Listdialogs=[]
        Filepath=dialogdir +"/" + filename[:-4]
        Rowdicts=FileHandling.read_csv(Filepath)
        Rowdicts.sort(key=operator.itemgetter("Dialog_Turn"))
        for key, items in itertools.groupby(Rowdicts, operator.itemgetter("Dialog_Turn")):
            ListRows=list(items)
            dictturn=dict()
            TotalWordCount=sum([ int(d["Word_Count"]) for d in ListRows ])
            dictturn["Dialog_Turn"]=ListRows[0]["Dialog_Turn"]
            dictturn["posts_author_id"]=ListRows[0]["posts_author_id"]
            dictturn["quote_source_author_id"]=ListRows[0]["quote_source_author_id"]
            dictturn["TotalWordCount"]=TotalWordCount
            Listdialogs.append(dictturn)
        sameauthorsdialogs=ExtractPairs(Listdialogs)
        dialog_MaxWord=DialogMaxWord(sameauthorsdialogs)
        Addrowdict(dialog_MaxWord,rowdict,Filepath)
                
def MT1discussion_turns(taskno):
    
    fieldnames=list()
    rowdict=dict()
    rows=list()
    createrowdict(rowdict)
    rowdict["dataset"]="forums"
    rowdict["topic"]=topic
    for k,v in  rowdict.iteritems():
        fieldnames.append(k)
    
    if not os.path.exists(directory):
                os.makedirs(directory)   
    rows.append(rowdict)
    outputfile=MTdirectory+ "MT" + str(taskno)+"_discussion_turns"
    FileHandling.write_csv(outputfile, rows, fieldnames)

class Mechanical_Turk:
    def __init__(self,InputcsvStr,dialogturns):
        
        self.inputcsvStr=InputcsvStr
        self.dialogturns=dialogturns
    
    def SqlAuthorList(self):
        try:
            db1 = mdb.connect(host="localhost",user="root",passwd="",db="iac")
            cursor = db1.cursor(mdb.cursors.DictCursor) 
            Query_Author="select username from authors where dataset_id=1"
            cursor.execute(Query_Author) 
            rows=cursor.fetchall()
            rows=list(rows)
            Authorlist=[row['username'] for row in rows]
            return Authorlist
        except db1.Error, e:
            print e
            if db1:
                db1.rollback()
            print "Error %d: %s" % (e.args[0],e.args[1])
            sys.exit(1)
            
            
    def formatascii(self,string):    
        removetokenlist="emoticon"
        #string=str(Htmltext)
        text_obj = TextObj(string)
        #text=text_obj.text
        tokens=text_obj.tokens
        tokens=NewFormat_text.removetokens(tokens,removetokenlist) 
        new_text=" ".join(tokens)
        new_text=new_text.decode('utf-8', 'ignore')
        new_text=NewFormat_text.correctPunct(new_text)
        return(NewFormat_text.ascii_only(new_text))   
             
    
    def createdialoginputfile(self,inputcsvStr,dialogturns,AllDialogs_more750,AllDialogs_midrange,AlltextAllFiles):
        try:
                global Textline_750
                global Textline_Mid
                rows=list()
                rowdicts=FileHandling.read_csv(self.inputcsvStr)
              
                for turn in self.dialogturns:
                    
                        rows = [row for row in rowdicts if row['Dialog_Turn'] == str(turn)]
                        Dialogrow=dict()
                        Htmltext=HTML()
                        Dialogrow["Dialogtext"]=""
                        first=True
                        count=1 
                        turnno=1
                        number_turns=len(rows)
                        for row in rows:
                            
                            row["Fullquote_text"]= self.formatascii(row["Fullquote_text"])
                            row["post_text"]=self.formatascii(row["post_text"])
                            
                            if (first==True):
                               
                                Htmltext.p("S1:" + str(turnno)+ row["Fullquote_text"])
                                
                                Dialogrow["Dialogtext"]=(Dialogrow["Dialogtext"])+ "<br><b>"+"S1:"+ \
                                            str(turnno)+"-  "+'</b>'+ (row["Fullquote_text"])
                                len_key=len(row["key"])
                                Dialogrow["key"]=row["key"][:len_key-1]            
                                
                                Dialogrow["Dialogtext"]= Dialogrow["Dialogtext"]+"<br><b> S2:"+\
                                            str(turnno)+"-  </b>"+(row["post_text"])
                                           
                                Htmltext.p("S2:" + str(turnno)+ row["post_text"])            
                                first=False
                                turnno=turnno+1
                            else:
                                if count==1:
                                    Dialogrow["Dialogtext"]=Dialogrow["Dialogtext"]+" <br><b>S1:"+ \
                                            str(turnno)+"-  </b>"+(row["post_text"]) 
                                    Htmltext.p("S1:" + str(turnno)+ row["post_text"])        
                                    
                                    Dialogrow["key"]= Dialogrow["key"]+"_"+ str(row["posts_post_id"])      
                                    count=2
                                else:
                                    if count==2:
                                        Dialogrow["Dialogtext"]=Dialogrow["Dialogtext"]+"<br><b> S2:"+str(turnno)+"-  </b>"+(row["post_text"] )
                                        Dialogrow["key"]= Dialogrow["key"]+"_"+str(row["posts_post_id"]) 
                                        Htmltext.p("S2:" + str(turnno)+ row["post_text"])    
                                        count=1
                                        turnno=turnno+1
                        
                        
                        Dialogrow["key"]= Dialogrow["key"]+"_"+ str(turn)  
                        text_obj = TextObj(Dialogrow["Dialogtext"])
                        #text=text_obj.text
                        tokens=text_obj.tokens
                        #string=unicode(Dialogrow["Dialogtext"],errors="ignore")
                        word_count=len(tokens)
                        Dialogrow["Total_Count"]=word_count
                        if word_count >= 750:
                            Textline_750= Textline_750 + "<p><b> KEY:</b>"+str(Dialogrow["key"]) + str(Dialogrow["Dialogtext"]) + "</p>"
                       
                        
                        #print Dialogrow["key"]
                        if word_count < 1500:
                            if word_count >= 750:  
                                AllDialogs_more750.append(Dialogrow)
                            else:    
                                        Textline_Mid= Textline_Mid + "<p><b> KEY:</b>"+str(Dialogrow["key"]) + str(Dialogrow["Dialogtext"]) + "</p>"
                                        AllDialogs_midrange.append(Dialogrow)        
                            AlltextAllFiles.append(Dialogrow["Dialogtext"])
                        else:
                            print "word count > 1500  " + str(Dialogrow["key"])
        except Exception as ex: 
            logging.exception("Something awful happened! in createdialoginputfile ")
            print " error in file" + self.inputcsvStr
            

            sys.exit(1)         
    
    def createhitinput(self,outputcsv,No_ofitem,No_ofhits):
        MTrow=dict()
        
        Fieldnames=list()
        MTRows=list()
        rowdicts=FileHandling.read_csv(self.inputcsvStr) 
        intc=1
        extc=0
        first=True
        for row in rowdicts:
            if extc < No_ofhits:  
                
                if intc<=No_ofitem:
                
                    MTrow["key" +str(intc)]=row["key"]
                    MTrow["Dialogtext"+str(intc)]=row["Dialogtext"]                    
                    if first:
                        Fieldnames.append("key" +str(intc))
                        Fieldnames.append("Dialogtext"+str(intc))
                    intc=intc+1       
                                         
                if intc-1==No_ofitem:
                    MTRows.append(MTrow)   
                    extc=extc+1
                    intc=1
                    MTrow=dict()
                    first=False 
                    
            else:
                break
                
        FileHandling.write_csv(outputcsv, MTRows, Fieldnames)            


           
if __name__ == '__main__':
    
    #topic="abortion"
    topic="evolution"
    No_ofhits=0
    
    
    
    global Textline_750
    global Textline_Mid
    FileStr=dict()
    taskno="All"
    directory=os.path.dirname(os.getcwd()) + "/Data_Pkg_Data/CSV/"+ topic+"/discussion_dialogs/"
    MTdirectory=os.path.dirname(os.getcwd()) + "/Data_Pkg_Data/CSV/"+ topic+"/MTdata/MT"+ str(taskno)+"/"
    outputcsvdialog=MTdirectory +"Dialog_File"
    
    outputMT1=MTdirectory+"MT" +str(taskno)+"_more750"
    outputMT3=MTdirectory+"MT"+str(taskno)+"_midrange"
    
    #Html files
    outputHT_750=MTdirectory+"MT" +str(taskno)+"_more750"
    outputHT_Mid=MTdirectory+"MT" +str(taskno)+"_Mid"
    
    MTfile=MTdirectory+"MT"+str(taskno)+"_discussion_turns"
    
    fieldnames=list()
    AlltextAllFiles=list()
    Alldialogs_more750=list()
    Alldialogs_midrange=list()
    
    
    Textline_750=""
    Textline_Mid=""
    No_ofitem=3
    
    fieldnames.append("Dialogtext")
    fieldnames.append("key")
    fieldnames.append("Total_Count")
    MT1discussion_turns(taskno) ## call this once for each MT task
    
    MTrow=FileHandling.read_csv(MTfile)
    FileStr=MTrow[0]
    outputtextfile=MTdirectory +"dialog_text"
    for k,v in FileStr.iteritems():
        if "dialog" in k:
            inputfile=directory+k
            inputdialogturns=[s for s in re.split('[\s,]+',v)]
            MTobject= Mechanical_Turk(inputfile,inputdialogturns)
            MTobject.createdialoginputfile(inputfile, inputdialogturns, Alldialogs_more750,Alldialogs_midrange,AlltextAllFiles)
    FileHandling.WriteTextFile(outputtextfile,AlltextAllFiles)
    inputdialogturns=""
    
    FileHandling.write_csv(outputcsvdialog+"_more750",Alldialogs_more750, fieldnames)
    MTobject1=Mechanical_Turk(outputcsvdialog+"_more750",inputdialogturns)

 
    FileHandling.write_csv(outputcsvdialog+"_midrange",Alldialogs_midrange, fieldnames)
    MTobject3=Mechanical_Turk(outputcsvdialog+"_midrange",inputdialogturns)
 
    FileHandling.writeHtml(outputHT_750, Textline_750)
    FileHandling.writeHtml(outputHT_Mid, Textline_Mid)
    
    #For gun control 
    #-------------------------------------------------------------- No_ofhits=33
    #------------------- MTobject1.createhitinput(outputMT1,No_ofitem,No_ofhits)
#------------------------------------------------------------------------------ 
    #-------------------------------------------------------------- No_ofhits=60
    #------------------- MTobject3.createhitinput(outputMT3,No_ofitem,No_ofhits)
    
    # For Abortion
    No_ofhits=52
    MTobject1.createhitinput(outputMT1,No_ofitem,No_ofhits)
    No_ofhits=62
    MTobject3.createhitinput(outputMT3,No_ofitem,No_ofhits)
    
    
   # For Evolution
    No_ofhits=31
    MTobject1.createhitinput(outputMT1,No_ofitem,No_ofhits)
    No_ofhits=39
    MTobject3.createhitinput(outputMT3,No_ofitem,No_ofhits)
    