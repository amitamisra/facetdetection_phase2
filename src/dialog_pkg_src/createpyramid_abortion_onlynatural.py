'''
Created on Jul 7, 2015

Take input from natural summaries HIT. For the approved hits, create a pyramid file by taking natural summaries.
@author: amita
'''
import os 
from collections import defaultdict
from data_pkg import FileHandling
import operator
import itertools
from data_pkg import NewFormat_text
from bs4 import BeautifulSoup
import re
import shutil,os;
from copy import deepcopy
if __name__ == '__main__':
    pass


def writedir(dict_key,outdir):
    if os.path.exists(outdir):
        shutil.rmtree(outdir)
    os.mkdir(outdir)
    for key in dict_key.keys():
        #if not os.path.exists(outdir):
    #   os.makedirs(outdir)
        Outfile=outdir + "/"+key
        Lines=list()
        Lines.append(dict_key[key])
        FileHandling.WriteTextFile(Outfile, Lines)
        
        
#replace occurrences of s1,s2 by author1 author2
def replace_s1s2(Text):
    Text1=re.sub("s1|s1:","Author1",Text,flags=re.IGNORECASE ) 
    Text2=re.sub("s2|s2:","Author2",Text1,flags=re.IGNORECASE ) 
    return Text2   

    
def allSummary(HitFile,Nat_Dir): 
    dict_key_natural=defaultdict(str)
    
    counter_key=defaultdict(int)
    rowdicts=FileHandling.read_csv(HitFile)
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for key, items in itertools.groupby(rowdicts, operator.itemgetter('HITId')):
        Hitids=list(items)
        for row in Hitids:
            for count in range(1,noofitems+1):
                    key=row["Input.key"+str(count)]
                    
                    Answer_Natural=row["Answer.Dialog_Natural"]
                    Answer_Natural=replace_s1s2(Answer_Natural)
                    
                    dialog_html=row["Input.Dialogtext"+str(count)]
                    soup=BeautifulSoup(dialog_html)
                    dialogplain=soup.get_text()
                    dialogplain=replace_s1s2(dialogplain)
                    
                    Natural_text=(NewFormat_text.ascii_only(Answer_Natural)) 
                    
                    dict_key_natural[key]=dict_key_natural[key]+"\n"+ str(RegularExp[counter_key[key]]) + "\n" + Natural_text +"\n\n"
                    counter_key[key]=counter_key[key]+1    
    writedir(dict_key_natural,Nat_Dir)
 

if __name__ == '__main__':
    Noofsummaries=5 
    topic="abortion"  
    noofitems=1 
    RegularExp=["----------\nD"+ str(i) +"\n"+ "----------" for i in range(Noofsummaries)]  
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir+"/data/dialog_data/CSV/" + topic + "/MTdata/Phase1/MTSummary/MT"
    
    TaskNo=1
    
    Directory1=Directory+str(TaskNo)
    HitFile=basedir + "/data/mechanicalturk_data/"+topic+ "/MTdata/Phase1/MT"+str(TaskNo)+"/Summary_results/"+ "MT"+ str(TaskNo)+ "_midrangeResults"
    Nat_Dir=Directory1 + "/MT" + str(TaskNo) +"_midrange"
    allSummary(HitFile,Nat_Dir)
    