'''
Created on May 17, 2015
calculate UMBC Similarity between a pair of labels
@author: amita
'''
import copy
from requests import get
import itertools
from data_pkg import FileHandling 
import os 
from file_formatting import csv_wrapper

def ReadCSV(inputcsv):
    Rows=FileHandling.read_csv(inputcsv)
    return Rows

def UMBC(label1, label2):
    
    url= """http://swoogle.umbc.edu/StsService/GetStsSim?operation=api&phrase1=""" + label1+ """ &phrase2=""" + label2
    response=get(url)
    return  (response.text.strip())
    
def STS_for_Row ( row, field1, field2):
    label1=row[field1]
    label2=row[field2]
    if len(label1) > 0 and len(label2) > 0:
        sim=UMBC(label1, label2)
    else:
        sim= str(-1 )   
    return sim

def combinationlist(fieldpairlist,r):
    combs=itertools.combinations(fieldpairlist,r)
    listcomb=list(combs)
    return listcomb


#create fieldpairnames for sts similarity
def createfieldpairs_sts(rows,label):
    fieldpairlist=[]
    fields=rows[0].keys()
    for field in fields:
        if str(field).startswith("Id_"):
            fieldpairlist.append(field)
    fieldpairlist.append(label)
    return fieldpairlist

#Add sts to  row, comblistpairs contains  list of tuples with fieldnames for sim 
def AddSTScomblistpairs(row, comblistpairs):
    newrow=copy.deepcopy(row)
    for fieldpair in comblistpairs:
        field1=fieldpair[0]
        field2=fieldpair[1]
        sim=STS_for_Row(row, field1,field2)
        newrow["STSComb_"+ field1+"_"+field2]=sim
    return newrow
def AddSTSRows(rows,r,label):
    AllnewRows=[]
    fieldpairlist=createfieldpairs_sts(rows,label)
    comblistpairs=combinationlist(fieldpairlist,r)
    for row in rows:
        newrow=AddSTScomblistpairs(row, comblistpairs)
        AllnewRows.append(newrow)
    return  AllnewRows   
def Execute(inputcsv,MTUMBC,r,label):  
    rows=  ReadCSV(inputcsv)
    AllnewRows=AddSTSRows(rows,r,label) 
    csv_wrapper.write_csv(MTUMBC, AllnewRows)
       
if __name__ == '__main__':
    label1="A gun can be traced wiouth the serial numbers in some cases."
    label2="how the guns were traced without their serial numbers."
    topic="gun-control"    #done for naacl begin end
    basedir=os.path.dirname(os.path.dirname(os.getcwd())) 
    MTdir=  basedir+ "/data/mechanicalturk_data/gun-control/MTdata/Phase1/Phase1_1/"
    MTResults=MTdir + "MTResults/MT1Results/MT1_dialog_labelmapResults"
    MTsplitResults=MTdir +"MTResults/MT1Results/MT1_dialog_labelmap_split_worker"
    MTUMBC=MTdir +"MTResults/MT1Results/MT1_dialog_label_UMBC_worker.csv"
    
    r=2
    label="label"
    Execute(MTsplitResults,MTUMBC,r,label)
