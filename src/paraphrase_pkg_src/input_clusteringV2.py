'''
Created on Aug 24, 2014
This program takes input labels from pyramids created by running scores pyramid from data package  and creates  clusters
 #ADDPOS_string returns a dictionary with keys as Noun, Verb, Adj, AllPOSstring. 
            # For Naacl Used   AllPOSstring, this includes noun, verb and adj
            #AllPOSstring signifies to take the key   
@author: amita
'''
from sklearn.feature_extraction.text  import CountVectorizer
from sklearn.feature_extraction.text  import TfidfVectorizer
from  sklearn.cluster import AgglomerativeClustering
from paraphrase import preprocess_text
import os
import data_pkg.FileHandling
from sklearn import metrics
import sys
import operator
import itertools
from copy import deepcopy
import Add_POSV2



class Cluster:
    def __init__(self,num_cluster,stringsCsv,vec,clusterfield,keyid,clusterfile_stem,POS,affinity): 
        self.stringsCsv= stringsCsv
        self.OutclusterFile=clusterfile_stem
        self.num_cluster=num_cluster
        self.affinity=affinity
        self.POS=POS
        self.clusterdfield=clusterfield
        self.keyid=keyid
        self.vec=vec
    # creates a Term Doc vector using CountVectorizer, takes input from /MTdata/AllMTSummarycsv file uses reg expression to remove DO,D1  tags added to summaries
    #-- changes to lowercase , removes stop words, if stem is True then it also performs stemming
   
    #creates a string as row for input to clustering and writes individual sentences to file outstringsCsv
    #==========================================================================
    # create a tuple of cluster labels with corr documents , doc ids begin with 0   
    #==========================================================================
    def tuple_cluster_doc(self,Res_Labels,Allstrings,Allrow_dicts):
        
        doccount=0
        cluster_list=list() 
        for label in Res_Labels:
            row_cluster=deepcopy(Allrow_dicts[doccount])
            row_cluster["label_cluster"]=label
            row_cluster["doccount"]=doccount
            #row_cluster["string"]=Allstrings[doccount]
           # row_cluster["key"]=Allrow_dicts[doccount][self.keyid]
            doccount=doccount+1
            cluster_list.append(row_cluster)
        fieldnames=cluster_list[0].keys()  
        data_pkg.FileHandling.write_csv(self.OutclusterFile, cluster_list, fieldnames)       
        return cluster_list
    
    def Create_Agg_cluster(self,stem,stop,processing,remS): 
        
        Allrow_dicts=data_pkg.FileHandling.read_csv(self.stringsCsv)
        Allstrings=[rowdict_str[self.clusterdfield] for rowdict_str in Allrow_dicts]
        if self.POS=="ALL":
            # Allstrings_process=[preprocess_text.preprocess(string_text, stem,stop) for string_text in Allstrings] 
            print "Error in Part of speech in function Create_Agg_cluster"
            sys.exit(0)
        else:
            POS_Strings=list()
            if self.POS=="Noun_Verb_AdJ" :
                POS_List=["Noun","Adj","Verb"]
            else:
                    print "Error in Part of speech in function Create_Agg_cluster"
                    sys.exit(0)
            for rowdict  in Allrow_dicts:
                POS_String=Add_POSV2.ADDPOS_string(rowdict[self.clusterdfield],rowdict,POS_List)["AllPOSstring"] 
                POS_Strings.append(POS_String)                  
            Allstrings_process=[preprocess_text.preprocess(string_text, stem,stop) for string_text in POS_Strings]  
        
        if remS:
            Allstrings_process=[preprocess_text.removeS(text) for text in Allstrings_process]            
        if self.vec=="CountVectorizer":
            vectorizer = CountVectorizer()
        else:
            if self.vec=="TFIdfCountVectorizer":
                vectorizer= TfidfVectorizer()      
        term_doc=vectorizer.fit_transform(Allstrings_process)
        if self.affinity=='euclidean':
            Agg_cluster=AgglomerativeClustering(n_clusters=self.num_cluster,affinity='euclidean')
        if self.affinity=='cosine':
            Agg_cluster=AgglomerativeClustering(n_clusters=self.num_cluster,linkage='average',affinity=self.affinity)
        if self.affinity=='l1':
            Agg_cluster=AgglomerativeClustering(n_clusters=self.num_cluster,linkage='average',affinity=self.affinity)    
        Res_Labels=Agg_cluster.fit_predict(term_doc.toarray())
        self.cluster_tup_list=self.tuple_cluster_doc(Res_Labels,Allstrings,Allrow_dicts)
        #self.metric=metrics.silhouette_score(term_doc.toarray(), Res_Labels, metric=self.affinity)
        
    def  write_cluster(self,processing):
        AllList=list()
        self.cluster_tup_list.sort(key= operator.itemgetter("label_cluster"))
        AllList.append("PREPROCESSING :"+ processing)
       # AllList.append("Metric"+ str(self.metric))
        count=0
        for key, items in itertools.groupby(self.cluster_tup_list, operator.itemgetter('label_cluster')):
            AllList.append("\n\n NEW CLUSTER  " + str(count)+"\n\n\n")
            OneCluster=list(items) 
            count=count+1
            for contrib in  OneCluster:
                AllList.append("\n" + contrib["label"] + "\n")
                AllList.append("key  "+contrib["key_user"] + "\n")
        data_pkg.FileHandling.WriteTextFile(self.OutclusterFile,AllList)
            
def stem_cluster(stringsCsv,vec,clusterfield,keyid,stem,stop,processing,POS,clusterfile_stem,num_cluster,remS,affinity,):
        Clusterobj=Cluster(num_cluster,stringsCsv,vec,clusterfield,keyid,clusterfile_stem,POS,affinity)
        Clusterobj.Create_Agg_cluster(stem,stop,processing,remS)
        Clusterobj.write_cluster(processing)
     
if __name__ == '__main__':
    
    topic="gun-control"
    basedir=os.path.dirname(os.path.dirname(os.getcwd()))
    Directory=basedir +"/data/paraphrase_data/"+ topic+"/cluster/TFIdf/Phase1"
    clusterfile_stem=Directory + "/Cos_Cluster_70_AVG_Noun_Verb_Ad"
    stringsjson= basedir + "/data/dialog_data/CSV/"+ topic +"/MTdata/Phase1/Pyramids_Natural/CoreNLPmore2_Scus_EXT.json"
    stringsCsv=basedir + "/data/dialog_data/CSV/"+ topic +"/MTdata/Phase1/Pyramids_Natural/CoreNLPmore2_Scus_EXT"
    
    Regex="[-]*\sD[0-9]*\s[-]*"
    #Initialize all values for clustering
    stem=True 
    stop=True  
    remS=True 
    affinity="cosine"
    vect="TFIdfCountVectorizer"
    num_cluster=70
    # AVG indicates type linkage in cluster class in scikit
    

    #To be done with labels as input
    clusterfield="label" #Field in input file for clustering
    keyid="key_user"
    data_pkg.FileHandling.convert_json_tocsv(stringsjson, stringsCsv)
    
    POS="Noun_Verb_AdJ"
    processing="STEMMING, cos Distance, num_cluster=70Nouns_Verb_AdJ"
    stem_cluster(stringsCsv,vect,clusterfield,keyid,stem,stop,processing,POS,clusterfile_stem,num_cluster,remS,affinity)
   
            